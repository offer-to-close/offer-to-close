<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Invoices_Items extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Invoices-Items';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public static function eraseByInvoiceId($invoiceID)
    {
        return static::where('Invoices_ID', '=', $invoiceID)->delete();
    }
    public static function itemsByInvoiceId($invoiceID)
    {
        return static::where('Invoices_ID', '=', $invoiceID)->get();
    }
}
