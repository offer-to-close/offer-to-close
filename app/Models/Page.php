<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;


class Page extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public static function findBySlug($slug)
    {
        return new Page([
            'title'   => Str::title(str_replace('-', ' ', $slug)),
            'content' => '<p>The page you are looking for is either under maintenance or development.</p><p>We are sorry for the inconvenience.</p>',
            'slug'    => $slug,
        ]);
    }
}

