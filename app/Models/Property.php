<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Property extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Properties';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const PROPERTY_TYPE_CONDOMINIUM = 'condominium';
    const PROPERTY_TYPE_TOWNHOUSE = 'townhouse';
    const PROPERTY_TYPE_SINGLEFAMILY = 'singleFamily';
    const PROPERTY_TYPE_MULTIFAMILY = 'multiFamily';

}
