<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buyer extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Buyers';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

    /**
     * @param $transactionID
     *
     * @return mixed
     */
    public static function getByTransactionID($transactionID)
    {
        return static::where('isTest', 0)->where('Transactions_ID', $transactionID)->get();
    }
    public static function createFromUser($transactionID, $userID)
    {
        $user = User::find($userID);
        $rv = self::upsert(['Users_ID'=>$user->ID,
                      'Transactions_ID' => $transactionID,
                      'NameFirst'       => $user->NameFirst,
                      'NameLast'        => $user->NameLast,
                      'Email'           => $user->email,
        ]);
        return $rv;
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
