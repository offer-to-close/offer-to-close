<?php

namespace App\Models;

use App\Combine\TransactionCombine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Transactions';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const SALES_TYPE_NORMAL = 'normal';
    const SALES_TYPE_REO = 'reo';
    const SALES_TYPE_SHORTSALE = 'shortSale';
    const SALES_TYPE_PROBATE = 'probate';

    const LOAN_TYPE_FHA = 'fha';
    const SALES_TYPE_VA = 'va';
    const SALES_TYPE_CONVENTIONAL = 'conventional';

    const USER_ROLE_BUYER = 'b';
    const USER_ROLE_SELLER = 's';
    const USER_ROLE_BUYERSAGENT = 'ba';
    const USER_ROLE_HOMESUMER = 'h';
    const USER_ROLE_BUYER_SELLER = 'bs';

    const COMMISSION_TYPE_FLAT = 'flat';
    const COMMISSION_TYPE_PERCENTAGE = 'percentage';

    const REFERRAL_FEE_TYPE_FLAT = 'flat';
    const REFERRAL_FEE_TYPE_PERCENTAGE = 'percentage';

    public function open($transactionCoordinatorID = null)
    {

        if ($transactionCoordinatorID)
        { //where('foo', '=', 'bar')->get();
            $transactions = Transaction::where('TransactionCoordinators_ID', '=', $transactionCoordinatorID)
                                       ->where('DateAcceptance', 'is not', null)
                                       ->where('DateEnd', 'is', null)
                                       ->where('deleted_at', 'is', null)
                                       ->where('Status', '!=', 'canceled')
                                       ->get();
        }
        else
        {
            $transactions = Transaction::where('DateAcceptance', 'is not', null)
                                       ->where('DateEnd', 'is', null)
                                       ->where('deleted_at', 'is', null)
                                       ->where('Status', '!=', 'canceled')
                                       ->get();
        }

        return $transactions;

    }

    public static function hasAccess($transactionID, $userID)
    {
        $rec = static::find($transactionID)->toArray();
        if (count($rec) == 0) return false;

        $roles    = TransactionCombine::getRoleByUserID($transactionID, $userID);
        $sameSide = false;
        foreach ($roles as $role)
        {
            if ($rec['Side'] == substr($role['role'], 0, 1)) $sameSide = true;
            if ($role['role'] == 'o' || $role['role'] == 'c') $sameSide = true;
            if ($rec['Side'] == 'bs') $sameSide = TRUE;
        }

        return $sameSide;
    }

    public static function count($type=['open'], $returnArray=false)
    {
        if (!is_array($type)) $type = [$type];

        if (in_array('notClosed', $type)) $findNotClosed = true;
        else $findNotClosed = false;

        $query = static::selectRaw('Status, count(ID) as Count')->where('isTest', '!=', true);

        if ($findNotClosed) $query = $query->where('ID', '!=', 'closed');
        elseif (count($type) == 0) {}
        else $query = $query->whereIn('Status', $type);

        if ($returnArray) $query = $query->groupBy('Status');

        if ($returnArray) return $query->get()->toArray();
        else return $query->get()->first()->count;
    }

    public function buyers()
    {
        return $this->hasMany(Buyer::class,'Transactions_ID', 'ID');
    }

    public function sellers()
    {
        return $this->hasMany(Seller::class,'Transactions_ID', 'ID');
    }
}
