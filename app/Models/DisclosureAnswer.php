<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DisclosureAnswer extends Model_Parent
{
    protected $table = 'DisclosureAnswers';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;
}
