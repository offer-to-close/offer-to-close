<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MailTemplates extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'MailTemplates';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function mailByFromRole($fromRole)
    {
        return static::where('FromRole', $fromRole)->get();
    }
    static public function mailByCategory($category)
    {
        return static::where('Category', $category)->get();
    }
    static public function mailByFromRoleAndCategory($fromRole, $category)
    {
        return static::where('FromRole', $fromRole)->where('Category', $category)->get();
    }
}
