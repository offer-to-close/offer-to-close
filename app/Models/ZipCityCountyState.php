<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ZipCityCountyState extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'ZipCityCountyState';

    public static function getCountyByZip($zip)
    {
        return self::where('Zip', $zip)->first()->County;
    }
}
