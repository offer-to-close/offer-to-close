<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TransactionCoordinator extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'TransactionCoordinators';

    public $timestamps = false;

    const TYPE_BUYER = 'b';
    const TYPE_SELLER = 's';
    const TYPE_BUYERTC = 'btc';
    const TYPE_SELLERTC = 'stc';
    const TYPE_BOTH = 'bs';
    public static function getNames($id)
    {
        return static::find($id)->select('NameFirst', 'NameLast', 'NameFull')->first();
    }
}
