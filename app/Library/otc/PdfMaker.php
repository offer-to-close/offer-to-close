<?php
namespace App\Library\otc;

abstract class PdfMaker
{
    abstract public function __construct($input, $parameters=[]);
    abstract public function write($outputFile=null, $parameters=[]);
    /**
     * @param $parameters
     * @param $defaults
     *
     * @return mixed
     */
    protected function assignDefaults($parameters, $defaults)
    {
        foreach ($defaults as $key=>$value)
        {
            if (is_null($value)) continue;
            if (!is_array($value) && strpos($value, ';') !== false)
            {
                list($val, $options) = explode(';', $value);
                $options = explode('|', $options);
            }
            else
            {
                $val = $value;
                $options = [];
            }
            if (isset($parameters[$key]) && empty($options)) continue;
            if (!isset($parameters[$key]))
            {
                $parameters[$key] = $val;
                continue;
            }
            if (!in_array($parameters[$key], $options)) $parameters[$key] = $val;
        }
        return $parameters;
    }
}