<?php

namespace App\Library\otc;

use Exception;
use Illuminate\Support\Facades\Log;

class IncludeWhen
{
    private $logThis;
    private $transaction = null;
    private $tableTranslation = ['property'         => 'property',
                                 'transaction'      => 'transaction',
                                 'buyer'            => 'buyer',
                                 'buyersagent'      => 'buyersAgent',
                                 'seller'           => 'seller',
                                 'sellersagent'     => 'sellersAgent',
                                 'buyersbrokerage'  => 'sellersAgent',
                                 'sellersbrokerage' => 'sellersAgent',
    ];

    /**
     * IncludeWhen constructor.
     *
     * @param      $transactionRecord
     * @param bool $logThis
     */
    public function __construct($transactionRecord=[], $logThis = false)
    {
        $this->transaction = $transactionRecord;
        $this->logThis     = $logThis;
    }

    /**
     * The current implementation allows for either a string of .and.s or .or.s but not both or complex relationships
     * with parenthesis, that will come later
     *
     * @param $expression
     *
     * @return bool
     */
    public function evaluate($expression): bool
    {
        if ($expression == '{optional}') return false;

        $andCount = substr_count(strtolower($expression), '.and.');
        $orCount  = substr_count(strtolower($expression), '.or.');

        if ($andCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.and.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;
                $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
                $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                if (!$result) return false;
            }
            return true;
        }

        if ($orCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.or.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;
                $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
                $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                if ($result) return true;
            }
            return false;
        }
        $conditionParts = $this->separateCondition($expression);
        if (count($conditionParts) == 0) return false;
        $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
        $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
        return $result;
    }

    /**
     * @param $delimiter
     * @param $expression
     *
     * @return array
     */
    public function caseInsensitiveExplode($delimiter, $expression): array
    {
        $retVal = [];

        if (empty($delimiter) || empty($expression)) return is_array($expression) ? $expression : [$expression];

        if (($p = stripos($expression, $delimiter)) === false) return [$expression];

        $retVal[] = substr($expression, 0, $p - 1);
        $retVal[] = substr($expression, $p + strlen($delimiter));

        return $retVal;
    }

    /**
     * @param $condition
     *
     * @return array
     */
    public function separateCondition($condition): array
    {
        $compOps = ['.eq.', '.ne.', '.lt.', '.gt.', '.le.', '.ge.'];

        foreach ($compOps as $compOp)
        {
            $values = $this->caseInsensitiveExplode($compOp, $condition);

            // ... Indices 1 and 2 are returned instead of the more standard 0 & 1 to make the code consistent with the user
            // ... documentation.
            if (count($values) == 2) return [1 => trim($values[0]), 2 => trim($values[1]), 'operator' => $compOp];
        }

        return [];
    }

    /**
     * @param $value1
     * @param $compOp
     * @param $value2
     *
     * @return bool
     */
    public function evaluateIf($value1, $compOp, $value2): bool
    {
        // ... Indices 1 and 2 are used instead of the more standard 0 & 1 to make the code consistent with the user
        // ... documentation.
        $value = [1 => $value1, 2 => $value2];

        foreach ($value as $idx => $val)
        {
            if (substr($val, 0, 1) == '[' &&
                substr($val, -1) == ']')
            {
                $value[$idx] = $this->getFieldValue($val);
            }
            else $value[$idx] = $this->getLiteralValue($val);
        }

        $this->logThis('literal expression = '. implode(' ', [$value[1], $compOp, $value[2]]), __METHOD__ .'=>'. __LINE__);

        switch (strtolower($compOp))
        {
            case '.eq.':
                return ($value[1] == $value[2]);

            case '.ne.':
                $value[1] = strval($value[1]);
                $value[2] = strval($value[2]);
                if ($value[1] != $value[2]) return true;
                return false;

            case '.lt.':
                return ($value[1] < $value[2]);

            case '.gt.':
                return ($value[1] > $value[2]);

            case '.le.':
                return ($value[1] <= $value[2]);

            case '.ge.':
                return ($value[1] >= $value[2]);

            default:
                return false;
        }
    }

    /**
     * @param $value1
     * @param $compOp
     * @param $value2
     *
     * @return string
     */
    public function translateIf($value1, $compOp, $value2): string
    {
        $oper = ['eq' => 'Equal To', 'ne' => 'Not Equal To',
                 'lt' => 'Less Than', 'gt' => 'Greater Than',
                 'le' => 'Less Than or Equal To', 'ge' => 'Greater Than or Equal To',];

        if (substr($value1, 0, 1) == '[' &&
            substr($value1, -1) == ']')
        {
            list($tableName, $colName) = explode('.', substr($value1, 1, -1));
            $value1 = $colName . ' field from ' . $tableName . ' table';
        }
        else $value1 = $this->getLiteralValue($value1);

        if (substr($value2, 0, 1) == '[' &&
            substr($value2, -1) == ']')
        {
            list($tableName, $colName) = explode('.', substr($value2, 1, -1));
            $value2 = $colName . ' field from ' . $tableName . ' table';
        }
        else $value2 = $this->getLiteralValue($value2);

        $rv = $value1 . ' ' . $oper[str_replace('.', null,  strtolower($compOp) )] ?? 'Unknown Operator' . ' ' .
                                                                                      $value2;

        return $rv;
    }

    /**
     * @param $fieldName
     *
     * @return null
     */
    private function getFieldValue($fieldName)
    {
        if (substr($fieldName, 0, 1) == '[' &&
            substr($fieldName, -1) == ']')
        {
            $fieldName = substr($fieldName, 1, -1);
        }

        list($table, $field) = explode('.', $fieldName);

        if (!isset($this->tableTranslation[$table])) $table = strtolower(str_singular($table));

        $ay = $this->transaction[$this->tableTranslation[strtolower($table)]]->toArray();
        if (count($ay) == 1) $ay = reset($ay);
//        ddd(['ay', $ay, 'field'=>$field]);
        try
        {
            return $ay[$field];
        }
        catch (Exception $e)
        {
            return null;
        }
    }

    /**
     * @param $value
     *
     * @return bool|mixed|null|string
     */
    private function getLiteralValue($value)
    {
        // ... Process special literals
        if (substr($value, 0, 1) == '!')
        {
            $value = str_replace(' ', null, $value);

            switch (strtolower($value))
            {
                case '!null':
                    return null;

                case '!true':
                    return true;

                case '!false':
                    return false;
            }
        }

        // ... Process Dates
        if (substr($value, 0, 1) == '#' &&
            substr($value, -1) == '#')
        {
            return substr($value, 1, -1);  // ToDo: figure out the right way to do this
        }

        // ... Return everything else as is and let PHP figure it out.
        return $value;
    }

    /**
     * @param $parameter
     */
    protected function logThis($parameter, $loc=null)
    {
        if ($this->logThis) Log::debug([$parameter, 'location'=>$loc ?? __METHOD__ .'=>'. __LINE__]);
    }

    /**
     * The current implementation allows for either a string of .and.s or .or.s but not both or complex relationships
     * with parenthesis, that will come later
     *
     * @param $expression
     *
     * @return bool
     */
    public function translate($expression): string
    {
        $andCount = substr_count(strtolower($expression), '.and.');
        $orCount  = substr_count(strtolower($expression), '.or.');

        $exTranslated = [];

        if ($andCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.and.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;

                $exTranslated[] = $this->translateIf($conditionParts[1],
                    $conditionParts['operator'], $conditionParts[2]);
            }
            return implode(' AND ', $exTranslated);
        }

        if ($orCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.or.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;

                $exTranslated[] = $this->translateIf($conditionParts[1],
                    $conditionParts['operator'], $conditionParts[2]);
            }
            return implode(' AND ', $exTranslated);
        }

        $conditionParts = $this->separateCondition($expression);
        if (count($conditionParts) == 0) return false;

        return $this->translateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);

    }
    /**
     * @param $value
     *
     * @return bool|mixed|null|string
     */
    private function translateLiteralValue($value)
    {
        // ... Process special literals
        if (substr($value, 0, 1) == '!')
        {
            $value = str_replace(' ', null, $value);

            switch (strtolower($value))
            {
                case '!null':
                    return 'NULL';

                case '!true':
                    return 'TRUE';

                case '!false':
                    return 'FALSE';
            }
        }

        // ... Process Dates
        if (substr($value, 0, 1) == '#' &&
            substr($value, -1) == '#')
        {
            return substr($value, 1, -1);  // ToDo: figure out the right way to do this
        }

        // ... Return everything else as is and let PHP figure it out.
 //       if (is_numeric($value) && $value == 0) return '0';

        return (string) $value;
    }


}
