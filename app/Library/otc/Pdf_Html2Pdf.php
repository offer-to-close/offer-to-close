<?php
namespace App\Library\otc;

use App\Library\Utilities\_Variables;
use Illuminate\Support\Facades\Log;
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Spipu\Html2Pdf\Exception\Html2PdfException;

/**
 * Class Pdf
 *
 * Documentation for the Html2Pdf class library is found at: https://github.com/spipu/html2pdf/tree/master/doc
 * See (at the bottom of this file) how to include an Electronic Signature in the HTML
 * See (at the bottom of this file) standard PDF Core fonts
 *
 */
class Pdf_Html2Pdf extends PdfMaker
{
    public $html2pdf = null;

    public $input;

    public $constructorDefaults = ['orientation'=>'P;P|L',
                                   'format'=>'Letter',   // There are a lot of valid options, see documentation
                                   'language'=>'en',     // There are a lot of valid options, see documentation
                                   'isUnicode'=>true,
                                   'encoding'=>'UTF-8',
                                   'margins'=>[12, 12, 12, 12], // in mm in the following order: left, top, right, bottom
                                   'isPdfa' => true,
    ];

    public $displayDefaults = ['zoom'   => 'fullpage;fullpage|fullwidth|real|default',
                               'layout' => 'SinglePage;SinglePage|OneColumn|TwoColumnLeft|TwoColumnRight|TwoPageLeft|TwoPageRight',
                               'mode'   => 'UseNone;UseNone|UseOutlines|UseThumbs|FullScreen|UseOC|UseAttachments',
    ];

    public $metaDefaults = ['author'=>'Offer To Close',
                            'title'=>'Offer To Close Document',
                            'subject'=>'Real Estate Documents',
                            'keywords'=>['real estate', ],
                            'font'=>'Arial',
    ];

    /*
    $OutputDefaults

    Destination is where to send the document. It can take one of the following values:
        I: send the file inline to the browser. The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name (default).
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option
        FD: equivalent to F + D option
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
     */
    public $outputDefaults = ['destination'=>'F;I|D|F|S|FI|FD|E'];

    public function __construct($input, $parameters=[])
    {
        $this->input = $input;
        $p = $this->assignDefaults($parameters, $this->constructorDefaults);
        $this->html2pdf = new Html2Pdf( $p['orientation'],
            $p['format'],
            $p['language'],
            $p['isUnicode'],
            $p['encoding'],
            $p['margins'],
            $p['isPdfa']);

        $this->html2pdf->setTestIsImage(false);  // will not fail if required image is not found.
        $this->setDisplayMode();
        $this->setMetaData();
    }

    public function setDisplayMode($parameters=[])
    {
        $p = $this->assignDefaults($parameters, $this->displayDefaults);
        $this->html2pdf->pdf->SetDisplayMode( $p['zoom'], $p['layout'], $p['mode']);
    }
    public function setMetaData($parameters=[])
    {
        $p = $this->assignDefaults($parameters, $this->metaDefaults);

        $this->html2pdf->pdf->SetAuthor($p['author']);
        $this->html2pdf->pdf->SetTitle($p['title']);
        $this->html2pdf->pdf->SetSubject($p['subject']);

        $keywords = is_array($p['keywords']) ? implode(', ', $p['keywords']) : $p['keywords'];
        $this->html2pdf->pdf->SetKeywords($keywords);
        $this->html2pdf->setDefaultFont($p['font']);
    }
    public function write($outputFile=null, $parameters=[])
    {
        $p = $this->assignDefaults($parameters, $this->outputDefaults);
        $input = $input ?? $this->input;

        $pi = pathinfo($outputFile);
        if (($pi['extension']??'') != 'pdf') $outputFile = $pi['dirname'] . DIRECTORY_SEPARATOR . $pi['filename'] . '.pdf';
        if (!canCreateFile($outputFile)) return false;

        try
        {
            $this->html2pdf->writeHTML($input, false);
            $this->html2pdf->output($outputFile, $p['destination']);
        }
        catch (\Exception $e)
        {
            if (stripos($e->getMessage(), 'Could not include font definition file') !== false)
            {
                $this->html2pdf->setDefaultFont('Arial');
                Log::error($e->getMessage() . ' Switched to Arial.');
                $this->write($outputFile, $parameters);
            }

            $errInfo = ['&&',
                        'EXCEPTION' => $e->getMessage(),
                        'Line'      => $e->getLine(),
                        'Code'      => $e->getCode(),
                        //                        'Trace'     => $e->getTraceAsString(),
                        __METHOD__=>__LINE__,
            ];

            Log::error($errInfo);
            ddd($errInfo,'**');
        }

        return $outputFile;
    }

    /**
     * @param $parameters
     * @param $defaults
     *
     * @return mixed
     */
    protected function assignDefaults($parameters, $defaults)
    {
        foreach ($defaults as $key=>$value)
        {
            if (!is_array($value) && strpos($value, ';') !== false)
            {
                list($val, $options) = explode(';', $value);
                $options = explode('|', $options);
            }
            else
            {
                $val = $value;
                $options = [];
            }
            if (isset($parameters[$key]) && empty($options)) continue;
            if (!isset($parameters[$key]))
            {
                $parameters[$key] = $val;
                continue;
            }
            if (!in_array($parameters[$key], $options)) $parameters[$key] = $val;
        }
        return $parameters;
    }
}
/*
   Electronic Signature
   back

   You can add an electronic signature to the PDF, by using the following specific html TAG:

   <cert
       src="/path/to/cert.pem"
       privkey="/path/to/priv.pem"
       name="sender_name"
       location="sender_location"
       reason="sender_reason"
       contactinfo="sender_contact"
   >
      ** html **
   </cert>

Attribute       Description	                        Example 1	            Example 2
-----------     ---------------------------------   -------------------     ------------------
src	            Path to the Cert	                /www/certs/my.pem	    /www/certs/my.crt
privkey	        Private key of the Cert if needed	/www/certs/priv.pem	    nothing
name	        Name of the Cert	                My.org Cert
location	    Country of the Cert	                France
reason	        Purpose of the Cert	                Invoice validation
contactinfo	    EMail of organisation's contact	    contact@my.org

** HTML ** part could be any HTML formatted string, img, etc...
*/

/*
The PDF Core (standard) fonts are:
        courier : Courier
        courierB : Courier Bold
        courierBI : Courier Bold Italic
        courierI : Courier Italic
        helvetica : Helvetica
        helveticaB : Helvetica Bold
        helveticaBI : Helvetica Bold Italic
        helveticaI : Helvetica Italic
        symbol : Symbol
        times : Times New Roman
        timesB : Times New Roman Bold
        timesBI : Times New Roman Bold Italic
        timesI : Times New Roman Italic
        zapfdingbats : Zapf Dingbats
 */
