<?php
namespace App\Library\otc;

use App\Library\Utilities\_Variables;
use Illuminate\Support\Facades\Log;


/**
 * Class Pdf_Restpack
 *
 * Documentation for the restpack Html  to PDF API is found at: https://www.html2pdfrocket.com/
 *
 */
class Pdf_Html2PdfRocket extends PdfMaker
{
    public $conversionParameters = [];

    public $input;
    public $accessToken;
    public $apiUrl;

    public $parameterDefaults = [
        'MarginTop'          => null, //Value of top margin - default is 0
        'MarginBottom'       => null, //Value of bottom margin - default is 0
        'UseGrayscale'       => null, //true to generate PDF in grayscale, false or leave blank for full colour
        'UseLandscape'       => null, //true to rotate page to landscape, false or leave blank for portrait
        'EnableForms'        => null, //true to turn html form fields into pdf form fields
        'LowQuality'         => null, //true to reduce the quality, which may lower your network usage if quality is still satisfactory
        'ImageQuality'       => null, //override the default image quality percentage (94) and use your own
        'DisableShrinking'   => null, //true to disable the intelligent shrinking process we use make the pixel/dpi ratio constant
        'DisableJavascript'  => null, //true to disable running JS on page, otherwise javascript runs
        'JavascriptDelay'    => null, //Milliseconds to wait for JS to finish executing before converting the page.  Useful for ajax calls.
        'UsePrintStylesheet' => null, //true to use the print media stylesheet, false or leave blank to use normal stylesheet
        'FooterSpacing'      => null, //Spacing between the header and the content in mm - default is 0
        'HeaderSpacing'      => null, //Spacing between the footer and the content in mm - default is 0
        'PageSize'           => 'Letter;A0|A1|A2|A3|A4|A5|A6|Legal|Letter|Tabloid|Ledger|Full', //Default size is A4 but you can use Letter, A0, A2, A3, A5, Legal, etc.
        'PageWidth'          => null, //Page width - if you use this, you must also use PageHeight
        'PageHeight'         => null, //Page height - if you use this, you must also use PageWidth
        'ViewPort'           => null, //e.g. 800x600 - Set if you have custom scrollbars or css attribute overflow to emulate window size
        'Dpi'                => null, //Explicitly set the DPI, which is 96 by default.  Also see Zoom settings
        'Zoom'               => null, //Default zoom is 1.00  You can use any floating point number, e.g. 0.5, 0.75, 1.10, 2.55, 3...
        'OutputFormat'       => null, //Must be either "pdf", "jpg", "png", "bmp" or "svg" if not supplied the default is PDF
        'FileName'           => null, //Optionally the name you want the file to be called when downloading or $unique$ for guid
        'Username'           => null, //For URL conversions, creates a secure basic authentication connection to your server
        'Password'           => null, //For URL conversions, creates a secure basic authentication connection to your server
        'Cookie'             => null, //Supply in format: NAME|||VALUE|||NAME|||VALUE (that's 2 cookies).  Often used for authentication.
/*
 * Headers and footers, including automatic page numbering can be done by either passing in a url to
 * HeaderUrl/HeaderHtml or FooterUrl/FooterHtml, or passing in a small snippet of plain text to HeaderLeft,
 * HeaderRight, FooterLeft or FooterRight. Note that if you want to use a header or footer, you must also use a margin
 * parameter like MarginTop or MarginBottom to ensure there is space for the header or footer.
 * Your Html should also start with <!DOCTYPE html> and include the <html> and <body> tags
 */
        'HeaderHtml'      => null, //To use an html header on each page - a string starting with <!DOCTYPE html>
        'FooterHtml'      => null, //To use an html footer on each page - a string starting with <!DOCTYPE html>
        'HeaderUrl'       => null, //To use an html header on each page - a url starting with http containing the html
        'FooterUrl'       => null, //To use an html footer on each page - a url starting with http containing the html
        'HeaderLeft'      => null, //Top left header text (can use replacement tags below)
        'HeaderRight'     => null, //Top right header text (can use replacement tags below)
        'FooterLeft'      => null, //Bottom left footer (can use replacement tags below)
        'FooterRight'     => null, //Bottom right footer (can use replacement tags below)
        'FooterFontName'  => null, //Footer font names - Arial by default
        'HeaderFontName ' => null, //Header font names - Arial by default
        'FooterFontSize'  => null, //The font sizes - 12 by default.  Use the the plain value (do not use px or pt)
        'HeaderFontSize'  => null, //The font sizes - 12 by default.  Use the the plain value (do not use px or pt)
/*
 * The replacement tags in the following table can be used to set the page numbering. For example, using FooterRight with a
 * value of 'Page [page] of [toPage]', and remembering to also use a margin per above, e.g. MarginBottom with a value of 20

        [page]      Tag replaced with number of the pages currently being printed
        [frompage]  Tag replaced with the number of the first page to be printedr
        [topage]    Tag replaced with the number of the last page to be printed
        [section]   Tag replaced with the name of the current section
 */
    ];

    public function __construct($input, $parameters=[])
    {
        $this->input = $input;
        $this->accessToken = config('otc.Html2PdfRocket_Token');
        $this->apiUrl = config('otc.Html2PdfRocket_URL');

        $this->conversionParameters = $this->assignDefaults($parameters, $this->parameterDefaults);
    }
    /**
     * @param string  $outputFile  The path to where the PDF should be written
     * @param array   $parameters
     *
     * @return bool|string|null On failure, returns FALSE; on success, returns the output file path
     */
    public function write($outputFile=null, $parameters=[])
    {
        $doc = $this->input ?? '<html>Input Not Found.<br/><br/>'. date('l jS \of F Y h:i:s A') .' </html>';
        if (strtolower(substr($doc, -7)) != '</html>' ) $doc .= '</html>';

        $token = $this->accessToken;

        $pi = pathinfo($outputFile);
        if (($pi['extension']??'') != 'pdf') $outputFile = $pi['dirname'] . DIRECTORY_SEPARATOR . $pi['filename'] . '.pdf';
        if (!canCreateFile($outputFile)) return false;

        if (count($parameters) > 0) $parameters = $this->assignDefaults($parameters, $this->conversionParameters);
        else $parameters = $this->conversionParameters;

        // ... These are the request parameters. You can check the full list of possible parameters
        // ...     at https://restpack.io/html2pdf/docs#route
        $params = array_merge($parameters, ['apikey' => $token, 'value' => $doc ,]);
        $postdata = http_build_query(
            $params
        );

        // ... Build a POST request
        $opts = ['http' =>
                     [
                         'method'  => 'POST',
                         'header'  => 'Content-type: application/x-www-form-urlencoded',
                         'content' => $postdata,
                     ],
        ];

        $context = stream_context_create($opts);

        try
        {
            $result = file_get_contents($this->apiUrl, false, $context);
            file_put_contents($outputFile, $result);
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception' => $e->getMessage(), 'output file'=>$outputFile, 'postdata'=>$params], '**');
        }
        return $outputFile;
    }


}