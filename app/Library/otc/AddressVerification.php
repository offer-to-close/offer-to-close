<?php

namespace App\Library\otc;

$incPath = base_path('vendor/smartystreets/phpsdk/src');

require_once($incPath . '/ClientBuilder.php');
require_once($incPath . '/US_Street/Lookup.php');
require_once($incPath . '/StaticCredentials.php');
require_once($incPath . '/Exceptions/SmartyException.php');

use App\Combine\TransactionCombine;
use App\Library\Utilities\_Variables;
use App\Library\Utilities\Toumai;
use App\Models\AddressOverride;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use SmartyStreets\PhpSdk\StaticCredentials;
use SmartyStreets\PhpSdk\ClientBuilder;
use SmartyStreets\PhpSdk\US_Street\Lookup;
use SmartyStreets\PhpSdk\Exceptions\SmartyException;
use SimpleXMLElement;

class AddressVerification
{
    private $apiKey = null;
    private $authToken = null;
    private $serviceProvider = null;

    public function __construct($method = 'smartyStreets')
    {
        $this->serviceProvider = $method;

        switch ($method)
        {
            case ('smartyStreets'):
                $this->apiKey    = env('SMARTYSTREETS_AUTH_ID');
                $this->authToken = env('SMARTYSTREETS_AUTH_TOKEN');
                break;

            default:
                break;
        }
    }

    public function isValid($address = null)
    {
        if (is_array($address)) $address = trim(implode('+', $address));
        if (empty($address)) return false;
        switch ($this->serviceProvider)
        {
            case ('smartyStreets'):
                return $this->smartyStreets($address);
                break;

            case ('usps'):
                return $this->usps($address);
                break;
        }
        return false;
    }

    public function smartyStreets($address)
    {
        $authId    = env('SMARTYSTREETS_AUTH_ID');
        $authToken = env('SMARTYSTREETS_AUTH_TOKEN');

        $staticCredentials = new StaticCredentials($authId, $authToken);
        $client            = (new ClientBuilder($staticCredentials))
            // uncomment this line to point to the specified proxy.
            // ->viaProxy("http://localhost:8080", "username", "password")
            ->buildUsStreetApiClient();

        $lookup = new Lookup();
        $lookup->setStreet($address);
        //        $lookup->setCity("Mountain View");
        //        $lookup->setState("CA");

        try
        {
            $client->sendLookup($lookup);
        }
        catch (SmartyException $ex)
        {
            echo($ex->getMessage());
        }
        catch (\Exception $ex)
        {
            echo($ex->getMessage());
        }

        $hash = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        if(AddressOverride::where('Hash',$hash)->count()!=0){
            return 1;
        }

        return $this->packageSmartyStreets($lookup);

    }

    public function usps($address)
    {
        $address1 = $address2 = $city = $zip5 = $zip4 = null;
        $state    = 'CA';

        if (substr_count($address, ',') > 0)
        {
            $adr = explode(',', $address);
            $address2 = reset($adr);

            if (is_numeric(last($adr)))
            {
                $zip5 = last($adr);
            }
            else
            {
                if (trim(substr_count(last($adr)), ' ') > 0)
                {
                    list($state, $zip5) = explode('-', last($adr));
                }
                else
                {
                    if (substr_count(last($adr), '-') > 0)
                    {
                        list($zip5, $zip4) = explode('-', last($adr));
                    }
                }
            }
        }
        else
        {
            $adr = explode(' ', $address);
            if (is_numeric(last($adr)))
            {
                $zip5 = last($adr);
            }
            else
            {
                if (substr_count(last($adr), '-') > 0)
                {
                    list($zip5, $zip4) = explode('-', last($adr));
                }
            }

        }

        $xml = <<<XML
<AddressValidateRequest USERID="[userid]">
  <Revision>1</Revision>
   <Address ID="0">
     <Address1>[address1]</Address1>
     <Address2>[address2]</Address2>
     <City>[city]</City>
     <State>[state]</State>
     <Zip5>[zip5]</Zip5>
     <Zip4>[zip4]</Zip4>
  </Address>
</AddressValidateRequest>
XML;
        $xml = str_replace('[userid]', env('USPS_USERNAME'), $xml);
        $xml = str_replace('[address1]', $address1, $xml);
        $xml = str_replace('[address2]', $address2, $xml);
        $xml = str_replace('[city]', $city, $xml);
        $xml = str_replace('[state]', $state, $xml);
        $xml = str_replace('[zip5]', $zip5, $xml);
        $xml = str_replace('[zip4]', $zip4, $xml);
        $xml = str_replace(PHP_EOL, null, $xml);
        $xml = str_replace('    <', '<', $xml);
        $xml = str_replace('  <', '<', $xml);
        $xml = str_replace(' <', '<', $xml);

        $service = html_entity_decode(env('USPS_URL') . $xml);
        //Log::debug([$service, __METHOD__ => __LINE__]);
        $rv  = file_get_contents($service);
        $xml = new SimpleXMLElement($rv);
        return (array) $xml;
    }

    public function packageSmartyStreets(Lookup $lookup) {
        $results = $lookup->getResult();

        if (empty($results))
        {
            echo("\nNo candidates. This means the address is not valid.");
            return false;
        }

        $rv = [];
        foreach ($results as $idx => $candidate)
        {
            $rv[$idx]['Street1'] = str_replace('  ', ' ',
                implode(' ', [$candidate->getComponents()->getPrimaryNumber(),
                    $candidate->getComponents()->getStreetPreDirection(),
                    $candidate->getComponents()->getStreetName(),
                    $candidate->getComponents()->getStreetPostDirection(),
                    $candidate->getComponents()->getStreetSuffix(),
                ]));
            $rv[$idx]['Street2'] = $candidate->getComponents()->getSecondaryNumber();
            $rv[$idx]['City']    = $candidate->getComponents()->getCityName();
            $rv[$idx]['State']   = $candidate->getComponents()->getStateAbbreviation();
            $rv[$idx]['Zip']     = $candidate->getComponents()->getZIPCode();
            $rv[$idx]['County']  = $candidate->getMetadata()->getCountyName();
        }
        return $rv;
    }
    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function formatAddress($record, $lineCount=1)
    {
        $addressIndices = ['Street1'=>null, 'Street2'=>', ', 'City'=>', ', 'State'=>', ', 'Zip'=>' '];
        $address = null;

        if ($lineCount < 1) $lineCount = 1;
        if ($lineCount > 2) $lineCount = 2;
        if ($lineCount == 2) $addressIndices['City'] = '<br/>';

        if (_Variables::getObjectName($record) == 'Collection' ||
            _Variables::getObjectName($record) == 'stdClass' ||
            get_parent_class($record) == 'App\Models\Model_Parent' ||
            str_contains(get_parent_class($record) , 'Model'))
        {
            if (method_exists($record, 'getTable'))
            {
                if (Schema::hasColumn($record->getTable(), 'Street1')) {} //return false;
            }

            foreach ($addressIndices as $field=>$delim)
            {
                if (!isset($record->{$field}))
                {
                    //Log::info(['field'=>$field, _Variables::getObjectName($record), 'record'=>$record, __METHOD__=>__LINE__]);
                    continue;
                }
                else //Log::info(['field'=>$field, $record->{$field}, __METHOD__=>__LINE__]);

                $address .= (!empty($record->{$field} ?? null) ? $delim : null) . $record->{$field} ;
            }
        }
        elseif (is_array($record))
        {
            if (!isset($record['Street1'])) return false;
            foreach ($addressIndices as $field=>$delim)
            {
                $address .= (!empty($record[$field]) ? $delim : null) . $record[$field] ;
            }
        }

        return $address;
    }

    public static function addressSplitter($string)
    {
        while (substr_count($string, '  ') > 0)
        {
            $string = str_replace('  ', ' ', $string);
        }
        $streetTypes = ['street', 'str', 'st', 'ave', 'av', 'avenue', 'blvd',
                        'road', 'rd', 'drive', 'dr', 'circle', 'cir'];
        $street1 = $street2 = $city = $state = $zip = '';

        $cnt = substr_count($string, ',');
        if ($cnt == 4) list($street1, $street2, $city, $state, $zip) = explode(',', $string);
        if ($cnt == 3) list($street1, $city, $state, $zip) = explode(',', $string);
        if ($cnt == 2)
        {
            $parts = explode(',', $string);
            $street1 = array_shift($parts) . ' ';
            $city = array_shift($parts);

            $tmp = trim(reset($parts));
            list($state, $zip) = explode(' ', $tmp);
        }
        if ($cnt == 0)
        {
            $parts = explode(' ', $string);
            $zip = array_pop($parts);
            $state = array_pop($parts);
            $street1 = array_shift($parts) . ' ';
            $street1 .= array_shift($parts);

            if (count($parts) == 1) $city = array_shift($parts);
            elseif (count($parts) == 2)
            {
                $tmp = array_shift($parts);
                if (in_array(strtolower(str_replace('.', null, $tmp)),
                    $streetTypes))
                {
                    $street1 .= ' ' . $tmp;
                    $city = implode(' ', $parts);
                }
                else $city = $tmp . ' ' . implode(' ', $parts);
            }
            elseif (count($parts) == 3)
            {
                $tmp = array_shift($parts);
                if (in_array(strtolower(str_replace('.', null, $tmp)), $streetTypes))
                {
                    $street1 .= ' ' . $tmp;
                    $city = implode(' ', $parts);
                }
                else $city = $tmp . ' ' . implode(' ', $parts);
            }
            else ddd($parts);
        }
        return ['Street1'=>trim($street1) ?? '', 'Street2'=>trim($street2) ?? '',
                'City'=>trim($city) ?? '', 'State'=>trim($state) ?? '', 'Zip'=>trim($zip) ?? ''];
    }
    public static function getAddressState($string)
    {
        while (substr_count($string, '  ') > 0)
        {
            $string = str_replace('  ', ' ', $string);
        }

        $string = str_replace(',', null, $string);
        $string = '{' . str_replace(' ', '}{', $string) . '}';

        $details = $a = [];
        $str = [$string];
        while (true)
        {
            $rv = Toumai::_findSet($string, '{', false, $details);
            if (!$rv) break;

            $a[] = $rv;
            $string = substr($string, $details['setEnd']+2);
        }

        while (count($a) > 0)
        {
            $rv = array_pop($a);
            if (!is_numeric($rv) && strlen($rv) == 2) return strtoupper($rv);
        }
        return false;
    }
}