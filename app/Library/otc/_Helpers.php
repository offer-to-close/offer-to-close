<?php

namespace App\Library\otc;


class _Helpers
{
    public static function clearTransactionSession()
    {
        $vars = ['transaction_id',
   //              'transactionSide'
        ];
        foreach ($vars as $var)
        {
            session()->forget($var);
        }
    }

    public static function getTextInCurlyBraces($string, $limitOne = TRUE)
    {
        if($limitOne)
        {
            if(preg_match('/{+(.*?)}/', $string, $match))
            {
                return $match;
            }
        }
        else
        {
            if(preg_match_all('/{+(.*?)}/', $string, $matches))
            {
                return $matches;
            }
        }

        return FALSE;
    }
}