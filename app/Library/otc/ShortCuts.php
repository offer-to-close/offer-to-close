<?php

namespace App\Library\otc;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class ShortCuts
{
    public static function roleCodeToDisplay($code)
    {
        $roles = Config::get('constants.USER_ROLE');
        $dsp = array_search($code, $roles);
        if (!$dsp) return false;
        return title_case(str_replace('_', ' ', $dsp));
     }

    public static function isServerLive()
    {
        if (strtolower(env('APP_ENV')) != 'live' &&
            strtolower(env('APP_ENV')) != 'prod' &&
            strtolower(env('APP_ENV')) != 'production')
        {
            return false;
        }
        return true;
    }

    public static function isServerLocal()
    {
        if (strtolower(env('APP_ENV')) != 'local') return false;
        return true;
    }

    public static function isServerTest()
    {
        if (strtolower(env('APP_ENV')) != 'test') return false;
        return true;
    }

    public static function isServerStage()
    {
        if (strtolower(env('APP_ENV')) != 'stage' &&
            strtolower(env('APP_ENV')) != 'staging')
        {
            return false;
        }
        return true;
    }

    public static function getServer()
    {
        return env('APP_ENV');
    }
}