<?php

namespace App\Http\Resources;

use App\Library\otc\AddressVerification;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use App\Models\BrokerageOffice;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Log;

class Agent extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (isset($this->additional['meta']['output'])) return parent::toArray($request);

        try
        {
            return [
                'agent_id'        => $this->ID ?? $this->id,
                'name'            => $this->NameFirst . ' ' . $this->NameLast,
                'first_name'      => $this->NameFirst,
                'last_name'       => $this->NameLast,
                'full_name'       => $this->NameFull,
                'email'           => $this->Email,
                'phone'           => $this->PrimaryPhone,
                'secondary_phone' => $this->SecondaryPhone,
                'address'         => AddressVerification::formatAddress($this),
                'license'         => $this->License,
                'license_name'    => $this->NameFull,
                'state'           => $this->State,
                'brokerage_office' => !empty($boid = $this->BrokerageOffices_ID) ? BrokerageOffice::getName($boid) : '',
            ];
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION',
                'msg'  => $e->getMessage(),
                'code' => $e->getCode(),
                'line' => $e->getLine(),
                'this' => $this,
                'request' => $request,
                'Stack Trace' => _Log::stackDump(),
                ]);
        }
    }

    public function with($request)
    {
        return [
          'api_version' => config('otc.API.version'),
          'valid_as_of' => date('D, d M Y H:i:s'),
        ];
    }
}
