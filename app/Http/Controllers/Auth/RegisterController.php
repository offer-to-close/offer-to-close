<?php

namespace App\Http\Controllers\Auth;

use App\Combine\TransactionCombine;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\MailController;
use App\Library\Utilities\_Debug;
use App\Library\Utilities\_Get;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Log;
use App\Models\lk_Users_Roles;
use App\Models\lk_UsersTypes;
use App\Models\LoginInvitation;
use App\Models\lu_UserRoles;
use App\Models\MemberRequest;
use App\Models\TransactionCoordinator;
use App\User;
use App\Http\Controllers\Controller;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Config;
use App\Models\MailTemplates;
use App\Mail\SystemTemplate;
use Spipu\Html2Pdf\Tag\Html\I;

class RegisterController extends Controller
{
    public static $modelPath = 'App\\Models\\';
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RedirectsUsers;

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return redirect()->back()->with([
                'errors' => $validator->getMessageBag(),
            ])->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'NameFirst' => 'required|string|max:255',
            'NameLast'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Route = 'register'
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showRegistrationForm(Request $request)
    {
        $path = explode('/', $request->getPathInfo());

        $queryString = $request->getQueryString();
        if (!empty($queryString))
        {
            $qs = explode('&', $queryString);
            $code = null;
            foreach ($qs as $arg)
            {
                if (strtolower(substr($arg, 0, 2)) == 'c=')
                {
                    list($tmp, $code) = explode('=', $arg);
                }
            }
        }

        if (empty(reset($path))) array_shift($path);
        while (empty(end($path)) || end($path) == '*')
        {
            array_pop($path);
        }

        switch (count($path))
        {
            case 1:
                session(['ic'=>null]);
                return view ('auth.register', ['code'=>$code??null]);
                break;

            case 2:
                $role = null;
                $inviteCode = array_pop($path);
                if (in_array($inviteCode, config('constants.USER_ROLE')))
                {
                    $role = $inviteCode;
                }
                if (!empty($role)) return view('auth.register', ['role'=>$role,'code'=>$code??null]);

                break;

            case 3:
                $inviteCode = array_pop($path);
                $role = array_pop($path);
                break;
        }

        $invitationController = new InvitationController();
        if (!($codeArray = $invitationController->verifyInviteCode($inviteCode)))
        {
            Log::debug(['codeArray'=>$codeArray, ]);
            return redirect()->route('otc.register.invite')->with([
                'inviteCodeErrors' => 'That invite code is invalid or may have expired.']);
        }

        if(isset($codeArray['errors'])) return redirect()->route('otc.register.invite')->with([
            'inviteCodeErrors' => 'The following errors have occurred: '.$codeArray['errors']]);

        $invitation = LoginInvitation::find($codeArray['ID']);
        $invite = InvitationController::translateInviteCode($inviteCode);
        return view('auth.register', [
            'inviteCode'=>$inviteCode,
            'role'=>$role,
            'nameFirst'=>$invitation->NameFirstInvitee,
            'nameLast'=>$invitation->NameLastInvitee,
            'email'=>$invitation->EmailInvitee,
            'shareRoomID'=>$invite['shareRoomID'],
        ]);
    }

    /**
     * Route = /register/request
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function requestMembership(Request $request)
    {
        $data = $request->all();

        $validation = [
            'NameFirst'     => 'required|max:100',
            'NameLast'      => 'required|max:100',
            'email'         => 'required|string|max:100',
            'RequestedRole' => 'required',
        ];

        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return redirect()->back()->with([
                'errors' => $validator->getMessageBag(),
            ])->withInput();
        }

        $memberRequest = new MemberRequest();
        $memReq = $memberRequest->where('Email', '=', $data['email'] ?? '')->get()->toArray();

        if (count($memReq) > 0)
        {
            return redirect()->back();  //  ToDo:: put in validation errors junk to display on calling form
        }

        $info = ['NameFirst'=>$memReq->NameFirst ?? $data['NameFirst'],
                 'NameLast'=>$memReq->NameLast ?? $data['NameLast'],
                 'Email'=>$memReq->email ?? $data['email'],
                 'ReasonRequested'=>$memReq->ReasonRequested ?? $data['ReasonRequested'],
                 'RequestedRole'=>$memReq->RequestedRole ?? $data['RequestedRole'],
                ];

        $memberRequest = new MemberRequest();
        $row = $memberRequest->upsert($info);
        Session::flash('success','Thank you for your interest. You will be sent login details.');

// ... Send Email to OTC Staff
        $memberRequestID = $row->ID;
        $mailer = new MailController();
        $rv = $mailer->sendMemberRequestAlert($memberRequestID, isServerLocal());

        return redirect(route('preLogin.welcome') );
    }

    protected function returnRequestMembershipView($data, $errors = FALSE)
    {
        if(!$errors) $errors = collect();
        return view('auth.requestMembership', [
            'NameFirst'         => $data['NameFirst']        ?? '',
            'NameLast'          => $data['NameLast']         ?? '',
            'Email'             => $data['Email']            ?? '',
            'requestID'         => $data['ID']               ?? $data->id ?? $data['requestID'] ??  -1,
            'stateRequested'    => $data['State']            ?? 'California',
            'roleRequested'     => $data['roleRequested']    ?? '',
            'addressRequested'  => $data['addressRequested'] ?? '',
            'whyRequested'      => $data['whyRequested']     ?? NULL,
            'heardRequested'    => $data['heardRequested']   ?? '',
            'companyRequested'  => $data['companyRequested'] ?? '',
            'transactionID'     => $data['transactionID']    ?? '',
            'errors'            => $errors,
        ]);
    }

    public function submitMembership(Request $request)
    {
        $view = 'waitingRoom';
        $data = $request->all();
        $validation = [
            'roleRequested'     => 'required',
            'whyRequested'      => 'required',
        ];
        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return $this->returnRequestMembershipView($data, $validator->getMessageBag());
        }

        $memberRequest = new MemberRequest();

        if (is_numeric($data['requestID']) && $data['requestID'] > 0)
        {
            $info = ['ID'              => $data['requestID'],
                     'State'           => $data['stateRequested'],
                     'RequestedRole'   => $data['roleRequested'],
                     'PropertyAddress' => $data['addressRequested'] ?? '',
                     'ReasonRequested' => $data['whyRequested'],
                     'HeardFrom'       => $data['heardRequested'],
                     'Company'         => $data['companyRequested'],
                     'TransactionID'   => $data['transactionID'],
            ];

            $memReq = $memberRequest->upsert($info);
        }

        $mailCtrl = new MailController();
        $rv = $mailCtrl->sendMemberRequestAlert($data['requestID']);

        return view(_LaravelTools::addVersionToViewName($view,
                                                            ['data'=>$memReq->toArray()])
        );
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        $invite = $invitation = false;
        $userRole = $data['userRole'] ?? session('userRole');
        $userType = $data['userType'] ?? session('userType') ?? 'u';

        if (isset($data['inviteCode']) && $data['inviteCode'] != '*' && strlen($data['inviteCode']) > 1)
        {
            $inviteCode = InvitationController::translateInviteCode($data['inviteCode']);
            $invCtlr    = new InvitationController();
            $invite     = $invCtlr->verifyInviteCode($data['inviteCode']);
        }
        else $inviteCode = false;

        if ($inviteCode) $invitation = LoginInvitation::find($invite['ID']);

        $userRoleTables = [
            'b'  => 'Buyer',  'ba' => 'Agent',     'btc' => 'TransactionCoordinator',
            's'  => 'Seller', 'sa' => 'Agent',     'stc' => 'TransactionCoordinator',
            'a'  => 'Agent',  'g'  => 'Guest',     'tc'  => 'TransactionCoordinator',
            'e'  => 'Escrow', 'l'  => 'Loan',      't'   => 'Title',
            'br' => 'Broker', 'bg' => 'Brokerage', 'bo'  => 'BrokerOffice',
        ];

        $user = User::create([
            'name'      => implode(' ', [$data['NameFirst'], $data['NameLast'],]),
            'NameFirst' => $data['NameFirst'],
            'NameLast'  => $data['NameLast'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
        ]);

        $data['userRole'] = ($inviteCode) ?  $inviteCode['inviteeRole'] : $userRole;
        session(['userRole' => $data['userRole']]);
        $userRole = $data['userRole'];
        if (substr($userRole, -2) == 'tc') $userRole = 'tc';
        if (substr($userRole, -1) == 'a') $userRole = 'a';
        lk_Users_Roles::insertUserRolePair($user->id, $userRole);

        $userType = ($inviteCode) ?  $inviteCode['userType'] : $userType;
        session(['userType' => $userType]);
        session(['isAdmin' => false]);

        lk_UsersTypes::insertUserTypePair($user->id, $userType);

        // ... If the new user is already in a role table then update the role record
        if ($invitation && $invitation->InviteeRole_ID)
        {
            $db = self::$modelPath . $userRoleTables[$data['userRole']];
            $obj = new $db;
            if (!empty($obj)) $data_updated = $obj->upsert(['ID'=>$invitation->InviteeRole_ID, 'Users_ID'=>$user->id]);
            if (!$data_updated) Log::warn(['INSERT TO '.$userRoleTables[$data['userRole']].' TABLE FAILED', 'ID'=>$data['alreadyInDatabase'], __METHOD__=>__LINE__]);

            if ($data_updated) LoginInvitation::setAsUsed($invite['ID']);

            return $user;
        }

// ... If the new user is NOT already in a role table then create a record for that table
        $record = [
            'Users_ID'    => $user->id,
            'NameFull'    => implode(' ', [$data['NameFirst'], $data['NameLast'],]),
            'NameFirst'   => $data['NameFirst'],
            'NameLast'    => $data['NameLast'],
            'Email'       => $data['email'],
            'DateCreated' => date('Y-m-d H:i:s'),
        ];

        try
        {
            $log = new MailController();
            $registrationThankYouCode = 'otc-2019-acc-2';
            $mailTemplateID = MailTemplates::select('ID')
                ->where('Code', '=', $registrationThankYouCode)
                ->get()
                ->first()
                ->ID;
            $mail                   = MailTemplates::find($mailTemplateID);
            $view                   = $mail->Message;
            if (substr($view, 0, 2) == '*.')
                $view = _LaravelTools::addVersionToViewName(substr($view, 2));
            $mailData['subject']    = $mail->Subject;
            $mailData['u']          = implode(' ', [$data['NameFirst'], $data['NameLast'],]);
            $email                  = new SystemTemplate($data['email'], $view, $mailData);
            $log->logSentMail($email);
            Mail::send($email);
        }
        catch (\Exception $e)
        {
            Log::info([
                'Failure' => 'Sending Registration Thank You Email. Code: '.$registrationThankYouCode,
                'Exception Message' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }

// ... The Guests table ID is NOT auto incremented. The ID value is the same as the Users.ID value.
        if ($userRole == 'g') $record['ID'] = $user->id;

        try
        {
            if (isset($userRoleTables[$data['userRole']]))
            {
                $model = 'App\\Models\\' . $userRoleTables[$data['userRole']];
                $obj = new $model();
            }
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION' => $e, __METHOD__ => __LINE__]);
        }

        if (!empty($obj)) $data_inserted = $obj->upsert($record);

        return $user;
    }
}
