<?php

namespace App\Http\Controllers;

use App\Library\otc\_Helpers;
use App\Library\otc\Laravel;
use App\Library\Utilities\_LaravelTools;

use App\Models\Invoices;
use App\Models\lk_Invoices_Items;
use \DB;
use Illuminate\Http\Request;
use App\Combine\InvoiceCombine;
use App\Combine\AccountCombine;
class InvoiceController extends Controller
{
    /**
     * Instatiate a new controller instance
     *
     */

    public $modelPath = 'App\\Models\\';

    public function __construct()
    {
        // methods protected by auth middleware
        //$this->middleware('auth')->only('inputBuyer', 'inputSeller');
    }

    public function inputInvoice($invoiceID = null)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        _Helpers::clearTransactionSession();
        if (empty($invoiceID))
        {
            $invoice = (new Invoices())->getEmptyCollection();
            $invoiceItems = (new lk_Invoices_Items())->getEmptyCollection();
        }
        else
        {
            $invoice = collect(InvoiceCombine::getInvoice($invoiceID));
            $invoiceItems = collect(InvoiceCombine::getInvoiceItems($invoiceID));
        }

        return view('2a.invoices.generic',
        [
            'invoice' =>$invoice,
            'invoiceItems'=>$invoiceItems,
            'userID' => CredentialController::current()->ID(),
        ]);
    }
    public function saveInvoice(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $request->validate([
            // 'ID'            => 'integer',
            'BillTo'        => 'required',
            'DateDue'       => 'required',
            'DateInvoiced'  => 'required',
            'Total'         => 'required|integer|min:1',
            //'PaymentTerms'  => 'required',
            ]);
        $data       = $request->all();
        $items=$data['items'];
        unset($data['items']);
        $invoice=new Invoices();
        $invy=$invoice->upsert($data);
        $invoiceID=($invy->ID==null)?$invoice->id:$invy->ID;
        foreach($items as $iti){
            $itemOpject=new lk_Invoices_Items();
            $iti['Invoices_ID']=$invoiceID;
            $itemOpject->upsert($iti);
        }
        return  [route('PrintInvoice',['invoiceID'=>$invoiceID]),route('invoice.input',['invoiceID'=>$invoiceID])];
    }
    public function PrintInvoice($invoiceID)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'invoices.printInvoice';

        $invoice = collect(InvoiceCombine::getInvoice($invoiceID));
        $invoiceItems = collect(InvoiceCombine::getInvoiceItems($invoiceID));
        $invoice_input_url=route('invoice.input',['invoiceID'=>$invoice['ID']]);
        return view(_LaravelTools::addVersionToViewName($view),
        [
            'invoice'           =>$invoice,
            'invoiceItems'      =>$invoiceItems,
            'userID'            => CredentialController::current()->ID(),
            'invoice_input_url' =>$invoice_input_url,
        ]);

    }
    public function getInvoicesByUserID($userID = '*')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        _Helpers::clearTransactionSession();
        if (!is_numeric($userID) ) $userID = AccountCombine::getTransactionCoordinators(CredentialController::current()->ID())->first()->ID;

        $invoices = new Invoices();
        $view = '2a.invoices.all';

        if( $userID == 0 )
        {
            $data = Invoices::orderBy('DateInvoiced', 'desc')->paginate(20);
        }
        else if( is_numeric($userID) )
        {
            $data = Invoices::where('users_id', $userID)->
                              orderBy('DateInvoiced', 'desc')->paginate(20);
        }
        else{
            return $invoices->getEmptyCollection();
        }
        return view(_LaravelTools::addVersionToViewName($view), [
                    'data' => $data,
        ]);

    }

    public function updateInvoiceStatus(Request $request)
    {
         if (!\Auth::check()) return response('ERROR', 500); // ... Make sure the user is logged in
         $request->validate([
            'invoiceID'            => 'integer',
        ]);
        $invoice          = Invoices::where('id', request('invoiceID'))
                                    ->update(['Status' => Date('Y-m-d')]);

        if( $invoice )
        {
            return response(['status' => 'Success', 'Status' => Date('Y-m-d')], 200);
        }
        else{
            return response(500);
        }
    }

    public function searchInvoice(Request $request)
    {
         if (!\Auth::check()) return response('ERROR', 500); // ... Make sure the user is logged in
         $request->validate([
            '_table'          => 'required',
            'search_query'    => 'required|max:100',
        ]);
         //Fetch the data from table
        $table   = request('_table');
        $model   = request('_model');
        $query   = '%' . request('search_query') . '%';
        if( $table == 'Invoices')
        {
            $results = Invoices::where('users_id', CredentialController::current()->ID())
                        ->where( function($q) use ($query){
                            $q->where('BillTo', 'LIKE', $query)
                              ->orwhere('BillToAddress', 'LIKE', $query)
                              ->orwhere('Notes', 'LIKE', $query);
                        })
                        ->distinct()
                        ->get()->toArray();
           if( $results )
           {
               $results       = collect($results)->map(function($x){ return (array) $x; })->toArray();
                $data         = $results;
                $search_query = request('search_query');
                $_screenMode  = 'Search';
                $view         = '2a.invoices.all';
                return view($view, compact('data', 'search_query', '_screenMode'));
           }
           else {
               $data = null;
               $search_query = request('search_query');
               $_screenMode  = 'Search';
               $view = '2a.invoices.all';
               return view($view, compact('data', 'search_query', '_screenMode'));
           }
        }
    }
}
