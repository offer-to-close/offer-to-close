<?php

namespace App\Http\Controllers;

use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\QuestionnaireCombine;
use App\Combine\SignatureCombine;
use App\Combine\TransactionCombine;
use App\Library\otc\_PdfMaker;
use App\Library\otc\Pdf_Restpack;
use App\Library\Utilities\_LaravelTools;
use App\Models\Document;
use App\Models\lk_Transactions_Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use DocuSign;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;

class QuestionnairesController extends Controller
{
    public function ajaxGetDisclosures(Request $request)
    {
        $data = $request->all();
        $transactionsID = $data['transactionID'];
        $userRoles = TransactionCombine::getUserTransactionRoles($transactionsID);
        $fetched = QuestionnaireCombine::questionnairesByTransaction($transactionsID,'disclosure', $userRoles,NULL,true);
        $disclosures = collect();
        //check to make sure user has edit permission for this document
        foreach ($fetched as $disclosure)
        {
            $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionsID,$disclosure->Documents_Code);
            if (isset($permissions['e'])) $disclosures->push($disclosure);
        }
        return response()->json([
            'disclosures'   => $disclosures
        ]);
    }

    public function questionnaire($dID,$tID, $type = 'disclosure', $key = -1)
    {
        $view = 'questionnaires.questionMaster';
        $userRoles = TransactionCombine::getRoleByUserID($tID,CredentialController::current()->ID());
        $systemRoles = TransactionCombine::getUserTransactionSystemRoles($tID,CredentialController::current()->ID());
        if(empty($userRoles) || empty($systemRoles)) return redirect()->route(config('otc.DefaultRoute.dashboard'));
        $questionnaires = QuestionnaireCombine::questionnairesByTransaction($tID, $type);
        $displays_and_values = QuestionnaireCombine::answerDisplaysAndValues($dID,$tID);
        $answers = QuestionnaireCombine::checkAnswers($tID,$dID);
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($dID,$tID);
        $title = NULL;
        $lk_Transactions_Questionnaires_ID = NULL;
        foreach ($questionnaires as $questionnaire)
        {
            if($questionnaire->ID == $dID)
            {
                $title = $questionnaire->Description;
                $lk_Transactions_Questionnaires_ID = $questionnaire->lk_ID;
            }
        }
        if(empty($questions))
            return redirect()->route('review.questionnaire', [
                'transactionID' => $tID,
                'lk_ID' => $lk_Transactions_Questionnaires_ID]);
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'questionnairesID'      => $dID,
                'transactionsID'        => $tID,
                'questions'             => $questions,
                'displays_and_values'   => $displays_and_values,
                'answers'               => $answers,
                'questionnaires'        => $questionnaires,
                'title'                 => $title,
                'key'                   => $key
            ]);
    }

    public function reviewSignQuestionnaire($transactionID, $lk_ID, $returnJSON = false, $returnHTML = false, $sign = FALSE)
    {
        $userRoles = TransactionCombine::getRoleByUserID($transactionID,auth()->id());
        $systemRoles = TransactionCombine::getUserTransactionSystemRoles($transactionID,CredentialController::current()->ID());
        if(empty($userRoles) || empty($systemRoles)) return redirect()->route(config('otc.DefaultRoute.dashboard'));
        $lk_row = DB::table('lk_Transactions-Questionnaires')
            ->leftJoin('Questionnaires', 'Questionnaires.ID', '=', 'lk_Transactions-Questionnaires.Questionnaires_ID')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Questionnaires.Documents_Code')
            ->select(
                'Questionnaires.ID as Questionnaires_ID',
                'Questionnaires.ShortName',
                'Questionnaires.State',
                'Questionnaires.Documents_Code',
                'lk_Transactions-Questionnaires.ID as lk_ID'
            )
            ->where('lk_Transactions-Questionnaires.ID', $lk_ID)
            ->limit(1)
            ->get();
        if (!$lk_row->isEmpty())
        {
            $parsedDocumentCode = SignatureCombine::parseDocumentCode($lk_row->first()->Documents_Code,$transactionID);
            if($parsedDocumentCode == 'No Document')
            {
                //do nothing
            }
            elseif($parsedDocumentCode)
            {
                $doc = lk_Transactions_Documents::where('Documents_Code' , $parsedDocumentCode)
                    ->where('Transactions_ID', $transactionID)
                    ->get();
                if(!$doc->isEmpty())
                {
                    $doc = $doc->first();
                    $doc_lk_ID = $doc->ID;
                }
                else
                {
                    Session::flash('error', 'You do have access to that document.');
                    Session::push('flash.old','error');
                    return redirect()->route('transactionSummary.disclosures', ['transactionID' => $transactionID]);
                }
            }
            else
            {
                Session::flash('error', 'You do not have access to that document.');
                Session::push('flash.old','error');
                return redirect()->route('transactionSummary.disclosures', ['transactionID' => $transactionID]);
            }
            $questionnaireID = $lk_row->first()->Questionnaires_ID;

            $answers = QuestionnaireCombine::checkAnswers($transactionID, $questionnaireID);
            if ($sign) $returnHTML          = TRUE;
            if ($returnHTML) $returnJSON    = TRUE;
            if ($returnJSON) {
                //USE friendly index for html/pdf, and non friendly index for reviewing questions + being able to edit
                $answers = QuestionnaireCombine::checkAnswers($transactionID, $questionnaireID, true);
                $brokerageOffices = TransactionCombine::brokerageOffices($transactionID)->toArray();
                $property = TransactionCombine::getPropertySummary($transactionID);
                $client = TransactionCombine::getClient($transactionID);
                $agents = TransactionCombine::getAgents($transactionID);

                if($agents->isNotEmpty())
                {
                    $buyersAgent = NULL;
                    $sellerAgent = NULL;
                    foreach ($agents as $agent)
                    {
                        if(isset($agent->AgentType))
                        {
                            if($agent->AgentType == 'Buyer')        $buyersAgent = $agent;
                            elseif($agent->AgentType == 'Seller')   $sellerAgent = $agent;
                        }
                    }
                }

                $SWAH_DATA = [
                    "QuestionnaireID" => $questionnaireID,
                    "lk_ID" => $lk_ID,
                    "PropertyData" => $property,
                    "TransactionID" => $transactionID,
                ];

                /****** Do specific things for questionnaires (in this case the AVID / EHR)  ***/
                $buyers = TransactionCombine::buyers($transactionID);
                $sellers = TransactionCombine::sellers($transactionID);

                foreach ($buyers as $b) $tempBuyers[] = $b->NameFull ?? ($b->NameFirst . ' ' . $b->NameLast);
                foreach ($sellers as $s) $tempSellers[] = $s->NameFull ?? ($s->NameFirst . ' ' . $s->NameLast);
                $sellers = $tempSellers ?? NULL;
                $buyers = $tempBuyers ?? NULL;

                $userSides = TransactionCombine::getUserTransactionSides($transactionID);

                $additional_data = [
                    "RealEstateBrokerSeller"    => $brokerageOffices[0]->Name ?? NULL,
                    "RealEstateBrokerBuyer"     => $brokerageOffices[1]->Name ?? NULL,
                    "Buyers"                    => $buyers,
                    "Sellers"                   => $sellers,
                    'Client'                    => $client,
                    'BuyersAgent'               => $buyersAgent,
                    'SellersAgent'              => $sellerAgent,
                    'UserSides'                 => $userSides,
                ];


                /****** *****/

                $dataToSend = array_merge($SWAH_DATA, $answers, $additional_data);
                $view = _LaravelTools::addVersionToViewName('documents.templates.' . $lk_row->first()->ShortName);
                if(!view()->exists($view))
                {
                    Session::flash('error', 'This feature is currently not supported for this document.');
                    Session::push('flash.old','error');
                    return redirect()->back();
                }

                if($sign && $returnHTML)
                {
                    if (!isset($doc_lk_ID))
                    {
                        Session::flash('error', 'Signing this document is not currently supported for this document.');
                        Session::push('flash.old','error');
                        return redirect()->back();
                    }
                    $doc = trim(view($view, [
                        'data'     => $dataToSend,
                        'jsonData' => json_encode($dataToSend),
                    ]));
                    if (strtolower(substr($doc, -7)) != '</html>') $doc .= '</html>';

                    $docBagPath = 'Undefined';
                    try
                    {
                        File::isDirectory(public_path('_uploaded_documents')) or File::makeDirectory(public_path('_uploaded_documents'), 0755, true, true);
                        File::isDirectory(public_path('_uploaded_documents/transactions')) or File::makeDirectory(public_path('_uploaded_documents/transactions'), 0755, true, true);
                        $docBagPath = Config::get('constants.DIRECTORIES.transactionDocuments') . $transactionID . '/';
                        File::isDirectory($docBagPath) or File::makeDirectory($docBagPath, 0755, true, true);
                    }
                    catch (\Exception $e)
                    {
                        ddd(['EXCEPTION' => $e, 'bagPath' => $docBagPath, __METHOD__ => __LINE__]);
                    }

                    $output       = $docBagPath. $lk_row->first()->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf';
                    /*
                     * Use restpack API to generate a pdf at the output location.
                     */
                    $pdf          = new Pdf_Restpack($doc);
                    $pdf_path     = $pdf->write($output);
                    $fileInfo = new \stdClass();
                    $fileInfo->originalName = $lk_row->first()->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf';
                    $fileInfo->url = $pdf_path;
                    $fileInfo->signedBy = [];
                    /*
                     * Put the file into the document bag pre-signature.
                     */
                    $bag_Document = DocumentCombine::putExternalDocumentIntoBag($transactionID,$doc_lk_ID,$fileInfo, TRUE);
                    /*
                     * Reference that file(s) when setting the signature data. You can reference a set of files
                     * by a string with a (.) delimiter.
                     */
                    SignatureController::signatureGateway($transactionID,$bag_Document->ID);
                    /*
                     * Redirect to bag view, which will take the user through the signature process.
                     * Once the user has finished the signature process, the document will be added into the bag and the
                     * user will be redirected into the document bag.
                     */
                    $documentBagViewRoute = route('openDocumentBag', ['transactionID' => $transactionID, 'documentCode' => $parsedDocumentCode]);
                    return redirect()->to($documentBagViewRoute.'#show'.$bag_Document->ID.'#sign');
                }
                elseif ($returnHTML)
                {
                    $view = View::make(_LaravelTools::addVersionToViewName('documents.templates.' . $lk_row->first()->ShortName), [
                        'data' => $dataToSend,
                    ]);
                    $html = $view->render();
                    return $html;
                }

                return response()->json([
                    'data' => $dataToSend,
                ]);
            }
            $view = 'questionnaires.review';
            return view(_LaravelTools::addVersionToViewName($view),
                [
                    'transactionID' => $transactionID,
                    'lk_ID' => $lk_ID,
                    'doc_lk_ID' => $doc_lk_ID ?? NULL,
                    'questionnaireID' => $questionnaireID,
                    'answers' => $answers,
                ]);
        }
        Session::flash('error', 'Questionnaire not found.');
        Session::push('flash.old','error');
        return redirect()->back();
    }

    public function ajaxAnswersDisplaysAndValues(Request $request)
    {
        $data = $request->all();
        $questionnairesID = $data['questionnairesID'];
        $transactionsID = $data['transactionsID'];
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($questionnairesID,$transactionsID);
        $displays_and_values = QuestionnaireCombine::answerDisplaysAndValues($questionnairesID,$transactionsID);
        $answerMetaData         =QuestionnaireCombine::checkAnswers($transactionsID,$questionnairesID);
        return response()->json([
            'questions'             => $questions,
            'displays_and_values'   => $displays_and_values,
            'answerMetaData'        => $answerMetaData
        ]);
    }

    public function ajaxCheckAnswer(Request $request)
    {
        $data           = $request->all();
        $transactionsID = $data['transactionsID'];
        $questionnairesID  = $data['questionnairesID'];
        $questionID     = $data['questionID'];
        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        $answer = DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionID)
            ->get();

        if($answer->isEmpty())
        {
            return response()->json([
                'answer'     => false
            ]);
        }
        else return response()->json(['answer' => $answer->first()->Answer ]);

    }

    public function ajaxUpdateAnswer(Request $request)
    {
        $data                   = $request->all();
        $questionnaireQuestionID= $data['DQ_ID'];
        $lk_ID                  = $data['lk_ID'];
        $answer                 = $data['answer'];
        $addInfo                = $data['addInfo'];
        if($addInfo == 'DateCompleted')
        {
            DB::table('lk_Transactions-Questionnaires')
                ->where('ID',$lk_ID)
                ->update([
                    'DateCompleted' => date('Y-m-d'),
                ]);
            return response()->json([
                'complete' => 'true'
            ]);
        }

        $notes = NULL;
        if(is_array($addInfo))
        {

            foreach($addInfo as $k => $a)
            {
                $notes .= '[';
                $notes .= $k.'|';
                $notes .= $a;
                $notes .= ']';
            }

        }

        if(is_array($answer)) $answer = implode(',',$answer);
        if($answer == NULL || $answer == '')
        {
            $updateData['Answer'] = NULL;
        }
        else
        {
            $updateData['Answer'] = $answer;
            DB::table('QuestionnaireAnswers')
                ->where('lk_Transactions-Questionnaires_ID',$lk_ID)
                ->where('QuestionnaireQuestions_ID', $questionnaireQuestionID)
                ->update([
                    'Answer' => $updateData['Answer'],
                    'Notes'  => $notes,
                    'DateUpdated' => date('Y-m-d H:i:s'),
                ]);
        }

        return response()->json([
            'result' => 'success'
        ]);
    }

}

