<?php

namespace App\Http\Controllers;

use App\Models\lk_Transactions_Timeline;
use Illuminate\Support\Facades\DB;

class MilestoneController extends Controller
{
     public static function byTransactionId($transactionID, $onlyActive=true)
    {
        $query = lk_Transactions_Timeline::where('Transactions_ID', '=', $transactionID);

        if ($onlyActive) $query = $query->where('isActive', true);

        $milestones = $query->get();

        return view('general.genericTable',
            [
                'title'      => title_case(__FUNCTION__),
                'collection' => $milestones,
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
