<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Library\otc\Credentials;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CredentialController extends Controller
{
    private static $instance;
    public static $original = null;
    public static $current = null;
    public static $history = [];
    public static $originalUserType = 'u';

    protected static function initialize()
    {
        $userID = auth()->id();
        self::$original = new Credentials($userID);
        self::$current = self::$original;
        self::$history = [];
        self::$originalUserType = AccountCombine::getType($userID);
        return true;
    }
    public static function get()
    {
        return self::instance();
    }
    public static function instance()
    {
        if(!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class;
        }
        if (empty(self::$original)) self::initialize();
        return self::$instance;
    }
    public static function switch($userID)
    {
        if ($userID < 1) return self::initialize();
        $privilege = AccessController::hasAccess('a', self::$originalUserType);
        if (!$privilege) return false;

        if ($userID == self::$current->get('id')) return true;
        array_push(self::$history, self::$current);
        self::$current = new Credentials($userID);
    }
    public static function current()
    {
        if (empty(self::$original)) self::initialize();

        return self::$current;
    }
    public static function original()
    {
        if (empty(self::$original)) self::initialize();
        return self::$original;
    }
}
