<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\BaseCombine;
use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\MilestoneCombine;
use App\Combine\QuestionnaireCombine;
use App\Combine\ShareRoomCombine;
use App\Combine\TaskCombine;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\DisplayTable;
use App\Library\Utilities\FormElementHelpers;
use App\Library\Utilities\Toumai;
use App\Models\bag_TransactionDocument;
use App\Models\Document;
use App\Models\lk_Transactions_Tasks;

use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use App\Models\lu_DocumentAccessActions;
use App\Models\lu_DocumentAccessRoles;
use App\Models\Task;
use App\Models\TransactionCoordinator;
use http\Env\Response;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Combine\TransactionCombine;
use DateTime;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Question\Question;

class TransactionSummaryController extends Controller
{
    public $transactionID = null;

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function keyPeople(int $transactionsID)
    {
        return $this->showSummary($transactionsID);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function showSummary(int $transactionsID)
    {
        //
        // ... Header display info
        $transaction        = TransactionCombine::fullTransaction($transactionsID);
        $property           = $transaction['property']->first();
        $transactionDetails = $transaction['transaction']->first();
        $data['address']    = implode(', ', [$property['Street1'] ?? null,
                                             $property['City'] ?? null,
                                             $property['State'] ?? null]);
        $data['address']    = implode(' ', [$data['address'], $property['Zip'] ?? null]);
        $data['address']    = trim(str_replace(' ,', null, $data['address']));
        if (strlen($data['address']) < 5) $data['address'] = null;

        if (is_null($property)) $property = new Property(['PropertyType' => 'Unknown']);
        try
        {
            $data['propertyDetails'] = implode('; ',
                ['<span style="color: black;">Property Type:</span> ' . (is_null($property->PropertyType) ? 'Unknown'
                     : $property->PropertyType),
                 '<span style="color: black;">Sale Type:</span> ' . (is_null($transactionDetails->SaleType) ? 'Unknown' : $transactionDetails->SaleType),
                 '<span style="color: black;">Loan Type:</span> ' . (is_null($transactionDetails->LoanType) ? 'Unknown' : $transactionDetails->LoanType),
                ]);
        }
        catch (\Exception $e)
        {
            Log::error(['Exception' => $e, 'property' => $property, 'transaction details' => $transactionDetails,
                        __METHOD__  => __LINE__]);
        }

        $clientRole     = $transaction['transaction']->ClientRole;
        $personData     = TransactionCombine::getPerson($clientRole, $transactionsID);
        $person         = $personData['data']->first();
        $client['role'] = array_search($personData['role'], Config::get('constants.USER_ROLE'));
        $client['role'] = title_case(str_replace('_', ' ', $client['role']));
        $client['name'] = title_case(trim(implode(' ', [$person->NameFirst ?? null, $person->NameLast ?? null,])));

        $timeline = TransactionCombine::getTimeline($transactionsID);
        if ($timeline->isEmpty())
        {
            $data['dateClose'] = 'Not Defined';
        }
        else
        {
            $closeEscrow       = $timeline->where('MilestoneName', 'Close Escrow');
            $data['dateClose'] = $closeEscrow->isEmpty() ?
                '-' : date('F j, Y', strtotime($closeEscrow->first()->MilestoneDate));
        }

        //
        // ... Documents
        if (DocumentCombine::checkForDocListData($transactionsID))
        {
            $documents = DocumentCombine::getDocumentList($transactionsID);
        }
        else $documents = [];
        $propertyError = session('propertyError') ?? false;
        $detailErrors  = session('detailErrors') ?? ['crap'];
        session()->forget(['propertyError', 'detailErrors']);

        //
        // ... Tasks
        if (TaskCombine::checkForTaskListData($transactionsID))
        {
            $clientRole = TransactionCombine::getTransactionRole($transactionsID);
            $tasks      = TaskCombine::getTaskList($transactionsID, $clientRole);
        }
        else $tasks = [];
        try
        {
            foreach ($tasks as $idx => $task)
            {
                $tasks[$idx]['ShortName']     = empty($task['ShortName']) ? '-' : $task['ShortName'];
                $tasks[$idx]['DateDue']       = date('l <b\r/\> M j, Y', strtotime($task['DateDue'] ?? $task['DueDate']));
                $tasks[$idx]['DateCompleted'] = empty($task['DateCompleted']) ? '-' : date('l<br/>M j, Y', strtotime
                ($task['DateCompleted']));
            }
        }
        catch (\Exception $e)
        {
            Log::error(['Exception' => $e, 'tasks' => $tasks, __METHOD__ => __LINE__]);
        }

        $result = TransactionCombine::keyPeople($transactionsID);
        $result = BaseCombine::collectionToArray($result);  //todo: kludge - fix this
        $result = reset($result);

        $data['Buyer']['Name']           = $result['Buyer'] ?? '';
        $data['Seller']['Name']          = $result['Seller'] ?? '';
        $data['Buyer\'s Agent']['Name']  = $result['Buyer\'s Agent'] ?? '';
        $data['Seller\'s Agent']['Name'] = $result['Seller\'s Agent'] ?? '';
        $data['Buyer\'s TC']['Name']     = $result['Buyer\'s Transaction Coordinator'] ?? '';
        $data['Seller\'s TC']['Name']    = $result['Seller\'s Transaction Coordinator'] ?? '';

        $data['Buyer']['Phone']           = $result['B Phone'] ?? '';
        $data['Seller']['Phone']          = $result['S Phone'] ?? '';
        $data['Buyer\'s Agent']['Phone']  = $result['BA Phone'] ?? '';
        $data['Seller\'s Agent']['Phone'] = $result['SA Phone'] ?? '';
        $data['Buyer\'s TC']['Phone']     = $result['BTC Phone'] ?? '';
        $data['Seller\'s TC']['Phone']    = $result['STC Phone'] ?? '';

        $data['Buyer']['Email']           = $result['B Email'] ?? '';
        $data['Seller']['Email']          = $result['S Email'] ?? '';
        $data['Buyer\'s Agent']['Email']  = $result['BA Email'] ?? '';
        $data['Seller\'s Agent']['Email'] = $result['SA Email'] ?? '';
        $data['Buyer\'s TC']['Email']     = $result['BTC Email'] ?? '';
        $data['Seller\'s TC']['Email']    = $result['STC Email'] ?? '';

        $buyers  = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.BUYER'), $transactionsID);
        $sellers = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.SELLER'), $transactionsID);

        $milestones = MilestoneCombine::milestonesByTransaction($transactionsID);
        if ($milestones->count() < 1)
        {
            $milestones = MilestoneCombine::saveTransactionTimeline($transactionsID,
                $transaction['transaction']->DateAcceptance ?? date('Y-m-d'), $transaction['transaction']->EscrowLength);
            Log::debug([__METHOD__, __LINE__, 'milestones' => $milestones]);
        }
        $today = strtotime('today');
        foreach ($milestones as $idx => $ms)
        {
            $ms               = _Convert::toArray($ms);
            $diff             = _Time::dateDiff($today, $ms['MilestoneDate']);
            $ms['diff']       = $diff['days'] * $diff['multiplier'];
            $milestones[$idx] = $ms;
        }

        return view(_LaravelTools::addVersionToViewName('transaction.summary'),
            [
                'transactionID' => $transactionsID,
                'data'          => $data,
                'kpIndices'     => ['Name', 'Phone', 'Email'],
                'roles'         => ['Buyer', 'Buyer\'s Agent', 'Buyer\'s TC',
                                    'Seller', 'Seller\'s Agent', 'Seller\'s TC',],
                'docsHeaders'   => ['Included', 'Code', 'Title', 'Due Date', 'Date Completed', 'Who Signs', 'Uploaded Files'],
                'docsFields'    => ['isIncluded', 'ShortName', 'Description', 'DueDateFormated', 'CompletedDateFormated', 'SignaturesNeeded', 'UploadedDocuments'],
                'documents'     => $documents,
                'tasksHeaders'  => ['Code', 'Task', 'Due Date', 'Date Completed',],
                'tasksFields'   => ['ShortName', 'Description', 'DateDue', 'DateCompleted',],
                'propertyError' => $propertyError,
                'detailErrors'  => $detailErrors,
                'buyers'        => is_null($buyers['data']) ? [] : $buyers['data']->toArray(),
                'sellers'       => is_null($sellers['data']) ? [] : $sellers['data']->toArray(),
                'client'        => $client,
                'role'          => $personData['role'],
                'milestones'    => $milestones,
            ]);
    }

    public function deprecated_tasks($transactionsID, $sort = 'Chrono')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

// ... If there are no tasks, make them.
        if (!lk_Transactions_Tasks::hasTasks($transactionsID))
        {
            $transactionRoles = TransactionCombine::getUserTransactionRoles($transactionsID, true);
            $transactionRoles = TaskCombine::getTaskRoles($transactionRoles, $transactionsID);
            TaskCombine::fillTransactionTaskTable($transactionsID, $transactionRoles);
        }

        session(['transaction_id' => $transactionsID]);
        $view       = 'transactionSummary.tasks';
        $clientRole = TransactionCombine::getClientRole($transactionsID);

        // ... Tasks
        $noTasks = false;
        if (!TaskCombine::checkForTaskListData($transactionsID)) $noTasks = true;

        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine::getTimeline($transactionsID),
                'daysUntilClose' => TransactionCombine::daysUntilClose($transactionsID),
                'property'       => TransactionCombine::getPropertySummary($transactionsID),
                'people'         => TransactionCombine::keyPeople($transactionsID),
                'noTasks'        => $noTasks,
                'clientRole'     => $clientRole,
                'sort'           => $sort,
            ]);
    }

    public function deprecated_ajaxGetTasks(Request $request)
    {
        $data           = $request->all();
        $transactionsID = $data['id'];
        $userRoles      = TransactionCombine::getUserTransactionRoles($transactionsID, true);
        if (TaskCombine::checkForTaskListData($transactionsID))
        {
            $tasks = [];
            $userRoles   = TaskCombine::getTaskRoles($userRoles, $transactionsID);
            foreach ($userRoles as $key => $u)
            {
                $tempTasks = TaskCombine::getTaskList($transactionsID, $u);
                $tasks     = array_merge($tasks, $tempTasks);
            }
        }
        else
        {
            $tasks = [];
        }
        if ($data['sort'] == 'Alpha')
        {
            _Arrays::sortByTwoColumns($tasks, 'CategoryOrder', SORT_ASC, 'DateDue', SORT_ASC);
        }
        else
        {
            _Arrays::sortByTwoColumns($tasks, 'DateDue', SORT_ASC, 'Category', SORT_ASC);
        }

        return response()->json([
            'tasks' => $tasks,
            'sort'  => $data['sort'],
        ]);
    }

    public function documents(int $transactionsID, $sortBy = 'DocumentCategory')
    {
        //if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        /*
         * Once this is verified to be working as expected we can remove the commented out code and replace the
         * documents blade file to documents-v-2
         */
        return view(_LaravelTools::addVersionToViewName('transactionSummary.documents-v-2'),[
            'transactionID'  => $transactionsID,
            'property'       => TransactionCombine::getPropertySummary($transactionsID),
            'daysUntilClose' => TransactionCombine::daysUntilClose($transactionsID),
        ]);
    }

    /**
     * This function is used to return the list of documents to the documents-v2 blade file, which contains the
     * documents.vue component.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public static function ajaxDocuments(Request $request)
    {
        $transactionsID = $request->input('transactionID');
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $docAccess = lk_TransactionsDocumentAccess::getByTransactionID($transactionsID);

        if (count($docAccess) == 0) $docAccess = DocumentAccessCombine::setDocumentAccessToTransaction($transactionsID, 0);

        $docTransfer = lk_TransactionsDocumentTransfer::getByTransactionID($transactionsID);

        if (count($docTransfer) == 0) $docTransfer = DocumentAccessCombine::setDocumentTransferToTransaction($transactionsID, 0);

        session(['transaction_id' => $transactionsID]);
        // ... Documents
        if (DocumentCombine::checkForDocListData($transactionsID))
        {
            $documents = DocumentCombine::getDocumentList($transactionsID, 'Description');
        }
        else
        {
            $documents = [];
        }
        $documentByCategory = [];
        $userRoles = TransactionCombine::getUserTransactionRoles($transactionsID);

        foreach ($documents as $key => $doc)
        {
            $document = Document::where('Code', $doc['Code'])->with('questionnaires')->get();
            $questionnaires = $document->first()->questionnaires;
            $doc['questionnaireID'] = 0;
            if($questionnaires)
            {
                foreach($questionnaires as $questionnaire)
                {
                    if ($doc['questionnaireID'] == 0)
                        if (in_array($questionnaire->ResponderRole, $userRoles))
                            $doc['questionnaireID'] = $questionnaire->ID;
                }
            }
            $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionsID,$doc['Code']);
            $doc['Permissions'] = $permissions;
            $doc['TrimmedCategory']   = str_replace('/','',$doc['Category']);

            foreach ($doc['UploadedDocuments'] as $uploadedKey => $uploadedDocument)
            {
                $uploadedDocument->isVisible = FALSE;
                $shareRoles = DocumentAccessController::decodeTransferSum($uploadedDocument->SharingSum);
                if(!empty($shareRoles))
                {
                    foreach ($userRoles as $role)
                    {
                        if(isset($shareRoles[$role]))
                        {
                            $uploadedDocument->isVisible = TRUE;
                        }
                    }
                }
            }
            $documents[$key] = $doc;
            $documentByCategory[$doc['TrimmedCategory']][] = $doc;
        }
        $signers = DocumentCombine::getSigners(NULL,$transactionsID,false);
        return response()->json([
            'documents'                 => $documents,
            'userID'                    => auth()->id(),
            'userRole'                  => \session('userRole'),
            'documentsByCategory'       => $documentByCategory,
            'signers'                   => $signers,
        ]);
    }

    public function deprecated_timeline(int $transactionsID)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u') || !AccessController::canAccessTransaction($transactionsID))
        {
            Log::debug(["you can't see this", 'userType' => session('userType')]);
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }
//Log::debug(['taid'=>$transactionsID, __METHOD__=>__LINE__]);
        $view = 'transactionSummary.timeline';
        session(['transaction_id' => $transactionsID]);
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine::getTimeline($transactionsID),
                'daysUntilClose' => TransactionCombine::daysUntilClose($transactionsID),
                'property'       => TransactionCombine::getPropertySummary($transactionsID),
                'people'         => TransactionCombine::keyPeople($transactionsID),
            ]);
    }

    public function post_timeline(Request $request)
    {
        $transactionsID = request('id');
        if (is_array($transactionsID)) $transactionsID = reset($transactionsID);
        $rv = TransactionCombine::getTimeline($transactionsID);
        return response()->json([
            'items' => $rv,
        ]);

    }

    public function listAffectedTasks(Request $request)
    {
        $milestoneID    = $request->input('milestoneID');
        $transactionID  = $request->input('transactionID');
        $list           = TaskCombine::previewTaskDueDatesChanges($milestoneID, $transactionID);
        return response()->json([
            'list' => $list,
        ]);
    }

    public function adjustTaskDueDatesToMilestone(Request $request)
    {
        /*This is re-purposed for ajax functionality */
        $data = $request->all();
        $add  = ' +';
        if (!isset($data['pendingTasks']) || $data['pendingTasks'] == false)
        {
            $milestoneID    = $request->input('milestoneID');
            $transactionID  = $request->input('transactionID');
            $list           = TaskCombine::previewTaskDueDatesChanges($milestoneID,$transactionID);
            $filteredList   = [];
            $list           = _Convert::toArray($list);
            $enteredDate    = new DateTime($data['newDate']);
            DB::table('lk_Transactions-Timeline')
              ->where('ID', $milestoneID)
              ->update([
                  'MilestoneDate' => $enteredDate,
                  'Notes'         => $data['Notes'] ?? NULL,
              ]);
            for ($i = 0; $i < count($list); $i++)
            {
                $list[$i] = _Convert::toArray($list[$i]);
                $offset   = null;
                $days = $list[$i]['DateOffset'];

                if (strpos($days, '-') === false)
                {
                    $days = $add . $days . ' days ';
                }
                else $days = $days . ' days ';

                $milestoneDate = new DateTime($list[$i]['MilestoneDate']);
                $taskDueDate   = new DateTime($list[$i]['TaskDateDue']);

                $comparisonDate = strtotime($milestoneDate->format('Y-m-d') . $days);
                $comparisonDate = date('Y-m-d', $comparisonDate);
                $newTaskDueDate = new DateTime($enteredDate->format('Y-m-d') . $days);
                if ($comparisonDate == $taskDueDate->format('Y-m-d'))
                {
                    DB::table('lk_Transactions-Tasks')
                      ->where('ID', $list[$i]['ID'])
                      ->update(['DateDue' => $newTaskDueDate->format('Y-m-d')]);
                    continue;
                }
                else
                {
                    $filteredList[] = $list[$i];
                }
            }
            array_values($list);
            if (!empty($list))
            {
                return response()->json([
                    'status' => 'success',
                    'list'   => $filteredList,
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'success',
                    'list'   => false,
                ]);
            }
        }
        else
        {
            if (isset($data['list']))
            {
                $list = $data['list'];
                foreach ($list as $taskItem) {
                    DB::table('lk_Transactions-Tasks')
                        ->where('ID', $taskItem['ID'])
                        ->update(['DateDue' => $taskItem['NewDate']]);
                }
            }
            return response()->json(['status' => 'success']);
        }
    }

    public function update_IsCompleted(Request $request)
    {
        $status     = $request->input('status');
        $notes      = $request->input('Notes');
        $id         = $request->input('id');
        $dateDue    = $request->input('DateDue');
        $update     = FALSE;
        if ($status == 'edit')
        {
            $update = lk_Transactions_Timeline::where('ID', $id)
                ->update([
                    'Notes' => $notes,
                    'MilestoneDate' => $dateDue,
                ]);
        }
        elseif($status == 'completed')
        {
            $update = DB::table('lk_Transactions-Timeline')
                ->where('ID', $id)
                ->update(['isComplete' => 1]);
        }
        elseif($status == 'incomplete')
        {
            $update = DB::table('lk_Transactions-Timeline')
                ->where('ID', $id)
                ->update(['isComplete' => 0]);
        }
        if($update) return response()->json([
            'status'    => 'success',
            'message'   => 'Milestone updated!',
        ]);
        else return response()->json([
           'status' => 'fail',
           'message' => 'An error has occurred when updating the Milestone. Error code: u-mil-001',
        ]);
    }

    /**
     * Validate and update/insert Tasks record
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTask(Request $request)
    {
        $data                    = $request->all();
        if(!$data['Description'] || !$data['DateDue'])
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Missing required fields.',
                'Description'   => $data['Description'],
                'DateDue'       => $data['DateDue'],
            ]);
        }

        $data['Transactions_ID'] = $data['transactionID'];
        $data['Tasks_Code']      = TaskCombine::getAdHocCode($data['taskRole']);
        $data['DateCreated']     = date('Y-m-d H:i:s');
        $data['Status']          = 'incomplete';
        $data['UserRole']        = session('userRole');
        unset($data['ID']);

        $task       = new lk_Transactions_Tasks();
        $task       = $task->upsert($data);
        if($task) return response()->json([
            'status'    => 'success',
            'message'   => 'Task added.'
        ]);
        else return response()->json([
            'status'    => 'fail',
            'message'   => 'Unable to add task. Error code: add-task-002',
        ]);
    }

    /**
     * Save uploaded file
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTransactionDocument(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $request->validate([
            'lk_id'    => 'required',
            'signedBy' => 'required',
            //'taDoc'    => 'required',
            'fileInfo' => 'required',
        ]);

        $ajax          = $request->ajax;
        $external      = $request->isExternal;
        $transactionID = $request->transactionID;
        $lk_ID         = $request->lk_id;
        $fileInfo      = $request->fileInfo;
        if ($external)
        {
            $fileInfo           = json_decode($fileInfo);
            $fileInfo->signedBy = $request->signedBy;
            if (DocumentCombine::putExternalDocumentIntoBag($transactionID, $lk_ID, $fileInfo))
            {
                if ($ajax)
                {
                    return response()->json([
                        'status' => 'success',
                    ]);
                }
                session()->flash('alert-success', 'File ' . ' Uploaded');
                return redirect()->back()->with([
                    'transactionID' => request('transactionID'),
                ]);
            }
        }
        if (DocumentCombine::putDocumentIntoBag($request->transactionID, $request->lk_id, 'taDoc'))
        {
            if ($ajax)
            {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            session()->flash('alert-success', 'File ' . ' Uploaded');
            return redirect()->back()->with([
                'transactionID' => request('transactionID'),
            ]);
        }
        else
        {
            if ($ajax)
            {
                return response()->json([
                    'status' => 'fail',
                ]);
            }
        }
    }


    public function updateTask(Request $request)
    {

        $data             = $request->all();
        $taskID           = $data['id'];
        $status           = $data['status'];
        $updateData['ID'] = $taskID;
        $task             = new lk_Transactions_Tasks();

        if ($status == 'edit')
        {
            $updateData['Description']  = $data['Description'];
            $updateData['DateDue']      = $data['DateDue'];
            $updateData['Notes']        = $data['Notes'];
            $updated = $task->upsert($updateData);
            if ($updated)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Task edited!',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Unable to edit task. Error code: e-task-001',
                ]);
            }
        }

        if ($status == 'delete')
        {
            $updateData['DateCompleted'] = date('Y-m-d G:i:s');
            $updateData['Status']        = 'deleted';
            $updateData['deleted_at']    = date('Y-m-d G:i:s');
            $task->upsert($updateData);
            return response()->json([
                'status'    => 'success',
                'message'   => 'Task successfully deleted!',
                'Task ID'   => $taskID,
                'Status'    => 'deleted',
            ]);
        }
        if ($status == 'completed')
        {
            $updateData['DateCompleted'] = date('Y-m-d G:i:s');
            $updated = $task->upsert($updateData);
            if($updated)
            {
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'Task marked complete!',
                    'Task ID'       => $taskID,
                    'DateCompleted' => $updateData['DateCompleted'],
                ]);
            }
            else
            {
                return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark task complete. Error code e-task-003',
                ]);
            }
        }
        if ($status == 'incomplete')
        {
            $updateData['DateCompleted'] = null;
            $updated = $task->upsert($updateData);
            if($updated)
            {
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'Task marked incomplete!',
                ]);
            }
            else
            {
                return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark task incomplete. Error code e-task-004',
                ]);
            }
        }

        return response()->json([
            'status'    => 'fail',
            'message'   => 'Task was not updated. Error code: e-task-005',
            'Task ID' => $taskID,
            'Status'  => 'Error: Nothing was updated',
        ]);
    }

    public function markDateComplete(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); //...Make sure the user is logged in
        $request->validate([
            'lk_id'         => 'required|numeric',
            'transactionID' => 'required|numeric',
        ]);
        $lk_id = request('lk_id');
        $query = DB::table('lk_Transactions-Documents')->where('ID', $lk_id)
                   ->update([
                       'DateCompleted'   => now(),
                       'ApprovedByUsers_ID' => CredentialController::current()->ID(),
                   ]);
        if ($query)
        {
            return response('Completed', 200);
        }
        // <!-- Use Ajax to set lk_Tranactions-Documents.DateCompleted to <now> and lk_Tranactions-Documents.ApprovedByTC_ID to the current user's TC ID ($currentTCID) -->
    }

    public function disclosures($transactionsID)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $view = 'transactionSummary.disclosures';
        session(['transaction_id' => $transactionsID]);
        $userRoles = TransactionCombine::getUserTransactionRoles($transactionsID);
        //get disclosures as a query
        $disclosures = QuestionnaireCombine::questionnairesByTransaction($transactionsID,'disclosure', $userRoles, null, false);
        //pass the query into the filterByResponderRole to only get relevant disclosures for that role
        $disclosures = $disclosures->get();
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine::getTimeline($transactionsID),
                'daysUntilClose' => TransactionCombine::daysUntilClose($transactionsID),
                'property'       => TransactionCombine::getPropertySummary($transactionsID),
                'people'         => TransactionCombine::keyPeople($transactionsID),
                'disclosures'    => $disclosures,
            ]);
    }

    public function shareRoom(int $transactionID, $userID = null)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $view = 'transactionSummary.shareRoom';
        session(['transaction_id' => $transactionID]);
        $userID = $userID ?? CredentialController::current()->ID();

        $excludeCols = ['isTest', 'Status', 'deleted_at', 'DateUpdated', 'Documents_ID',
                        'DocumentPath', 'OriginalDocumentName'];

        $docs = ShareRoomCombine::getVisibleDocuments($transactionID, $userID);

        $table = DisplayTable::getBasic($docs, ['excludeColumns' => $excludeCols]);
        //            $legendAccess = lu_DocumentAccessActions::displayValueintArray();
        //            $legendTransfer = lu_DocumentAccessRoles::displayValueintArray();

        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionID,
                'data'           => $docs,
                'table'          => $table,
                'daysUntilClose' => TransactionCombine::daysUntilClose($transactionID),
                'property'       => TransactionCombine::getPropertySummary($transactionID),
                'people'         => TransactionCombine::keyPeople($transactionID),
            ]);
    }

    public function timeline($transactionID) //rename this so that it stresses that it is only a view.
    {
        $view           = 'transactionSummary.timeline';
        $property       = TransactionCombine::getPropertySummary($transactionID);
        $daysUntilClose = TransactionCombine::daysUntilClose($transactionID);
        $keyPeople      = TransactionCombine::keyPeople($transactionID);
        return view(_LaravelTools::addVersionToViewName($view),[
            'transactionID'     => $transactionID,
            'property'          => $property,
            'daysUntilClose'    => $daysUntilClose,
            'people'            => $keyPeople,
        ]);
    }

    public function getMergedTasksTimeline(Request $request)
    {
        $transactionID  = $request->input('transactionID');
        $userRoles      = TransactionCombine::getUserTransactionRoles($transactionID, true);

        /** --------------------------------------------------------------------
         * Get the tasks
         */
        $tasks          = [];
        $userRoles      = TaskCombine::getTaskRoles($userRoles, $transactionID);
        foreach ($userRoles as $key => $u)
        {
            /**
             * The reason we don't check first to see if there are tasks that exist is because
             * TaskCombine::getTaskList checks and fills the necessary tasks if there are none.
             */
            $tempTasks = TaskCombine::getTaskList($transactionID, $u);
            $tasks     = array_merge($tasks, $tempTasks);
        }

        /** ---------------------------------------------------------------------
         * Get the timeline
         */
        $timeline = TransactionCombine::getTimeline($transactionID);

        /**
         * Merge timeline and tasks where the category is the index.
         */

        $mergedObject = [];
        $mergedArray = []; //final data must be an array, not object. For using in javascript
        /*
         * We cannot use the index, ID or link IDs to reference tasks and timeline together because they
         * may have the same value in their respective tables. So counter will serve as a distinguishing integer
         * for both.
         */
        $counter = 0;

        $milestones = [];
        $milestonesObject = [];

        $tasksArray = [];
        $taskObject = [];

        foreach ($timeline as $key => $item)
        {
            $milestoneData                              = new \stdClass();
            $milestoneData->type                        = 'timeline';
            $milestoneData->Link_ID                     = $item->ID;
            $milestoneData->Description                 = $item->MilestoneName;
            $milestoneData->Display                     = $item->MilestoneName;
            $milestoneData->DateDue                     = $item->MilestoneDate;
            $milestoneData->Code                        = $item->Code;
            $milestoneData->mustBeBusinessDay           = $item->mustBeBusinessDay;
            $milestoneData->isActive                    = $item->isActive;
            $milestoneData->isComplete                  = $item->isComplete;
            $milestoneData->isTest                      = $item->isTest;
            $milestoneData->Tasks_ID                    = NULL;
            $milestoneData->DateCompleted               = NULL;
            $milestoneData->TaskDateCreated             = NULL;
            $milestoneData->TaskDateOffset              = NULL;
            $milestoneData->UserRole                    = NULL;
            $milestoneData->Counter                     = $counter;
            $milestoneData->Notes                       = $item->Notes ?? NULL;
            $milestones[]                               = $milestoneData;
            $milestonesObject[$item->Code][]            = $milestoneData;
            $mergedObject[$item->Code][]                = $milestoneData;
            array_push($mergedArray,$milestoneData);
            $counter++;
        }

        foreach ($tasks as $key => $task)
        {
            $taskData = new \stdClass();
            $taskData->type                              = 'task';
            $taskData->Code                              = $task->Timeline_Code;
            $taskData->Tasks_ID                          = $task->ID;
            $taskData->Link_ID                           = $task->lk_ID;
            $taskData->DateCompleted                     = $task->DateCompleted;
            $taskData->TaskDateCreated                   = $task->DateCreated;
            $taskData->DateDue                           = $task->DateDue;
            $taskData->TaskDateOffset                    = $task->DateOffset;
            $taskData->Description                       = $task->Description;
            $taskData->Display                           = $task->Display;
            $taskData->UserRole                          = $task->UserRole;
            $taskData->mustBeBusinessDay                 = NULL;
            $taskData->isActive                          = NULL;
            $taskData->isComplete                        = NULL;
            $taskData->isTest                            = NULL;
            $taskData->Counter                           = $counter;
            $taskData->Notes                             = $task->Notes ?? NULL;
            $tasksArray[]                                = $taskData;
            $taskObject[$task->Timeline_Code][]          = $taskData;
            $mergedObject[$task->Timeline_Code][]        = $taskData;
            array_push($mergedArray,$taskData);
            $counter++;
        }

        _Arrays::sortByTwoColumns($mergedArray,'DateDue',SORT_ASC,'type',SORT_DESC);
        foreach ($mergedObject as $key => $obj)
        {
            _Arrays::sortByTwoColumns($obj, 'type', SORT_DESC, 'DateDue', SORT_ASC);
            $mergedObject[$key] = $obj;
        }
        _Arrays::sortByColumn($tasksArray, 'DateDue', SORT_ASC);

        return response()->json([
            'mergedArray'           => $mergedArray         ?? [],
            'mergedObject'          => $mergedObject        ?? new \stdClass(),
            'tasks'                 => $tasksArray          ?? [],
            'tasksObject'           => $taskObject          ?? new \stdClass(),
            'milestones'            => $milestones          ?? [],
            'milestonesObject'      => $milestonesObject    ?? new \stdClass(),
            'taskRoles'             => $userRoles,
        ]);
    }

}
