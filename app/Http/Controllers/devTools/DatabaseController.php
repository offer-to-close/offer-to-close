<?php

namespace App\Http\Controllers\devTools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DatabaseController extends Controller
{

    /**
     * Update the isTest field value on all the tables from NULL to 0 (zero)
     *
     * @param array $tables
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function zeroIsTest($tables=[])
    {
        if (env('APP_ENV') != 'local' &&  !Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        if (is_string($tables)) $tables = [$tables];
        if (count($tables) == 0) $tables = self::listAllTables();
        foreach ($tables as $table)
        {
            try
            {
                DB::table($table)->update(['isTest'=>0])->whereNull('isTest')->where('ID', '>' , 0);
            }
            catch (\Exception $e)
            {
                Log::error(['EXCEPTION'=>$e, 'table'=>$table, __METHOD__=>__LINE__]);
            }
        }
    }

    /**
     * Return the names of all the tables in the database
     *
     * @return mixed
     */
    public static function listAllTables()
    {
        return  DB::select('SHOW TABLES');
    }
}
