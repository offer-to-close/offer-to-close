<?php

namespace App\Http\Controllers;

use App\Models\lu_Accesses;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    private static $accessValues = [];

    /**
     * Uses values stored in Accesses table to determine access rights
     *
     * @param $target Access sum value to compare against
     * @param string|int $accessCode Is either a single character (c|r|u|d) asking if access requested is available, or
     *                               is an integer value that corresponds to the combination of one or more of the crud values
     *
     * @return bool|int Returns true if requested access is available, otherwise return false
     */
    public static function hasAccess($target, $accessCode)
    {
        if (empty(self::$accessValues)) self::initialize();

        if (!is_numeric($target) || $target <= 0) return false;

        if (is_numeric($accessCode))
        {
            return ($target & $accessCode);
        }
        else
        {
            if (!isset(self::$accessValues[$accessCode])) return false;
            return ($target & self::$accessValues[$accessCode]['ValueInt']);
        }

    }

    /**
     *
     */
    private function initialize()
    {
        $tmp = lu_Accesses::where('isTest', '!=', 1)->orWhereNull('isTest')->get();
        foreach ($tmp as $row)
        {
            self::$accessValues[$row->Value] = [
                'Value' => $row->Value,
                'Display' => $row->Display,
                'ValueInt' => $row->ValueInt,
            ];
        }
    }
}
