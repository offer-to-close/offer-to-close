<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\ShareRoomCombine;
use App\Library\otc\_Helpers;
use App\Library\Utilities\_LaravelTools;
use App\Models\ShareRoom;
use Illuminate\Http\Request;

use App\Library\Utilities\DisplayTable;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Combine\TransactionCombine;

class DashboardController extends Controller
{
    public $transactionID = null;

    public $transactionParts = ['transaction',
                                'property',
                                'buyer',
                                'buyersAgent',
                                'seller',
                                'sellersAgent',
                                'timeline',
    ];
    public $nonInputFields = ['ID',
                              'isTest',
                              'Status',
                              'DateCreated',
                              'DateUpdated',
                              'deleted_at',
    ];

    public $transaction = null;

    protected $isEmpty = true;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array \Illuminate\Http\Response
     */
    public function getEmptyTransaction(array $values)
    {
        $models = ['transaction'  => new Transaction,
                   'property'     => new Property,
                   'buyer'        => new Buyer,
                   'buyersAgent'  => new Agent,
                   'seller'       => new Seller,
                   'sellersAgent' => new Agent,
                   'timeline'     => new Timeline,
        ];

        $rv = [];

        foreach ($models as $name => $model)
        {
            foreach ($model::getEmptyRecord() as $field => $value)
            {
                if (array_search($field, $model->nonInputFields) === false) continue;
                if (!isset($values[$name][$field]) || is_null($values[$name][$field])) continue;
                $model->$field = $value;
            }
            $rv[$name] = $model->save();
        }
        $this->isEmpty = true;
        return $rv;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public function getTransaction(int $transactionsID)
    {
        $transaction = Transaction::find($transactionsID);
        return [
            'transaction'  => $transaction,
            'property'     => Property::where('Transactions_ID', $transactionsID)->get(),
            'buyer'        => Buyer::where('Transactions_ID', $transactionsID)->get(),
            'buyersAgent'  => Agent::where('ID', $transaction->BuyersAgent_ID)->get(),
            'seller'       => Seller::where('Transactions_ID', $transactionsID)->get(),
            'sellersAgent' => Agent::where('ID', $transaction->SellersAgent_ID)->get(),
            'timeline'     => TransactionCombine::getTimeline($transactionsID),
        ];
    }

    /**
     * Returns view of associated data depending on the role and id of users passed in
     *
     * @param null $role
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactionList($role = null, $roleID = null, $userID=null, $view = NULL)
    {
        if (is_null($view))
        {
            if ($role == 'g') $view = 'dashboard.guest.default';
            else $view = 'dashboard.prosumer.TransactionList';
        }

        if (in_array($role, ['g'])) $result = ShareRoomCombine::getRoomsByUser($userID);
        else $result = TransactionCombine::transactionList($role, $roleID, $userID);
//ddd(['result'=>$result,'view' => $view, 'role' => $role, 'roleID' => $roleID, 'userID' => $userID, __METHOD__=>__LINE__]);

        if (is_array($result) && isset($result['type']))
        {
            switch ($result['type'])
            {
                case 'success':
                    $view = \App\Library\Utilities\_LaravelTools::addVersionToViewName($view);
                    TransactionCombine::instantiateSessionTransactionList($result['success']['info']['collection']);
                    return view($view, compact('result'));
                    break;

                case 'genericError':
                default:
                    return view('general.genericError', $result['error']['info'] ?? []);
                    break;
            }
        }
        $view = \App\Library\Utilities\_LaravelTools::addVersionToViewName($view);
        return view($view, compact('result'));
//        return ['Death by lack of result', 'location' => implode('/', [__CLASS__, __FUNCTION__, __LINE__])];
    }

    /**
     * Returns view of associated data depending on the role and id of users passed in
     *
     * @param null $role
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactionListByUser($role = null, $userID = null, $view = NULL)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        _Helpers::clearTransactionSession();
        if (is_null($role)) $role = session('userRole') ?? 'g';
        if (is_null($userID)) $userID = CredentialController::current()->ID();

        if (is_null($view))
        {
            if ($role == 'g') $view = 'dashboard.guest.default';
            else $view = 'dashboard.prosumer.TransactionList';
        }

        $id = AccountCombine::getRoleID($role, $userID);
        if (!$id || is_null($id) || count($id) == 0 || !$id->first())
        {
            $roleController = new RoleController();
            $savedRole = $roleController->saveNewUserToRole($role);
            if (!$savedRole)
            {
                return view(_LaravelTools::addVersionToViewName('tools.broken'),
                ['data'=>['role' => $role, 'id' => $id, __METHOD__=>__LINE__]]);
            }
            else
            {
                $id = collect(['ID' => $savedRole->id]);
            }
        }
        try
        {
            if (!$id->isEmpty())
            {
                $id = $id->first();
                $id = is_array($id) ? $id['ID'] : $id->ID;
                return $this->transactionList($role, $id, $userID, $view);
            }
            else
            {
                return view(_LaravelTools::addVersionToViewName($view),
                    ['result'  =>
                          ['type'    => 'success',
                           'success' => ['info' => ['collection' => collect([]),],],]
                ]);
            }
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION'=>$e->getMessage(), 'input'=>['role'=>$role, 'userID'=>$userID,],  'id'=>$id, __METHOD__=>__LINE__]);
            ddd(['EXCEPTION'=>$e->getMessage(), 'input'=>['role'=>$role, 'userID'=>$userID,], 'id'=>$id, __METHOD__=>__LINE__], 30, 3000);
        }

    }

    /**
     * @param string $state
     * @param null   $role
     * @param null   $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questionList($state = 'CA', $role = null, $id = null)
    {

        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $select     = '
                    ID, Category, `Order`, ShortName, Description, Provider, Question, InclusionResult 
                    , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'b\'), true, false) as `Buyer Signs`
                    , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'s\'), true, false) as `Seller Signs`
                    , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'ba\'), true, false) as `Buyer\'s Agent Signs`
                    , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'sa\'), true, false) as `Seller\'s Agent Signs`
                   ';
        $select     = str_replace("\r\n", "\n", $select);
        $where      = 'state = ? and  deleted_at is null';
        $bindings[] = $state;
//
// ... Run query for transactions
//
        $query     = DB::table('Documents')
                       ->selectRaw($select)
                       ->whereRaw($where, $bindings)
                       ->orderBy('Provider')
                       ->orderBy('Category')
                       ->orderBy('Order');
        $documents = $query->get();

        $ayQuestions = [];
        $ayDocuments = [];

        foreach ($documents as $idx => $document)
        {
            $ayDocuments[$idx] = (array) $document;
            if (!empty($ayDocuments[$idx]['Question']))
            {
                $ayQuestions[$idx] = [
                    'Doc-ID'          => $ayDocuments[$idx]['ID'],
                    'Q-Code'          => $ayDocuments[$idx]['Category'] . '-' . $ayDocuments[$idx]['Order'],
                    'Doc-Name'        => $ayDocuments[$idx]['ShortName'],
                    'Question'        => $ayDocuments[$idx]['Question'],
                    'InclusionResult' => $ayDocuments[$idx]['InclusionResult'],
                ];
            }
        }

        $tableDisplay = DisplayTable::getBasic($ayQuestions);

        return view('dashboard.questions',
            [
                'table'     => $tableDisplay,
                'questions' => $ayQuestions,
                'documents' => $ayDocuments,
            ]
        );
    }

    /**
     * Walk record array and determine if any value is not empty
     *
     * @return array \Illuminate\Http\Response
     */
    protected function isEmpty($record)
    {
        if (!is_array($record) || empty($record)) return true;
        foreach ($record as $part)
        {
            if (is_array($part))
            {
                foreach ($part as $field)
                {
                    if (!empty($field)) return false;
                }
            }
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**             WORK IN PROGRESS
     * Insert a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function createTransaction(Request $request)
    {
        $table = new Transaction();
        $data  = $request->validated();
        $table->upsert($data);
        return redirect(route('dash.ta.list'))->with('success', 'New ' . substr(__FUNCTION__, 6) . ' has been created!');
    }

    /**             WORK IN PROGRESS
     * Insert a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $model
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insert(Request $request, string $model)
    {
        $modelClass = 'App\\Models\\' . $model;
        $table      = new $modelClass;
        Log::info([$model, $request, __METHOD__ => __LINE__]);
        $data = $request->validate([
            'NameFull'  => 'bail|required',
            'NameFirst' => 'required',
            'NameLast'  => 'required',
        ]);

        Log::info([$data, __METHOD__ => __LINE__]);
        $table->upsert($data);
        return redirect('/')->with('success', 'New ' . substr(__FUNCTION__, 6) . ' has been created!');
    }


    private function evaluateQuestionReply($column, $formAnswer)
    {
        switch ($column)
        {
            case 'willRentBack':
                if ($formAnswer == 'No')
                {
                    return false;
                }
                else return true;
                break;

            case 'RentBackLength':
                if ($formAnswer == 'No') return null;
                if ($formAnswer == 'Yes, for less than 30 days') return 1;
                if ($formAnswer == 'Yes, for more than 30 days') return 31;
                break;

            case 'IsBuyerATrust':
                return ($formAnswer == 'Yes');
                break;

            case 'IsSellerATrust':
                return ($formAnswer == 'Yes');
                break;

            case 'hasHOA':
                return ($formAnswer == 'Yes');
                break;

            case 'hasSeptic':
                return ($formAnswer == 'Yes');
                break;

            case 'hasBuyerWaivedInspection':
                return ($formAnswer == 'Yes');
                break;
        }
    }

    public function prosumerDashboard()
    {
        $view = 'dashboard.prosumer.TransactionList';
        //if has admin access   -> admin dashboard
        //if has staff access   -> staff dashboard

        if(AccessController::hasAccess('u'))
        {
            $view = 'dashboard.prosumer.dashboard';
            $role = session('userRole');
            return $this->transactionListByUser($role,CredentialController::current()->ID(),$view);
        }
    }
}
