<?php

namespace App\Http\Controllers;

use App\Combine\DocumentAccessCombine;
use App\Combine\TransactionCombine;
use App\Library\otc\_Locations;
use App\Library\Utilities\_Crypt;
use App\Library\Utilities\_LaravelTools;
use App\Models\Document;
use App\Models\lk_TransactionsDocumentAccess;
use DateTime;
use App\Models\lk_Transactions_Documents;
use function foo\func;
use Illuminate\Http\Request;
use App\Combine\DocumentCombine;
use Illuminate\Support\Facades\Log;
use App\Models\bag_TransactionDocument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class DocumentController extends Controller
{

    public function byTransactionId($transactionID)
    {
        $documents = DocumentCombine::getDocuments($transactionID);
        $files     = DocumentCombine::decodeDocumentFilename(DocumentCombine::filesByTransaction($transactionID));

        $transactionController = new TransactionController();
        $transaction           = $transactionController->getTransaction($transactionID);

        return view('transaction.summary.toDo',
            [
                'title'      => 'Documents',
                'property'   => $transaction['property'],
                'collection' => collect($documents),
                'files'      => $files,
            ]);
    }
    /**
     * Display all the documents for a given state
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function allByState($stateCode)
    {
        $documents   = DocumentCombine::byState($stateCode);
        $shortName   = $documents->pluck('ShortName')->all();
        $description = $documents->pluck('Description')->all();
        $provider    = $documents->pluck('Provider')->all();
        $data        = [];

        for ($i = 0; $i < count($documents); $i++)
        {
            $data[$i] = ['Name'        => $shortName[$i],
                         'Description' => $description[$i],
                         'Provider'    => $provider[$i]];
        }

        return view('general.genericTable',
            [
                'title'      => title_case(__FUNCTION__),
                'stateCode'  => $stateCode,
                'collection' => collect($data),
            ]
        );

    }

    /**
     * Display all the documents for a given state
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function allByTransaction($transactionID)
    {
        $documents   = DocumentCombine::byTransaction($transactionID);
        $shortName   = $documents->pluck('ShortName')->all();
        $description = $documents->pluck('Description')->all();
        $provider    = $documents->pluck('Provider')->all();
        $data        = [];

        for ($i = 0; $i < count($documents); $i++)
        {
            $data[$i] = ['Name'        => $shortName[$i],
                         'Description' => $description[$i],
                         'Provider'    => $provider[$i]];
        }

        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionID,
                'collection'    => collect($data),
            ]
        );

    }

    public function saveDocumentInBag(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $maxYear = 1 + date('Y');

        $request->validate([
            'ID'           => 'integer',
            'lktd_id'      => 'required|integer',
        ]);

        $data          = $request->all();

        $address       = $request->input('PropertyAddress');
        $addressVerify = new AddressVerification();
        $addressList   = $addressVerify->isValid($address);

        if ($addressList == false)
        {
            return response()->json([
                'status'          => 'AddressError',
                'PropertyAddress' => 'Invalid Address',
            ], 404);
        }

        $data['Street1'] = $addressList[0]['Street1'];
        $data['Street2'] = $addressList[0]['Street2'];
        $data['City']    = $addressList[0]['City'];
        $data['State']   = $addressList[0]['State'];
        $data['Zip']     = $addressList[0]['Zip'];
        $property        = new Property();
        $property        = $property->upsert($data);
        $data['ID']      = $data['ID'] ?? $property->ID ?? $property->id;
        Log::debug(['&&', $property]);

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Properties_ID' => $data['ID']]);

        if ($property && $rv)
        {
            return response()->json([
                'status'       => 'success',
                'YearBuilt'    => $data['YearBuilt'],
                'address'      => $address,
                'PropertyType' => $data['PropertyType'],
                'hasHOA'       => $data['hasHOA'] ? 'Yes' : 'No',
                'hasSeptic'    => $data['hasSeptic'] ? 'Yes' : 'No',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
            ]);
        }

    }

    public function ajaxFetchDocument(Request $request)
    {
        $bagID = $request->input('bagID');

        $bag = bag_TransactionDocument::where('ID', $bagID)->get()->first();
        if ($bag)
        {
            $path = explode('\\', str_replace('/', '\\', $bag['DocumentPath']??null));
            $file = array_pop($path);
            $taid = array_pop($path);
            $theURL = explode('/',$bag['DocumentPath'] ?? NULL);
            $url = \App\Library\otc\_Locations::url('uploads', implode('/', ['transactions', $taid, $file]));
            if(isset($theURL[2])) if (in_array($theURL[2],Config::get('constants.STORAGE_URLS'))) $url = $bag['DocumentPath'];
            return response()->json([
                "status"        => 'success',
                'file'          => $file,
                'taid'          => $taid,
                'url'           => $url,
                'header'        => $bag->OriginalDocumentName,
                'timeCreated'   => \App\Library\Utilities\_Time::convertToDateMDY($bag->DateCreated),

            ]);
        }

        return response()->json([
            'status' => 'empty'
        ]);
    }

    public function openDocumentBag($transactionID, $documentCode)
    {
        if(!$transactionID || !$documentCode || is_null($transactionID) || is_null($documentCode))
        {
            Session::flash('error','Missing transaction or document code.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID,$documentCode);
        $permissions = json_encode($permissions);
        return view(_LaravelTools::addVersionToViewName('documents.bagView'),
            [
                'transactionID' => $transactionID,
                'documentCode'  => $documentCode,
                'permissions'   => $permissions,
            ]
        );
    }

    public function ajaxGetDocumentsInBag(Request $request)
    {
        $transactionID = $request->input('transactionID');
        $documentCode  = $request->input('documentCode');

        $bagDocuments = DB::table('bag_TransactionDocuments')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.ID', '=', 'bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->leftJoin('users', 'bag_TransactionDocuments.UploadedByUsers_ID', '=', 'users.id')
            ->select([
                'bag_TransactionDocuments.ID',
                'bag_TransactionDocuments.lk_Transactions-Documents_ID as lk_ID',
                'bag_TransactionDocuments.isActive',
                'bag_TransactionDocuments.SignedBy',
                'bag_TransactionDocuments.isComplete',
                'bag_TransactionDocuments.DateCreated',
                'bag_TransactionDocuments.ApprovedByUsers_ID',
                'bag_TransactionDocuments.DateApproved',
                'bag_TransactionDocuments.OriginalDocumentName',
                'bag_TransactionDocuments.DocumentPath',
                'bag_TransactionDocuments.SharingSum',
                'bag_TransactionDocuments.UploadedBy_ID',
                'bag_TransactionDocuments.UploadedByRole',
                'bag_TransactionDocuments.UploadedByUsers_ID',
                'lk_Transactions-Documents.Documents_ID',
                'lk_Transactions-Documents.Documents_Code',
                'lk_Transactions-Documents.Description',
                'users.name',
                'users.image'
            ])
            ->where('Documents_Code', '=', $documentCode)
            ->where('Transactions_ID', '=', $transactionID)
            ->orderBy('ID')
            ->get()
            ->toArray();

        $userRoles = TransactionCombine::getUserTransactionRoles($transactionID);
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID,$documentCode);
        foreach ($bagDocuments as $key => $doc)
        {
            $doc->isOwner   = FALSE;
            $doc->canShare  = FALSE;
            $unset          = TRUE;

            if ($doc->UploadedByUsers_ID == auth()->id())
            {
                $doc->isOwner   = TRUE;
                $doc->canShare  = TRUE;
                $unset = FALSE;
            }

            if (!$doc->isOwner)
            {
                $shareRoles = DocumentAccessController::decodeTransferSum($doc->SharingSum);
                foreach ($userRoles as $urole)
                {
                    if (isset($shareRoles[$urole]))
                    {
                        $unset          = FALSE;
                        $doc->canShare  = TRUE;
                    }
                }
            }

            if($unset)
            {
                unset($bagDocuments[$key]);
                continue;
            }
            $theURL = explode('/', $doc->DocumentPath ?? NULL);
            if (isset($theURL[2]))
            {
                if (!in_array($theURL[2], Config::get('constants.STORAGE_URLS'))) {
                    $path = explode('\\', str_replace('/', '\\', $doc->DocumentPath ?? null));
                    $file = array_pop($path);
                    $doc->DocumentPath = _Locations::url('uploads', implode('/', ['transactions', $transactionID, $file]));
                }
            }
            if (!is_null($doc->image)) {
                $doc->image = explode('/', $doc->image);
                if(isset($doc->image[3])) $doc->image = _Locations::url('images', $doc->image[2].'/'.$doc->image[3]);
                else $doc->image = _Locations::url('images', $doc->image[2]);
            }
            $doc->signers = NULL;
            if (!is_null($doc->SignedBy)) {
                $signers = DocumentCombine::getSigners($doc->SignedBy, $transactionID);
                $doc->signers = $signers;
            }
        }

        $bagDocuments = array_values($bagDocuments);
        if (empty($bagDocuments))
        {
            $document                           = [];
            $document[0]                        = [];
            $document[0]['isIncluded']          = 1;
            $document[0]['docCode']             = $documentCode;
            $document[0]['MilestoneDate']       = NULL;
            $lk_ID = lk_Transactions_Documents::where('Documents_Code','=',$documentCode)
                ->where('Transactions_ID', '=', $transactionID)
                ->select('ID')->limit(1)->get()->toArray();
            $signers = [
              'b' => [],
              's' => [],
              'ba'=> NULL,
              'sa'=> NULL
            ];
            foreach ($signers as $key => $s)
            {
                $person = TransactionCombine::getPerson($key,$transactionID);
                $signers[$key] = $person['data']->toArray();
            }
            return response()->json([
                'lk_ID'     => empty($lk_ID) ? NULL : $lk_ID[0]['ID'] ?? NULL,
                'signers'   => $signers,
                'status'    => 'fail',
                'message'   => 'There are no uploaded documents.',
            ]);
        }
        else if(!$bagDocuments) return response()->json([
            'status' => 'fail',
            'message' => 'Unable to Perform the query.',
        ]);

        return response()->json([
            'status' => 'success',
            'bagDocuments' => $bagDocuments,
        ]);
    }

    public function getFilestackSignature()
    {
        $date = new DateTime('today +1000 days');
        $date = strtotime($date->format('Y-m-d H:i:s'));
        $policy = array(
            'call' => array(
                'pick',
                'read',
                'store',
            ),
            'expiry' => $date,
        );
        $jsonPolicy = json_encode($policy);
        $urlSafeBase64EncodedPolicy = _Crypt::base64url_encode($jsonPolicy);
        $signature = hash_hmac('sha256',$urlSafeBase64EncodedPolicy,Config::get('constants.FILESTACK_SECRET'));
        return response()->json([
            'policy' => $urlSafeBase64EncodedPolicy,
            'signature' => $signature,
        ]);
    }

    public function getRequiredSigners(Request $request)
    {
        $documentCode   = $request->input('documentCode');
        $transactionID  = $request->input('transactionID');
        $isAdHoc = strpos($documentCode,'*') !== FALSE;
        if(is_null($documentCode)) return response()->json([
            'status'    => 'fail',
            'message'   => 'Document Code was not set. Error Code: req-s-001',
        ]);
        $requiredSigners = DB::table('Documents')
            ->select('Documents.requiredSignatures')
            ->when($isAdHoc, function ($query){
                $query->where('Documents.Code', '*');
            })
            ->when(!$isAdHoc, function ($query) use ($documentCode){
                $query->where('Documents.Code', $documentCode);
            })
            ->limit(1)
            ->get()
            ->toArray();

        if($transactionID) $signers = DocumentCombine::getSigners('', $transactionID);
        if(empty($requiredSigners)) return response()->json([
            'status'    => 'fail',
            'message'   => 'The response was empty. Error Code: req-s-002',
        ]);
        $requiredSigners = (int)$requiredSigners[0]->requiredSignatures;
        $requiredSigners = DocumentAccessController::decodeTransferSum($requiredSigners);
        if (empty($requiredSigners)) return response()->json([
            'status'    => 'success',
            'message'   => 'No signatures are required',
        ]);
        if($transactionID)
        {
            return response()->json([
                'status'            => 'success',
                'requiredSigners'   => $requiredSigners,
                'documentSigners'   => $signers,
                'message'   => '',
            ]);
        }
        return response()->json([
            'status'                => 'success',
            'requiredSigners'    =>  $requiredSigners,
            'message'   => '',
        ]);
    }

    public function markDocumentAsCompleteIncomplete(Request $request)
    {
        $ID = $request->input('lk_ID');
        $lk_TransactionDocument = lk_Transactions_Documents::find($ID);
        if(!is_null($lk_TransactionDocument))
        {
            if(is_null($lk_TransactionDocument->DateCompleted))
            {
                $query = lk_Transactions_Documents::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => auth()->id(),
                        'DateCompleted'         => date('Y-m-d H:i:s'),
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked complete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as complete.',
                    'error_code'    => 'doc-com-001'
                ]);
            }
            else
            {
                $query = lk_Transactions_Documents::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => NULL,
                        'DateCompleted'         => NULL,
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked as incomplete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as incomplete.',
                    'error_code'    => 'doc-com-003'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Document not found.',
                'error_code'    => 'doc-com-002',
            ]);
        }

    }

    public function markBagDocumentAsCompleteIncomplete(Request $request)
    {
        $ID = $request->input('lk_ID');
        $bag_Document = bag_TransactionDocument::find($ID);
        if(!is_null($bag_Document))
        {
            if(is_null($bag_Document->DateApproved) || $bag_Document->isComplete == 0)
            {
                $query = bag_TransactionDocument::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => auth()->id(),
                        'DateApproved'          => date('Y-m-d H:i:s'),
                        'isComplete'            => 1,
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked complete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as complete.',
                    'error_code'    => 'bag-com-001'
                ]);
            }
            else
            {
                $query = bag_TransactionDocument::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => NULL,
                        'DateApproved'          => NULL,
                        'isComplete'            => 0,
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked as incomplete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as incomplete.',
                    'error_code'    => 'bag-com-003'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Document not found.',
                'error_code'    => 'bag-com-002',
            ]);
        }
    }

    public function signerAddRemove(Request $request)
    {
        $ID     = $request->input('ID');
        $role   = $request->input('role');
        $bagID  = $request->input('bagID');

        $bagDocument = bag_TransactionDocument::find($bagID);
        if(!is_null($bagDocument))
        {
            $signedBy = $bagDocument->SignedBy;

            if ($signedBy != '' && !is_null($signedBy))
            {
                if (strpos($signedBy, '_') !== FALSE) {
                    $signedBy = explode('_', $signedBy);
                } else {
                    $signedBy = [$signedBy];
                }
                $key = array_search($role . '+' . $ID, $signedBy);
                if ($key !== FALSE) {
                    unset($signedBy[$key]);
                } else {
                    $signedBy[] = $role . '+' . $ID;
                }

                $signedBy = implode('_',$signedBy);
            }
            else
            {
                $signedBy = $role.'+'.$ID;
            }

            $query = bag_TransactionDocument::where('ID', $bagID)->update(['SignedBy' => $signedBy]);
            if($query)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully edited signers!',
                ], 200);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message'=> 'There was an error processing the changes, please try again later.',
                    'error_code' => 'SIADRM-001',
                ], 200);
            }
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'File not found or does not exist.',
                'error_code' => 'SIADRM-002',
            ]);
        }
    }

    public function saveAdHocDocument(Request $request)
    {
        $transactionID = $request->input('Transactions_ID');
        $description   = $request->input('Description');
        $isIncluded    = $request->input('isIncluded');
        //AH stands for Ad-hoc. You didn't know that of course.
        $adHocDocument = Document::where('ShortName', 'AH')->limit(1)->get();
        if(!$adHocDocument->isEmpty())
        {
            $documentCode = $adHocDocument->first()->Code;
            $countAdHocs = lk_Transactions_Documents::where('Transactions_ID', $transactionID)
                ->where('Documents_Code', 'LIKE', DB::raw("CONCAT('$documentCode', '%')"))->get();
            $documentCode .= count($countAdHocs) + 1;
            if ($description != '' && $isIncluded != '' && $transactionID != '')
            {
                $lk_TransDoc = new lk_Transactions_Documents();
                $data = $request->all();
                $data['Documents_Code'] = $documentCode;
                $data['isIncluded'] = (int)$data['isIncluded'];
                $savedData = $lk_TransDoc->upsert($data);
                if($savedData) return response()->json([
                    'status'  => 'success',
                    'message' => 'Document Created.',
                ]);
                else return response()->json([
                    'status'  => 'fail',
                    'message' => 'An error occurred when saving the document. Error code:  add-ah-doc-003',
                ]);
            }
            else
            {
                return response()->json([
                    'status'  => 'fail',
                    'message' => 'Missing data. Error code: add-ah-doc-002',
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Could not find user-defined template. Error code: add-ah-doc-004',
            ]);
        }
    }
}
