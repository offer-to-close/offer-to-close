<?php

namespace App\Traits;

trait IsActiveScopeTrait
{
/**
 * Defines a local scope to only select 'active' records (records with the isActive field value equal to TRUE
 */
    protected static function isActiveScope($query)
    {
        return $query->where('isActive', true);
    }
}