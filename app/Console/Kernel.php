<?php

namespace App\Console;

use App\Console\Commands\DailySummary;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DailySummary::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('daily:summary')->dailyAt('4:00');
        /*
         * This executes daily summary command every minute only if you want to test the command to work
         * with cron. Use a 'testCron' key inside of your ENV file.
         */
        $schedule->command('daily:summary')->everyMinute()->when(function ()
        {
            $test = env('testCron') == true ? true : false;
            return $test;
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
