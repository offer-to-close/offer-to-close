<?php
namespace App\Combine;

use App\Library\otc\AddressVerification;
use App\Models\Escrow;
use App\Models\Loan;
use App\Models\Title;
use App\Models\TransactionCoordinator;
use App\models\ZipCityCountyState;
use Faker\Provider\Address;
use Illuminate\Support\Collection;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Combine\MilestoneCombine;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use App\Library\Utilities\DisplayTable;

class OfferCombine extends BaseCombine
{
    /*public static function decideIfCreateTransaction()
    {
        $input = Input::get('convertToTBtn');
        if (isset($input))
            return true;
        return false;
    }*/
    public static function addAgentGetId($nameFirst,$nameLast)
    {
        $nameFull = $nameFirst.' '.$nameLast;
        $agent_ID = DB::table('Agents')
            ->insertGetId([
                'NameFirst'     => $nameFirst,
                'NameLast'      => $nameLast,
                'NameFull'      => $nameFull
            ]);

        return $agent_ID;
    }
    /**
     * addAgentOrGetId checks to see if the agent exists, if it does it returns the ID
     * if it doesn't, it adds the agent and returns the ID.
     */
    public static function addAgentOrGetId($nameFirst,$nameLast)
    {
        $agent = Agent::where('NameFirst', $nameFirst)
            ->where('NameLast' , $nameLast)
            ->limit(1)
            ->get();

        if($agent->isEmpty())
        {
            $agent_ID = self::addAgentGetId($nameFirst,$nameLast);
        }
        else
        {
            $agent_ID = $agent->first()->ID;
        }
        return $agent_ID;

    }

    /*
     * addPropertyGetId adds the property and returns the ID. It doesn't check whether or not the property exists.
     */
    public static function addPropertyGetId($street1,$city,$state,$zip)
    {
        $addressArray       =[  'Street1'=>$street1,
                                'Street2'=>NULL,
                                'City'=>$city,
                                'State'=>$state,
                                'Zip'=>$zip ];
        $formattedAddress   = AddressVerification::formatAddress($addressArray);
        $addressVerify      = new AddressVerification();
        $addressList        = $addressVerify->isValid($formattedAddress);
        if ($addressList == false)
        {
            $addressArray['County'] = ZipCityCountyState::getCountyByZip($zip);
        }
        else
        {
            $addressArray['Street1'] = $addressList[0]['Street1'];
            $addressArray['Street2'] = $addressList[0]['Street2'];
            $addressArray['City']    = $addressList[0]['City'];
            $addressArray['State']   = $addressList[0]['State'];
            $addressArray['Zip']     = $addressList[0]['Zip'];
            $addressArray['County']  = $addressList[0]['County'] ?? null;
        }
        $property                = new Property();
        $property                = $property->upsert($addressArray);
        Log::debug(['&&', $property]);
        return $property->ID ?? $property->id;
    }

    /*
     * createTransaction adds a new transaction to the database from the information provided in the offer creation page.
     * Then it returns the transaction ID.
     *
     *
     */
    public static function createTransaction($priceListing,
                                             $agent_ID,
                                             $property_ID,
                                             $buyersTransactionCoordinatorsID,
                                             $offerID,
                                             $dateOffer,
                                             $acceptedDate,
                                             $escrowLength)
    {
        $transaction_ID = DB::table('Transactions')
            ->insertGetId([
                'PurchasePrice'                     => $priceListing,
                'Properties_ID'                     => $property_ID,
                'BuyersAgent_ID'                    => $agent_ID,
                'BuyersTransactionCoordinators_ID'  => $buyersTransactionCoordinatorsID,
                'Offers_ID'                         => $offerID,
                'DateOfferPrepared'                 => $dateOffer,
                'DateAcceptance'                         => $acceptedDate,
                'ClientRole'                        => 'ba',
                'EscrowLength'                      => $escrowLength
            ]);
        return $transaction_ID;
    }

}