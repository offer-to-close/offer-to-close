<?php

namespace App\Combine;

use App\Http\Controllers\DocumentAccessController;

use App\Models\DocumentAccessDefault;
use App\Models\DocumentTransferDefault;

use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DocumentAccessCombine extends BaseCombine
{
    public static function setDocumentAccessToTransaction($transactionID, $documentID, $state='CA')
    {
        if (!is_array($documentID)) $documentID = [$documentID];
        $access = DocumentAccessDefault::getByDocument($documentID, $state);


        if (!empty($access) && count($access) == 0) return false;
        foreach ($access as $row)
        {
            $lkDocAccess = new lk_TransactionsDocumentAccess();
            $r = $row->toArray();
            $docAccess = ['Documents_Code'    => $r['Documents_Code'],
                          'Transactions_ID' => $transactionID,
                          'PovRole'         => $r['PovRole'],
                          'AccessSum'       => $r['AccessSum'],
                          'Notes'           => $r['Notes']];
            $rv = $lkDocAccess->upsert($docAccess);
        }
        return lk_TransactionsDocumentAccess::getByTransactionID($transactionID);
    }
    public static function setDocumentTransferToTransaction($transactionID, $documentID, $state='CA')
    {
        if (!is_array($documentID)) $documentID = [$documentID];
        $transfer = DocumentTransferDefault::getByDocument($documentID, $state);

        if (!empty($transfer) && count($transfer) == 0) return false;

        foreach ($transfer as $row)
        {
            $lkDocTransfer = new lk_TransactionsDocumentTransfer();
            $r = $row->toArray();
            $docTransfer = ['Documents_Code'    => $r['Documents_Code'],
                          'Transactions_ID' => $transactionID,
                          'PovRole'         => $r['PovRole'],
                          'TransferSum'     => $r['TransferSum'],
                          'Notes'           => $r['Notes']];
            $rv = $lkDocTransfer->upsert($docTransfer);
        }
        return lk_TransactionsDocumentTransfer::getByTransactionID($transactionID);
    }

    public static function getDocumentUserAccessPermissions($transactionID, $documentCode, $role = null)
    {
        $isAdHoc = strpos($documentCode,'*') !== FALSE; // an asterisk means the document is ad hoc.
        $docAccessData = lk_TransactionsDocumentAccess::where('Transactions_ID', '=', $transactionID)
            ->when($isAdHoc,function ($query) use ($documentCode){
                $query->where('Documents_Code','=',DB::raw("SUBSTRING('$documentCode',1,1)"));
            })
            ->when(!$isAdHoc,function ($query) use ($documentCode){
                $query->where('Documents_Code','=',$documentCode);
            })
            ->get()->toArray();

        if (!is_null($role))
        {
            $userRoles = array($role);
        }
        else
        {
            $userRoles = TransactionCombine::getUserTransactionRoles($transactionID);
        }
        $accessSum = 0;
        $temporaryAccessPermissions = [];
        foreach ($userRoles as $role)
        {
            $key = array_search($role, array_column($docAccessData,'PovRole'));
            if($key !== FALSE)
            {
                $perms = DocumentAccessController::decodeAccessSum($docAccessData[$key]['AccessSum']);
                $temporaryAccessPermissions = array_merge($temporaryAccessPermissions, $perms);
            }
        }
        foreach ($temporaryAccessPermissions as $perm)
        {
            $accessSum += (int)$perm['valueInt'];
        }
        if($accessSum)
        {
            $permissions = DocumentAccessController::decodeAccessSum($accessSum);
            return $permissions;
        }
        else return FALSE;
    }
}