<?php

namespace App\Combine;

use App\Library\Utilities\_LaravelTools;
use App\Models\ContactRequest;
use App\Models\lk_UsersTypes;
use App\Models\MemberRequest;
use App\Models\Model_Parent;
use App\Models\ShareRoom;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class AccountCombine extends BaseCombine
{
    public static $userRoles = null;
    public static $roleCategory = ['homesumer' => ['b', 's', 'h',], 'prosumer' => ['ba', 'sa', 'btc', 'stc', 'a', 'tc',],];
    public static $username = null;

    /**
     * @param $role
     *
     * @return bool
     */
    public static function isHomesumer($role)
    {
        if (!is_array($role)) $role = [$role];
        foreach ($role as $val)
        {
            if (in_array($role, self::$roleCategory['homesumer'])) return true;
        }
        return false;
    }

    /**
     * @param $role
     *
     * @return bool
     */
    public static function isProsumer($role)
    {
        if (!is_array($role)) $role = [$role];
        foreach ($role as $val)
        {
            if (in_array($role, self::$roleCategory['prosumer'])) return true;
        }
        return false;
    }

    /**
     * Used to learn whether a user has a specific role. The list of roles the user
     * has is passed in the $haystack argument and the role of interest is passed as the
     * $needle argument.
     *
     * @param $needle   mixed
     * @param $haystack mixed
     *
     * @return bool Returns true if the needle is found in the haystack and false if it is not
     */
    public static function isThisRole($needle, $haystack)
    {
        if (!is_array($haystack)) $haystack = [$haystack];
        return in_array($needle, $haystack);
    }
    public static function isTC($role)
    {
        return (substr($role, -2) == 'tc');
    }
    public static function isAgent($role)
    {
        return (substr($role, -1) == 'a');
    }
    public static function getRolesByUserId($userID)
    {
        $roles = [];
        foreach (Config::get('constants.RoleModel') as $role=>$model)
        {
            if($model == NULL) continue;
            if (self::isThisUserInThisModel($userID, $model)) $roles[] = $role;
        }
        return $roles;
    }

    public static function getByName($table, ...$args)
    {
        if (empty($table)) return false;

        $query = DB::table($table)
                   ->select('*');

        if (count($args) == 1)
        {
            if (empty($args[0])) return false;
            $query = $query->where(function ($q) use ($query, $args)
            {
                $q->where('NameFirst', 'like', $args[0] . '%')
                    ->orWhere('NameLast', 'like', $args[0])
                    ->orWhere('NameFull', 'like', str_replace(' ', '%', $args[0]));
            });
            $query = Model_Parent::scopeNoTest($query);
        }

        if (count($args) == 2)
        {
            if (empty($args[0])) return false;
            $query = $query->where(function ($q) use ($query, $args)
            {
                $q->where('NameFull', 'like', '%' . implode('%', $args) . '%')
                  ->orWhere('NameFirst', 'like', $args[0] . '%')
                  ->orWhere('NameLast', 'like', $args[1]);
            });
            $query = Model_Parent::scopeNoTest($query);
        }
        return $query->get();
    }
    public static function isThisUserInThisModel($id=0, $model=null)
    {
        if (!$id) trigger_error(__CLASS__. '::' .__FUNCTION__. ' - ID must not be zero', E_USER_WARNING);
        if (empty($model)) trigger_error(__CLASS__. '::' .__FUNCTION__. ' - Model must not be blank', E_USER_WARNING);
        $obj = _LaravelTools::modelFactory($model);
        if($model === 'User' && $id !== 0) return TRUE;
        else
        {
            if (!is_null($obj->fieldName) && in_array('Users_ID', $obj->fieldName))
            {
                $result = $obj::where('Users_ID', $id)->first();
                if($result != NULL && $result->count() > 0) return TRUE;
            }
        }
        return FALSE;
    }
    /**
     * Used to learn whether a user is a specific type. The list of types the user
     * has is passed in the $haystack argument and the type of interest is passed as the
     * $needle argument.
     *
     * @param $needle   mixed
     * @param $haystack mixed
     *
     * @return bool Returns true if the needle is found in the haystack and false if it is not
     */
    public static function isThisType($target)
    {
        return ($target & session('userType'));
    }

    /**
     * Used to learn what type the user has with the highest access.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return string type code
     */
    public static function getTypeMax(int $userID)
    {
        $query = DB::table('lk_UsersTypes')->select('Type', 'ValueInt')
            ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value' )
                        ->where('lk_UsersTypes.isActive', '=', true)
                        ->where('lk_UsersTypes.isTest', '=', false)
                        ->where('lk_UsersTypes.Users_ID', '=', $userID)
                        ->orderBy('ValueInt', 'desc');
        $query = Model_Parent::scopeNoTest($query, ['lk_UsersTypes', 'lu_UserTypes']);
        $type = $query->get()->first();
        try
        {
            if (count($type) != 1) return null;
        }
        catch (\Exception $e)
        {
            return null;
        }
        return $type->Type;
    }
    /**
     * Used to learn what roles a user has.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getRoles(int $userID): Collection
    {
        $query = User::select('id', 'name', 'email')->where('id', $userID)->limit(1);
        $userData = $query->get()->first();

        $query1 = DB::table('lk_Users-Roles')
                    ->leftJoin('lu_UserRoles', 'lk_Users-Roles.Role', '=', 'lu_UserRoles.Value')
                    ->selectRaw('"' . $userData->id . '" as id, "' .$userData->name. '" as Name, "'.$userData->email.'" as Username, Role, Display')
                    ->where('isActive', '=', true)
                    ->where('Users_ID', '=', $userID);
        $query1 = Model_Parent::scopeNoTest($query1, ['lk_Users-Roles', 'lu_UserRoles']);
        return $query1->get();
    }
    /**
     * Used to learn what type a user is.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return
     */
    public static function getType( $userID)
    {
        if (empty($userID)) return null;
        try
        {
            $query1 = DB::table('lk_UsersTypes')
                        ->select('Type', 'Display', 'ValueInt')
                        ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value')
                        ->where('Users_ID', '=', $userID)
                        ->orderBy('ValueInt', 'desc');
            $query1 = Model_Parent::scopeNoTest($query1, ['lk_UsersTypes', 'lu_UserTypes']);

            $result = $query1->get();
            if (empty($result) || count($result) == 0) return null;
            return $result->first()->Type;
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION'=>$e->getMessage(), 'Location'=>$e->getFile() . ':' . $e->getLine(),
                        'sql'=>$query1->toSql(), 'result'=>$result, 'user id'=>$userID, __METHOD__=>__LINE__]);
            return false;
        }
    }
    public static function getTypeValue(int $userID)
    {
        try
        {
            $query1 = DB::table('lk_UsersTypes')
                        ->select('Type', 'Display', 'ValueInt')
                        ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value')
                        ->where('Users_ID', '=', $userID)
                        ->orderBy('ValueInt', 'desc');
            $query1 = Model_Parent::scopeNoTest($query1, ['lk_UsersTypes', 'lu_UserTypes']);

            $result = $query1->get();
            return $result->first()->ValueInt;
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION'=>$e,
                'sql'=>$query1->toSql(), 'result'=>$result, 'user id'=>$userID, __METHOD__=>__LINE__]);

            return false;
        }
    }

    /**
     * Returns the username (email) for the passed userID argument
     *
     * @param int $userID The user's id value from the users table
     *
     * @return string
     */
    public static function getUsername(int $userID): string
    {
        if (!self::$userRoles)
        {
            $query1 = DB::table('users')
                        ->selectRaw('email as Username');
            $query1 = Model_Parent::scopeNoTest($query1);

            $result         = $query1->get();
            $rows           = AccountCombine::collectionToArray($result);
            self::$username = $rows[0]['Username'];
        }
        return self::$username;
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return string
     */
    public static function getFirstName(int $userID): string
    {
        $rv = Auth::user()->NameFirst ?? null;
        if (empty($rv))
        {
            $name = Auth::user()->name;
            $name = explode(' ', $name);
            $rv = reset($name);
        }
        return $rv;
//        $rv = self::getName($userID);
//        return $rv['NameFirst'];
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return array
     */
    public static function getName(int $userID): array
    {
        if (empty($userID)) return ['NameFirst' => 'None', 'NameLast' => null];
        $roles = self::getRoles($userID);
        $role  = $roles->first();
        if (!is_object($role))
        {
            $name = Auth::user()->name;
            @list($fname, $lname) = explode(' ', $name, 2);
            return ['NameFirst' => $fname, 'NameLast' => $lname];
        }

        $rv = collect();
        switch ($role->Role)
        {
            case 'b':
                $rv = self::getBuyers($userID);
                break;
            case 's':
                $rv = self::getSellers($userID);
                break;
            case 'a':
            case 'ba':
            case 'sa':
                $rv = self::getAgents($userID);
                break;
            case 'tc':
            case 'btc':
            case 'stc':
                $rv = self::getTransactionCoordinators($userID);
                break;
            case 'e':
//                $rv = self::getEscrow($userID);
                break;
            case 'l':
//                $rv = self::getLoan($userID);
                break;
            case 't':
//                $rv = self::getTitle ($userID);
                break;
            default:
                break;
        }

        return ['NameFirst' => $rv->first()->NameFirst, 'NameLast' => $rv->first()->NameLast];
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getAgents(int $userID): Collection
    {
        $query1 = DB::table('users')
                    ->leftJoin('Agents', 'users.ID', '=', 'Agents.users_ID')
                    ->selectRaw('Agents.*')
                    ->where('users.ID', '=', $userID);

        return $query1->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTransactionCoordinators(int $userID): Collection
    {
        $query1 = DB::table('users')
                    ->leftJoin('TransactionCoordinators', 'users.ID', '=', 'TransactionCoordinators.users_ID')
                    ->selectRaw('TransactionCoordinators.*')
                    ->where('users.ID', '=', $userID);
        $query1 = Model_Parent::scopeNoTest($query1, ['users', 'TransactionCoordinators']);

        return $query1->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getBuyers(...$args): Collection
    {
        if (is_integer($args[0]))
        {
            $userID = (int) $args[0];
            $query  = DB::table('users')
                        ->leftJoin('Buyers', 'users.ID', '=', 'Buyers.Users_ID')
                        ->selectRaw('Buyers.*')
                        ->where('users.ID', '=', $userID);
            $query = Model_Parent::scopeNoTest($query, ['users', 'Buyers']);

            return $query->get();
        }
        else
        {
            return self::getByName('Buyers', $args);
        }
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getSellers(int $userID): Collection
    {
        $query = DB::table('users')
                    ->leftJoin('Sellers', 'users.ID', '=', 'Sellers.Users_ID')
                    ->selectRaw('Sellers.*')
                    ->where('users.ID', '=', $userID);
        $query = Model_Parent::scopeNoTest($query, ['users', 'Sellers']);

        return $query->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getBrokers(int $userID): Collection
    {
        $query = DB::table('users')
                    ->leftJoin('Brokers', 'users.ID', '=', 'Brokers.Users_ID')
                    ->selectRaw('Brokers.*')
                    ->where('users.ID', '=', $userID);
        $query = Model_Parent::scopeNoTest($query, ['users', 'Brokers']);

        return $query->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTransactions($user, $status=null): Collection
    {
        if (!Auth::check()) redirect(route('otc.login'));
        if (is_object($user))
        {
            $userID = $user->id;
        }
        else $userID = (int) $user;

        $qryBuyer = DB::table('Transactions')
                      ->leftJoin('Buyers', 'Transactions.ID', '=', 'Buyers.Transactions_ID')
                      ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.BUYER') . '\'' . ' as Role')
                      ->where('Buyers.Users_ID', '=', $userID);
        if (!empty($status))  $qryBuyer->where('Transactions.Status', '=', $status);
        $qryBuyer = Model_Parent::scopeNoTest($qryBuyer, ['Transactions', 'Buyers']);

        $qrySeller = DB::table('Transactions')
                       ->leftJoin('Sellers', 'Transactions.ID', '=', 'Sellers.Transactions_ID')
                       ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.SELLER') . '\'' . ' as Role')
                       ->where('Sellers.Users_ID', '=', $userID);
        if (!empty($status))  $qrySeller->where('Transactions.Status', '=', $status);
        $qrySeller = Model_Parent::scopeNoTest($qrySeller, ['Transactions', 'Sellers']);

        $qryBAgent = DB::table('Transactions')
                       ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                       ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.BUYERS_AGENT') . '\'' . ' as Role')
                       ->where('Agents.Users_ID', '=', $userID);
        if (!empty($status))  $qryBAgent->where('Transactions.Status', '=', $status);
        $qryBAgent = Model_Parent::scopeNoTest($qryBAgent, ['Transactions', 'Agents']);


        $qrySAgent = DB::table('Transactions')
                       ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                       ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.SELLERS_AGENT') . '\'' . ' as Role')
                       ->where('Agents.Users_ID', '=', $userID);
        if (!empty($status))  $qrySAgent->where('Transactions.Status', '=', $status);
        $qrySAgent = Model_Parent::scopeNoTest($qrySAgent, ['Transactions', 'Agents']);

        $qryBTC = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators',
                        'Transactions.BuyersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR') . '\'' . ' as Role')
                    ->where('Users_ID', '=', $userID);
        if (!empty($status))  $qryBTC->where('Transactions.Status', '=', $status);
        $qryBTC = Model_Parent::scopeNoTest($qryBTC, ['Transactions', 'TransactionCoordinators']);

        $qrySTC = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators',
                        'Transactions.SellersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR') .
                                '\'' . ' as Role')
                    ->where('TransactionCoordinators.Users_ID', '=', $userID);
        if (!empty($status))  $qrySTC->where('Transactions.Status', '=', $status);
        $qrySTC = Model_Parent::scopeNoTest($qrySTC, ['Transactions', 'TransactionCoordinators']);

        $qryE = DB::table('Transactions')
                    ->leftJoin('Escrows',
                        'Transactions.Escrows_ID', '=', 'Escrows.ID')
                    ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.ESCROW') .
                                '\'' . ' as Role')
                    ->where('Escrows.Users_ID', '=', $userID);
        if (!empty($status))  $qryE->where('Transactions.Status', '=', $status);
        $qryE = Model_Parent::scopeNoTest($qryE, ['Transactions', 'Escrows']);

        $qryL = DB::table('Transactions')
                    ->leftJoin('Loans',
                        'Transactions.Loans_ID', '=', 'Loans.ID')
                    ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.LOAN') .
                                '\'' . ' as Role')
                    ->where('Loans.Users_ID', '=', $userID);
        if (!empty($status))  $qryL->where('Transactions.Status', '=', $status);
        $qryL = Model_Parent::scopeNoTest($qryL, ['Transactions', 'Loans']);

        $qryT = DB::table('Transactions')
                    ->leftJoin('Titles',
                        'Transactions.Titles_ID', '=', 'Titles.ID')
                    ->selectRaw('Transactions.*, \'' . Config::get('constants.USER_ROLE.TITLE') .
                                '\'' . ' as Role')
                    ->where('Titles.Users_ID', '=', $userID);
        if (!empty($status))  $qryT->where('Transactions.Status', '=', $status);
        $qryT = Model_Parent::scopeNoTest($qryT, ['Transactions', 'Titles']);

        $qry = $qryBuyer->union($qrySeller)
                        ->union($qryBAgent)
                        ->union($qrySAgent)
                        ->union($qryBTC)
                        ->union($qrySTC)
                        ->union($qryE)
                        ->union($qryL)
                        ->union($qryT);
//                        ->distinct();
//        dd(['usrID'=>$userID, 'status'=>$status, $qry->toSql()]);
        return $qry->get();
    }

    public static function getRoleID($role = 'tc', $userID = 0)
    {
        $roles = Config::get('constants.USER_ROLE');
        if ($role == $roles['OWNER'])
        {
            $qry = DB::table('users')->select('id as ID')->where('id', '=', $userID);
            $qry = Model_Parent::scopeNoTest($qry);
            return $qry->get();
        }

        $table = [$roles['BUYER']                           => 'Buyers',
                  $roles['SELLER']                          => 'Sellers',
                  $roles['BUYERS_AGENT']                    => 'Agents',
                  $roles['SELLERS_AGENT']                   => 'Agents',
                  $roles['AGENT']                           => 'Agents',
                  $roles['BUYERS_TRANSACTION_COORDINATOR']  => 'TransactionCoordinators',
                  $roles['SELLERS_TRANSACTION_COORDINATOR'] => 'TransactionCoordinators',
                  $roles['TRANSACTION_COORDINATOR']         => 'TransactionCoordinators',
                  $roles['ESCROW']                          => 'Escrows',
                  $roles['TITLE']                           => 'Titles',
                  $roles['LOAN']                            => 'Loans',
                  $roles['BROKER']                          => 'Brokers',
                  $roles['GUEST']                           => 'Guests',
                  $roles['OWNER']                           => 'users',
        ];

        if (!$userID) $userID = CredentialController::current()->ID();

        if (!isset($table[$role]) || !$userID) return false;
        $qry = DB::table($table[$role])
                 ->select('ID')
                 ->where('Users_ID', '=', $userID);
        $qry = Model_Parent::scopeNoTest($qry);
        return $qry->get();
    }
    public static function memberRequestCount($justNew=true)
    {
        return count(self::getMemberRequests($justNew));
    }
    public static function contactRequestCount($justOpen=true)
    {
        return count(self::getContactRequests($justOpen));
    }
    public static function getMemberRequests($justNew=true)
    {
        $query =  MemberRequest::orderby('ID');

        if ($justNew) $query->where('isApproved', 0)->where('isRejected', 0);
        else
        {
            $query->where( function($q) use ($query) {
                $q->where('isApproved', 1)
                  ->orwhere('isRejected', 1);
          });
        }
        return $query->get();
    }
    public static function getContactRequests($justOpen=true)
    {
        $query =  ContactRequest::where('isTest', 0);

        if ($justOpen) $query->where('isClosed', 0);
        return $query->get();
    }
}