<?php

namespace App\Combine;

use App\Http\Controllers\CredentialController;
use App\Http\Controllers\TransactionController;
use App\Library\otc\_Locations;
use App\Library\otc\Credentials;
use App\Library\otc\RequiredSignatures;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\MigrationHelpers;
use App\Models\bag_TransactionDocument;
use App\Models\lk_Transactions_Documents;
use App\Models\lk_Transactions_Timeline;
use App\Models\MilestoneReference;
use App\Models\Timeline;
use App\Models\Transaction;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use App\Library\Utilities\_Time;
use App\Library\otc\IncludeWhen;
use App\Models\Document;
use Illuminate\Support\Facades\Request;

class DocumentCombine extends BaseCombine
{
    /**
     * @param $transactionsID
     *
     * @return mixed
     */

    public static $modelPath = 'App\\Models\\';

    public static function byState($stateCode)
    {
        $query = DB::table('Documents')
                   ->select('*')
                   ->where('State', '=', $stateCode);
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function byTransaction($transactionID, $side=null)
    {
        $query = DB::table('Documents')
            ->distinct()
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.Documents_Code', 'LIKE', DB::raw("CONCAT(Documents.Code,'%')"))
            ->leftJoin('lu_DocumentTypes', 'Documents.Category', '=', 'lu_DocumentTypes.Value')
            ->leftJoin('TransactionCoordinators', 'lk_Transactions-Documents.ApprovedByUsers_ID', '=', 'TransactionCoordinators.ID')
                   ->selectRaw('Documents.*, 
                                `lk_Transactions-Documents`.Description,
                                `lk_Transactions-Documents`.Documents_Code as Code,
                                lu_DocumentTypes.Display as DocumentCategory, 
                                `lk_Transactions-Documents`.ID as lk_ID, 
                                Transactions_ID, isIncluded, 
                                DateDue, 
                                DateCompleted, 
                                `lk_Transactions-Documents`.Status as lk_Status, 
                                concat(TransactionCoordinators.NameFirst, " ", TransactionCoordinators.NameLast) as ApprovedBy 
--                                ,bag_TransactionDocuments.DocumentPath, 
--                                bag_TransactionDocuments.OriginalDocumentName
                                ')
                   ->where('Transactions_ID', '=', $transactionID)
                   ->orderBy('isIncluded', 'desc');

        if ($side == 'b')  $query = $query->where('forBuyer', true);
        if ($side == 's')  $query = $query->where('forSeller', true);
        return $query->get();
    }

    public static function getDocumentList($transactionID, $sortBy = 'DocumentCategory'): array
    {
        $documentList = [];

        $side = Transaction::find($transactionID)->Side;

        session(['transactionSide'=>$side]);
        $documents = self::byTransaction($transactionID);

        if ($documents->isEmpty())
        {
            self::fillTransactionDocumentTable($transactionID, $side);
            $documents = self::createDocumentList($transactionID, true, $side);
            $documents = self::byTransaction($transactionID, $side);
        }

        $documents = is_array($documents) ? collect($documents) : $documents; // from lk_Transcations-Documents
        foreach (DocumentCombine::filesByTransaction($transactionID) as $upload)  //
        {
            $docSigners = explode('_', $upload->SignedBy);

            try
            {
                foreach ($docSigners as $idx => $v)
                {
                    if (empty($v)) continue;
                    list($role, $id) = explode('+', $v);
                    $docSigners[$idx] = ['role' => $role, 'id' => $id];
                }
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION', 'docSigners'=>$docSigners, __METHOD__=>__LINE__]);

            }

            $signers = "";

            foreach ($docSigners as $signer)
            {
                if (!isset($signer['role'])) continue;
                switch ($signer['role'])
                {
                    case 'b':
                        $modelName = self::$modelPath . 'Buyer';
                        $name      = $modelName::where('id', $signer['id'])->pluck('NameFull')->first();
                        $signers   = $signers . $name . "<br/>";
                        break;

                    case 's':
                        $modelName = self::$modelPath . 'Seller';
                        $name      = $modelName::where('id', $signer['id'])->pluck('NameFull')->first();
                        $signers   = $signers . $name . "<br/>";
//                        ddd(['model name'=>$modelName, 'name'=>$name, 'signers'=>$signers, 'docSigners'=>$docSigners, __METHOD__=>__LINE__]);
                        break;

                    case 'ba':
                        $agentModal = self::$modelPath . 'Transaction';
                        $agentID = $agentModal::where('id', $transactionID)
                            ->pluck('BuyersAgent_ID')
                            ->first();
                        $modelName = self::$modelPath . 'Agent';
                        $name      = $modelName::where('id', $signer['id'])->pluck('NameFull')->first();
                        $signers   = $signers . $name . "<br/>";
                        break;

                    case 'btc' || 'stc':
                        $modelName = self::$modelPath . 'TransactionCoordinator';
                        $name      = $modelName::where('id', $signer['id'])->pluck('NameFull')->first();
                        $signers   = $signers . $name . "<br/>";
                        break;

                    case 'sa':
                        $agentModal = self::$modelPath . 'Transaction';
                        $agentID   = $agentModal::where('id', $transactionID)
                                    ->pluck('SellersAgent_ID')
                                    ->first();
                        $modelName = self::$modelPath . 'Agent';
                        $name = $modelName::where('id', $agentID)->pluck('NameFull')->first();
                        $signers = $signers . $name . "<br/>";
                        break;
                }
            }
            $fileDetails = ['url' => $upload->DocumentPath, 'signedBy' => $signers];
            $files[$upload->ID][] = $fileDetails;
        }

        $canSort = TRUE;
        foreach ($documents as $doc)
        {
            if (is_array($doc))
            {
                $doc1 = $doc;
                $doc  = collect($doc);
            }
            else
            {
                $doc1 = _Convert::toArray($doc);
            }

            if(!isset($doc->{$sortBy})) $canSort = FALSE;

            $documentList[] = array_merge($doc1,
                ['isIncluded'     => isset($doc->isIncluded) ? $doc->isIncluded : true,
                 'DueDate'               => isset($doc->DateDue) ? $doc->DateDue : null,
                 'DueDateFormated'       => _Time::formatDate(isset($doc->DateDue) ? $doc->DateDue : null, 4),
                 'CompletedDate'         => isset($doc->DateCompleted) ? $doc->DateCompleted : null,
                 'CompletedDateFormated' => is_null(isset($doc->DateCompleted) ? $doc->DateCompleted : null) ?
                                            '-' : _Time::formatDate($doc->DateCompleted, 4),
                 'SignaturesNeeded'      => isset($doc->RequiredSignatures) ?
                                            RequiredSignatures::decodeNumber($doc->RequiredSignatures) : null,
                 'UploadedDocuments'     => self::filesByTransactionAndDocument($transactionID,$doc->Code),
                ]);
        }
        if ($sortBy && $canSort)
        {
            _Arrays::sortByColumn($documentList, $sortBy);
        }
        return $documentList;
    }

    /**
     * @param $transactionID
     *
     * @return mixed
     */
    public static function filesByTransaction($transactionID)
    {
        $query = DB::table('lk_Transactions-Documents')
            ->join('bag_TransactionDocuments', 'lk_Transactions-Documents.ID', '=', 'bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->select('bag_TransactionDocuments.*')
            ->where('lk_Transactions-Documents.Transactions_ID', '=', $transactionID);
        return $query->get();
    }

    public static function filesByTransactionAndDocument($transactionID,$documentCode)
    {
        $query = DB::table('lk_Transactions-Documents')
            ->join('bag_TransactionDocuments', 'lk_Transactions-Documents.ID', '=', 'bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->select('bag_TransactionDocuments.*')
            ->where('lk_Transactions-Documents.Transactions_ID', '=', $transactionID)
            ->where('lk_Transactions-Documents.Documents_Code', '=', $documentCode);
        return $query->get();
    }

    /**
     * Creates the list of documents needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function instantiateTimeline($dateAcceptance, $escrowLength = 45)
    {
        $timeline = Timeline::where('isTest', false)->get();

        foreach ($timeline as $milestone)
        {
            $osTyp   = ($milestone->OffsetDaysTypical < 0) ? $milestone->OffsetDaysTypical + $escrowLength :
                $milestone->OffsetDaysTypical;
            $typical = _Time::addDays($dateAcceptance, $osTyp);
            $osMax   = ($milestone->OffsetDaysMax < 0) ? $milestone->OffsetDaysMax + $escrowLength :
                $milestone->OffsetDaysTypical;
            $max     = _Time::addDays($dateAcceptance, $osMax);

            $milestones[$milestone->ID] = array_merge(['dueDate' => min($typical, $max)],
                $milestone->toArray());
        }
        return $milestones;
    }

    /**
     * Creates the list of documents needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function createDocumentList($transactionID, $storeInDB = true, $side=null)
    {
        $documentList = [];

        $transactionController = new TransactionController();
        $transactionRecord     = $transactionController->getTransaction($transactionID);

        $query = DB::table('lk_Transactions-Timeline')
                ->select('Timeline.ID as mrID', 'lk_Transactions-Timeline.*')
                ->leftJoin('Timeline', 'MilestoneName', '=', 'Name')
                ->where('Transactions_ID', $transactionID)
                ->where('lk_Transactions-Timeline.isActive', true);

        $ms = $query->get();

        foreach ($ms as $row)
        {
            $milestones[$row->mrID] = $row;
        }

        $includeWhen = new IncludeWhen($transactionRecord);

        $documents = DocumentCombine::getDocumentsByFilter($transactionRecord['transaction']->ID, $side);

        $files = DocumentCombine::filesByTransaction($transactionID);

        foreach ($documents as $doc)
        {
            if (empty(trim($doc->TrueIncludeWhen))) $include = true;
            elseif (trim($doc->TrueIncludeWhen) == '{optional}') $include = false;
            elseif (!$includeWhen->evaluate(($doc->TrueIncludeWhen))) continue;
            else $include = true;

            if (!isset($milestones[$doc->Timeline_ID])) continue;

            $ayDoc = _Convert::toArray($doc);
            $documentList[$doc->Code] = array_merge($ayDoc,
                ['isIncluded'        => $include,
                 'DueDate'           => $milestones[$doc->Timeline_ID]->MilestoneDate,
                 'DueDateFormated'   => _Time::formatDate($milestones[$doc->Timeline_ID]->MilestoneDate, 2),
                 'SignaturesNeeded'  => RequiredSignatures::decodeNumber($doc->RequiredSignatures),
                 'UploadedDocuments' => $files[$doc->ID] ?? [],
                ]);
        }
        _Arrays::sortByTwoColumns($documentList,  'isIncluded', SORT_DESC, 'DueDate' );
        return $documentList;
    }

    /**
     * Creates the list of documents needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function fillTransactionDocumentTable($transactionID, $side=null)
    {
        @lk_Transactions_Documents::eraseByTransactionId($transactionID);

        $documentList = self::createDocumentList($transactionID, true, $side);

        foreach ($documentList as $doc)
        {
            lk_Transactions_Documents::insert(['Transactions_ID'    => $transactionID,
                                               'Documents_Code'     => $doc['Code'],
                                               'isIncluded'         => $doc['isIncluded'],
                                               'DateDue'            => _Time::formatDate($doc['DueDate'], 0),
                                               'DateCreated'        => date('Y-m-d h:i:s'),
                                               'Description'        => $doc['Description']
            ]);
        }
    }

    /**
     * Creates the list of documents needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getDocuments($transactionID, $sortBy = 'DateDue')
    {
        $qry = DB::table('lk_Transactions-Documents')
                 ->leftJoin('Transactions', '`lk_Transactions-Documents`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Documents', '`lk_Transactions-Documents`.Documents_Code', '=', 'Documents.Code')
                 ->select('Documents.ID', 'Documents.Code', 'DateDue', 'isIncluded', 'ShortName', 'Description', 'DateCompleted', 'IncludeWhen', 'RequiredSignatures', 'Provider')
                 ->whereRaw('`lk_Transactions-Documents`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);

        $qry = str_replace('```', '`', $qry->toSql());

        return DB::select($qry, [$transactionID]);
    }

    public static function getDocumentSets()
    {
        $qry = DB::table('DocumentFilters')
                 ->leftJoin('Documents', 'DocumentFilters.Documents_Code', '=', 'Documents.Code')
                 ->leftJoin('lu_DocumentSets', 'DocumentFilters.DocumentSets_Value', '=', 'lu_DocumentSets.Value')
                 ->leftJoin('Timeline', 'Documents.Timeline_ID', '=', 'Timeline.ID')
                 ->leftJoin('lu_DocumentTypes', 'Documents.Category', '=', 'lu_DocumentTypes.Value')
                 ->selectRaw('Documents.ID, Documents.Code,
        lu_DocumentSets.Display as `Document Set`, 
        lu_DocumentTypes.Display as Category, 
        Documents.ShortName, 
        Documents.Description, 
        Documents.Provider, 
        Documents.IncludeWhen, 
        concat(Timeline.Name, " ( ", Documents.DateOffset, " )") as `Milestone Reference`')
                 ->orderBy('DocumentFilters.DocumentSets_Value')->orderBy('ID');

        $qry = str_replace('```', '`', $qry->toSql());

        return DB::select($qry);
    }

    public static function getDocumentWithSets()
    {
        $qry = DB::table('DocumentFilters')
                 ->leftJoin('Documents', 'DocumentFilters.Documents_Code', '=', 'Documents.Code')
                 ->leftJoin('lu_DocumentSets', 'DocumentFilters.DocumentSets_Value', '=', 'lu_DocumentSets.Value')
                 ->leftJoin('Timeline', 'Documents.Timeline_ID', '=', 'Timeline.ID')
                 ->leftJoin('lu_DocumentTypes', 'Documents.Category', '=', 'lu_DocumentTypes.Value')
                 ->selectRaw('Documents.ID, Documents.Code,
        lu_DocumentTypes.Display as Category, 
        Documents.ShortName as `Document Code`, 
        Documents.Description as `Document Title`, 
        Documents.Provider, 
 --       Documents.IncludeWhen, 
        concat(Timeline.Name, " ( ", Documents.DateOffset, " )") as `Milestone Reference (day offset)`

      , if(DocumentFilters.DocumentSets_Value = "sfr", concat("IF: ",DocumentFilters.IncludeWhen), "Always") as `For Single Family, Include When`
      , if(DocumentFilters.DocumentSets_Value = "mfr", concat("IF: ",DocumentFilters.IncludeWhen), "Always") as `For Manufactured, Include When`
      , if(DocumentFilters.DocumentSets_Value = "probate", concat("IF: ", DocumentFilters.IncludeWhen), "Always") as `For Probate, Include When`
      , if(DocumentFilters.DocumentSets_Value = "vl", concat("IF: ", DocumentFilters.IncludeWhen), "Always") as `For Vacant, Include When`
  
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'b\'), true, false) as `Buyer Signs`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'s\'), true, false) as `Seller Signs`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'ba\'), true, false) as `Buyer\'s Agent Signs`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'sa\'), true, false) as `Seller\'s Agent Signs`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'e\'), true, false) as `Escrow Signs`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'bbr\'), true, false) as `Buyer\'s Broker`
      , if(RequiredSignatures & (select ValueInt from `offer-to-close`.lu_userroles where Value=\'sbr\'), true, false) as `Seller\'s Broker`
        ')->orderBy('ID');

        $qry = str_replace('```', '`', $qry->toSql());

        return DB::select($qry);
    }

    public static function getDocumentsByFilter($transactionID, $side=null)
    {
        $transaction = Transaction::where('ID', $transactionID)->get()->first();
        $property    = TransactionCombine::getProperty($transactionID);

        $set = false;
        if ($property->PropertyType = 'sfr')         $set = 'sfr';
        else if ($property->PropertyType = 'mfr')    $set = 'mfr';
        else if ($property->PropertyType = 'vacant') $set = 'vl';
        else if ($transaction->SaleType = 'probate') $set = 'probate';

        if ($set)
        {
             $select = '
                         Documents.*, DocumentSets_Value, MilestoneName, MilestoneDate, 
                         DocumentFilters.IncludeWhen as TrueIncludeWhen, Documents.ID as docID,
                         Documents.Code as docCode
                      ';
            $select = str_replace("\r\n", "\n", $select);

            $where = 'DocumentSets_Value = ? and  `lk_Transactions-Timeline`.Transactions_ID = ?';

            $bindings = [$set, $transactionID];

            $query = DB::table('DocumentFilters')
                       ->leftJoin('Documents', 'DocumentFilters.Documents_Code', '=', 'Documents.Code')
                       ->leftJoin('Timeline', 'Documents.Timeline_ID', '=', 'Timeline.ID')
                       ->leftJoin('lk_Transactions-Timeline', 'Timeline.Name', '=', 'lk_Transactions-Timeline.MilestoneName')
                       ->selectRaw($select)
                       ->whereRaw($where, $bindings)
                       ->orderBy('MilestoneDate');

            if ($side == 'b')  $query = $query->where('Documents.forBuyer', true);
            if ($side == 's')  $query = $query->where('Documents.forSeller', true);

            return $query->get();
        }
        else return collect([]);
    }

    /**
     * Creates the list of documents needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getDocumentsAsLookup($transactionID, $sortBy = 'DateDue', $includeAll = true)
    {
        $qry = DB::table('lk_Transactions-Documents')
                 ->leftJoin('Transactions', '`lk_Transactions-Documents`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Documents', '`lk_Transactions-Documents`.Documents_Code', '=', 'Documents.Code')
                 ->select('Documents.ID as ID', 'Documents.Code as Value', DB::raw('CONCAT(ShortName," - ",Description) as Display'))
                 ->whereRaw('`lk_Transactions-Documents`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);
        if (!$includeAll) $qry = $qry->whereRaw('isIncluded', true);

        $qry = str_replace('```', '`', $qry->toSql());
        $rv  = DB::select($qry, [$transactionID]);
        foreach ($rv as $idx => $rec)
        {
            if (is_a($rec, 'StdClass')) $rec = DocumentCombine::stdClassToArray($rec);
            if (is_a($rec, 'Collection')) $rec = DocumentCombine::collectionToArray($rec);

            $rv[$idx] = $rec;
        }
        return $rv;
    }

    /**
     * Check if enough data has been stored to create document list.
     *
     * @param integer $transactionID
     *
     * @return mixed
     */
    public static function checkForDocListData($transactionID)
    {
        $detailFields  = ['PurchasePrice', 'DateOfferPrepared',
                          'DateAcceptance', 'EscrowLength', 'SaleType',
                          'LoanType', 'RentBackLength', 'Side'];
        $transaction   = TransactionCombine::fullTransaction($transactionID);
        $propertyError = false;
        if ($transaction['property']->count() < 1) $propertyError = true;
        $detailErrors = [];
        foreach ($detailFields as $fname)
        {
            if (!isset($transaction['transaction']->{$fname})) $detailErrors[] = $fname;
        }
        session(['propertyError' => $propertyError, 'detailErrors' => $detailErrors]);

        if ($propertyError || count($detailErrors)) return false;
        return true;
    }

    /**
     * Decodes the meaning of the uploaded document filename
     *
     * @param string $docPath
     *
     * @return mixed
     */
    public static function decodeDocumentFilename($docPath): array
    {
        $fileType = ['pdf' => 'PDF', 'jpg' => 'Image', 'png' => 'Image', 'gif' => 'Image',];

        $rv = [];

        if (!is_array($docPath)) $docPath = [$docPath];

        foreach ($docPath as $idx => $doc)
        {
            if (!is_file($doc)) continue;

            $pi = pathinfo($doc);

            @list($transactionID, $documentID, $signer) = explode('-', $pi['filename']);
            $signers = explode('_', $signer);
            foreach ($signers as $idx => $sig)
            {
                list($role, $id) = explode('+', $sig);
                $signers[$idx] = ['role' => $role, 'id' => $id];
            }

            $fileExt = $pi['extension'];

            $rv[$documentID][] = ['transactionID' => $transactionID,
                                  'documentID'    => $documentID,
                                  'signers'       => $signers,
                                  'fileType'      => $fileType[strtolower($fileExt)],
                                  'url'           => url('/_uploaded_documents/transactions/' . $transactionID . '/' . $pi['basename']),
            ];
        }

        return $rv;
    }
    /**
     * Decodes the meaning of the uploaded document filename
     *
     * @param string $docPath
     *
     * @return mixed
     */
    public static function deprecated_getTransactionDocumentDetailsByFilename($docPath): array
    {
        $fileType = ['pdf' => 'PDF', 'jpg' => 'Image', 'png' => 'Image', 'gif' => 'Image',];

        $rv = [];

        if (!is_array($docPath)) $docPath = [$docPath];

        foreach ($docPath as $idx => $doc)
        {
            if (!is_file($doc)) continue;

            $pi = pathinfo($doc);

            @list($lk_td_ID, $uploadDate) = explode('_', $pi['filename']);
            $signers = explode('_', $signer);
            foreach ($signers as $idx => $sig)
            {
                list($role, $id) = explode('+', $sig);
                $signers[$idx] = ['role' => $role, 'id' => $id];
            }

            $fileExt = $pi['extension'];

            $rv[$documentID][] = ['transactionID' => $transactionID,
                                  'documentID'    => $documentID,
                                  'signers'       => $signers,
                                  'fileType'      => $fileType[strtolower($fileExt)],
                                  'url'           => url('/_uploaded_documents/transactions/' . $transactionID . '/' . $pi['basename']),
            ];
        }

        return $rv;
    }
    /**
     * Decodes the meaning of the uploaded document filename
     *
     * @param int lkDT_ID
     *
     *
     *
     * @return mixed
     */
    public static function encodeDocumentFilename($docPath): array
    {
        $fileType = ['pdf' => 'PDF', 'jpg' => 'Image', 'png' => 'Image', 'gif' => 'Image',];

        $rv = [];

        if (!is_array($docPath)) $docPath = [$docPath];

        foreach ($docPath as $idx => $doc)
        {
            if (!is_file($doc)) continue;

            $pi = pathinfo($doc);

            @list($transactionID, $documentID, $signer) = explode('-', $pi['filename']);
            $signers = explode('_', $signer);
            foreach ($signers as $idx => $sig)
            {
                list($role, $id) = explode('+', $sig);
                $signers[$idx] = ['role' => $role, 'id' => $id];
            }

            $fileExt = $pi['extension'];

            $rv[$documentID][] = ['transactionID' => $transactionID,
                                  'documentID'    => $documentID,
                                  'signers'       => $signers,
                                  'fileType'      => $fileType[strtolower($fileExt)],
                                  'url'           => url('/_uploaded_documents/transactions/' . $transactionID . '/' . $pi['basename']),
            ];
        }

        return $rv;
    }
    public static function saveTransactionDocumentList($transactionID, $documents, $reset=true)
    {
        $i = 0;
        if (!is_array($documents)) return false;

        if ($reset) lk_Transactions_Documents::eraseByTransactionId($transactionID);

        foreach($documents  as $doc)
        {
            $lktd = new lk_Transactions_Documents();
            $rec = ['Transactions_ID'=>$transactionID,
                    'Documents_Code'=>$doc['docCode'],
                    'isIncluded'=>$doc['isIncluded'],
                    'DateDue'=>$doc['MilestoneDate'],
                ];
            $lktd->upsert($rec);
        }
        return;
    }
    public static function putExternalDocumentIntoBag($transactionID, $lk_TransactionDocument_ID, $fileInfo, $useTemplate = FALSE)
    {
        $signedBy       = implode('_', $fileInfo->signedBy);
        if ($fileInfo)
        {

            $currentUserRoles = TransactionCombine::getRoleByUserID($transactionID, CredentialController::current()->ID());
            $userRole = reset($currentUserRoles)['role'];
            $roleID = reset($currentUserRoles)['id'];

            try
            {
                $bag                                      = new bag_TransactionDocument();
                $data['lk_Transactions-Documents_ID']     = $lk_TransactionDocument_ID;
                $data['DocumentPath']                     = $fileInfo->url;
                $data['OriginalDocumentName']             = $fileInfo->originalName;
                $data['isActive']                         = true;
                $data['useTemplate']                      = $useTemplate;
                $data['isComplete']                       = false;
                $data['SignedBy']                         = $signedBy;
                $data['UploadedByRole']                   = $userRole;
                $data['UploadedBy_ID']                    = $roleID;
                $data['UploadedByUsers_ID']               = auth()->id();
                $data['DateUpdated']                      = date('Y-m-d H:i:s');
                return $bag->upsert($data);
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION'=>$e, 'bag'=>$bag??'Not Defined', __METHOD__=>__LINE__ ]);
            }
        }
    }
    public static function putDocumentIntoBag($transactionID, $lk_TransactionDocument_ID, $uploadFileVarName='document')
    {
        if ($transactionID)
        {
            try
            {
                File::isDirectory(public_path('_uploaded_documents')) or File::makeDirectory(public_path('_uploaded_documents'), 0755, true, true);
                File::isDirectory(public_path('_uploaded_documents/transactions')) or File::makeDirectory(public_path('_uploaded_documents/transactions'), 0755, true, true);
                $docBagPath = Config::get('constants.DIRECTORIES.transactionDocuments') . $transactionID . '/';
                File::isDirectory($docBagPath) or File::makeDirectory($docBagPath, 0755, true, true);
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION' => $e, 'bagPath' => $docBagPath, __METHOD__ => __LINE__]);
            }
        }
        else
        {
            return response()->json(['invalid_target_transaction'], 400);
        }

        if (!Request::hasFile($uploadFileVarName))
        {
            return response()->json(['upload_file_not_found'], 400);
        }

        $file          = Request::file($uploadFileVarName);
        $transactionID = request('transactionID');
        $signedBy      = implode('_', request('signedBy'));

        $newFileName = $lk_TransactionDocument_ID . '_' . date('Y-m-d-h-i-s') . '.' .
                       pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

        if (!$file->isValid())
        {
            return response()->json(['invalid_file_upload'], 400);
        }

        if ($file->move($docBagPath, $newFileName))
        {
            $currentUserRoles = TransactionCombine::getRoleByUserID($transactionID, CredentialController::current()->ID());
            $userRole = reset($currentUserRoles)['role'];
            $roleID = reset($currentUserRoles)['id'];

            try
            {
                $bag                                   = new bag_TransactionDocument();
                $bag->{'lk_Transactions-Documents_ID'} = $lk_TransactionDocument_ID;
                $bag->DocumentPath                     = $docBagPath . $newFileName;
                $bag->OriginalDocumentName             = $file->getClientOriginalName();
                $bag->isActive                         = true;
                $bag->isComplete                       = false;
                $bag->SignedBy                         = $signedBy;
                $bag->UploadedByRole                   = $userRole;
                $bag->UploadedBy_ID                    = $roleID;
                $bag->UploadedByUsers_ID               = auth()->id();
                $bag->DateUpdated                      = date('Y-m-d H:i:s');
                return $bag->save();
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION'=>$e, 'bag'=>$bag??'Not Defined', __METHOD__=>__LINE__ ]);
            }
        }
        else ddd(['bag path'=>$docBagPath, 'new file'=>$newFileName,  __METHOD__=>__LINE__]);
    }
    public static function adjustDocumentDueDatesToMilestone($lk_taTask_ID)
    {

        $sql = 'UPDATE `lk_Transactions-Documents`
	left join `lk_Transactions-Timeline` on `lk_Transactions-Documents`.Transactions_ID = `lk_Transactions-Timeline`.Transactions_ID
	left join Documents on `lk_Transactions-Documents`.Documents_Code = Documents.Code
	left join Timeline on `lk_Transactions-Timeline`.MilestoneName = Timeline.Name

SET `lk_Transactions-Documents`.DateDue = `lk_Transactions-Timeline`.MilestoneDate

WHERE `lk_Transactions-Timeline`.ID = {{lk_Transactions-Timeline.ID}}
      AND   `lk_Transactions-Documents`.DateDue is not null
      AND   `lk_Transactions-Timeline`.MilestoneDate != `lk_Transactions-Documents`.DateDue
        ';

        $sql = str_replace('{{lk_Transactions-Timeline.ID}}', $lk_taTask_ID, $sql);
    DB::statement($sql);
    }

    public static function getDocumentAccessRoles(){
        $documentAccessRoles = DB::table('lu_DocumentAccessRoles')->get()->toArray();
        return $documentAccessRoles;
    }

    public static function getAllDocumentsByState($state){
        $documents = Document::where('State','=',$state)->get()->toArray();
        return response()->json($documents);
    }

    public static function getBagDocument($bagID=null)
    {
        $query = DB::table('bag_TransactionDocuments')
            ->select('lk_Transactions-Documents.Transactions_ID'
                , 'lk_Transactions-Documents.Documents_Code'
                , 'Documents.ShortName'
                , 'Documents.Description'
                , 'lk_TransactionsDocumentAccess.PovRole'
                , 'lk_TransactionsDocumentAccess.AccessSum'
                , 'lk_TransactionsDocumentTransfers.TransferSum'
                , 'bag_TransactionDocuments.ID as bagID'
            )
            ->join('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=', 'lk_Transactions-Documents.ID')
            ->leftJoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Documents.Code')
            ->leftJoin('lk_TransactionsDocumentAccess', function($join)
            {
                $join->on('Documents.Code', '=', 'lk_TransactionsDocumentAccess.Documents_Code');
                $join->on('lk_Transactions-Documents.Transactions_ID', '=', 'lk_TransactionsDocumentAccess.Transactions_ID');
            })
            ->leftJoin('lk_TransactionsDocumentTransfers', function($join)
            {
                $join->on('Documents.Code', '=', 'lk_TransactionsDocumentTransfers.Documents_Code');
                $join->on('lk_Transactions-Documents.Transactions_ID', '=', 'lk_TransactionsDocumentTransfers.Transactions_ID');
            })
            ->whereNotNull('lk_Transactions-Documents.Transactions_ID')
            ->whereRaw('lk_TransactionsDocumentTransfers.PovRole = lk_TransactionsDocumentAccess.PovRole' )
            ->orderBy('isIncluded', 'desc')
            ->orderBy('Transactions_ID')
            ->orderBy('ShortName');

        if (!empty($bagID)) $query = $query->where('bag_TransactionDocuments.ID', '=', $bagID);

        return $query->get();
    }
    /**
     * Takes the string in the column SignedBy inside of bag_TransactionDocuments
     * These signers are only the ones used for documents and as such are declared inside of signerArray.
     * Only the roles inside of signerArray will be pulled from transaction details.
     *
     * @param      $signedBy
     * @param      $transactionID
     * @param bool $returnJSON
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public static function decodeSigners($signedBy, $returnJSON = FALSE)
    {
        if($signedBy != NULL )$signers = explode('_', $signedBy);
        else return [];

        foreach ($signers as $s)
        {
            list($role, $id) = explode('+', $s);

            $userID = RoleCombine::getUserID($role, $id);

            $user = User::where('id', $userID)->first();
            if (empty($user)) continue;
            $user = $user->toArray();

            $data = ['roleID'    => $id, 'role' => $role,
                     'NameFull'  => $user['name'],
                     'NameFirst' => $user['NameFirst'],
                     'NameLast'  => $user['NameLast'],
                     'Users_ID'  => $user['id'],
            ];

            $rv[$role][$id] = $data;
        }

        if($returnJSON) return response()->json(['signers' => $rv]);
        return $rv;
    }

    /**
     * Takes the string in the column SignedBy inside of bag_TransactionDocuments
     * These signers are only the ones used for documents and as such are declared inside of signerArray.
     * Only the roles inside of signerArray will be pulled from transaction details.
     *
     * @param      $signedBy
     * @param      $transactionID
     * @param bool $returnJSON
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public static function getSigners($signedBy, $transactionID, $returnJSON = FALSE)
    {
        if($signedBy != NULL )$signers = explode('_', $signedBy);
        else $signers = [];
        $signerArray = [
            'b' => [],
            's' => [],
            'ba'=> null,
            'sa'=> null,
        ];

        foreach ($signerArray as $key => $s)
        {
            $person = TransactionCombine::getPerson($key,$transactionID);
            $signerArray[$key] = $person['data']->toArray();
        }

        foreach ($signers as $s)
        {
            if (!is_null($s) && $s != '')
            {
                $signerParts = explode('+', $s);
                $role = $signerParts[0];
                $id = $signerParts[1];
                switch ($role)
                {
                    case 'b':
                        if (count($signerArray[$role]) > 0) {
                            foreach ($signerArray[$role] as $key => $person) {
                                if ($id > 0 && $id == $person['ID']) {
                                    if (!is_null($person['Users_ID'])) {
                                        $modelName = 'App\User';
                                        $icon = $modelName::where('id', $person['ID'])->pluck('image')->first();
                                        $data['Icon'] = self::formatIconURL($icon);
                                    }
                                    $data['ID'] = $person['ID'];
                                    $data['NameFull'] = $person['NameFull'];
                                    $data['NameFirst'] = $person['NameFirst'];
                                    $data['NameLast'] = $person['NameLast'];
                                    $data['Signed'] = TRUE;
                                    $data['Users_ID'] = $person['Users_ID'];
                                    $signerArray[$role][$key] = $data;
                                }
                            }
                        }
                        break;

                    case 's':
                        if (count($signerArray[$role]) > 0) {
                            foreach ($signerArray[$role] as $key => $person) {
                                if ($id > 0 && $id == $person['ID']) {
                                    if (!is_null($person['Users_ID'])) {
                                        $modelName = 'App\User';
                                        $icon = $modelName::where('id', $person['ID'])->pluck('image')->first();
                                        $data['Icon'] = self::formatIconURL($icon);
                                    }
                                    $data['ID'] = $person['ID'];
                                    $data['NameFull'] = $person['NameFull'];
                                    $data['NameFirst'] = $person['NameFirst'];
                                    $data['NameLast'] = $person['NameLast'];
                                    $data['Signed'] = TRUE;
                                    $data['Users_ID'] = $person['Users_ID'];
                                    $signerArray[$role][$key] = $data;
                                }
                            }
                        }
                        break;

                    case 'ba':
                        if (count($signerArray[$role]) > 0) {
                            $person = _Convert::toArray($signerArray[$role][0]);
                            if ($id > 0) {
                                if (!is_null($person['Users_ID'])) {
                                    $modelName = 'App\User';
                                    $icon = $modelName::where('id', $person['ID'])->pluck('image')->first();
                                    $data['Icon'] = self::formatIconURL($icon);
                                }
                                $data['ID'] = $person['ID'];
                                $data['NameFull'] = $person['NameFull'];
                                $data['NameFirst'] = $person['NameFirst'];
                                $data['NameLast'] = $person['NameLast'];
                                $data['Signed'] = TRUE;
                                $data['Users_ID'] = TRUE;
                                $signerArray[$role][0] = $data;
                            }
                        }
                        break;

                    case 'sa':
                        if (count($signerArray[$role]) > 0) {
                            $person = _Convert::toArray($signerArray[$role][0]);
                            if ($id > 0) {
                                if (!is_null($person['Users_ID'])) {
                                    $modelName = 'App\User';
                                    $icon = $modelName::where('id', $person['ID'])->pluck('image')->first();
                                    $data['Icon'] = self::formatIconURL($icon);
                                }
                                $data['ID'] = $person['ID'];
                                $data['NameFull'] = $person['NameFull'];
                                $data['NameFirst'] = $person['NameFirst'];
                                $data['NameLast'] = $person['NameLast'];
                                $data['Signed'] = TRUE;
                                $data['Users_ID'] = TRUE;
                                $signerArray[$role][0] = $data;
                            }
                        }
                        break;
                }
            }
        }

        if($returnJSON) return response()->json(['signers' => $signerArray]);
        return $signerArray;
    }

    public static function formatIconURL($iconURL)
    {
        $icon = explode('/', $iconURL);
        unset($icon[0]);
        unset($icon[1]);
        array_values($icon);
        $icon = implode('/', $icon);
        return _Locations::url('images',$icon);
    }
}