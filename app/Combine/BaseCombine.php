<?php

namespace App\Combine;

use Illuminate\Support\Collection;

class BaseCombine
{

    public static function tableHeader(Collection &$collection, array $parameters = [])
    {
        $rv = '<table>' . PHP_EOL . '<tr>';
        foreach ($collection->first() as $key => $val)
        {
            $rv .= '<th>' . title_case($key) . '</th>';
        }
        $rv .= '</tr>' . PHP_EOL;
        return $rv;
    }

    public static function tableData(Collection &$collection, array $parameters = [])
    {
        $rv = '<tr>';
        foreach ($collection as $record)
        {
            foreach ($record as $key => $val)
            {
                $rv .= '<td>' . $val . '</td>';
            }
            $rv .= '</tr>' . PHP_EOL;
        }
        return $rv;
    }

    public static function tableFooter(Collection &$collection, array $parameters = [])
    {
        $rv = '</table>' . PHP_EOL;
        return $rv;
    }

    public static function collectionToArray(Collection &$collection)
    {
        $ay = [];
        foreach ($collection as $idx => $record)
        {
            foreach ($record as $key => $val)
            {
                $ay[$idx][$key] = $val;
            }
        }
        return $ay;
    }

    public static function stdClassToArray(&$stdClass)
    {
        if (!is_a($stdClass, 'StdClass')) return $stdClass;

        $ay = [];
        foreach ($stdClass as $idx => $value)
        {
            $ay[$idx] = self::stdClassToArray($value);
        }
        return $ay;
    }

}
