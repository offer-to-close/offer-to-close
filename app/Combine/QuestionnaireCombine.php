<?php
namespace App\Combine;


use App\Library\Utilities\_Convert;
use App\Models\Questionnaire;
use App\Models\lk_Transactions_Questionnaires;
use DateTime;
use App\Combine\TransactionCombine;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class QuestionnaireCombine extends BaseCombine
{
    public static function questionnairesByTransaction($transactionsID, $type = 'disclosure', $roles = [], $includeWhen = NULL, $getCollection = true)
    {
        $query = DB::table('Questionnaires')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.Questionnaires_ID', '=', 'Questionnaires.ID')
            ->selectRaw(
                'Questionnaires.ID,Questionnaires.State, 
                Questionnaires.Description, 
                Questionnaires.ShortName,
                Questionnaires.IncludeWhen,
                Questionnaires.Version,
                Questionnaires.ResponderRole,
                Questionnaires.Documents_Code,
                `lk_Transactions-Questionnaires`.DateDue, 
                `lk_Transactions-Questionnaires`.DateCompleted,
                `lk_Transactions-Questionnaires`.ID as lk_ID'
            )
            ->where('lk_Transactions-Questionnaires.Transactions_ID', $transactionsID)
            ->when(!empty($roles),function ($query) use ($roles){
                $query->whereIn('Questionnaires.ResponderRole', $roles);
            })
            ->where('Questionnaires.Type', $type);

        if($includeWhen != NULL)
        {
            self::filterQueryByIncludeWhen($query,$includeWhen);
        }

        QuestionnaireCombine::generateQuestionnaires($transactionsID);
        if($getCollection)
        {
            return $query->get();
        }
        return $query;
    }

    public static function generateQuestionnaires($transactionsID)
    {
        $transactionsProperty = TransactionCombine::getProperty($transactionsID)->first();

        if(is_null($transactionsProperty->State)) Session::now('error', 'Transaction state not set.');
        $relevantQuestionnaires = DB::table('Questionnaires')
            ->where('State', $transactionsProperty->State)
            ->get();


        $data = array();
        $dateDue = date('Y-m-d');
        foreach ($relevantQuestionnaires as $key => $questionnaire)
        {
            $result = DB::table('lk_Transactions-Questionnaires')
                ->where('Transactions_ID', $transactionsID)
                ->where('Questionnaires_ID',$questionnaire->ID)
                ->get();
            if($result->isEmpty())
            {
                $lk = new lk_Transactions_Questionnaires();
                $data['Questionnaires_ID']     = $questionnaire->ID;
                $data['Transactions_ID']    = $transactionsID;
                $data['DateDue']            = $dateDue;
                $lk->upsert($data);
            }
        }
    }

    public static function getQuestionnaireQuestions($questionnaireID,$transactionsID)
    {
        $search_lk = DB::table('lk_Transactions-Questionnaires')
            ->where('Questionnaires_ID', $questionnaireID)
            ->where('Transactions_ID', $transactionsID)->get();
        $lk_ID = $search_lk->first()->ID;



        QuestionnaireCombine::createQuestionnaireAnswers($questionnaireID,$lk_ID);

        $questionnaireQuestions = DB::table('QuestionnaireQuestions')
            ->leftJoin('QuestionnaireAnswers', 'QuestionnaireQuestions_ID', '=', 'QuestionnaireQuestions.ID')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.ID', '=', 'QuestionnaireAnswers.lk_Transactions-Questionnaires_ID')
            ->selectRaw('QuestionnaireAnswers.Answer,QuestionnaireAnswers.DateUpdated,
            QuestionnaireAnswers.QuestionnaireQuestions_ID,QuestionnaireQuestions.Questionnaires_ID,QuestionnaireQuestions.DisplayOrder,QuestionnaireQuestions.Hint,
            `lk_Transactions-Questionnaires`.ID,QuestionnaireQuestions.IncludeWhen,QuestionnaireAnswers.Notes,`lk_Transactions-Questionnaires`.OtherSigners,
            QuestionnaireQuestions.PossibleAnswers,QuestionnaireQuestions.Question,QuestionnaireQuestions.QuestionType,QuestionnaireAnswers.Status,
            `lk_Transactions-Questionnaires`.Transactions_ID as Transactions_ID,`lk_Transactions-Questionnaires`.signedByProviders,
            QuestionnaireQuestions.Section')
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnaireID)
            ->where('QuestionnaireQuestions.deleted_at', NULL)
            ->where('lk_Transactions-Questionnaires.ID', $lk_ID)
            ->orderBy('DisplayOrder')
            ->get()->toArray();

        $questionnaireQuestions = self::processConditions($questionnaireQuestions);
        return $questionnaireQuestions;
    }

    public static function processConditions($questionnaireQuestions)
    {
        $keys = $conditions = $lk_ID = $D_ID = array();
        $counter = 0;
        //todo make these into switch statements
        /*
         * condition[0] = type
         * condition[1] = index
         * condition[2] = condition
         * condition[3] = value
         */
        foreach ($questionnaireQuestions as $q)
        {

            //var_dump($questionnaireQuestions);
            if($q->IncludeWhen != NULL)
            {
                DB::table('lk_Transactions-Questionnaires')
                    ->where('ID', $q->ID)
                    ->update(['DateCompleted' => NULL]);
                $multiple = strpos($q->IncludeWhen,'|');
                if($multiple)
                {
                    $multiple_conditions = explode('|',$q->IncludeWhen);
                    foreach ($multiple_conditions as $condition)
                    {
                        $condition = self::parseCondition($condition);
                        self::conditionOptions($questionnaireQuestions,$q,$condition,$lk_ID,$D_ID,$keys,$counter);
                    }
                }
                else
                {
                    $condition = self::parseCondition($q->IncludeWhen);
                    self::conditionOptions($questionnaireQuestions,$q,$condition,$lk_ID,$D_ID,$keys,$counter);
                }

            }
            $counter++;
        }
        for($i=0;$i<count($keys);$i++)
        {
            self::updateQuestionnaireAnswer($lk_ID[$i],$D_ID[$i],NULL);
            unset($questionnaireQuestions[$keys[$i]]);
        }
        $questionnaireQuestions = array_values($questionnaireQuestions);
        return $questionnaireQuestions;

    }

    /**
     * This is where the different logic options for Include When in questionnnaire should exist.
     * @param $questionnaireQuestions
     * @param $q
     * @param $condition
     * @param $lk_ID
     * @param $D_ID
     * @param $keys
     * @param $counter
     */
    public static function conditionOptions($questionnaireQuestions, $q, $condition, &$lk_ID, &$D_ID, &$keys, &$counter)
    {
        foreach($questionnaireQuestions as $key => $d)
        {
            if ( $d->DisplayOrder == $condition[1] ) $displayOrder = $key;
        }
        if($condition[0] == 'Question')
        {
            if($condition[2] == 'eq')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer == $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
            if($condition[2] == 'gt')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer > $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
            if($condition[2] == 'ne')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer != $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
        } //end if condition is a Question.
    }

    public static function parseCondition($condition)
    {
        $segments = explode('.',$condition);
        $segments[0] = str_replace('[','',$segments[0]);
        $segments[1] = str_replace(']','',$segments[1]);

        return $segments;
    }

    public static function lookupCommonAnswerValues($value)
    {
        $string = NULL;
        $valuesList = ['dk' => 'I Don\'t Know', 'da' => 'Doesn\'t Apply', 'no' => 'No', 'yes' => 'Yes', 100 => 'Other'];
        $searchString = ',';

        if( strpos($value, $searchString) ) {
            $value = explode(',',$value);
        }
        if(is_array($value))
        {
            $max = count($value);
            for ($i=0;$i<$max;$i++)
            {
                if(array_key_exists($value[$i], $valuesList))
                {
                    $string .= $valuesList[$value[$i]];
                    if($i < $max - 1)
                    {
                        $string .= ',';
                    }
                }
            }
            return $string;
        }
        if(array_key_exists($value, $valuesList))
        {
            return $valuesList[$value];
        }
        return false;
    }

    public static function parseNotes($notes)
    {
        $segments = explode(']',$notes);
        $size = count($segments);
        unset($segments[$size-1]);
        $notes = array();
        foreach ($segments as $s)
        {
            $s = str_replace('[','',$s);
            $ea_note = explode('|',$s);
            $notes[$ea_note[0]] = $ea_note[1];
        }
        return $notes;
    }


    public static function updateQuestionnaireAnswer($lk_ID,$questionnaireID,$answer)
    {
        DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID',$lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionnaireID)
            ->update(['Answer'=> $answer]);
    }

    public static function createQuestionnaireAnswers($questionnaireID, $lk_ID)
    {
        $questionnaireQuestions = DB::table('QuestionnaireQuestions')
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnaireID)
            ->orderBy('DisplayOrder')
            ->get();

        foreach($questionnaireQuestions as $q)
        {
            $query = DB::table('QuestionnaireAnswers')
                ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
                ->where('QuestionnaireQuestions_ID', $q->ID)
                ->get();

            if($query->isEmpty())
            {
                $answer = NULL;
                if($q->QuestionType == 'single')
                {
                    $answer = $q->PossibleAnswers;
                }
                DB::table('QuestionnaireAnswers')->insert([
                    'lk_Transactions-Questionnaires_ID'    => $lk_ID,
                    'QuestionnaireQuestions_ID'            => $q->ID,
                    'Answer'                            => $answer
                ]);
            }

        }

    }


    //pass in the question element as a collection
    public static function parsePossibleAnswers($questionnaireQuestion)
    {
        $displays = $values = $ids = $addInfo = array();
        if($questionnaireQuestion->PossibleAnswers != NULL)
        {
            $possibleAnswers = explode(',',$questionnaireQuestion->PossibleAnswers);
            for($i = 0; $i < count($possibleAnswers); $i ++)
            {
                $possibleAnswersValues []= explode('.', $possibleAnswers[$i]);
                $match = strpos($possibleAnswersValues[$i][0],'*');
                if($match)
                {
                    $addInfo[] = true;
                }
                else
                {

                    $addInfo[] = false;
                }
                $displays[] = $possibleAnswersValues[$i][1] ?? NULL;
                $values[]=$possibleAnswersValues[$i][2] ?? NULL;
            }
        }
        else {
            $displays = NULL;
            $values = NULL;
        }

        $id     = $questionnaireQuestion->ID;
        $type   = $questionnaireQuestion->QuestionType;
        $displays_and_values = [
            'Question'      => $questionnaireQuestion->Question,
            'displays'      => $displays,
            'values'        => $values,
            'ID'            => $id,
            'QuestionType'  => $type,
            'addInfo'       => $addInfo
        ];

        return $displays_and_values;
    }

    public static function answerDisplaysAndValues($questionnaireID,$transactionsID)
    {
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($questionnaireID,$transactionsID);
        $displays_and_values = array();
        foreach($questions as $q)
        {
            $displays_and_values[] = QuestionnaireCombine::parsePossibleAnswers($q);
        }

        return $displays_and_values;
    }

    public static function checkAnswers($transactionsID, $questionnairesID, $friendlyIndex = false)
    {

        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        $answers = DB::table('QuestionnaireQuestions')
            ->leftJoin('QuestionnaireAnswers', 'QuestionnaireQuestions_ID', '=', 'QuestionnaireQuestions.ID')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.ID', '=', 'QuestionnaireAnswers.lk_Transactions-Questionnaires_ID')
            ->selectRaw('QuestionnaireQuestions.Question, `lk_Transactions-Questionnaires`.ID as lk_ID,QuestionnaireAnswers.Answer,
            QuestionnaireQuestions.DisplayOrder, QuestionnaireQuestions.FriendlyIndex, QuestionnaireAnswers.Notes, QuestionnaireQuestions.ChildIndex')
            ->where('lk_Transactions-Questionnaires.Transactions_ID', $transactionsID)
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnairesID)
            ->where('QuestionnaireQuestions.deleted_at', NULL)
            ->orderBy('QuestionnaireQuestions.DisplayOrder')
            ->get();

        $answerCounter = 0;
        foreach($answers as $a)
        {
            if($a->Answer != NULL)
            {
                $answerCounter++;
            }
        }

        if($friendlyIndex)
        {
            $indexedAnswers = [];
            foreach ($answers as $answer)
            {
                $indexedAnswers[$answer->FriendlyIndex] = $answer;
            }
            return $indexedAnswers;
        }

        $answers['Size'] = count($answers);
        $answers['AnsweredSize'] = $answerCounter;

        return $answers;
    }

    public static function checkAnswer($transactionsID, $questionnairesID, $questionID)
    {
        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        $answer = DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionID)
            ->get();

        if($answer->isEmpty())
        {
            return false;
        }
        else return $answer->first()->Answer;
    }

    /**
     * Concatenate the notes from a question in this format:
     * ChildIndex or Answer: Explanation/Note for each question.
     * Return a single string that can be put into templates.
     * @param $questionObjects
     * @param string $prefix
     * @return array
     */
    public static function getNotesFromQuestionObject($questionObjects, $prefix = 'ChildIndex')
    {
        $allNotes = [];
        foreach ($questionObjects as $question)
        {
            if($question->Notes && $question->Answer != 'no' && $question->Answer != 'No')
            {
                $noteText = '';
                $notes = self::parseNotes($question->Notes);
                if ($prefix == 'ChildIndex')
                {
                    foreach ($notes as $answer => $note)
                    {
                        if($note != '')
                        {
                            $noteText .= $question->{$prefix} . ':';
                            $noteText .= $note . ' ';
                        }
                    }
                }
                else
                {
                    foreach ($notes as $answer => $note)
                    {
                        if($note != '') $noteText .= $answer . ':' . $note . ' ';
                    }
                }
                $allNotes[] = $noteText;
            }
        }
        return $allNotes;
    }

    /**
     * First parameter is an array of strings or just a single string.
     * Second parameter is an array of input limits. For example:
     * array = (
     *    100,
     *    120,
     *    120,
     * )
     *
     * The function will split all of the text to make it fit into each input (based on character count)
     * It will return an array:
     *
     * return  = [
     *      'inputs' => [input1,input2,input3]
     *      'extra' => any text that did not fit.
     * ]
     * @param $notes
     * @param array $inputLimits
     * @return array
     */
    public static function splitArrayOfTextIntoMultipleInputs($notes, $inputLimits = [])
    {
        $fullText = '';
        $fullExtraText = '';
        $fullTextArray = [];
        $limit = 0;
        foreach ($inputLimits as $l) $limit += $l;
        if(!is_array($notes)) $notes = [$notes];
        foreach ($notes as $note)
        {
            if (!empty($note) && !is_null($note))
            {
                $note = trim($note);
                $words = explode(' ', $note);
                foreach ($words as $word) if ($word != '' && $word != ' ') $fullTextArray[] = $word;
            }
        }
        $inputs = [];
        $extra = FALSE;
        $inputQuantity = count($inputLimits);
        $wordArrayKey = 0;
        for($i = 0; $i < $inputQuantity; $i++)
        {
            $tempInputText = '';
            $inputText = '';
            $fullTextWordLength = count($fullTextArray);
            while ($wordArrayKey < $fullTextWordLength)
            {
                $word = $fullTextArray[$wordArrayKey].' ';
                $tempInputText .= $word;
                $fullText .= $word;
                $wordArrayKey++;
                if(!$extra) if(strlen($fullText) > $limit) $extra = TRUE;
                if($extra) $fullExtraText .= $word;
                else
                {
                    $fits = strlen($tempInputText) <= $inputLimits[$i];
                    if ($fits)
                    {
                        $inputText .= $word;
                    }
                    else
                    {
                        $wordArrayKey = $wordArrayKey - 1;
                        break;
                    }
                }
            }
            $inputs[] = $inputText;
        }

        return [
            'inputs' => $inputs,
            'extra'  => $fullExtraText
        ];
    }

    public static function calculateDisclosureDueDate($transactionsID)
    {
        $timeline = TransactionCombine::getTimeline($transactionsID);
        $acceptanceDate = $timeline[1]->MilestoneDate;
        $d = new DateTime( $acceptanceDate );
        $d->modify( '+7 days' );
        $date = $d->format( 'Y-m-d G:i:s' );
        return $date;
    }

    //Filter Functions
    public static function filterQueryByResponderRole(&$query, $role)
    {
        if (substr($role, -2) == 'tc') $role = 'tc';
        if (substr($role, -1) == 'a') $role = 'a';
        $query->where('Questionnaires.ResponderRole', $role);
    }

    public static function filterQueryByIncludeWhen(&$query, $includeWhen)
    {
        //IncludeWhen logic
        //use parser
        $query->where('Questionnaires.IncludeWhen', $includeWhen);
    }
}
