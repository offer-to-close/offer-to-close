<?php

namespace App\Combine;

use App\Library\otc\_Locations;
use App\Library\Utilities\_Convert;
use App\Models\lu_UserRoles;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class RoleCombine extends BaseCombine
{
    public static $modelPath = 'App\\Models\\';
    /**
     * Takes a string of the form <role code>+<id>_<role code>+<id> and returns a list of people
     * Example: input of s+45_ba+3_e+5 translates to Sellers.ID = 45, Agents.ID = 3, Escrows.ID = 5
     *
     * @param $list
     *
     * @return bool
     */
    public static function decodeRoleList($list)
    {
        $roles = Config::get('constants.USER_ROLE');
        $people = explode('_', $list);
        $rv = [];

        foreach ($people as $val)
        {
            list($role, $id) = explode('+', $val);
            switch ($role)
            {
                case $roles['BUYER']:
                    break;
                case $roles['SELLER']:
                    break;
                case $roles['BUYERS_AGENT']:
                case $roles['SELLERS_AGENT']:
                    break;
                case $roles['BUYERS_TRANSACTION_COORDINATOR']:
                case $roles['SELLERS_TRANSACTION_COORDINATOR']:
                    break;
                case $roles['ESCROW']:
                    break;
                case $roles['LOAN']:
                    break;
                case $roles['TITLE']:
                    break;
            }
        }
        return $rv;
    }

    public static function doRolesMatch($role1, $role2)
    {
        return (substr($role1, 0, 1) == substr($role2, 0, 1));
    }

    public static function getUserID($role, $roleID)
    {
        if (!($rec = self::getRole($role, $roleID))) return false;
        return $rec->Users_ID ?? false;
    }
    public static function getRole($role, $roleID)
    {
        if (empty($role) || empty($role) ) return false;
        $db = self::$modelPath . config('constants.RoleModel')[$role];
        $obj = new $db;
        if (!empty($obj))
        {
            $rec = $obj->where('ID', $roleID)->get();
            if($rec->isEmpty()) return false;
            return $rec->first();
        }
        return false;
    }
    public static function getDisplayByRole($role)
    {
        return title_case(str_replace('_', ' ', lu_UserRoles::getDisplay($role)));
    }
    public static function transactionsRoles($userID, $onlyNotClosed=false)
    {
        $where = ['b.Users_ID = ?',
                  's.Users_ID = ?',
                  'ba.Users_ID = ?',
                  'sa.Users_ID = ?',
                  'btc.Users_ID = ?',
                  'stc.Users_ID = ?',
                  'e.Users_ID = ?',
                  't.Users_ID = ?',
                  'l.Users_ID = ?',
                  'trans.CreatedByUsers_ID = ?',
                  'trans.OwnedByUsers_ID = ? ',
        ];

        $bindings = array_fill(0, count($where), $userID);

        $select = 'trans.ID
                    , trans.Status
                    
                    , b.Users_ID as b
                    , s.Users_ID as s
                    , ba.Users_ID as ba
                    , sa.Users_ID as sa
                    , btc.Users_ID as btc
                    , stc.Users_ID as stc
                    , e.Users_ID as e
                    , l.Users_ID as l
                    , t.Users_ID as t

                    , trans.CreatedByUsers_ID as c
                    , trans.OwnedByUsers_ID as o
                ';
        $select = str_replace(["\r", "\n"], '', $select);

        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('Escrows AS e', 'trans.Escrows_ID', '=', 'e.ID')
                   ->leftJoin('Loans AS l', 'trans.Loans_ID', '=', 'l.ID')
                   ->leftJoin('Titles AS t', 'trans.Titles_ID', '=', 't.ID')
                   ->leftJoin('users AS c', 'trans.CreatedByUsers_ID', '=', 'c.ID')
                   ->leftJoin('users AS o', 'trans.OwnedByUsers_ID', '=', 'o.ID')
                   ->selectRaw($select);

        $query->whereRaw(implode(' OR ', $where), $bindings)
              ->orderBy('trans.ID')->orderBy('b.ID')->orderBy('s.ID')
              ->distinct();
//dd($query->get());
        $transactions = $query->get();

        $roles = [];
        foreach ($transactions as $i => $transaction)
        {
            $tmp = _Convert::toArray($transaction);
            if ($onlyNotClosed && $tmp['Status'] == 'closed') continue;

            foreach ($tmp as $fld => $val)
            {
                if (in_array($fld, ['ID', 'Status'])) continue;
                if (!isset($roles[$fld])) $roles[$fld] = 0;
                if ($val == $userID) ++$roles[$fld];
            }
        }

        foreach ($roles as $role=>$count)
        {
            if ($count == 0) unset($roles[$role]);
        }
//   ddd($roles);
        return $roles;
    }

}