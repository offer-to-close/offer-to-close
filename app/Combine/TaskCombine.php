<?php

namespace App\Combine;

use App\Http\Controllers\AlertController;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\TransactionController;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Variables;
use App\Models\lk_Transactions_Tasks;
use App\Models\lk_Transactions_Timeline;
use App\Models\Model_Parent;
use App\Scopes\NoTestScope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Library\Utilities\_Time;
use App\Library\otc\IncludeWhen;
use App\Models\Task;
use function Psy\debug;

class TaskCombine extends BaseCombine
{
    /**
     * @param $transactionsID
     *
     * @return mixed
     */

    public static $modelPath = 'App\\Models\\';
    public static $adHocIDs = [];

    /**
     * @param $stateCode
     *
     * @return mixed
     */
    public static function byState($stateCode)
    {
        $query = DB::table('Tasks')
                   ->select('*')
                   ->where('State', '=', $stateCode);
        $query = Model_Parent::scopeNoTest($query);
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function byTransaction($transactionID, $transactionRole)
    {
        $query = DB::table('Tasks')
                   ->leftJoin('Timeline', 'Timeline.Code', '=', 'Tasks.Timeline_Code')
                   ->leftJoin('lk_Transactions-Tasks', 'lk_Transactions-Tasks.Tasks_Code', '=', 'Tasks.Code')
                   ->leftJoin('lu_TaskCategories', 'Tasks.Category', '=', 'lu_TaskCategories.Value')
                   ->selectRaw('Tasks.ID, Tasks.Code, Tasks.State, Tasks.Category, Tasks.TaskSets_Value, Tasks.Order, Tasks.ShortName, 
                `lk_Transactions-Tasks`.UserRole,
                `lk_Transactions-Tasks`.Description, Tasks.IncludeWhen, Tasks.Timeline_ID, Tasks.Timeline_Code,
                 Tasks.DateOffset, Tasks.isTest, Tasks.Status, `lk_Transactions-Tasks`.Notes, Tasks.DateCreated, Tasks.DateUpdated, 
                 Tasks.deleted_at, Timeline.Name as Display, 
                `lk_Transactions-Tasks`.ID as lk_ID, Transactions_ID, DateDue, DateCompleted, 
                `lk_Transactions-Tasks`.Status as lk_Status, `lk_Transactions-Tasks`.Notes as lk_Notes, lu_TaskCategories.Order as CategoryOrder')
                   ->where('Transactions_ID', '=', $transactionID)
                   ->where('lk_Transactions-Tasks.deleted_at', NULL)
                   ->where('lu_TaskCategories.Order', '>', 0)
                   ->where(function ($query) use ($transactionRole)
                   {
                       $query->where('lk_Transactions-Tasks.Status', '!=', 'deleted')
                             ->orWhereNull('lk_Transactions-Tasks.Status');
                   })
                   ->orderBy('lu_TaskCategories.Order');
        $query = Model_Parent::scopeNoTest($query, ['Tasks', 'lk_Transactions-Tasks', 'lu_TaskCategories']);

        if (!is_array($transactionRole)) $transactionRole = [$transactionRole];
        foreach ($transactionRole as $tr)
        {
            $ah[] = $tr . '-ah';
        }
        $query = $query->where(function ($query) use ($transactionRole, $ah)
        {
            $query->whereIn('Tasks.TaskSets_Value', $transactionRole)
                  ->orWhereIn('Tasks.TaskSets_Value', $ah);
        });
//Log::debug(['taid'=>$transactionID, 'roles'=>$transactionRole,  'ahs'=>$ah, 'sql'=>$query->toSql(), __METHOD__=>__LINE__]);
        /*
         * The previous function inside of $query separates the where clauses.
         * In order:
         * Where and (Where or Where), as opposed to (Where and Where) or Where
         */

        return $query->get(); //toSql();
    }

    /**
     * @param      $transactionID
     * @param null $transactionRole
     *
     * @return array
     */
    public static function getTaskList($transactionID, $transactionRole = null)
    {
        if (empty($transactionRole))
        {
            $transactionRole = TransactionCombine::getUserTransactionRoles($transactionID, true);
        }
        $taskRole = TaskCombine::getTaskRoles($transactionRole, $transactionID);

        $tasks = self::byTransaction($transactionID, $taskRole);
        $adHocTasks = 0;
        foreach ($tasks as $task) if ($task->Category == 'ah') $adHocTasks++;
        if (count($tasks) != 0 && count($tasks) != $adHocTasks) return _Convert::toArray($tasks);

        self::fillTransactionTaskTable($transactionID, $transactionRole);
        $tasks = self::byTransaction($transactionID, $taskRole);
        return _Convert::toArray($tasks);
    }

    /**
     * @param $taskRole
     * @param $transactionID
     *
     * @return array
     */
    public static function getTaskRoles($taskRole, $transactionID)
    {
        if (!is_array($taskRole)) $taskRole = [$taskRole];

        $rv = [];
        $roles = TransactionCombine::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
        $sides = [];
        foreach ($taskRole as $tRole)
        {
            if (substr($tRole, -1) != 'a' && substr($tRole, -2) != 'tc')
            {
                $rv[] = $tRole;
                continue;
            }

            if (substr($tRole, -1) == 'a')
            {
                $r = 'a';
                $l = 1;
                foreach ($roles as $role)
                {
                    if (substr($role, -$l) == $r
                        && strlen($role) != $l)
                    {
                        $sides[] = $role;
                    }
                }
            }
            elseif (substr($tRole, -2) == 'tc')
            {
                $r = 'tc';
                $l = 2;
                foreach ($roles as $role)
                {
                    if (substr($role, -$l) == $r
                        && strlen($role) != $l)
                    {
                        $sides[] = $role;
                    }
                }
            }
            $sides = array_unique($sides);

            if (count($sides) != 2)
            {
                $rv[] = $tRole;
                continue;
            }

            if (substr($tRole, -1) == 'a')
            {
                $rv[] = 'da';
                foreach(['ba', 'sa'] as $needle)
                {
                    if (in_array($needle, $rv)) unset($rv[array_search($needle, $rv)]);
                }
            }

            if (substr($tRole, -2) == 'tc')
            {
                $rv[] = 'dtc';
                foreach(['btc', 'stc'] as $needle)
                {
                    if (in_array($needle, $rv)) unset($rv[array_search($needle, $rv)]);
                }
            }
        }
        $rv = array_unique($rv);
        sort($rv);

        return $rv;
    }

    /**
     * Returns the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function createNewTaskList($transactionID, $transactionRole, $storeInDB = true)
    {
        $transactionController = new TransactionController();
        $transactionRecord     = $transactionController->getTransaction($transactionID);
        $milestones            = TransactionCombine::getTimeline($transactionID);
        if ($milestones->isEmpty())
        {
            MilestoneCombine::saveTransactionTimeline($transactionID,
                $transactionRecord['transaction']->DateAcceptance,
                $transactionRecord['transaction']->EscrowLength);
        }

        $property = $transactionRecord['property']->first();

        $query = Task::withoutGlobalScope(NoTestScope::class)
                     ->leftJoin('Timeline', function ($join)
        {
            $join->on('Tasks.Timeline_Code', '=', 'Timeline.Code');
            $join->on('Tasks.State', '=', 'Timeline.State');
        })
                     ->leftJoin('lk_Transactions-Timeline', 'Timeline.Name', 'lk_Transactions-Timeline.MilestoneName')
                     ->select('Tasks.*', 'lk_Transactions-Timeline.MilestoneName', 'lk_Transactions-Timeline.MilestoneDate')
                     ->where('Tasks.TaskSets_Value', $transactionRole)
                     ->where('lk_Transactions-Timeline.isActive', '!=', false )
                     ->where('Tasks.State', $property->State ?? Config::get('constants.DEFAULT.STATE_CODE'))
                     ->where('lk_Transactions-Timeline.Transactions_ID', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Tasks','lk_Transactions-Timeline',]);
//Log::debug(['TaskSets_Value'=>$transactionRole, 'taid'=>$transactionID, $query->toSql(), __METHOD__=>__LINE__]);
        $tasks = $query->get();

        $lktasks = [];
// =====================================================================================================================
// ... The Offset is a numeric value that moves the deadline for the task by that number of days from the task's
//     reference milestone. The DateOffset value in the Tasks table is a string and can have information about what
//        units the offset is in. The default unit is days (in the field a 'd' is used.) If units are specified then
//        the numeric value and the units MUST be separated with a pipe (|). E.g. "7|d" is seven days after the milestone,
//        while "-3|d" is three days before the milestone.
//
//        An alternative unit value is "bd" which indicates 'business days'. If bd is used then when days are counted
//        from the milestone, only valid business days are used. In other words, if the milestone falls on a Friday and
//        the offset is "2|d", the task would fall on a Sunday, but if the offset is "2|bd" then since Sunday is not a
//        business day it would be pushed to Monday.
// =====================================================================================================================

        foreach ($tasks as $idx => $task)
        {
            $units = 'd';
            if (!is_numeric($task->DateOffset))
            {
                list($offset, $units) = explode('|', $task->DateOffset);
            }
            else $offset = $task->DateOffset;
            $dateDue = date('Y-m-d', _Time::addDays($task->MilestoneDate, $offset));
            if ($units == 'bd') $dateDue = date('Y-m-d', strtotime(_Time::moveToBusinessDay($dateDue)));

            $lktasks[] = ['Transactions_ID' => $transactionID,
                          'UserRole'        => $transactionRole,
                          'DateDue'         => $dateDue,
                          'Description'     => $task->Description,
                          'Tasks_Code'      => $task->Code,
            ];
        }

        _Arrays::sortByColumn($lktasks, 'DateDue');
        return $lktasks;
    }

    /**
     * Stores a new list of Tasks needed to be completed for a given transaction and role into the linking table
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function fillTransactionTaskTable($transactionID, $transactionRole)
    {
        if (is_array($transactionRole)) $transactionRole = reset($transactionRole);

        $taskList = self::createNewTaskList($transactionID, $transactionRole);
        foreach ($taskList as $task)
        {
            try
            {
                $createdTask = lk_Transactions_Tasks::create(array_merge(['DateCreated' => date('Y-m-d h:i:s'),], $task));
                AlertController::createAlert('lk_Transactions_Tasks', $createdTask->id);
            }
            catch (\Exception $e)
            {
                Log::error(['EXCEPTION' => $e, 'task' => $task, __METHOD__ => __LINE__]);
            }
        }
    }

    /**
     * Creates the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTasks($transactionID, $sortBy = 'DateDue')
    {
        $qry = DB::table('lk_Transactions-Tasks')
                 ->leftJoin('Transactions', '`lk_Transactions-Tasks`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Tasks', '`lk_Transactions-Tasks`.Tasks_Code', '=', 'Tasks.Code')
                 ->select('Tasks.ID', 'DateDue', 'ShortName', 'Description', 'DateCompleted', 'IncludeWhen', 'RequiredSignatures', 'Provider')
                 ->whereRaw('`lk_Transactions-Tasks`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);
        $qry = Model_Parent::scopeNoTest($qry, ['Tasks', 'lk_Transactions-Tasks', 'Transactions']);

        $qry = str_replace('```', '`', $qry->toSql());

        return DB::select($qry, [$transactionID]);
    }

    /**
     * Creates the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTasksAsLookup($transactionID, $sortBy = 'DateDue', $includeAll = true)
    {
        $qry = DB::table('lk_Transactions-Tasks')
                 ->leftJoin('Transactions', '`lk_Transactions-Tasks`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Tasks', '`lk_Transactions-Tasks`.Tasks_Code', '=', 'Tasks.Code')
                 ->select('Tasks.ID as Value', DB::raw('CONCAT(ShortName," - ",Description) as Display'))
                 ->whereRaw('`lk_Transactions-Tasks`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);
        $qry = Model_Parent::scopeNoTest($qry, ['Tasks', 'lk_Transactions-Tasks', 'Transactions']);

        $qry = str_replace('```', '`', $qry->toSql());
        $rv  = DB::select($qry, [$transactionID]);
        foreach ($rv as $idx => $rec)
        {
            if (is_a($rec, 'StdClass')) $rec = TaskCombine::stdClassToArray($rec);
            if (is_a($rec, 'Collection')) $rec = TaskCombine::collectionToArray($rec);

            $rv[$idx] = $rec;
        }
        return $rv;
    }

    /**
     * Check if enough data has been stored to create Task list.
     *
     * @param integer $transactionID
     *
     * @return mixed
     */
    public static function checkForTaskListData($transactionID)
    {
     //   Log::info([__FUNCTION__, func_get_args()]);

        if (true) return true;
        $detailFields  = ['ClientRole',];
        $transaction   = TransactionCombine::fullTransaction($transactionID);
        $propertyError = false;
        if ($transaction['property']->count() < 1) $propertyError = true; //kludge, remove comment to push
        $detailErrors = [];
        //Client role for some reason is not set, need to look into this. todo
        foreach ($detailFields as $fname)
        {
            if (!isset($transaction['transaction']->{$fname})) $detailErrors[] = $fname;
        }
        session(['propertyError' => $propertyError, 'detailErrors' => $detailErrors]);
        if ($propertyError || count($detailErrors)) return false;
        return true;
    }

    /**
     * @param mixed $role If false then return array of IDs, indexed by role, else return the specific role's ID
     *
     * @return array|bool|mixed
     */
    public static function getAdHocCode($role = false)
    {
        if (!empty(self::$adHocIDs))
        {
            if (!$role) return self::$adHocIDs;
            if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
            return false;
        }

        $query          = Task::where('TaskSets_Value', 'like', '%-ah')->select('TaskSets_Value', 'ID', 'Code');
        $results        = $query->get();
        self::$adHocIDs = [];
        foreach ($results as $result)
        {
            list($theRole, $suf) = explode('-', $result->TaskSets_Value);
            self::$adHocIDs[$theRole] = $result->Code;
        }

        if (!$role) return self::$adHocIDs;
        if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
        return false;
    }

    /**
     * @param $lk_taTask_ID
     *
     * @return mixed
     */
    public static function previewTaskDueDatesChanges($lk_taTask_ID, $transactionID)
    {
        $userRoles = TransactionCombine::getUserTransactionRoles($transactionID, true);
        $taskRoles = TaskCombine::getTaskRoles($userRoles, $transactionID);

        $sql = '
        SELECT `lk_Transactions-Tasks`.ID
        , `lk_Transactions-Timeline`.ID as timelineID
        , `lk_Transactions-Tasks`.DateDue as TaskDateDue
        , `lk_Transactions-Timeline`.MilestoneDate
        ,  Tasks.Description as Task
        ,  Tasks.DateOffset as DateOffset
        ,  Timeline.Name as TimelineName
        
        FROM `lk_Transactions-Tasks` 
            LEFT JOIN `lk_Transactions-Timeline` on `lk_Transactions-Tasks`.Transactions_ID = `lk_Transactions-Timeline`.Transactions_ID 
            LEFT JOIN Tasks on `lk_Transactions-Tasks`.Tasks_Code = Tasks.Code
            LEFT JOIN Timeline on Tasks.Timeline_Code = Timeline.Code
        
        WHERE `lk_Transactions-Timeline`.ID = {{lk_Transactions-Timeline.ID}}
        AND   `lk_Transactions-Tasks`.UserRole in ({{taskRoles}})
        AND    Timeline.Name = `lk_Transactions-Timeline`.MilestoneName
        AND   `lk_Transactions-Tasks`.DateDue is not null
        AND   `lk_Transactions-Tasks`.deleted_at is null
        AND   `lk_Transactions-Timeline`.isActive = true
                ';

        $sql = str_replace('{{lk_Transactions-Timeline.ID}}', $lk_taTask_ID, $sql);
        $sql = str_replace('{{taskRoles}}', '\''.implode(',',$taskRoles).'\'',$sql);
        return DB::select($sql);
    }

    /**
     * @param $transactionID
     */
    public static function resetTasks($transactionID)
    {
        $tasks = DB::table('lk_Transactions-Tasks')
            ->leftJoin('Tasks', 'Tasks.Code', '=', 'lk_Transactions-Tasks.Tasks_Code')
            ->leftJoin('Alerts', 'Alerts.Model_ID', '=', 'lk_Transactions-Tasks.ID')
            ->where('Alerts.Model', 'lk_Transactions_Tasks')
            ->where('lk_Transactions-Tasks.Transactions_ID', $transactionID)
            ->where('Tasks.Category' ,'!=' , 'ah')
            ->update([
                'Alerts.deleted_at'                 => date('Y-m-d H:i:s'),
                'lk_Transactions-Tasks.deleted_at'  => date('Y-m-d H:i:s'),
            ]);
    }
}