/**
 * This could've been included inside of the view but there are compatibility
 * issues with the way documents is currently setup and this code. Particularly
 * if you enter Category view this code will not work.
 * @type {jQuery}
 */

let documentsTable = $('#documentsTable').DataTable({
    "order": [[1, "desc"]], //Document Name
    "columnDefs": [
        {"orderable": false, "targets": [0, 3]} //Required,Incomplete,Options
    ],
    "sDom": "ltipr"
});

$('#documentSearch').keyup(function () {
    documentsTable.search(this.value).draw();
});
