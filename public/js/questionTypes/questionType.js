

function questionTypeDisplay(questionArr,displaysArr)
{
    let button_group_html;
    let answer_html;
    function parseNote(notes)
    {
        var segments = notes.split(']');
        var notesSize = segments.length;
        segments.splice(notesSize-1,1);
        notes = {};
        for(var i=0;i<segments.length;i++)
        {
            var s = segments[i].replace('[','');
            var ea_note = s.split('|');
            notes[ea_note[0]] = ea_note[1];
        }
        return notes;
    }

    function removeIllegalQuestionnaireChars(word) {
        return word.replace(/[^a-zA-Z 0-9,.()/;:%$#@!?=+-]/g, '');
    }

    function searchNote(arr,value)
    {
        for(k in arr)
        {
            if(value == k)
            {
                return arr[k];
            }
        }
        return false;
    }

    function checkLineItems()
    {
        let allLineItemsFilledOut = true;
        $('.line-items input, .line-items textarea').each(function () {
            if(!$(this).val() || $(this).val() == '') allLineItemsFilledOut = false;
        });
        return allLineItemsFilledOut;
    }

    if(questionArr['QuestionType'] === 'checkbox')
    {
        function searchArray(arr,value)
        {

            for(k=0;k<arr.length;k++)
            {
                if(arr[k].trim() === value.trim())
                {
                    return true;
                }
            }
            if(!value)
            {
                return false;
            }
            return false;
        }
        var gridColumnsMd = 3; //size of each column out of 12
        var gridColumnsLg = 3;
        var gridColumnsSm = 4;
        var counter = 0;
        button_group_html = '<div class="checkbox-group container"><div class="row"></div></div>';
        $(".answers").append(button_group_html);
        if(questionArr['Answer'])
        {
            var answers = questionArr['Answer'].split(",");
        }
        if(questionArr['Notes'])
        {
            var notes = parseNote(questionArr['Notes']);
        }
        var startAdditionalInfoOpen = false;
        for(i = 0; i < displaysArr.displays.length; i++)
        {
            if(displaysArr.addInfo[i] === true) counter++;
            if(questionArr['Answer'])
            {
                var key = searchArray(answers,displaysArr.values[i]);
            }else key = false;
            if(key)
            {

                answer_html =
                    '<div class="col-md-'+gridColumnsMd+' col-lg-'+gridColumnsLg+' col-sm-'+gridColumnsSm+'">\n'+
                    '<input data-status="'+displaysArr.addInfo[i]+'" id="checkbox-'+displaysArr.values[i]+'" class="answer-choice" type="checkbox"  value="'+displaysArr.values[i]+'" checked="checked">\n'+
                    '<label class="checkbox-label checkbox-inline" for="checkbox-'+displaysArr.values[i]+'">'+displaysArr.displays[i]+'</label>\n'+
                    '</div>';
                if(displaysArr.addInfo[i] === true)
                {
                    startAdditionalInfoOpen = true;
                }
            }
            else
            {
                answer_html =
                    '<div class="col-md-'+gridColumnsMd+' col-lg-'+gridColumnsLg+' col-sm-'+gridColumnsSm+'">\n'+
                    '<input data-status="'+displaysArr.addInfo[i]+'" id="checkbox-'+displaysArr.values[i]+'" class="answer-choice" type="checkbox"  value="'+displaysArr.values[i]+'">\n'+
                    '<label class="checkbox-label checkbox-inline" for="checkbox-'+displaysArr.values[i]+'">'+displaysArr.displays[i]+'</label>\n'+
                    '</div>';
            }
            $('.update-button').data("type", 'checkbox');
            $(".checkbox-group .row").append(answer_html);
            $(".checkbox-group #checkbox-"+displaysArr.values[i]).data({"about":displaysArr.displays[i],"val":displaysArr.values[i], "type" : 'checkbox'});
            if(startAdditionalInfoOpen)
            {
                $('.checkbox-group').append('<textarea id="addInfo-'+displaysArr.values[i]+'" class="additionalInfo" rows="5" cols="70" placeholder="Explanation (if applicable)"></textarea>');
                if(n = searchNote(notes,displaysArr.values[i]))
                {
                    $('#addInfo-'+displaysArr.values[i]).val(n);
                }
            }

            startAdditionalInfoOpen = false;
        }
        $('.answer-choice').data("amt",counter);
    }
    if(questionArr['QuestionType'] === 'radio')
    {
        button_group_html = '<ul class="radio-group"></ul>';
        $(".answers").append(button_group_html);
        var startAdditionalInfoOpen = false;
        if(questionArr['Notes'])
        {
            var notes = parseNote(questionArr['Notes']);
        }
        for(i = 0; i < displaysArr.values.length; i++)
        {
            if(questionArr['Answer'] === displaysArr.values[i])
            {
                answer_html =
                    '<li>\n'+
                    '<input data-status="'+displaysArr.addInfo[i]+'" type="radio" id="question-'+displaysArr.values[i]+'" class="answer-choice" name="question-'+displaysArr.ID+'" value="'+displaysArr.values[i]+'" checked>\n'+
                    '<label class="radio-label" for="question-'+displaysArr.values[i]+'">'+displaysArr.displays[i]+'</label>\n'+
                    '</li>';
                if(displaysArr.addInfo[i] == true)
                {
                    startAdditionalInfoOpen = true;
                }
            }
            else
            {
                answer_html =
                    '<li>\n'+
                    '<input data-status="'+displaysArr.addInfo[i]+'" type="radio" id="question-'+displaysArr.values[i]+'" class="answer-choice" name="question-'+displaysArr.ID+'" value="'+displaysArr.values[i]+'">\n'+
                    '<label class="radio-label" for="question-'+displaysArr.values[i]+'">'+displaysArr.displays[i]+'</label>\n'+
                    '</li>';
            }


            $('.update-button').data("type", 'radio');
            $(".radio-group").append(answer_html);
            $(".radio-group li #question-"+displaysArr.values[i]).data({"about":displaysArr.displays[i],"val":displaysArr.values[i], "type" : 'radio'});
            if(startAdditionalInfoOpen)
            {
                $('.answer-section').append('<textarea id="addInfo-'+displaysArr.values[i]+'" class="additionalInfo" rows="5" cols="70" placeholder="Explanation (if applicable)"></textarea>');
            }
            if(n = searchNote(notes,displaysArr.values[i]))
            {
                $('#addInfo-'+displaysArr.values[i]).val(n);
            }
            startAdditionalInfoOpen = false;
        }

    }

    if(questionArr['QuestionType'] === 'dropdown')
    {
        button_group_html = '<div class="dropdown-group"></div>';
        $(".answers").append(button_group_html);
        answer_html_pre = '<select name="question-'+displaysArr.ID+'" class="form-control">';
        for(i = 0; i < displaysArr.values.length; i++)
        {

            if(questionArr['Answer'] === displaysArr.values[i])
            {
                //todo find out how to pre-select based on value in database
                answer_html = '<option class="answer-choice" type="radio"  value="'+displaysArr.values[i]+'" selected>'+displaysArr.displays[i]+'<br>\n';
            }
            else
            {
                answer_html = '<option class="answer-choice" type="radio"  value="'+displaysArr.values[i]+'">'+displaysArr.displays[i]+'<br>\n';
            }
            answer_html_pre += answer_html;
        }
        answer_html_pre += '</select>';
        $('.update-button').data("type", 'dropdown');
        $(".dropdown-group").append(answer_html_pre);

    }

    if(questionArr['QuestionType'] === 'text')
    {
        button_group_html = '<div class="text-group"></div>';
        $(".answers").append(button_group_html);
        if(questionArr['Answer'] !== null)
        {
            answer_html = '<input class="answer-choice input-group" type="text" name="question-'+displaysArr.ID+'" value="'+questionArr['Answer']+'"><br>';
        }
        else
        {
            answer_html = '<input class="answer-choice input-group" type="text" name="question-'+displaysArr.ID+'" value=""><br>';
        }
        $('.update-button').data("type", 'text');
        $(".text-group").append(answer_html);
    }

    if(questionArr['QuestionType'] === 'textarea')
    {
        button_group_html = '<div class="textarea-group"></div>';
        $(".answers").append(button_group_html);
        if(questionArr['Answer'] !== null)
        {
            answer_html = '<textarea rows="5" cols="70" class="answer-choice" type="text" name="question-'+displaysArr.ID+'">'+questionArr['Answer']+'</textarea>';
        }
        else
        {
            answer_html = '<textarea rows="5" cols="70" class="answer-choice" type="text" name="question-'+displaysArr.ID+'" ></textarea>';
        }
        $('.update-button').data("type", 'textarea');
        $(".textarea-group").append(answer_html);
    }

    if(questionArr['QuestionType'] === 'date')
    {
        button_group_html = '<div class="date-group"></div>';
        $(".answers").append(button_group_html);
        if(questionArr['Answer'] !== null)
        {
            answer_html = '<input class="answer-choice" type="date" name="question-'+displaysArr.ID+'" value="'+questionArr['Answer']+'">';
        }
        else
        {
            answer_html = '<input class="answer-choice" type="date" name="question-'+displaysArr.ID+'" value="">';
        }

        $('.update-button').data("type", 'date');
        $(".date-group").append(answer_html);
    }

    if(questionArr['QuestionType'] === 'time')
    {
        button_group_html = '<div class="time-group"></div>';
        $(".answers").append(button_group_html);
        if(questionArr['Answer'] !== null)
        {
            answer_html = '<input class="answer-choice" type="time" name="question-'+displaysArr.ID+'" value="'+questionArr['Answer']+'">';
        }
        else
        {
            answer_html = '<input class="answer-choice" type="time" name="question-'+displaysArr.ID+'" value="">';
        }

        $('.update-button').data("type", 'time');
        $(".time-group").append(answer_html);
    }


    if(questionArr['QuestionType'] === 'single')
    {
        button_group_html = '<div class="single-group"></div>';
        $(".answers").append(button_group_html);

        answer_html = '<div class="single-answer">'+questionArr['PossibleAnswers']+'</div>';

        $('.update-button').data("type", 'single');
        $(".single-group").append(answer_html);
    }

    if (questionArr['QuestionType'] == 'lineitemservice')
    {
        button_group_html = '<div class="line-items row"></div>';
        let line_item_service =
            '<div class="q-s-line-item col-xs-12 col-sm-12 col-md-12" style="padding: 15px;">' +
            '   <textarea name="service" type="text" placeholder="Service"></textarea>' +
            '   <input name="price" type="number" placeholder="Price">' +
            '</div>';
        let answers;
        $('.answers').append(button_group_html);
        if(questionArr['Answer'])
        {
            answers = questionArr['Answer'].split('|');
            $.each(answers, (i,ans) => {
                let values = ans.split('`');
                let service = values[0];
                let price = values[1];
                let answered_line_item =
                    '<div class="q-s-line-item col-xs-12 col-sm-12 col-md-12">' +
                    '   <textarea name="service" type="text" placeholder="Service">'+service+'</textarea>' +
                    '   <input name="price" type="number" placeholder="Price" value="'+price+'">' +
                    '</div>';
                $('.line-items').append(answered_line_item);
            });
        }
        else
        {
            answer_html = line_item_service;
        }

        $('.update-button').data("type", 'lineitemservice');
        $(".line-items").append(answer_html);

        $(document).on('keyup click', '.q-s-line-item input, .q-s-line-item textarea', function () {
           if(checkLineItems()) $('.line-items').append(line_item_service);
        });
    }

    if (questionArr['QuestionType'] == 'lineitem')
    {
        button_group_html = '<div class="line-items row"></div>';
        let line_item =
            '<div class="q-line-item col-xs-12 col-sm-12 col-md-12" style="padding: 15px;">' +
            '   <textarea type="text" placeholder=""></textarea>' +
            '</div>';
        let answers;
        $('.answers').append(button_group_html);
        if(questionArr['Answer'])
        {
            answers = questionArr['Answer'].split('|');
            $.each(answers, (i,ans) => {
                let answered_line_item =
                    '<div class="q-line-item col-xs-12 col-sm-12 col-md-12">' +
                    '   <textarea type="text" placeholder="Service">'+ans+'</textarea>' +
                    '</div>';
                $('.line-items').append(answered_line_item);
            });
        }
        else
        {
            answer_html = line_item;
        }

        $('.update-button').data("type", 'lineitem');
        $(".line-items").append(answer_html);

        $(document).on('keyup click', '.q-line-item input, .q-line-item textarea', function () {
            if(checkLineItems()) $('.line-items').append(line_item);
        });
    }

    $('input, textarea').keyup(function () {
        $(this).val(removeIllegalQuestionnaireChars($(this).val()));
    });

}

//type is a string
function filterType(type)
{
    if (type === 'radio')
    {
        return $('input:checked').val();
    }
    if (type === 'dropdown')
    {
        return $('select:input').val();
    }
    if (type === 'text' || type === 'textarea')
    {
        return $('.answer-choice').val();
    }
    if(type === 'checkbox')
    {
        allVals = [];
        $('input:checked').each(function(){
            allVals.push($(this).val());
        });

        return allVals;
    }
    if(type === 'date')
    {
        return $('.answer-choice').val();
    }
    if(type === 'time')
    {
        return $('.answer-choice').val();
    }
    if(type == 'lineitemservice')
    {
        let allVals = [];
        let line_item = $('.q-s-line-item');
        line_item.each(function () {
            let service = $(this).children('textarea[name="service"]').val();
            let price   = $(this).children('input[name="price"]').val();
            if(service && price && service != '' && price != '') allVals.push(service+'`'+price);
        });
        allVals = allVals.join('|');
        return allVals;
    }
    if(type == 'lineitem')
    {
        let allVals = [];
        let line_item = $('.q-line-item');
        line_item.each(function () {
            let item = $(this).children('textarea').val();
            if(item && item != '') allVals.push(item);
        });
        allVals = allVals.join('|');
        return allVals;
    }

    return false;
}
