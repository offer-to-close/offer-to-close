CREATE DATABASE  IF NOT EXISTS `offer-to-close` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `offer-to-close`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: offer-to-close
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AccountAccess`
--

DROP TABLE IF EXISTS `AccountAccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountAccess` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL COMMENT 'Foreign Key to Users table',
  `Accounts_ID` int(11) DEFAULT NULL COMMENT 'Foreign Key to Accounts table',
  `Type` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Type of user this is: Lookup = lu_AccountTypes',
  `Access` int(11) DEFAULT NULL COMMENT 'The is the sum of values from lu_Accesses table',
  `AccessCode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'API password',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `acct_acc_acc_code_unq` (`AccessCode`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Accounts`
--

DROP TABLE IF EXISTS `Accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Accounts` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Username` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Equal to email',
  `Password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateLastLogin` timestamp NULL DEFAULT NULL,
  `DateClosed` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `accounts_username_unique` (`Username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Agents`
--

DROP TABLE IF EXISTS `Agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agents` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `License` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'State license number',
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BrokerageOffices_ID` int(11) DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `agents_namefirst_index` (`NameFirst`),
  KEY `agents_namelast_index` (`NameLast`),
  KEY `agents_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=729264 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrokerageOffices`
--

DROP TABLE IF EXISTS `BrokerageOffices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrokerageOffices` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Brokers_ID` int(11) DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `brokerageoffices_name_index` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=47476 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Brokerages`
--

DROP TABLE IF EXISTS `Brokerages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brokerages` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `License` int(11) DEFAULT NULL,
  `isVerified` tinyint(1) NOT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `brokerages_name_index` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Brokers`
--

DROP TABLE IF EXISTS `Brokers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brokers` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `Brokerages_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `brokers_namefirst_index` (`NameFirst`),
  KEY `brokers_namelast_index` (`NameLast`),
  KEY `brokers_namefull_index` (`NameFull`),
  KEY `brokers_license_index` (`License`)
) ENGINE=MyISAM AUTO_INCREMENT=213576 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Buyers`
--

DROP TABLE IF EXISTS `Buyers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Buyers` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `Transactions_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `buyers_namefirst_index` (`NameFirst`),
  KEY `buyers_namelast_index` (`NameLast`),
  KEY `buyers_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DocumentFilters`
--

DROP TABLE IF EXISTS `DocumentFilters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocumentFilters` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DocumentSets_Value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Foreign key to lu_DocumentSets.Value',
  `Documents_ID` int(11) NOT NULL COMMENT 'Foreign key to Documents',
  `IncludeWhen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Condition for when the document should be included. If blank, then optional, if \\"{required}\\" then always',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_doc_ds_docset_doc_id_unq` (`DocumentSets_Value`,`Documents_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Documents`
--

DROP TABLE IF EXISTS `Documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Documents` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `State` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Category` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The type of document this is e.g. disclosures, contracts, etc. lu_DocumentTypes',
  `Order` int(11) DEFAULT NULL COMMENT 'Part of OTC document code and to order the document in the stack',
  `ShortName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Short name or abbreviation used by industry',
  `Description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The full name of the document',
  `Provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The member of the transaction party that initializes the document, lu_TranasctionMembers',
  `RequiredSignatures` int(11) DEFAULT NULL COMMENT 'Who in the transaction party must sign the document',
  `IncludeWhen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'TBD',
  `Timeline_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Timeline Milestone as basis for calculating default date',
  `DateOffset` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The number of days from the Timeline Milestone date. ',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `documents_state_category_order_unique` (`State`,`Category`,`Order`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Escrows`
--

DROP TABLE IF EXISTS `Escrows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Escrows` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `escrows_namefirst_index` (`NameFirst`),
  KEY `escrows_namelast_index` (`NameLast`),
  KEY `escrows_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Invoices`
--

DROP TABLE IF EXISTS `Invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Invoices` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL COMMENT 'Author of invoice',
  `BillTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BillToAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInvoiced` date DEFAULT NULL,
  `PaymentTerms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateDue` date DEFAULT NULL,
  `SubTotal` double(8,2) DEFAULT NULL,
  `Total` double(8,2) DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Loans`
--

DROP TABLE IF EXISTS `Loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Loans` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `loans_namefirst_index` (`NameFirst`),
  KEY `loans_namelast_index` (`NameLast`),
  KEY `loans_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MailTemplates`
--

DROP TABLE IF EXISTS `MailTemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailTemplates` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Category` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'What is the email about',
  `Subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email subject',
  `Message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'View Name or if first chars are *** actual message',
  `Attachment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Path to attachment',
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data - identifiable code',
  `ToRole` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `FromRole` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `CcRoles` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'comma separated values',
  `BccRoles` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'comma separated values',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `mail_template_from_category` (`FromRole`,`Category`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Offers`
--

DROP TABLE IF EXISTS `Offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Offers` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TransactionCoordinators_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to TransactionCoordinators',
  `Transactions_ID` int(11) DEFAULT NULL COMMENT 'foreign key to Transactions',
  `DateOffer` date DEFAULT NULL COMMENT 'Date transaction actually began',
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PriceListing` double(12,2) DEFAULT NULL,
  `PriceAsking` double(12,2) DEFAULT NULL,
  `ContingencyInspection` int(11) DEFAULT '0' COMMENT 'in Days',
  `ContingencyAppraisal` int(11) DEFAULT '0' COMMENT 'in Days',
  `ContingencyLoan` int(11) DEFAULT '0' COMMENT 'in Days',
  `AdditionalDetails` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isFee` tinyint(1) DEFAULT NULL COMMENT 'is there a fee',
  `Fee` double(8,2) DEFAULT '0.00' COMMENT 'fee amount',
  `isPaid` tinyint(1) DEFAULT NULL COMMENT 'is the fee paid',
  `Invoices_ID` int(11) DEFAULT NULL COMMENT 'foreign key to Invoices_ID',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `offers_transactioncoordinators_id_index` (`TransactionCoordinators_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Properties`
--

DROP TABLE IF EXISTS `Properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Properties` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `County` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MetroArea` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ParcelNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'also called APN',
  `hasHOA` tinyint(1) DEFAULT NULL,
  `PropertyType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_PropertyTypes; e.g. condo | house | etc.',
  `YearBuilt` int(11) DEFAULT NULL,
  `hasSeptic` tinyint(1) DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Questions`
--

DROP TABLE IF EXISTS `Questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Questions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AnswerElement` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ChoiceSource` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SuccessCriteria` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Sellers`
--

DROP TABLE IF EXISTS `Sellers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sellers` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `Transactions_ID` int(11) NOT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `sellers_namefirst_index` (`NameFirst`),
  KEY `sellers_namelast_index` (`NameLast`),
  KEY `sellers_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TaskFilters`
--

DROP TABLE IF EXISTS `TaskFilters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TaskFilters` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TaskSets_Value` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Foreign key to lu_TaskSets.Value',
  `Tasks_ID` int(11) NOT NULL COMMENT 'Foreign key to Tasks',
  `IncludeWhen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Condition for when the document should be included. If blank, then optional, if \\"{required}\\" then always',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_task_ds_taskset_task_id_unq` (`TaskSets_Value`,`Tasks_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Tasks`
--

DROP TABLE IF EXISTS `Tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tasks` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `State` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Category` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The type of document this is e.g. disclosures, contracts, etc. lu_DocumentTypes',
  `Order` int(11) DEFAULT NULL COMMENT 'Part of OTC document code and to order the document in the stack',
  `ShortName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Short name or abbreviation used by industry',
  `Description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The full name of the document',
  `IncludeWhen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'TBD',
  `Timeline_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Timeline Milestone as basis for calculating default date',
  `DateOffset` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The number of days from the Timeline Milestone date. ',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `tasks_state_category_order_unique` (`State`,`Category`,`Order`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Timeline`
--

DROP TABLE IF EXISTS `Timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Timeline` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'The name of this milestone. E.g. open, close, inspection.',
  `DaysOffset` int(11) DEFAULT NULL COMMENT 'The offset is how many days from the starting event this milestone is. Positive days are measured from open and negative days are measured from close',
  `isStartEvent` tinyint(1) DEFAULT NULL COMMENT 'If true this is the event from which all others are measured. DaysOffset will be ignored or taken as zero',
  `isEndEvent` tinyint(1) DEFAULT NULL COMMENT 'If true this is the last milestone it is typically COE (close of escrow). It is the event from which all events with negative DaysOffset values are measured. DaysOffset will be ignored or taken as zero',
  `mustBeBusinessDay` tinyint(1) DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `milestonereferences_code_unique` (`Code`),
  UNIQUE KEY `milestonereferences_name_unique` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TimelineDetails`
--

DROP TABLE IF EXISTS `TimelineDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TimelineDetails` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Sender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Reciever` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PropertyAddress` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MutualAcceptanceDate` date NOT NULL,
  `DepositDueDate` date DEFAULT NULL,
  `DisclosureDueDate` date DEFAULT NULL,
  `InspectionContingency` date DEFAULT NULL,
  `AppraisalContingency` date DEFAULT NULL,
  `LoanContingency` date DEFAULT NULL,
  `CloseOfEscrowDate` date DEFAULT NULL,
  `PossessionOnProperty` date DEFAULT NULL,
  `LinkID` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Unique id to a link',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `timelinedetails_linkid_unique` (`LinkID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Titles`
--

DROP TABLE IF EXISTS `Titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Titles` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `titles_namefirst_index` (`NameFirst`),
  KEY `titles_namelast_index` (`NameLast`),
  KEY `titles_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TransactionCoordinators`
--

DROP TABLE IF EXISTS `TransactionCoordinators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionCoordinators` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Company` int(11) DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `transactioncoordinators_namefirst_index` (`NameFirst`),
  KEY `transactioncoordinators_namelast_index` (`NameLast`),
  KEY `transactioncoordinators_namefull_index` (`NameFull`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Transactions`
--

DROP TABLE IF EXISTS `Transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transactions` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BuyersTransactionCoordinators_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to TransactionCoordinators',
  `SellersTransactionCoordinators_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to TransactionCoordinators',
  `Offers_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Offers',
  `Properties_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Properties',
  `BuyersAgent_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Agents',
  `SellersAgent_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Agents',
  `Escrows_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Escrows table',
  `Titles_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Titles table',
  `Loans_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Loans table',
  `ClientRole` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_TransactionMembers => s | b | ba | sa | h',
  `DateStart` date DEFAULT NULL COMMENT 'Date transaction actually began',
  `DateStartContract` date DEFAULT NULL COMMENT 'Date the contract with OTC began',
  `EscrowLength` int(11) DEFAULT NULL COMMENT 'Planned length of escrow',
  `DateEnd` date DEFAULT NULL COMMENT 'Date transaction ends, escrow closes or otherwise',
  `isBuyerATrust` tinyint(1) DEFAULT NULL,
  `isSellerATrust` tinyint(1) DEFAULT NULL,
  `SaleType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_SaleTypes; e.g. normal | REO | etc.',
  `LoanType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_LoanTypes; e.g. VHA | etc.',
  `willRentBack` tinyint(1) DEFAULT NULL,
  `RentBackLength` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PurchasePrice` int(11) DEFAULT NULL,
  `CommissionType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_CommissionTypes e.g. flat | percentage',
  `BuyerCommission` double(8,2) DEFAULT NULL COMMENT 'The amount the buyer''s side gets. If % store a decimal < 1',
  `SellerCommission` double(8,2) DEFAULT NULL COMMENT 'The amount the seller''s side gets. If % store a decimal < 1',
  `ReferralFeeType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lu_ReferralFeeTypes e.g. flat | percentage',
  `BuyerReferralFee` double(8,2) DEFAULT NULL COMMENT 'The amount the buyer''s side gets. If % store a decimal < 1',
  `SellerReferralFee` double(8,2) DEFAULT NULL COMMENT 'The amount the seller''s side gets. If % store a decimal < 1',
  `hasBuyerWaivedInspection` tinyint(1) DEFAULT NULL,
  `hasBuyerSellContingency` tinyint(1) DEFAULT NULL,
  `hasSellerBuyContingency` tinyint(1) DEFAULT NULL,
  `hasInspectionContingency` tinyint(1) DEFAULT NULL,
  `needsTermiteInspection` tinyint(1) DEFAULT NULL,
  `willTenantsRemain` tinyint(1) DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `transactions_buyerstransactioncoordinators_id_index` (`BuyersTransactionCoordinators_ID`),
  KEY `transactions_sellerstransactioncoordinators_id_index` (`SellersTransactionCoordinators_ID`),
  KEY `transactions_properties_id_index` (`Properties_ID`),
  KEY `transactions_buyersagent_id_index` (`BuyersAgent_ID`),
  KEY `transactions_sellersagent_id_index` (`SellersAgent_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserManagement`
--

DROP TABLE IF EXISTS `UserManagement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserManagement` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `AccountTypes` int(11) DEFAULT NULL,
  `DateLastLoggedIn` date DEFAULT NULL,
  `isExpelled` tinyint(1) DEFAULT NULL,
  `isGuest` tinyint(1) DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZipCityCountyState`
--

DROP TABLE IF EXISTS `ZipCityCountyState`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZipCityCountyState` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Zip` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `City` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `County` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CountyNumber` int(11) DEFAULT NULL COMMENT 'Don''t know how this is used',
  `State` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `zccs_zip_county_unq` (`Zip`,`County`),
  KEY `zccs_zip_city_unq` (`Zip`,`City`),
  KEY `zccs_zip_state_unq` (`Zip`,`State`)
) ENGINE=MyISAM AUTO_INCREMENT=160209 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bag_TransactionDocuments`
--

DROP TABLE IF EXISTS `bag_TransactionDocuments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bag_TransactionDocuments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lk_Transactions-Documents_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to Transactions',
  `DocumentPath` varchar(200) DEFAULT NULL COMMENT 'Where the document is stored',
  `OriginalDocumentName` varchar(200) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT NULL COMMENT 'Is TRUE when this document is to be included in the transactions',
  `isComplete` tinyint(4) DEFAULT NULL COMMENT 'Is TRUE when this document is considered to be complete',
  `SignedBy` varchar(45) DEFAULT NULL COMMENT 'The list of roles that have signed this document',
  `ApprovedByTC_ID` int(11) DEFAULT NULL COMMENT 'The ID of the TransactionCoordinator who approved this document for inclusion in transaction',
  `UploadedByRole` varchar(45) DEFAULT NULL COMMENT 'Role code of who uploaded the document',
  `UploadedBy_ID` varchar(45) DEFAULT NULL COMMENT 'ID (within appropriate role table) of the person that uploaded the document',
  `DateUploaded` timestamp NULL DEFAULT NULL,
  `DateApproved` timestamp NULL DEFAULT NULL,
  `isTest` tinyint(4) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Notes` varchar(45) DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DocumentStack` (`lk_Transactions-Documents_ID`,`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='For each Transaction-Document combination there can be several versions of a specific documents. This table manages those documents by indicating which of the varioius versions in complete and which are to be included in the completed transaction.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lk_Invoices-Items`
--

DROP TABLE IF EXISTS `lk_Invoices-Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lk_Invoices-Items` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Invoices_ID` int(11) NOT NULL COMMENT 'Foreign key to Invoices',
  `Description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Quantity` double(8,2) DEFAULT NULL,
  `Rate` double(8,2) DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `lk_invoices_items_id_index` (`Invoices_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lk_Transactions-Documents`
--

DROP TABLE IF EXISTS `lk_Transactions-Documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lk_Transactions-Documents` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Transactions_ID` int(11) NOT NULL COMMENT 'Foreign key to Transactions',
  `Documents_ID` int(11) NOT NULL COMMENT 'Foreign key to Documents',
  `isIncluded` tinyint(1) DEFAULT NULL,
  `DateDue` timestamp NULL DEFAULT NULL,
  `DateCompleted` timestamp NULL DEFAULT NULL,
  `ApprovedByTC_ID` int(11) DEFAULT NULL COMMENT 'Foreign key to TransactionCoordinators table',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_tran_docs_st_tran_id_doc_id_unq` (`Transactions_ID`,`Documents_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=253 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lk_Transactions-Tasks`
--

DROP TABLE IF EXISTS `lk_Transactions-Tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lk_Transactions-Tasks` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Transactions_ID` int(11) NOT NULL COMMENT 'Foreign key to Transactions',
  `TaskFilters_ID` int(11) NOT NULL COMMENT 'Foreign key to Tasks',
  `Description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasAlerts` tinyint(1) DEFAULT NULL,
  `DateDue` timestamp NULL DEFAULT NULL,
  `DateCompleted` timestamp NULL DEFAULT NULL,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_tran_tasks_st_tran_id_task_id_unq` (`Transactions_ID`,`TaskFilters_ID`),
  UNIQUE KEY `lk_tran_tasks_st_tran_id_taskFil_id_unq` (`Transactions_ID`,`TaskFilters_ID`),
  KEY `lk_tran_tasks_st_tran_id_taskFil_id_index` (`Transactions_ID`,`TaskFilters_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=582 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lk_Transactions-Timeline`
--

DROP TABLE IF EXISTS `lk_Transactions-Timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lk_Transactions-Timeline` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Transactions_ID` int(11) NOT NULL COMMENT 'Foreign key to Transactions',
  `MilestoneName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Foreign key to Timeline',
  `MilestoneDate` timestamp NOT NULL,
  `isComplete` tinyint(1) NOT NULL DEFAULT '0',
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_transactions_timeline_transactions_id_milestonename_unique` (`Transactions_ID`,`MilestoneName`)
) ENGINE=MyISAM AUTO_INCREMENT=515 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lk_Users-Roles`
--

DROP TABLE IF EXISTS `lk_Users-Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lk_Users-Roles` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Users_ID` int(11) NOT NULL COMMENT 'Foreign key to Users',
  `Role` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Foreign key to UserRoles lookup table value',
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lk_users_roles_users_id_role_unique` (`Users_ID`,`Role`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_Mail`
--

DROP TABLE IF EXISTS `log_Mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_Mail` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL COMMENT 'Only used for out-going mail - Foreign Key to users table',
  `SenderRole` int(11) DEFAULT NULL COMMENT 'Only used for out-going mail',
  `wasSent` tinyint(1) DEFAULT NULL COMMENT 'From OTC',
  `wasReceived` tinyint(1) DEFAULT NULL COMMENT 'To OTC',
  `To` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `From` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `Cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `Bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `ReplyTo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data',
  `DateTransmitted` timestamp NULL DEFAULT NULL COMMENT 'Email meta data',
  `Category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'What is the email about',
  `Subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email subject',
  `Message` blob COMMENT 'Email text',
  `Attachment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Path to attachment',
  `Code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Email meta data - identifiable code',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NULL DEFAULT NULL,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `log_mail_category_date` (`Category`,`DateTransmitted`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_Accesses`
--

DROP TABLE IF EXISTS `lu_Accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_Accesses` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_accesses_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_AccountTypes`
--

DROP TABLE IF EXISTS `lu_AccountTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_AccountTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_accounttypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_CommissionTypes`
--

DROP TABLE IF EXISTS `lu_CommissionTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_CommissionTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_commissiontypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_DataSourceTypes`
--

DROP TABLE IF EXISTS `lu_DataSourceTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_DataSourceTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_datasourcetypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_DocumentSets`
--

DROP TABLE IF EXISTS `lu_DocumentSets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_DocumentSets` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_documentsets_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_DocumentTypes`
--

DROP TABLE IF EXISTS `lu_DocumentTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_DocumentTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_documenttypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_EmailFormats`
--

DROP TABLE IF EXISTS `lu_EmailFormats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_EmailFormats` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_emailformats_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_InputElements`
--

DROP TABLE IF EXISTS `lu_InputElements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_InputElements` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_inputelements_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_LoanTypes`
--

DROP TABLE IF EXISTS `lu_LoanTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_LoanTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_loantypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_PropertyTypes`
--

DROP TABLE IF EXISTS `lu_PropertyTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_PropertyTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an arbitrary sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_propertytypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_ReferralFeeTypes`
--

DROP TABLE IF EXISTS `lu_ReferralFeeTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_ReferralFeeTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_referralfeetypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_SaleTypes`
--

DROP TABLE IF EXISTS `lu_SaleTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_SaleTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_saletypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_TaskCategories`
--

DROP TABLE IF EXISTS `lu_TaskCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_TaskCategories` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_taskcategories_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_TaskSets`
--

DROP TABLE IF EXISTS `lu_TaskSets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_TaskSets` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_tasksets_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_UserRoles`
--

DROP TABLE IF EXISTS `lu_UserRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_UserRoles` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `isLoginRole` tinyint(1) DEFAULT NULL COMMENT 'Is this a role you can login as?',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_userroles_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lu_UserTypes`
--

DROP TABLE IF EXISTS `lu_UserTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lu_UserTypes` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Value` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The value that gets stored',
  `Display` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'What gets displayed for this value',
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Order` double(8,2) DEFAULT '1.00' COMMENT 'optional field that can be used to create an ''arbitrary'' sort order.',
  `ValueInt` int(11) DEFAULT NULL COMMENT 'field contains power of 2 value used for assigned multiple members to one field.',
  `isTest` tinyint(1) DEFAULT NULL COMMENT 'Standard table field used for testing',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `lu_usertypes_value_unique` (`Value`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/images/avatar.jpg',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-13  8:23:39
