<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentAccessDefaultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentAccessDefaults', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('State')->index()->default('CA');
            $table->integer('Documents_ID')->index()->comment="Foreign key to Documents table";
            $table->string('PovRole')->comment="Values come from list of transaction roles";
            $table->string('TransactionRole')->comment="Values come from list of transaction roles";
            $table->integer('AccessSum')->comment="Sum of the Document Access Action values that describe what the POV Role can do with the document";

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentAccessDefaults');
    }
}
