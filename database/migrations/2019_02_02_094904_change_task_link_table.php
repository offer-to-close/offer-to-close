<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTaskLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->string('Tasks_Code', 20)->after('Transactions_ID')->default('*')->comment('Foreign key to Tasks table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->dropColumn('Tasks_Code');
        });
    }
}
