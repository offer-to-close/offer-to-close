<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDisclosureAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__.PHP_EOL;
        Schema::rename('DisclosureAnswers', 'QuestionnaireAnswers');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('QuestionnaireAnswers', 'DisclosureAnswers');
    }
}
