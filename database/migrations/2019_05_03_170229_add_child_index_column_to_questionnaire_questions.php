<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildIndexColumnToQuestionnaireQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->string('ChildIndex')->after('FriendlyIndex')->nullable()->comment('A short reference to locate the question in a disclosure, agreement, etc. Example: 8a');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->dropColumn('ChildIndex');
        });
    }
}
