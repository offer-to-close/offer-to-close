<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContingencyFieldsToTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->boolean('hasLoanContingency')->after('hasInspectionContingency')->nullable();
            $table->boolean('hasAppraisalContingency')->after('hasInspectionContingency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->dropColumn('hasAppraisalContingency');
            $table->dropColumn('hasLoanContingency');
        });
    }
}
