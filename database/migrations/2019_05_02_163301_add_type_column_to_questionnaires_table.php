<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnToQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__.PHP_EOL;
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->string('Type')->after('ID')->comment('What type of Questionnaire this is: disclosure, document, etc.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->dropColumn('Type');
        });
    }
}
