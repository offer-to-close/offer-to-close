<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogSignaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_Signatures', function (Blueprint $table)
        {
            $table->increments('ID');
            $table->string('Documents_Code')->nullable()->comment='Foreign key to Documents.';
            $table->integer('Transactions_ID')->nullable()->comment="Foreign key to Transactions";
            $table->integer('SourceDocument_bag_ID')->nullable()->comment='bag_ID of the Document to be signed.';
            $table->longText('Envelope_ID')->nullable()->comment('ID of the envelope.');
            $table->integer('Users_ID')->nullable()->comment='Foreign Key to users table';
            $table->string('Event')->nullable()->comment('The action the signer took on the document. ');
            $table->string('AccountName')->nullable()->comment='Name of the user account who initiated signature.';
            $table->string('AccountEmail')->nullable()->comment='Email of the user account who initiated signature.';
            $table->string('RecipientName')->nullable()->comment='Name of the Recipient';
            $table->string('RecipientEmail')->nullable()->comment='Email of the Recipient';
            $table->string('ClientUserID')->nullable()->comment='Base64 encoded [Users_ID"-"TransactionID]';
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_Signatures');
    }
}
