<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateAccountAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AccountAccess', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('Users_ID')->nullable()->comment='Foreign Key to Users table';
            $table->integer('Accounts_ID')->nullable()->comment='Foreign Key to Accounts table';
            $table->string('Type', 5)->nullable()->comment='Type of user this is: Lookup = lu_AccountTypes';
            $table->integer('Access')->nullable()->comment='The is the sum of values from lu_Accesses table';
            $table->string('AccessCode', 50)->nullable()->comment='API password';

            MigrationHelpers::assignStandardSwahFields($table);

            $table->unique('AccessCode', 'acct_acc_acc_code_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AccountAccess');
    }
}
