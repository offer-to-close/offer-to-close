<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleIDField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Users-Roles', function (Blueprint $table) {
            $table->integer('Role_ID')->after('Role')->default(0)->comment('Foreign key to the appropriate role table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Users-Roles', function (Blueprint $table) {
            $table->dropColumn('Role_ID');
        });
    }
}
