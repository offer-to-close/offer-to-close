<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseTemplateFieldToLogSignaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_Signatures', function (Blueprint $table) {
            $table->boolean('useTemplate')->after('isTest')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_Signatures', function (Blueprint $table) {
            $table->dropColumn('useTemplate');
        });
    }
}
