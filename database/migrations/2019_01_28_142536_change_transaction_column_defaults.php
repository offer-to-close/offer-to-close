<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionColumnDefaults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try
        {
            Schema::table('Transactions', function (Blueprint $table)
            {
                $table->boolean('BuyersTransactionCoordinators_ID')->default(0)->change();
                $table->boolean('SellersTransactionCoordinators_ID')->default(0)->change();
                $table->boolean('BuyersAgent_ID')->default(0)->change();
                $table->boolean('SellersAgent_ID')->default(0)->change();
                $table->boolean('Escrows_ID')->default(0)->change();
                $table->boolean('Loans_ID')->default(0)->change();
                $table->boolean('Titles_ID')->default(0)->change();
            });
        }
        catch (Exception $e){echo '-';}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
