<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContingencyFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->smallInteger('hasInspectionContingency')->change();
            $table->smallInteger('hasAppraisalContingency')->change();
            $table->smallInteger('hasLoanContingency')->change();
            $table->smallInteger('hasBuyerSellContingency')->change();
            $table->smallInteger('hasSellerBuyContingency')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            //
        });
    }
}
