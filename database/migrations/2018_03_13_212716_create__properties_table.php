<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Properties', function (Blueprint $table) {
            $table->increments('ID');

            MigrationHelpers::assignStandardAddressFields($table);

            $table->string('County')->nullable();
            $table->string('MetroArea')->nullable();
            $table->string('ParcelNumber')->nullable()->comment='also called APN';

            $table->boolean('hasHOA')->nullable();
            $table->string('PropertyType')->nullable()->comment = 'lu_PropertyTypes; e.g. condo | house | etc.';
            $table->integer('YearBuilt')->nullable();
            $table->boolean('hasSeptic')->nullable();

            MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Properties');
    }
}
