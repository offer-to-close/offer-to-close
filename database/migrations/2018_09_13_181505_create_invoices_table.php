<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Invoices', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('users_id')->comment="Author of invoice";
            $table->string('BillTo')->nullable();
            $table->date('DateInvoiced')->nullable();
            $table->string('PaymentTerms')->nullable();
            $table->date('DateDue')->nullable();
            $table->float('SubTotal')->nullable();
            $table->float('Total')->nullable();

            MigrationHelpers::assignStandardSwahFields($table);
        });

        //then set autoincrement to 1001
        DB::update('ALTER TABLE Invoices AUTO_INCREMENT = 1001;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Invoices');
    }
}
