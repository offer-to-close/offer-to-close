<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixFieldNamesContactRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ContactRequests', function (Blueprint $table) {
            $table->integer('ResponderUsers_ID')->nullable()->after('haveContacted')->comment('The users.id of the person you closed the contact task');
            $table->timestamp('DateClosed')->nullable()->after('haveContacted');
            $table->timestamp('DateOpened')->nullable()->after('haveContacted');
            $table->boolean('isClosed')->default(0)->after('haveContacted');

            $table->renameColumn('RequestedRole', 'Role');
            $table->renameColumn('haveContacted', 'startedEngagement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ContactRequests', function (Blueprint $table) {
            $table->renameColumn('Role', 'RequestedRole');
            $table->renameColumn('startedEngagement', 'haveContacted');

            $table->dropColumn('ResponderUsers_ID');
            $table->dropColumn('DateClosed');
            $table->dropColumn('DateOpened');
            $table->dropColumn('isClosed');
        });
    }
}
