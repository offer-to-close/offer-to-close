<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeApprovedByTcIdInLkTransDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table `lk_Transactions-Documents` change column `ApprovedByTC_ID` `ApprovedByUsers_ID` integer(11) null');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table `lk_Transactions-Documents` change column `ApprovedByUsers_ID` `ApprovedByTC_ID` integer(11) null');
    }
}
