<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table)
        {
            $table->integer('OwnedByUsers_ID')->after('Status')->default(0)->comment('The user with control authority over transaction. Foreign key to users.id table.field');
            $table->integer('CreatedByUsers_ID')->after('Status')->default(0)->comment('The user that created the transaction. Foreign key to users.id table.field');
            $table->string('ClientRole', 20)->change();
        });
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->dropColumn('CreatedByUsers_ID');
            $table->dropColumn('OwnedByUsers_ID');
        });
    }}
