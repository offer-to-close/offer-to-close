<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::table('Transactions', function (Blueprint $table) {
            $table->renameColumn('DateStartContract', 'DateOfferPrepared');
            $table->renameColumn('DateStart', 'DateAcceptance');
            $table->boolean('isClientAgent')->default(true)->after('OwnedByUsers_ID')->comment('');
            $table->string('Side')->default('')->after('OwnedByUsers_ID')->comment('b if on buyers\'s side, s if on seller\'s side');
            $table->integer('DealRooms_ID')->default(0)->after('willTenantsRemain')->comment('Foreign key to DealRooms table');
            $table->integer('ShareRooms_ID')->default(0)->after('willTenantsRemain')->comment('Foreign key to ShareRooms table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::table('Transactions', function (Blueprint $table) {
            $table->renameColumn('DateOfferPrepared', 'DateStartContract');
            $table->renameColumn('DateAcceptance', 'DateStart');
            $table->dropColumn('isClientAgent');
            $table->dropColumn('Side');
            $table->dropColumn('DealRooms_ID');
            $table->dropColumn('ShareRooms_ID');
        });
    }
}
