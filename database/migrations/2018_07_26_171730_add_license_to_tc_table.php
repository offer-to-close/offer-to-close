<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseToTcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TransactionCoordinators', function (Blueprint $table)
        {
                $table->integer('License')->after('NameLast')->nullable();
                $table->integer('Company')->after('NameLast')->nullable();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TransactionCoordinators', function (Blueprint $table) {
            $table->dropColumn('License');
            $table->dropColumn('Company');
        });
    }
}
