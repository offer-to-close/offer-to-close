<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisclosureAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DisclosureAnswers', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('lk_Transactions-Disclosures_ID')->comment('Foreign key to lk_Transactions_Disclosures');
            $table->integer('DisclosureQuestions_ID')->comment('Foreign key to the DisclosureQuestions');
            $table->string('Answer')->nullable()->comment('The answer provided to the question');
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DisclosureAnswers');
    }
}
