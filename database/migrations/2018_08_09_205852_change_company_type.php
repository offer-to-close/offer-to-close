<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCompanyType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Escrows', function (Blueprint $table) {
            $table->string('Company')->change();
        });

        Schema::table('Loans', function (Blueprint $table) {
            $table->string('Company')->change();
        });

        Schema::table('Titles', function (Blueprint $table) {
            $table->string('Company')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
