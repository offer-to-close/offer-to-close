<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentFilters', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('DocumentSets_Value')->comment='Foreign key to lu_DocumentSets.Value';
            $table->integer('Documents_ID')->comment='Foreign key to Documents';
            $table->string('IncludeWhen')->nullable()->comment='Condition for when the document should be included. If blank, then optional, if \"{required}\" then always';

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->unique(['DocumentSets_Value', 'Documents_ID'], 'lk_doc_ds_docset_doc_id_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentFilters');
    }
}
