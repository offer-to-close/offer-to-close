<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__ . PHP_EOL;

        Schema::create('DealRooms', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('SellersTransactions_ID')->comment="Foreign key to Transactions table";
            $table->integer('BuyersTransactions_ID')->comment="Foreign key to Transactions table";
            $table->string('CreatedBySide')->comment('b if room initiated by buyer\'s side, s if initiated by seller\'s side');

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::dropIfExists('DealRooms');
    }
}
