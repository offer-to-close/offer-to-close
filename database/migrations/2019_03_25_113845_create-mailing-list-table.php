<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MailingList', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Email')->nullable(FALSE)->unique();
            $table->string('Subject')->nullable()->comment('FIRST TRANSACTION SPECIAL');
            $table->string('Reference')->nullable()->comment('Where did this email come from?');
            $table->mediumText('CouponCode')->nullable()->comment('Coupon code if any.');
            $table->boolean('isConfirmed')->default(0)->comment('Is this email confirmed?');
            $table->boolean('isActive')->default(1);
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MailingList');
    }
}
