<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocIdToDocCodeInDocumentFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentFilters', function (Blueprint $table) {
            $table->string('Documents_Code',20)->default('*')->after('DocumentSets_Value')->comment('Foreign key to the Documents.Code field');
            $table->dropUnique('lk_doc_ds_docset_doc_id_unq');
            $table->dropColumn('Documents_ID');
            $table->index(['DocumentSets_Value', 'Documents_Code'], 'lk_doc_ds_docset_doc_code_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentFilters', function (Blueprint $table) {
            $table->integer('Documents_ID')->default('0')->after('DocumentSets_Value')->comment('Foreign key to the Documents.ID field');
            $table->dropIndex('lk_doc_ds_docset_doc_code_unq');
            $table->dropColumn('Documents_Code');
            $table->unique(['DocumentSets_Value', 'Documents_ID'], 'lk_doc_ds_docset_doc_id_unq');
        });
    }
}
