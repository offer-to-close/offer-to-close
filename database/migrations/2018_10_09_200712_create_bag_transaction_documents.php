<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBagTransactionDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bag_TransactionDocuments', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->integer('lk_Transactions-Documents_ID')->comment = "Foreign key";
            $table->string('DocumentPath', 200)->nullable();
            $table->string('OriginalDocumentName', 200)->nullable();
            $table->boolean('isActive')->nullable();
            $table->boolean('isComplete')->nullable();
            $table->string('SignedBy')->nullable();
            $table->integer('ApprovedByTC_ID')->nullable();
            $table->string('UploadedByRole')->nullable();
            $table->integer('UploadedBy_ID')->nullable();
            $table->timestamp('DateUploaded')->nullable();
            $table->timestamp('DateApproved')->nullable();

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bag_TransactionDocuments');
    }
}
