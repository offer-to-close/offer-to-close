<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultsBagtrandoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try
        {
            Schema::table('bag_TransactionDocuments', function (Blueprint $table)
            {
                $table->boolean('isTest')->default(0)->change();
                $table->boolean('isActive')->default(0)->change();
                $table->boolean('isComplete')->default(0)->change();
                $table->boolean('isShared')->default(0)->change();
            });
        }
        catch (Exception $e){echo '-';}

        $tables = ['Transactions','AccountAccess','Accounts','AddressOverride','Agents','BrokerageOffices','Brokerages',
                   'Brokers','Buyers',
                   'DealRooms','DisclosureAnswers','DisclosureQuestions','Disclosures','DocumentAccessDefaults',
                   'DocumentFilters','DocumentTransferDefaults','Documents','Escrows','Guests','Invoices','Loans',
                   'LoginInvitations','MailTemplates','Offers','Properties','Questions','Sellers','ShareRooms',
                   'TaskFilters','Tasks','Timeline','Titles','TransactionCoordinators',
                   'ZipCityCountyState',
                   'lk_Transactions-Tasks',
                   'lk_Transactions-Timeline','lk_TransactionsDocumentAccess','lk_TransactionsDocumentTransfers',
                   'lk_Users-Roles','lk_UsersTypes','log_Mail','log_Reports','lu_Accesses','lu_AccountTypes',
                   'lu_CommissionTypes','lu_DataSourceTypes','lu_DocumentAccessActions','lu_DocumentAccessRoles',
                   'lu_DocumentSets','lu_DocumentTypes','lu_EmailFormats','lu_InputElements','lu_LoanTypes',
                   'lu_PropertyTypes','lu_ReferralFeeTypes','lu_ReportCategories','lu_SaleTypes','lu_TaskCategories',
                   'lu_TaskSets','lu_UserRoles','lu_UserTypes'];

        foreach ($tables as $table)
        {
            try
            {
                Schema::table($table, function (Blueprint $table)
                {
                    $table->boolean('isTest')->default(0)->change();
                });
            }
            catch (Exception $e){echo '-';}
            echo $table . PHP_EOL;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
