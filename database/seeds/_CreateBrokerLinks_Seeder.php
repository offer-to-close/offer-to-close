<?php

use Illuminate\Database\Seeder;

class _CreateBrokerLinks_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $links     = $brByLic = [];
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst        = true;
        $testLength        = 1000000;
        $insertRecordLimit = 5000;
        $skipCount         = 0;

        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $br = $bg = $bo = $a = [];

        $file     = public_path('_imports/181220/') . 'CA-DRE_Agent_Update.csv';
        $dateReference = '2018-12-20';

        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            if ($idx > $testLength) break;
            if (strtolower($rec['lic_type']) == 'corporation')
            {
                $links[] = ['boLic' => $rec['lic_number'], 'brLic' => $rec['rel_lic_number']];
            }
        }

        $timeParse = time();
        echo 'Total BrokerageOffice Broker Connections: ' . count($links) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds' . PHP_EOL;

        echo '===================' . PHP_EOL . PHP_EOL;

        $br = DB::table('Brokers')->select('ID', 'License')->where('Status', '=', 'upload')->get();

        $brByLic = [];

        foreach ($br as $line)
        {
            $brByLic[$line->License] = $line->ID;
        }

        echo PHP_EOL . 'Total Broker List: ' . count($brByLic) . PHP_EOL;
        echo 'Link Count: ' . count($links) . PHP_EOL . PHP_EOL;

        $linkCount = $errorCount = 0;

        $chunkSize = 1000;
        $cnt       = 0;
        $cycles    = ceil(count($links) / $chunkSize);
        $modSize   = floor($cycles * .1);
        $cycle     = 1;
        $chunks    = array_chunk($links, $chunkSize);
$ii = 0;
/*
        foreach ($chunks as $idx => $link)
        {
            ++$cnt;
            if (($cnt % $modSize) == 0)
            {
                echo '... Count = ' . $chunkSize * $cycle . ' out of ' . count($links) . PHP_EOL;
                $cnt = 0;
                ++$cycle;
            }
            $this->updateDisplayIndex($link, $brByLic, $ii);
        }

        echo '... Count = ' . $chunkSize * $cycle . ' out of ' . count($links) . PHP_EOL;
*/

        foreach ($links as $idx=>$link)
        {
            if (!isset($brByLic[$link['brLic']]))
            {
                if (!isset($brByLic[$link['brLic']])) $err =  'Broker Lic#: ' . $link['boLic'];
                else $err = null;

                ++$errorCount;
                \Illuminate\Support\Facades\Log::error('Not Found: ' . $err);
                continue;
            }
            DB::table('BrokerageOffices')->
            where('License', '=', $link['boLic'])->
            update(['Brokers_ID' => $brByLic[$link['brLic']]]);

            ++$linkCount;
            $delta = time() - $timeStart;
            if (($linkCount % 1000) == 0)
            {
                echo '... Count = ' . $linkCount . ' out of ' . count($links) . ' - ';
                echo ($delta > 180) ?
                    (round($delta / 60, 2) . ' min') : ($delta . ' sec');
                echo ', ' . PHP_EOL . PHP_EOL;
            }
        }

        $timeLinks = time();
        echo $errorCount . ' link errors were found' . PHP_EOL;
        echo $linkCount . ' links made' . PHP_EOL;
        echo '   ...  in ' . ($timeLinks - $timeParse) . ' seconds' . PHP_EOL;

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }


    public function updateDisplayIndex(array $values, &$brByLic, &$ii)
    {
        echo ++$ii . '  |-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+' . PHP_EOL;

        $caseString = 'case ID';
        $ids        = '';
        //      ddd(['values'=>var_export($values, true), /* 'brByLic'=>var_export($brByLic, true), */]);
        foreach ($values as $idx => $value)
        {
            $boLic = $value['boLic'];

            if (!isset($brByLic[$boLic]))
            {
                //               echo 'License #'.$boLic. ' not found in Brokers table' . PHP_EOL;
                continue;
            }
            $brID       = $brByLic[$boLic];
            $caseString .= " when $boLic then $brID" . PHP_EOL;
            $ids        .= " $boLic,";
        }
        $ids = trim($ids, ',');

        $sql = 'update BrokerageOffices set Brokers_ID = ' . $caseString . ' end where License in (' . $ids . ')';
        //var_export($sql);
        if (!strpos($sql, '()'))
        {
            $rv = DB::update($sql);
            if ($rv) var_export($sql);
                      echo PHP_EOL;
        }
    }
}
