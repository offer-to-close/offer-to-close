<?php

use Illuminate\Database\Seeder;

class oneTime_addTimelineCodeToTasks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $milestones = \App\Models\Timeline::select('ID', 'Code')->get();
        foreach($milestones as $milestone)
        {
            \App\Models\Task::where('Timeline_ID', '=', $milestone->ID)->update(['Timeline_Code'=>$milestone->Code]);
        }
        echo __CLASS__ . ': data change complete' . PHP_EOL . PHP_EOL;
    }
}
