<?php

use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ['Documents', 'DocumentFilters', 'DocumentAccessDefaults', 'DocumentTransferDefaults'];

        foreach($tables as $tbl)
        {
 //           $tbl = 'Documents';
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);
        }

        $this->fullBuilder();
    }


    public function fullBuilder()
    {
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        //       ini_set("memory_limit", "2400M");

        $testLength = 0;

        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $file     = public_path('_imports/documents/') . 'fullDocData.csv';
        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        $tblDocuments        = DB::table('Documents');
        $tblDocumentFilters  = DB::table('DocumentFilters');
        $tblDocumentAccess   = DB::table('DocumentAccessDefaults');
        $tblDocumentTransfer = DB::table('DocumentTransferDefaults');
        $dateCreated         = date('Y-m-d');

        //       var_dump($records);

        echo implode(', ', array_keys(reset($records))) . PHP_EOL . PHP_EOL;
        $sets = ['sfr', 'vl', 'probate', 'mfr'];
        $access = ['a-btc', 'a-ba', 'a-b', 'a-stc', 'a-sa',	'a-s', 'a-e', 'a-l', 'a-t'];
        $transfer = ['t-btc','t-ba', 't-b', 't-stc', 't-sa',	't-s', 't-e', 't-l', 't-t',];

        foreach ($records as $idx => $rec)
        {
            $docCode = $rec['Code'];

            echo implode(', ', $rec) . PHP_EOL;
            if ($testLength > 0 && $idx == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' .
                     PHP_EOL . PHP_EOL;
                break;
            }

            $includeWhen = $rec['IncludeWhen'];

            $recDocuments                = $rec;
            $recDocuments['DateCreated'] = $dateCreated;
            $recDocuments['isTest']      = false;
            foreach($sets as $set)
            {
                unset($recDocuments[$set]);
            }

            foreach($access as $role)
            {
                unset($recDocuments[$role]);
            }

            foreach($transfer as $role)
            {
                unset($recDocuments[$role]);
            }

//... Save Document into Documents table
            $tblDocuments->insert($recDocuments);

            $recDocumentFilters['Documents_Code'] = $docCode;
            $recDocumentFilters['DateCreated']    = $dateCreated;
            $recDocumentFilters['isTest']         = false;

// ... Create Document Filter Record
/* ... The following is the key to the values of the sfr, vl, probate & mfr columns
 * ... NR = Not Required (always optional)
 * ...  R = Required (always)
 * ...  C = Conditionally Required (only include when "includeWhen" evaluates to true)
 * ...  X = Excluded (never included)
 */
            foreach ($sets as $set)
            {
                $recDocumentFilters['DocumentSets_Value'] = $set;
                if ($rec[$set] == 'R') $recDocumentFilters['IncludeWhen']  = null;
                if ($rec[$set] == 'C') $recDocumentFilters['IncludeWhen']  = $rec['IncludeWhen'];
                if ($rec[$set] == 'NR') $recDocumentFilters['IncludeWhen'] = '{optional}';
                if ($rec[$set] == 'X') continue;

//... Save Document Filter Record
                $tblDocumentFilters->insert($recDocumentFilters);
            }

            $roles = ['btc', 'ba', 'b', 'stc', 'sa', 's', 'e', 'l', 't'];

//... Create Document Access Default record
            $recAccess['Documents_Code'] = $docCode;
            $recAccess['DateCreated']    = $dateCreated;
            $recAccess['State']          = $rec['State'];
            $recAccess['isTest']         = false;

            foreach ($roles as $role)
            {
                $recAccess['PovRole']   = $role;
                $recAccess['AccessSum'] = $rec['a-'.$role] ?? 0;
                //... Save Document Filter Record
                $tblDocumentAccess->insert($recAccess);
            }

//... Create Document Transfer Default record
            $recTransfer['Documents_Code'] = $docCode;
            $recTransfer['DateCreated']    = $dateCreated;
            $recTransfer['State']          = $rec['State'];
            $recTransfer['isTest']         = false;

            foreach ($roles as $role)
            {
                $recTransfer['PovRole']     = $role;
                $recTransfer['TransferSum'] = $rec['t-'.$role] ?? 0;
                //... Save Document Filter Record
                $tblDocumentTransfer->insert($recTransfer);
            }

            unset($records[$idx]);
        }

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
    public function builder()
    {
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        //       ini_set("memory_limit", "2400M");

        $testLength = 0;

        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $file     = public_path('_imports/documents/') . 'allDocs.csv';
        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        $tblDocuments       = DB::table('Documents');
        $tblDocumentFilters = DB::table('DocumentFilters');
        $dateCreated        = date('Y-m-d');

        //       var_dump($records);

        echo implode(', ', array_keys(reset($records))) . PHP_EOL . PHP_EOL;
        foreach ($records as $idx => $rec)
        {
            //           if (empty($rec['State'])) continue;
            echo implode(', ', $rec) . PHP_EOL;
            if ($testLength > 0 && $idx == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' .
                     PHP_EOL . PHP_EOL;
                break;
            }

            $includeWhen = $rec['IncludeWhen'];

            $sets = ['sfr', 'vl', 'probate', 'mfr'];

            $recDocuments                = $rec;
            $recDocuments['DateCreated'] = $dateCreated;
            $recDocuments['isTest']      = false;

            foreach($sets as $set)
            {
                unset($recDocuments[$set]);
            }

            //... Save Document into Documents table
            $documentID = $tblDocuments->insertGetId($recDocuments);

            $recDocumentFilters['Documents_ID'] = $documentID;
            $recDocumentFilters['DateCreated']  = $dateCreated;
            $recDocumentFilters['isTest']       = false;

            //... Create Document Filter Record
            foreach ($sets as $set)
            {
                if ($rec[$set] == 'NR') continue;

                $recDocumentFilters['DocumentSets_Value'] = $set;
                if ($rec[$set] == 'R') $recDocumentFilters['IncludeWhen'] = null;
                if ($rec[$set] == 'C') $recDocumentFilters['IncludeWhen'] = $rec['IncludeWhen'];
                //... Save Document Filter Record
                $tblDocumentFilters->insert($recDocumentFilters);
            }


            unset($records[$idx]);
        }

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}
