<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class _UpdateFromTempTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->updateTables();

    }

    private function updateTables()
    {
        echo PHP_EOL . __FUNCTION__ . PHP_EOL . '************************' . PHP_EOL . PHP_EOL;
        $timeStart = time();

// *** Updated tables based on changes in new data from source
        try
        {
            \App\Models\Agent::where('LicenseState', '')->update(['LicenseState' => 'CA']);
            echo PHP_EOL . PHP_EOL . '************************  Agents set licenseState '  . PHP_EOL . PHP_EOL;
        }
        catch (Exception $e){        }
        try
        {
            \App\Models\Broker::where('LicenseState', '')->update(['LicenseState' => 'CA']);
            echo PHP_EOL . PHP_EOL . '************************  Brokers set licenseState '  . PHP_EOL . PHP_EOL;
        }
        catch (Exception $e){        }
        try
        {
            \App\Models\BrokerageOffice::where('LicenseState', '')->update(['LicenseState' => 'CA']);
            echo PHP_EOL . PHP_EOL . '************************  BrokerageOffices set licenseState '  . PHP_EOL . PHP_EOL;
        }
        catch (Exception $e){        }

        $tables = ['Brokers', 'Agents',];
        foreach ($tables as $table)
        {
            if (!isset($errCount[$table])) $errCount[$table] = 0;
            $query            = 'Select max(ID) as maxID FROM ' . $table;
            $result           = DB::select($query);
            $maxIndex[$table] = reset($result)->maxID;

            $tmpTable = 'tmp_' . $table . 'Install';

            echo 'Prepare to update ' . $table . ' names...  ';

            $sql = 'UPDATE ' . $table;
            $sql .= ' INNER JOIN ' . $tmpTable . ' ON (' . $table . '.License = ' . $tmpTable . '.License ';
            $sql .= ' AND ' . $table . '.LicenseState = ' . $tmpTable . '.LicenseState ) ';
            $sql .= 'SET ';
            $sql .= '   ' . $table . '.NameFirst = ' . $tmpTable . '.NameFirst ';
            $sql .= ' , ' . $table . '.NameLast = ' . $tmpTable . '.NameLast ';
            $sql .= ' , ' . $table . '.NameFull = ' . $tmpTable . '.NameFull ';
            $sql .= ' WHERE ' . $table . '.NameFull != ' . $tmpTable . '.NameFull ';

            try
            {
                DB::select($sql);
            }
            catch (Exception $e)
            {
                ++$errCount[$table];
            }

            echo $table . ' names updated.' . PHP_EOL . PHP_EOL;
        }
        echo PHP_EOL . PHP_EOL . 'Max Indices: ' . var_export($maxIndex, true) . PHP_EOL . PHP_EOL;

// ... Update Addresses
        $tables = ['Brokers', 'Agents','BrokerageOffices',];
        foreach ($tables as $table)
        {
            if (!isset($errCount[$table])) $errCount[$table] = 0;
            $tmpTable = 'tmp_' . $table . 'Install';

            echo 'Prepare to update ' . $table . ' names...  ';

            $sql = 'UPDATE ' . $table;
            $sql .= ' INNER JOIN ' . $tmpTable . ' ON (' . $table . '.License = ' . $tmpTable . '.License ';
            $sql .= ' AND ' . $table . '.LicenseState = ' . $tmpTable . '.LicenseState ) ';
            $sql .= 'SET ';
            $sql .= '   ' . $table . '.Street1 = '      . $tmpTable . '.Street1 ';
            $sql .= ' , ' . $table . '.Street2 = '      . $tmpTable . '.Street2 ';
            $sql .= ' , ' . $table . '.Unit = '         . $tmpTable . '.Unit ';
            $sql .= '   ' . $table . '.City = '         . $tmpTable . '.City ';
            $sql .= ' , ' . $table . '.State = '        . $tmpTable . '.State ';
            $sql .= ' , ' . $table . '.Zip = '          . $tmpTable . '.Zip ';
            $sql .= '   ' . $table . '.Email = '        . $tmpTable . '.Email ';
            $sql .= ' WHERE ' . $table . '.Street1 != ' . $tmpTable . '.Street1 ';
            $sql .= '    OR ' . $table . '.Street2 != ' . $tmpTable . '.Street2 ';
            $sql .= '    OR ' . $table . '.Unit != '    . $tmpTable . '.Unit ';
            $sql .= '    OR ' . $table . '.City != '    . $tmpTable . '.City ';
            $sql .= '    OR ' . $table . '.State != '   . $tmpTable . '.State ';
            $sql .= '    OR ' . $table . '.Zip != '     . $tmpTable . '.Zip ';
 //           $sql .= '    OR ' . $table . '.Email != '   . $tmpTable . '.Email ';

            try
            {
                DB::select($sql);
            }
            catch (Exception $e)
            {
                ++$errCount[$table];
            }

            echo $table . ' names updated.' . PHP_EOL . PHP_EOL;
        }
        echo PHP_EOL . PHP_EOL . 'Max Indices: ' . var_export($maxIndex, true) . PHP_EOL . PHP_EOL;

        echo PHP_EOL . '&&&&&&&&&&&&&&&&' . PHP_EOL . var_export($errCount, true) . PHP_EOL . PHP_EOL;

// *** Add new records based on new data from source
        $tables    = ['Agents',   ];
        foreach ($tables as $table)
        {
            if (!isset($errCount[$table])) $errCount[$table] = 0;
            $tmpTable = 'tmp_' . $table . 'Install';
            echo 'Prepare to insert new ' . $table . ' names...  ';

            $sql = 'INSERT INTO '. $table. ' (Users_ID,License,LicenseState,NameFull,NameFirst,NameLast,Street1,Street2,Unit,City,State,Zip,PrimaryPhone,SecondaryPhone,Email,EmailFormat,ContactSMS,Status,BrokerageOffices_ID,Notes) ';
            $sql .= '    
SELECT new.*
  FROM (Select '. $tmpTable. '.Users_ID,
'. $tmpTable. '.License,
'. $tmpTable. '.LicenseState,
'. $tmpTable. '.NameFull,
'. $tmpTable. '.NameFirst,
'. $tmpTable. '.NameLast,
'. $tmpTable. '.Street1,
'. $tmpTable. '.Street2,
'. $tmpTable. '.Unit,
'. $tmpTable. '.City,
'. $tmpTable. '.State,
'. $tmpTable. '.Zip,
'. $tmpTable. '.PrimaryPhone,
'. $tmpTable. '.SecondaryPhone,
'. $tmpTable. '.Email,
'. $tmpTable. '.EmailFormat,
'. $tmpTable. '.ContactSMS,
'. $tmpTable. '.`Status`,
'. $tmpTable. '.BrokerageOffices_ID,
'. $tmpTable. '.Notes
 From '. $tmpTable. ' left join '. $table. ' on ('. $tmpTable. '.License = '. $table. '.License AND '. $tmpTable. '.LicenseState = '. $table. '.LicenseState)
WHERE '. $table. '.ID is null
) as new';

            try
            {
                DB::select($sql);
            }
            catch (Exception $e) { ++$errCount[$table]; }

            echo $table . ' names inserted.' . PHP_EOL . PHP_EOL;
        }


        $tables    = ['Brokers',   ];
        foreach ($tables as $table)
        {
            if (!isset($errCount[$table])) $errCount[$table] = 0;
            $tmpTable = 'tmp_' . $table . 'Install';
            echo 'Prepare to insert new ' . $table . ' names...  ';

            $sql = 'INSERT INTO '. $table. ' (Users_ID,License,LicenseState,NameFull,NameFirst,NameLast,Street1,Street2,Unit,City,State,Zip,PrimaryPhone,SecondaryPhone,Email,EmailFormat,ContactSMS,Status,Brokerages_ID,Notes) ';
            $sql .= '    
SELECT new.*
  FROM (Select '. $tmpTable. '.Users_ID,
'. $tmpTable. '.License,
'. $tmpTable. '.LicenseState,
'. $tmpTable. '.NameFull,
'. $tmpTable. '.NameFirst,
'. $tmpTable. '.NameLast,
'. $tmpTable. '.Street1,
'. $tmpTable. '.Street2,
'. $tmpTable. '.Unit,
'. $tmpTable. '.City,
'. $tmpTable. '.State,
'. $tmpTable. '.Zip,
'. $tmpTable. '.PrimaryPhone,
'. $tmpTable. '.SecondaryPhone,
'. $tmpTable. '.Email,
'. $tmpTable. '.EmailFormat,
'. $tmpTable. '.ContactSMS,
'. $tmpTable. '.`Status`,
'. $tmpTable. '.Brokerages_ID,
'. $tmpTable. '.Notes
 From '. $tmpTable. ' left join '. $table. ' on ('. $tmpTable. '.License = '. $table. '.License AND '. $tmpTable. '.LicenseState = '. $table. '.LicenseState)
WHERE '. $table. '.ID is null
) as new';

            try
            {
                DB::select($sql);
            }
            catch (Exception $e) { ++$errCount[$table]; }

            echo $table . ' names inserted.' . PHP_EOL . PHP_EOL;
        }

        echo PHP_EOL . var_export($errCount, true) . PHP_EOL . PHP_EOL;


        $tables    = ['BrokerageOffices',   ];
        foreach ($tables as $table)
        {
            if (!isset($errCount[$table])) $errCount[$table] = 0;
            $tmpTable = 'tmp_' . $table . 'Install';
            echo 'Prepare to insert new ' . $table . ' names...  ';

            $sql = 'INSERT INTO '. $table. ' (License,LicenseState,Street1,Street2,Unit,City,State,Zip,PrimaryPhone,SecondaryPhone,Email,EmailFormat,ContactSMS,Status,Notes) ';
            $sql .= '    
SELECT new.*
  FROM (Select  
'. $tmpTable. '.License,
'. $tmpTable. '.LicenseState,
'. $tmpTable. '.Street1,
'. $tmpTable. '.Street2,
'. $tmpTable. '.Unit,
'. $tmpTable. '.City,
'. $tmpTable. '.State,
'. $tmpTable. '.Zip,
'. $tmpTable. '.PrimaryPhone,
'. $tmpTable. '.SecondaryPhone,
'. $tmpTable. '.Email,
'. $tmpTable. '.EmailFormat,
'. $tmpTable. '.ContactSMS,
'. $tmpTable. '.`Status`,
'. $tmpTable. '.Notes
 From '. $tmpTable. ' left join '. $table. ' on ('. $tmpTable. '.License = '. $table. '.License AND '. $tmpTable. '.LicenseState = '. $table. '.LicenseState)
WHERE '. $table. '.ID is null
) as new';

            try
            {
                DB::select($sql);
            }
            catch (Exception $e) { ++$errCount[$table]; }

            echo $table . ' names inserted.' . PHP_EOL . PHP_EOL;
        }

        echo PHP_EOL . var_export($errCount, true) . PHP_EOL . PHP_EOL;

        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;

    }
}
