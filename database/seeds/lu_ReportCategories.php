<?php

use Illuminate\Database\Seeder;

class lu_ReportCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_ReportCategories';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'   => 'nhd',
            'Display' => 'Natural Hazards',
            'Notes'   => '',
            'Order'   => 1.,
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'ptr',
            'Display' => 'Preliminary Title',
            'Notes'   => '',
            'Order'   => 1.,
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'hw',
            'Display' => 'Home Warranty',
            'Notes'   => '',
            'Order'   => 1.,
        ]);
    }
}
