<?php

use Illuminate\Database\Seeder;

class lk_Users_Roles_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 1,
            'Role' => 'btc',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 2,
            'Role' => 'stc',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 1,
            'Role' => 'ba',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 2,
            'Role' => 'sa',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 3,
            'Role' => 's',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

        $tbl = 'lk_Users-Roles';
        DB::table($tbl)->insert([
            'isTest' => true,
            'Users_ID' => 3,
            'Role' => 'b',
            'isActive' => true,
            'DateCreated' => date('Y-m-d h:i:s',  strtotime("-1 week 2 days 4 hours 5 seconds") ),
        ]);

    }
}
