<?php

use Illuminate\Database\Seeder;
use App\Models\lu_DocumentAccessActions;

class DocumentAccessDefaults extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'DocumentAccessDefaults';
        DB::table($table)->truncate();
        foreach (lu_DocumentAccessActions::get()->toArray() as $action) $allActions[$action['Value']] = $action;
        $timeStart = time();
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst = true;
        $testLength = 500000;
        $insertRecordLimit = 5000;
        $skipCount = 0;


        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $a = [];

        $file    = public_path('_imports/documentAccess/') . 'DocumentAccessDefaults.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;
        //dd(reset($records));
//        ddd($records);
        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL. PHP_EOL;
                break;
            }

            $data            = [];
            $data['State'] = 'CA';
            $data['Documents_Code'] = $rec['Documents_Code'];
            foreach($rec as $fld=>$val)
            {
                if ($fld == 'Documents_Code') continue;
                $numericValue = 0;

                /*
                 * If the value is not numeric, expected values are in the following format:
                 * Example: r|s|u  => which means:  read, sign, upload.
                 * If the value is empty or null it will treat it as a 0. If the value is numeric, it will simply
                 * accept the numeric value.
                 *
                 */
                if(!is_numeric($val) && !is_null($val) && $val !== '')
                {
                    $actionArray = explode('|',$val);
                    foreach ($actionArray as $action)
                    {
                        if(isset($allActions[$action])) $numericValue += (int)$allActions[$action]['ValueInt'];
                    }
                }
                elseif(is_numeric($val)) $numericValue = $val;

                $val                = $numericValue;
                $data['PovRole']    = $fld;
                $data['AccessSum']  = $val;
                $data['isTest']     = false;
                $data['Status']     = 'upload';
                $a[]                = $data;
            }
            unset($records[$idx]);
        }

        $timeParse = time();
        echo 'Total Document Access records imported: ' . count($a) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds'  . PHP_EOL;

        echo '===================' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table($table)->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($a, $insertRecordLimit);
        foreach ($chunks as $a)
        {
            DB::table($table)->insert($a);
        }
        unset($a, $chunks);
        echo $table.' stored' . PHP_EOL. PHP_EOL;

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}
