<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LookupTables_Seeder::class,

            AgentsTableSeeder::class,
            PropertiesTableSeeder::class,
            TransactionsTableSeeder::class,
            BuyersTableSeeder::class,
            SellersTableSeeder::class,
            lk_Transactions_TimelineTableSeeder::class,
            TransactionCoordinators_TableSeeder::class,
            Questions_seeder::class,
            BrokerageOfficesSeeder::class,
            BrokeragesSeeder::class,
            BrokersSeeder::class,
            lk_Users_Roles_TableSeeder::class,
            Users_TableSeeder::class,
            lk_Transactions_Documents_tableSeeder::class,
            AccountAccess_seeder::class,

            BreDataInstall::class,
        ]);
    }
}
