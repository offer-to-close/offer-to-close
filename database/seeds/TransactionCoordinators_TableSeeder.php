<?php

use Illuminate\Database\Seeder;

class TransactionCoordinators_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TransactionCoordinators')->insert([
            'isTest'         => true,
            'Users_ID'       => 1,
            'NameFull'  => ' TC Coordinator',
            'NameLast'       => 'Coordinator',
            'NameFirst'      => str_random(5),
            'Street1'        => '123 ' . str_random(8),
            'City'           => str_random(6),
            'State'          => str_random(2),
            'Zip'            => '12345',
            'PrimaryPhone'   => '',
            'SecondaryPhone' => 0.,
            'Email'          => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat'    => 'html',
            'DateCreated'    => date('Y-m-d h:i:s'),
            'Notes'          => '',
        ]);

        DB::table('TransactionCoordinators')->insert([
            'isTest'         => true,
            'Users_ID'       => 1,
            'NameFull'  => ' TC2 Coordinator',
            'NameLast'       => 'Coordinator',
            'NameFirst'      => str_random(5),
            'Street1'        => '123 ' . str_random(8),
            'City'           => str_random(6),
            'State'          => str_random(2),
            'Zip'            => '12345',
            'PrimaryPhone'   => '',
            'SecondaryPhone' => 0.,
            'Email'          => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat'    => 'html',
            'DateCreated'    => date('Y-m-d h:i:s'),
            'Notes'          => '',
        ]);

    }
}
