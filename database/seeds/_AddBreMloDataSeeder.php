<?php

use Illuminate\Database\Seeder;

class _AddBreMloDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timeStart = time();
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst = true;
        $testLength = 500000;
        $insertRecordLimit = 5000;
        $skipCount = 0;


        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $c = $a = [];

        $file    = public_path('_imports/180719/') . 'mlo_list_1807.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;
//dd(reset($records));
        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL. PHP_EOL;
                break;
            }
            switch (strtolower($rec['BRE_License_Type']))
            {
                case  'corporation':
                    $data            = [];
                    $data['Company'] = trim(implode(' ', [$rec['Lastname_Primary_Name'] ?? reset($rec),
                                                          $rec['Firstname_Secondary_Name']]));
                    $data['exp'] = trim($rec['BRE_Expiration_Date']);
                    $data['License'] = trim($rec['BRE_License_ID']);
                    $data['Street1'] = title_case(trim($rec['Address_Line_1']));
                    $data['Street2'] = title_case(trim($rec['Address_Line_2']));
                    $data['City']    = title_case(trim($rec['City_Name']));
                    $data['State']   = trim($rec['USPS_State_Code']);
                    $data['Zip']     = trim($rec['Zip_Code']);
                    $data['Status']  = 'upload';
                    $data['isTest']  = false;
                    $c[]            = $data;
                    break;

                default:
                    $data              = [];
                    $data['NameLast']  = trim($rec['Lastname_Primary_Name'] ?? reset($rec));
                    $names             = explode(' ', $rec['Firstname_Secondary_Name']);
                    $data['NameFirst'] = reset($names);
                    $data['NameFull']  = trim(implode(' ', [$rec['Firstname_Secondary_Name'], $rec['Lastname_Primary_Name'] ?? reset($rec),
                                                            $rec['Name_Suffix']]));
                    $data['exp'] = trim($rec['BRE_Expiration_Date']);
                    $data['License'] = trim($rec['BRE_License_ID']);
                    $data['Street1'] = title_case(trim($rec['Address_Line_1']));
                    $data['Street2'] = title_case(trim($rec['Address_Line_2']));
                    $data['City']    = title_case(trim($rec['City_Name']));
                    $data['State']   = trim($rec['USPS_State_Code']);
                    $data['Zip']     = trim($rec['Zip_Code']);
                    $data['Status']  = 'upload';
                    $data['isTest']  = false;
                    $a[]               = $data;
                    break;
            }

            unset($records[$idx]);
        }

        $timeParse = time();
        echo 'Total Loan Agent records imported: ' . count($a) . PHP_EOL;
        echo 'Total Loan Company records imported: ' . count($c) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds'  . PHP_EOL;

        echo '===================' . PHP_EOL. PHP_EOL;

        \App\Library\Utilities\_Arrays::sortByTwoColumns($a, 'License', SORT_ASC, 'exp', SORT_ASC);
        foreach ($a as $rec)
        {
            $aNew[$rec['License']] = $rec;
        }
        $aNew = \App\Library\Utilities\_Arrays::removeColumn($aNew, 'exp');
        \App\Library\Utilities\_Arrays::sortByTwoColumns($c, 'License', SORT_ASC, 'exp', SORT_ASC);
        foreach ($c as $rec)
        {
            $cNew[$rec['License']] = $rec;
        }
        $cNew = \App\Library\Utilities\_Arrays::removeColumn($cNew, 'exp');

        unset ($a, $c);
        $timeCull = time();
        echo 'Total Culled Loan Agent records: ' . count($aNew) . PHP_EOL;
        echo 'Total Culled Loan Company records: ' . count($cNew) . PHP_EOL;
        echo '   ...  in ' . ($timeCull - $timeParse) . ' seconds'  . PHP_EOL;

        echo '===================' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table('Loans')->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($aNew, $insertRecordLimit);
        foreach ($chunks as $aNew)
        {
            DB::table('Loans')->insert($aNew);
        }
        unset($aNew, $chunks);
        echo 'Loan Agents stored' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table('Brokers')->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($cNew, $insertRecordLimit);
        foreach ($chunks as $cNew)
        {
            DB::table('Loans')->insert($cNew);
        }
        unset($cNew, $chunks);
        echo 'Loan Companies stored' . PHP_EOL. PHP_EOL;

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}
