<?php

use Illuminate\Database\Seeder;

class lu_DocumentAccessRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $docRoles = \Illuminate\Support\Facades\Config::get('constants.DocumentAccessRoles');
        $values = array_keys($docRoles);
        $intValues = array_column($docRoles, 'value');
        $display   = array_column($docRoles, 'display');
        $isVisible = array_column($docRoles, 'isVisible');
        $isViewable = $values;

        $tbl = 'lu_DocumentAccessRoles';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($values as $idx=>$value)
        {
            DB::table($tbl)->insert([
                'Value'       => $values[$idx],
                'Display'     => $display[$idx],
                'Notes'       => '',
                'Order'       => $isVisible[$idx] ? 1 : 0,
                'ValueInt'    => $intValues[$idx],
            ]);
        }

    }
}