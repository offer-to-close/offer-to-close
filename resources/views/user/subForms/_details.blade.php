@section('custom_css')
    <style>
        .long_label {
            width: 70% !important;
        }

        .checkbox_field {
            margin-left: 60% !important;
        }

        .read-only{
                border-style: none !important;
                background: white !important;
                box-shadow: none;
        }
        @media only screen and (max-width: 1000px) {
            .long_label {
                margin-top: 10px;
                position: absolute;
                width: 60% !important;
            }

            .checkbox_field {
                margin-left: 65% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .long_label {
                margin-top: 10px;
                position: absolute;
                width: 60% !important;
            }

            .checkbox_field {
                margin-left: 60% !important;
            }
        }

    </style>
@append

<div class="form-groups">
    <?php
    $textFields = [
        ['col' => 'PurchasePrice', 'type' => 'text', 'id' => '', 'label' => 'Purchase Price', 'default' => null, 'required' => true,],

        ['col' => 'DateOfferPrepared', 'type' => 'date', 'id' => '', 'label' => 'Contract Date', 'default' => date('Y-m-d'), 'required' => true, 'readOnly' => 'true'],
        ['col' => 'DateAcceptance', 'type' => 'date', 'id' => '', 'label' => 'Acceptance Date', 'default' => date('Y-m-d'), 'required' => true, 'readOnly' => 'true'],
        ['col' => 'EscrowLength', 'type' => 'text', 'label' => 'Length of Escrow','id' => '', 'default' => 45, 'required' => true,],
    ];

    foreach($textFields as $field)
    {
    $col           = $field['col'];
    $label         = $field['label'];
    $ph            = $field['placeholder'] ?? $label;
    $default       = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star          = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    $readOnly      = (@$field['readOnly'] && $_screenMode == 'edit') ? 'readonly' : '';
    $readOnlyClass = $readOnly ? 'read-only' : '';

    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" id ="{{ $field['id'] }}" class="form-control full-name {!! $readOnlyClass !!}" type="{{ $field['type'] }}"
               placeholder="{{$ph}}" value="{!! $default !!}" {!! $readOnly !!}>
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
    <?php   } ?>

    <?php
    $field = 'RentBack';
    $default = old($field) ?? ($data[$field] ?? '');
    ?>
    <div class="form-group{{ $errors->has($field) ? ' has-error' : '' }} with-label">
        <label class="label-control">Will seller rent after close <span class="important">*</span></label>
        <label class="checkbox"><input name="{{$field}}" class="form-control" type="radio"
                                       value="0" {{ $default == 0 ? 'CHECKED' : null }}>
            <span>NO.</span></label>
        <label class="checkbox"><input name="{{$field}}" class="form-control" type="radio"
                                       value="1" {{ $default == 1 ? 'CHECKED' : null }}>
            <span>YES, For less than 30 days.</span></label>
        <label class="checkbox"><input name="{{$field}}" class="form-control" type="radio"
                                       value="31" {{ $default == 31 ? 'CHECKED' : null }}>
            <span>YES, For 30 days or more.</span></label>
        @if($errors->has($field))
            <span class="help-block">
                <strong>{{ $errors->first($field) }}</strong>
            </span>
        @endif
    </div>

    <?php
    $textFields = [
        ['col' => 'ClientRole', 'label' => 'Client\'s Role', 'default' => null, 'required' => true, 'luTable' => 'lu_UserRoles',],
    ];

    foreach($textFields as $field)
    {
    $list = \App\Library\Utilities\FormElementHelpers::getKeyValuePairListFromTable($field['luTable'],
        ['sortByOrder'      => 'a',
         'removeSortOrder0' => true,
        ]);

    $col = $field['col'];
    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    $table = $field['luTable'];
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
        <select name="{{$col}}">
            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, $list, 'value', 'index', false, '** Select', null) !!}
        </select>
        @if($errors->has($col))
            <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
        @endif
    </div>
    <?php   } ?>

    <?php
    $textFields = [
        ['col' => 'SaleType', 'label' => 'Sale Type', 'default' => null, 'required' => true, 'luTable' => 'lu_SaleTypes',],
        ['col' => 'LoanType', 'label' => 'Loan Type', 'default' => null, 'required' => true, 'luTable' => 'lu_LoanTypes',],
    ];

    foreach($textFields as $field)
    {
    $col = $field['col'];
    //    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    $table = $field['luTable'];
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
        <select name="{{$col}}">
            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, $table, 'Display', 'Value', false, '** Select', null) !!}
        </select>
        @if($errors->has($col))
            <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
        @endif
    </div>
    <?php   } ?>

    <?php
    $textFields = [
        ['col' => 'hasBuyerSellContingency', 'label' => 'Is the sale contingent on the buyer selling another home', 'default' => 0, 'required' => true,],
        ['col' => 'hasSellerBuyContingency', 'label' => 'Is the sale contingent on the seller buying another home', 'default' => 0, 'required' => true,],
        ['col' => 'hasInspectionContingency', 'label' => 'Is there an inspection contingency', 'default' => 0, 'required' => true,],
        ['col' => 'needsTermiteInspection', 'label' => 'Is a termite inspection needed', 'default' => 0, 'required' => true,],
        ['col' => 'willTenantsRemain', 'label' => 'Will there be tenants remaining in the property after close of escrow', 'default' => 0, 'required' => true,],
    ];

    foreach($textFields as $field)
    {
    $col = $field['col'];
    if (!array_key_exists($col, $data))
    {
        \Illuminate\Support\Facades\Log::debug(['textFields' => $textFields, 'col' => $col, 'data' => $data]);
        continue;
    }
    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label class="label-control long_label">{{$label}} {!! $star !!}</label>
        <label class="checkbox checkbox_field"><input name="{{$col}}" class="form-control" type="radio"
                                                      value=1 {{ $default ? 'CHECKED' : '' }}>
            <span>YES</span></label>
        <label class="checkbox"><input name="{{$col}}" class="form-control" type="radio"
                                       value=0 {{ !$default ? 'CHECKED' : '' }}>
            <span>NO</span></label>
        @if($errors->has($col))
            <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
        @endif
    </div>
    <?php   } ?>

</div>
