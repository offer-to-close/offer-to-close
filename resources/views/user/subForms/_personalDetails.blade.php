@section('custom_css')
    <style>
        .office_search_list{
            position: absolute;
            left: 0px;
            list-style: none;
            padding-left: 5px;
            background: white;
            max-height: 150px;
            overflow-y: scroll;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            display: none;
        }

        .office_search_list::-webkit-scrollbar{
            width: 5px;
            background-color: white;
        }

        .office_search_list::-webkit-scrollbar-thumb{
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #D62929;
        }
        .office_search_list li:hover{
            background: #f2f2f2;
            cursor: pointer;
        }
         #search_brokerage_spinner{
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 95%;
            display: none;
        }
    </style>
@append

<div class="form-groups">
    <?php
    $textFields = [
        ['col' => 'NameFirst', 'label' => 'First Name', 'default' => null, 'required' => true,],
        ['col' => 'NameLast', 'label' => 'Last Name', 'default' => null, 'required' => true,],
    ];

    foreach($textFields as $field)
    {
    $col = $field['col'];
    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" class="form-control full-name" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
            <strong>{{ $errors->first($col) }}</strong>
        </span>
        @endif
    </div>
    <?php   } ?>

    <?php
    if (isset($nameType) && strtolower($nameType) == 'license')
    {
        $col     = 'NameFull';
        $label   = 'Name on License';
        $ph      = $label;
        $default = old($col) ?? ($data[$col] ?? '');
    }
    elseif (isset($nameType) && strtolower($nameType) == 'display')
    {
        $col     = 'NameFull';
        $label   = 'Name to Display';
        $ph      = $label;
        $default = old($col) ?? ($data[$col] ?? '');
    }
    elseif (isset($nameType) && strtolower($nameType) == 'document')
    {
        $col     = 'NameFull';
        $label   = 'Name on Documents';
        $ph      = $label;
        $default = old($col) ?? ($data[$col] ?? '');
    }
    else $col = false;

    if ($col !== false)
    {
    ?>
    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}}</label>
        <input name="{{$col}}" class="form-control full-name" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
    <?php } ?>

        <?php
        $textFields = [
            ['col' => 'License', 'label' => 'License #', 'default' => null, 'required' => false,],
        ];
        $rv = in_array($callingForm, ['aux', 'h']);
        if ($rv) $textFields = [];
        \Illuminate\Support\Facades\Log::debug(['rv'=>$rv, 'callingform'=>$callingForm,'tf'=>$textFields, 'data'=>$data]);

        foreach($textFields as $field)
        {
            $col = $field['col'];
            if (!array_key_exists($col, $data)) continue;

            $label = $field['label'];
            $ph = $field['placeholder'] ?? $label;
            $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
            $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
            ?>

            <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
                <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
                <input name="{{$col}}" class="form-control full-name" type="text"
                       placeholder="{{$ph}}" value="{{ $default }}">
                @if($errors->has($col))
                    <span class="help-block">
                    <strong>{{ $errors->first($col) }}</strong>
                </span>
                @endif
            </div>
        <?php   } ?>

        <?php
        $textFields = [
            ['col' => 'Company', 'label' => 'Company', 'default' => null, 'required' => false,],
        ];

    foreach($textFields as $field)
    {
        $col = $field['col'];
        if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
            <input name="{{$col}}" class="form-control full-name" type="text"
                   placeholder="{{$ph}}" value="{{ $default }}">
            @if($errors->has($col))
                <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
            @endif
        </div>
    <?php   } ?>

        <?php
        // ... Broker Lookup ...
        if (array_key_exists('Brokers_ID', $data))
        {
        $textFields = [
            ['col' => 'Brokers_ID', 'label' => 'Broker', 'default' => null, 'required' => false,
             'luTable' => 'Brokers', 'displayField'=>'NameFull', 'storeField'=>'ID'],
        ];

        foreach($textFields as $field)
        {
        $col = $field['col'];
        //    if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        $table = $field['luTable'];
        $target = substr($table,0, -1);
        $dspFld = $field['displayField'];
        $strFld = $field['storeField'];
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
            <a href="#addBrokerageOffice" class="pull-right" data-toggle="modal" style="font-size: 12px;">Add Brokerage Office</a>
            <select name="{{$col}}">
                {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, $table, $dspFld, $strFld, false, '** Select', null) !!}
            </select>
            @if($errors->has($col))
                <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
            @endif
        </div>
        <?php
        }
        }
        ?>

        <?php
        // ... Brokerage Office Lookup ...
        if (array_key_exists('BrokerageOffices_ID', $data))
        {
        $textFields = [
            ['col' => 'BrokerageOffices_ID', 'label' => 'Brokerage Office', 'default' => null, 'required' => false,
             'luTable' => 'BrokerageOffices', 'displayField'=>'Name', 'storeField'=>'ID'],
        ];

        foreach($textFields as $field)
        {
        $col = $field['col'];
        //    if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        $table = $field['luTable'];
        $target = substr($table,0, -1);
        $dspFld = $field['displayField'];
        $strFld = $field['storeField'];
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
            <a href="#add{{$target}}" class="pull-right" data-toggle="modal" style="font-size: 12px;">Add {{$target}}</a>
                <input type="hidden" name="BrokerageOffices_ID" id="BrokerageOffices_ID" value="{{ $data['BrokerageOffices_ID'] }}">
                <input type="text" id="search_office" autocomplete="off" class="form-control full-name" placeholder="Search for Brokerage Offices">
                 <i id="search_brokerage_spinner" class="fas fa-circle-notch fa-spin"></i>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul id="office_search_list" class="office_search_list col-md-12 col-md-12 col-xs-12">
                    </ul>
                </div>
            @if($errors->has($col))
                <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
            @endif
        </div>
        <?php
        }
        }
        ?>

</div>

@section('scripts')

<script>
    $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('change', '#BrokerageOffices_ID', function(){
                el    = $(this);
                data  = {};
                data['id'] = el.val();
                data['_model'] = 'App\\Models\\BrokerageOffice';

                 $.ajax({
                    url: '{!! route("details.Office") !!}',
                    type: "get",
                    data: data,
                    success: function(response){
                            $('#search_office').val(response[0]['Name']);

                            if( $('input[name="Street1"]').val() == "" ){
                                for(x in response[0]){
                                    if( !(x == 'PrimaryPhone') ){
                                        el = $('input[name="'+ x +'"]');
                                        el.val(response[0][x]);
                                    }
                                }                                
                                $('select[name="State"]').val(response[0]['State']);  
                            }

                            if( $('input[name="SecondaryPhone"]').val() == "" ){
                                $('input[name="SecondaryPhone"]').val(response[0]['PrimaryPhone']);
                            }

                    }
                 });

               


            });
            $('#BrokerageOffices_ID').trigger('change');
        //Brokerage Office search scripts
            $(document).on('keyup click', '#search_office', function(){
                $('#office_search_list').html(" ");
                $('#BrokerageOffices_ID').val("");
               var el                    = $(this),
                    LENGTH               = 2,
                    data                 = {},
                    form                 = el.closest('form');
                    APP_URL              = '{!! route('search.Office') !!}';              
                    data['_model']       = 'App\\Models\\BrokerageOffice';
                    data['search_query'] = el.val();
                var query = el.val();
                if ( query.length > LENGTH ){
                    $('.search_spinner').fadeIn(100);
                     $.ajax({
                         url: APP_URL ,
                         type: "POST",
                         data: data,
                         success: function(response){
                             if (response.length == 0){
                                 $('#office_search_list').html("");
                                 $('#office_search_list').append('<li style="pointer-events: none" class="text-muted text-center">No data found </li>');
                                 $('.search_spinner').hide();
                             }
                             else {
                                 for( i in response ){
                                    $('#office_search_list').append('<li data-id='+ response[i]["ID"] + '> ' + response[i]["Name"] + '</li>');
                                }
                             }
                             $('#office_search_list').show();
                             $('#office_search_list').scrollTop(0); 
                             $('.search_spinner').hide();
                         },
                         error: function(response){
                             $('.search_spinner').hide();
                         }
                     });
                }
                else{
                    $('#office_search_list').hide();
                }
              
            });
            
            $(document).on('click', '#office_search_list li', function(){
                el = $(this);
                $('#office_search_list').html("");
                $('input[name="BrokerageOffices_ID"]').val(el.attr("data-id")); 
                $('#BrokerageOffices_ID').trigger('change');
                $('#office_search_list').hide();
            });
            $(document).on('focusout', '#search_office', function(){
                    $('#office_search_list').fadeOut(500);
                    $('.search_spinner').fadeOut(500);
            });
    });
</script>

@append