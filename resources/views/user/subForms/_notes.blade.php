<?php
$col = 'Notes';
$default = old($col) ?? ($data[$col] ?? null);
?>
<div class="form-groups">
    <div class="form-group no-label">
        <textarea name="Notes" class="form-control" type="text">{{$default}}</textarea>
    </div>
</div>