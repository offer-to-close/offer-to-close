<div class="form-groups">
    <?php
    $fname = 'PropertyType';
    $label = 'Property Type';
    $default = old($fname) ?? ($data[$fname] ?? '');
    ?>
    <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
        <label for="{{$fname}}" class="label-control">{{$label}} <span
                    class="important">*</span></label>
        <select name="{{$fname}}">
            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, 'lu_PropertyTypes', 'Display', 'Value', false, '** Select Property Type', null) !!}
        </select>
        @if($errors->has($fname))
            <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
        @endif
    </div>


    <?php
    $textFields = [
        ['col'=>'Street1', 'label'=>'Street 1', 'default'=>null, 'required'=>true,],
        ['col'=>'Street2', 'label'=>'Street 2', 'default'=>null, 'required'=>false,],
        ['col'=>'Unit', 'label'=>'Unit #', 'default'=>null, 'required'=>false,],
        ['col'=>'City', 'label'=>'City', 'default'=>null, 'required'=>true,],
    ];

    foreach($textFields as $field)
    {
    $col   = $field['col'];
    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" class="form-control full-name" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
    <?php   } ?>

    <?php
    $default = old('State') ?? ($data['State'] ?? 'CA');
    ?>
    <div class="form-group{{ $errors->has('State') ? ' has-error' : '' }} with-label">
        <label for="State" class="label-control">State <span class="important">*</span></label>
        <select name="State">
            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, Config::get('otc.states'), 'index', 'value') !!}
        </select>
        @if($errors->has('State'))
            <span class="help-block"><strong>{{ $errors->first('State') }}</strong></span>
        @endif
    </div>


        <?php
        $textFields = [
            ['col'=>'Zip', 'label'=>'Zip Code', 'default'=>null, 'required'=>true,],
            ['col'=>'County', 'label'=>'County', 'default'=>null, 'required'=>true,],
            ['col'=>'ParcelNumber', 'label'=>'APN', 'placeholder'=>'Parcel Number', 'default'=>null, 'required'=>false,],
            ['col'=>'YearBuilt', 'label'=>'Year Built', 'default'=>null, 'required'=>true,],
        ];

        foreach($textFields as $field)
        {
        $col   = $field['col'];
        if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
            <input name="{{$col}}" class="form-control full-name" type="text"
                   placeholder="{{$ph}}" value="{{ $default }}">
            @if($errors->has($col))
                <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
            @endif
        </div>
        <?php   } ?>

        <?php
        $textFields = [
            ['col'=>'hasHOA', 'label'=>'Has HOA', 'default'=>0, 'required'=>true,],
            ['col'=>'hasSeptic', 'label'=>'Has Septic Tank', 'default'=>0, 'required'=>true,],
        ];

        foreach($textFields as $field)
        {
        $col   = $field['col'];
        if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label class="label-control">{{$label}} {!! $star !!}</label>
            <label class="checkbox"><input name="{{$col}}" class="form-control" type="radio"
                                           value=1 {{ $default ? 'CHECKED' : '' }}>
                <span>YES</span></label>
            <label class="checkbox"><input name="{{$col}}" class="form-control" type="radio"
                                           value=0 {{ !$default ? 'CHECKED' : '' }}>
                <span>NO</span></label>
            @if($errors->has($col))
                <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
            @endif
        </div>
       <?php   } ?>

</div>