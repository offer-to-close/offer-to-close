@extends('layouts.app')

@section('content')
    @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif

    <div class="mc-wrapper">
        <div class="mc-heading">

<?php
    $previous = stripos(URL::previous(), '/ta/d/') !== false ? 'Details ' : null;
?>
           @if (@$_action == 'saved' || session('data_saved'))
                @include('user._successMessageIcon')
            @endif

            <h1>
                @if ($_screenMode == 'create')
                    Create New
                @elseif ($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                Property
            </h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                There was an error submitting your form. Kindly review the errors below and resubmit your form
            </div>
        @endif

        <div class="mc-box-groups user new">
            <form class="" method="POST" action="{{ route('save' . $_role['display'] ) }}">
                {{ csrf_field() }}
                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="_roleCode" value="{{ (!empty(@$_role['code']) ? $_role['code'] : '') }}">
                <input type="hidden" name="_roleDisplay" value="{{ (!empty($_role['display']) ? $_role['display'] : '') }}">
                <input type="hidden" name="Transactions_ID"
                       value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden"{{ @$data['ID'] ? '' : 'disabled' }} name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-home"></i>ADDRESS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._property')

                    </div>
                </div>

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._notes')

                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.phoneMask')
    @include('scripts.disableAutofill')
@append