<?php
    if(isset($buyers))
    {
        $homesumer = $buyers;
        $label = 'Buyers: ';
    }
    elseif(isset($sellers))
    {
        $homesumer = $sellers;
        $label = 'Sellers: ';
    }
    $route = substr(trim($label),0,-2);

    $menuItems = [];
    foreach($homesumer as $person)
    {
        if (!isset($person->ID) || empty($person->ID)) continue;
        $menuItems[] = ['id'=>$person->ID,  'name'=>$person->NameFirst . ' ' . $person->NameLast,];
    }
       $href = route('input'.$route, ['transactionID'=>session('transaction_id')] ) ;
    ?>

    <div class="row text-center">
        <ul class="nav-list list-inline">
            <li>{{ $label }}</li>
            <li><a href="{{$href}}" id="addNew"><i class="fas fa-plus-circle"></i> New</a></li>

        @foreach ($menuItems as $id=>$name)
            <?php $href = route('input'.$route, ['transactionID'=>session('transaction_id'), strtolower($route). 'ID'=>$name['id']] ) ; ?>
            <li><a href="{{$href}}" id="{{ strtolower($name['id']) }}"><i class="fas fa-search"></i> {{ $name['name'] }} </a></li>
            @endforeach
        </ul>

    </div>

    @section('scripts')
        <script>

            $(document).ready(function() {

            });

            //Making link active based on current page
            var path = window.location.href;


            $('.nav-list li a').each(function () {
                var linkPage = this.href;

                if (path == linkPage) {
                    $(this).addClass("active");
                }
            });

        </script>
    @append