<?php
$action = $action ?? route('submitContactRequest');
$imageDir = \App\Library\otc\_Locations::url('images');
?>

@extends('prelogin.1b.layouts.master')

@section('custom_style')
    <style>
        .about-header .row{
            height: 500px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/otc-team-high-res.png") }}');
            background-size: cover;
        }

        .about-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .about-header .row .container-fluid{
            margin-top: 170px;
        }
        .about-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .about-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }
        .about-title{
            font-size: 50px;
            line-height: 47px;
            font-weight: 200;
        }
        .description{
            margin-top: 100px;
        }
        .description p{
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }
        .press{
            padding-top: 50px;
            margin-top: 70px;
            margin-bottom: -100px;
        }
        .press-card{
            background-color: white;
            border-radius: 5px;
            padding: 30px;
            box-shadow: 0px 0px 25px 0px rgba(0, 0, 0, 0.15);
            height: 370px;
            margin-top:70px;
        }
        .press-card .logo{
            position: relative;
            height: 100px;
            margin: 20px;
            background-color: white;
            border-radius: 5px;
        }
        .press-card .logo img{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
        }
        .press-card-desc{
            height: 100px;
        }



        .team{
            padding-top: 170px;
            background-color: #F9F9F9;
        }
        .team-photo{

        }
        .team-photo img{
            border-radius: 50%;
            height: 232px;
            width: 232px;
            object-fit:cover;
        }
        .member-name{
            padding-right: 0px;
            padding-left: 0px;
        }
        .member-name h2{
            margin-top: 30px;
            font-size: 22px !important;
        }
        .member-name p{
            font-size: 14px;
        }

        .about-desc{
            margin-top: 70px;
        }

        .about-desc p{
            padding-top: 25px;
            font-size:25px;
            line-height: 37px;
            color: rgb(0,0,0);
            font-family: "Montserrat";
            font-weight: 300;
        }

        .mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .content-wrapper:before{
            display: none;
            padding-top: 0px !important;
        }
        .offer-assistant{
            padding-top: 0px;
        }
        .content-wrapper .wrap{
            background-image: none;
            padding-top: 100px;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }
        .form-group.row div{
            margin-top: 20px;
        }
    </style>
@endsection
@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcRbZoUAAAAAO-fOefXKTpajPQ5S0AlBzuXR6Eb"></script>
    <div class="container-fluid banner">
        <div class="container" style="max-width: 1000px;">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Contact Us</h2>
                </div>
                <div class="col-sm-6">
                    <a href="{{route('preLogin.agents.get-started')}}"><button class="btn">HIRE US NOW</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        @if(Session::has('inviteCodeErrors'))
            @include('2a.partials._failureMessage', [
                '_Message' => Session::get('inviteCodeErrors'),
            ])

        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <br/>
                <h1 class=""><img src="{{ $imageDir . 'icons/icon-mail.png' }}" alt="Mail" width="65" height="43"> Contact Us</h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="contactForm" class="form-horizontal" method="POST" action="{{ $action ?? '?'}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="NameFirst" value="{{$NameFirst ?? ''}}">
                            <input type="hidden" name="NameLast"  value="{{$NameLast ?? ''}}">
                            <input type="hidden" name="Email"     value="{{$Email ?? ''}}">

                            <div class="form-group row" style="padding-left: 20px;">

                                <label style="width:100%;">State:</label>
                                    <select name="State" class="form-control form-select" style="display: inline; width: 200px;"  required>
                                        {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default??'CA',
                                                                                                        config('otc.states'), 'index', 'value') !!}
                                    </select>
                                <br/><br/>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label style="width: 40%">First Name:</label>
                                    <input style="width: 60%" type="text" name="NameFirst" placeholder="First Name"  required>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label style="width: 40%">Last Name:</label>
                                    <input style="width: 60%" type="text" name="NameLast" placeholder="Last Name" required>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label style="width: 40%">Company:</label>
                                    <input style="width: 60%" type="text" name="Company" placeholder="Company Name">
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label style="width: 40%">Email:</label>
                                    <input style="width: 60%" type="text" name="Email" placeholder="Email" required>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label style="width: 40%">Phone:</label>
                                    <input style="width: 60%" type="text" name="Phone" placeholder="Phone">
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label>Choose a role:</label>
                                    <br>
                                    <input type="radio" name="Role" value="tc"> Transaction Coordinator
                                    <br>
                                    <input type="radio"  name="Role" value="a"> Real Estate Agent
                                    <br>
                                    <input type="radio" name="Role"  value="b"> Buyer
                                    <br>
                                    <input type="radio"  name="Role" value="s"> Seller
                                    <br>
                                    <input type="radio"  name="Role" value="s"> Other
                                    <br>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label>What are your thoughts?</label> <br/>
                                <blockquote>
                                    <textarea name="Comments" style="height: 200px; width: 100%" placeholder="" required></textarea>
                                </blockquote>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="button" class="btn btn-primary contact-button">
                                        Send Comments
                                    </button>
                                    <button style="visibility: hidden;" type="submit" class="btn btn-primary">
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
            async defer>
    </script>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            function submitContactForm()
            {
                let formElement = $('form#contactForm *');
                formElement.find('[type="submit"]').trigger('click');
            }

            $(document).on('click', '.contact-button', function(){
                var onloadCallback = function () {
                    grecaptcha.ready(function () {
                        grecaptcha.execute('6LcRbZoUAAAAAO-fOefXKTpajPQ5S0AlBzuXR6Eb',
                            {action: 'register'}).then(function(token) {
                                $.ajax({
                                    url: '{{route('verify_reCaptcha')}}',
                                    type: 'POST',
                                    data: {
                                        _token: '{{csrf_token()}}',
                                        token: token,
                                    },
                                    success: function (r) {
                                        if(r.status === 'success')
                                        {
                                            if(r.score > 0.5)
                                            {
                                                submitContactForm();
                                            }
                                            else
                                            {
                                                Swal2({
                                                    title: 'Error',
                                                    text: 'An error has occurred, please try again. Error code: con=rec-01',
                                                    type: 'error',
                                                });
                                            }
                                        }
                                        else
                                        {
                                            Swal2({
                                                title: 'Error',
                                                text: 'There was an error processing your request, please try again. Error code: con=rec-02',
                                                type: 'error',
                                            });
                                        }
                                    },
                                    error: function (err) {
                                        Swal2({
                                            title: 'Error',
                                            text: 'An unknown error has occurred. Error code: con=rec-03',
                                            type: 'error',
                                        });
                                    },
                                });
                            }
                        ); //end grecaptcha execute
                    });
                };
                onloadCallback();
            });
        });
    </script>
@endsection