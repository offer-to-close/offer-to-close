@extends('prelogin.1b.layouts.master')
@section('page_title')
Create a Real Estate Purchase Agreement and Contract in Minutes
@endsection
@section('page_keywords')
Real estate purchase agreement, Real Estate Contract, OTC Offers, Transaction Management Software, Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, real estate transaction software
@endsection
@section('page_description')
Offer To Close is a transaction management solution merging first-class technology, such as our Offers App, with qualified, experienced real estate assistants. We simplify the process of buying and selling real estate by making it more transparent.
@endsection
@section('content')
<link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet">
<div class="container-fluid " id="banner_offers">
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            <div class="col-sm-6">
                <h1 style="font-family: 'Roboto', sans-serif;">Coming Soon</h1>
                <h2>Place an offer on a home <br>in just a few clicks</h2>
                <button class="btn"><img src="{{asset('images/prelogin/appstore2.png')}}" class="img-responsive"></button>
                <button class="btn"><img src="{{asset('images/prelogin/googleplay2.png')}}" class="img-responsive"></button>
            </div>
            <div class="col-sm-6">
                <img src="{{asset('images/prelogin/mobile.png')}}" class="img-responsive">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid services_offers" id="services_offers">
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            <h1>Why Use Offers?</h1>
            <div class="col-sm-5 col-md-4 col-lg-4">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-1.png')}}" class="img-responsive">
                    <h4>Fast</h4>
                    <p>Whether you're at home, work, or you haven't left the open house, it only takes a few minutes to place an offer on a new home.</p>
                </center>
            </div>
            <div class="col-sm-5 col-md-4 col-lg-4">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-2.png')}}" class="img-responsive">
                    <h4>Simple</h4>
                    <p>Buying and selling your home shouldn't be so hard, so we made the process simpler. Plus, if there is anything you are unsure about, you can call our support team to help answer any questions.</p>
                </center>
            </div>
            <div class="col-sm-5 col-md-4 col-lg-4">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-3.png')}}" class="img-responsive">
                    <h4>Free</h4>
                    <p>That's right, it's FREE! We don't charge you for placing offers on a home. We just hope that once you're under contract, you'll decide to hire us to help the transaction run smoothly. If not, we're just glad we could help.</p>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid process" id="offers-process">
    <div class="container" style="max-width: 1000px;">
        <h1>How it Works?</h1>
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-4.png')}}" class="img-responsive" id="icons">
                    <h4>Register</h4>
                    <img src="{{asset('images/prelogin/line-4.png')}}" class="img-responsive" id="mbl-view">
                    <center><img src="{{asset('images/prelogin/line-1.png')}}" class="img-responsive" id="lines"></center>
                    <p><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.--></p>
                </center>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-5.png')}}" class="img-responsive" id="icons">
                    <h4>Create Offer</h4>
                    <img src="{{asset('images/prelogin/line-4.png')}}" class="img-responsive" id="mbl-view">
                    <img src="{{asset('images/prelogin/line-2.png')}}" class="img-responsive" id="lines">
                    <p><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.--></p>
                </center>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-6.png')}}" class="img-responsive" id="icons">
                    <h4>Send to Seller</h4>
                    <img src="{{asset('images/prelogin/line-4.png')}}" class="img-responsive" id="mbl-view">
                    <img src="{{asset('images/prelogin/line-2.png')}}" class="img-responsive" id="lines">
                    <p><!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. --></p>
                </center>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <center>
                    <img src="{{asset('images/prelogin/icon-offers-7.png')}}" class="img-responsive" id="icons">
                    <h4>Get Confirmation</h4>
                    <img src="{{asset('images/prelogin/line-4.png')}}" class="img-responsive" id="mbl-view">
                    <img src="{{asset('images/prelogin/line-3.png')}}" class="img-responsive" id="lines">
                    <p><!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. --></p>
                </center>
            </div>
        </div>
    </div>
</div>
@endsection
