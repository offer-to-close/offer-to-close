@extends('prelogin.1b.layouts.master')
@section("content")
<div class="container-fluid " id="banner_thin_centered_text" style="margin-bottom: 40px;">
    <div class="container" style="max-width: 1000px;">
        <h2>Get Started</h2>
    </div>
</div>
@include('prelogin.1a.forms._form_ agentGetStarted')

@endsection

@section("scripts")
<script src="https://services.cognitoforms.com/scripts/embed.js"></script>
@endsection
