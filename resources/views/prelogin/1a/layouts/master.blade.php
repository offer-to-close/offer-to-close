<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>@yield('page_title', 'Transaction Coordinator Service from Offer To Close')</title>
	      <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
	      @yield('page_description')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name='ir-site-verification-token' value='246367830' />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/lightcase.css')}}">
        <link rel="stylesheet" href="{{asset('css/app_prelogin.css')}}">
        @yield('custom_style')
        <script type='text/javascript' src='{{asset('js/app_prelogin.js')}}'></script>
        <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />
    </head>
    <body>
      <aside id="sidepanel">
        <div class="wrap">
          <ul id="menu-main-menu" class="mobile-menu">
            <li class="menu-item"><a href="{{ route('preLogin.agents') }}">Agents</a></li>
            <li class="menu-item"><a href="{{ route('timeline.create') }}">Transaction Timeline</a></li>
            <li class="menu-item menu-item-has-children">
              <span class="toggle-menu open"></span><a href="#">Tools</a>
              <ul class="sub-menu">
                <li  class="menu-item">
                  <a href="{{ asset('preLogin.mls') }}">MLS</a>
                </li>
                <li  class="menu-item"><a href="{{ route('preLogin.offerAssistant') }}">Offer Assistant</a></li>
                <li  class="menu-item">
                  <a href="https://www.offertoclose.com/blog/">Blog</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="phone-link"><a href="8336333782"><span class="tracknumber">833-OFFER-TC</span></a></div>
      </aside>
      <header class="header">
        <div class="container">
          <div class="tb-wrap">
            <div class="tb-layout">
              <div class="tb-item left">
                <a href="{{ route('home') }}" class="site-logo">
                  <img src="{{asset('images/offertoclose-logo.png')}}" alt="OfferToClose Logo">
                </a>
              </div>
              <div class="tb-item center">
                <div class="mobile-view">
                  <div class="mobile-trigger">
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                </div>
                <div class="desktop-view">
                  <ul id="menu-main-menu-1" class="primary-menu">
                    <li class="menu-item"><a href="{{ route('preLogin.agents') }}">Agents</a></li>
                      <li class="menu-item"><a href="{{ route('timeline.create') }}">Transaction Timeline</a></li>
                    <li class="menu-item menu-item-has-children">
                      <a href="#">Tools</a>
                      <ul class="sub-menu">
                        <li class="menu-item">
                          <a href="{{ route('preLogin.mls') }}">MLS</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ route('preLogin.offerAssistant') }}">Offer Assistant</a>
                        </li>
                        <li class="menu-item">
                          <a href="https://www.offertoclose.com/blog/">Blog</a>
                        </li>
                      </ul>
                    </li>
                    <li class="menu-item"><a href="{{ route('preLogin.about') }}">About Us</a></li>

                  </ul>
                </div>
              </div>
              <div class="tb-item right">
                <span class="phone-link"><a href="tel:8336333782">833-OFFER-TC</a></span>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- MAIN CONTENT START -->
      <main id="mainpanel" class="mainpanel">
        @yield('content')
      </main>
      <!-- MAIN CONTENT END -->
      <footer id="footer" class="footer">
        <div class="wrap">
          <div class="container">
            <div class="tb-wrap">
              <div class="tb-layout">
                <div class="tb-item one">
                  <div id="media_image-2" class="widget">
                    <img width="160" height="267" src="{{asset('images/footer-logo.png')}}" alt="Footer Logo">
                  </div>
                </div>
                <div class="tb-item two">
                  <div class="widget">
                    <h3 class="widget-title">IMPORTANT LINKS</h3>
                    <div class="menu-important-links-container">
                      <ul id="menu-important-links" class="menu">
                        <li class="menu-item"><a href="{{ route('preLogin.agents') }}"><i class="fas fa-angle-right"></i> Agent Services</a></li>
                        <li class="menu-item"><a href="{{ route('preLogin.fsbo') }}"><i class="fas fa-angle-right"></i> For Sale By Owner</a></li>
                        <li class="menu-item"><a href="{{ route('preLogin.mls') }}"><i class="fas fa-angle-right"></i> MLS</a></li>
                        <li class="menu-item"><a href="https://www.offertoclose.com/offer-assistant/"><i class="fas fa-angle-right"></i> Offer Assistant</a></li>
                        <li class="menu-item"><a href="{{ route('timeline.create') }}"><i class="fas fa-angle-right"></i> Transaction Timeline</a></li>
                        <li class="menu-item"><a href="{{ route('register') }}"><i class="fas fa-angle-right"></i> Register</a></li>
                        <li class="menu-item"><a href="{{ route('otc.login') }}"><i class="fas fa-angle-right"></i> Login</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="tb-item three">
                  <div class="widget">
                    <h3 class="widget-title">SOCIAL MEDIA LINKS</h3>
                    <div class="menu-social-media-links-container">
                      <ul id="menu-social-media-links" class="menu">
                        <li class="icon-wrap twitter menu-item"><a target="_blank" href="https://www.twitter.com/offertoclose">Follow us on Twitter</a></li>
                        <li class="icon-wrap facebook menu-item"><a target="_blank" href="https://www.facebook.com/offertoclose">Like us on Facebook</a></li>
                        <li class="icon-wrap instagram menu-item"><a target="_blank" href="https://www.instagram.com/offertoclose">Follow us on Instagram</a></li>
                        <li class="icon-wrap youtube menu-item"><a target="_blank" href="https://www.youtube.com/channel/UCQXBNQ2etWmt6VFzV8hfntw">Subscribe us on Youtube</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="tb-item four">
                  <div class="box-wrap">
                    <div class="widget">
                      <h3 class="widget-title">CONTACT DETAILS</h3>
                      <div class="textwidget">
                        <div class="top">
                          <p style="text-align: center;">
                            <img class="img-responsive" src="{{asset('images/icons/icon-mail.png')}}" alt="Mail" width="65" height="43">
                            <a href="mailto:info@OfferToClose.com">info@OfferToClose.com</a>
                          </p>
                        </div>
                        <div class="middle">
                          <p style="text-align: center;">
                            <img class="img-responsive" src="{{asset('images/icons/icon-footer-phone.png')}}" alt="Phone" width="48" height="49">
                            <a href="tel:8336333782"><span class="tracknumber">833-633-3782</span></a>
                          </p>
                        </div>
                        <div class="bottom">
                          <img class="img-responsive" src="{{asset('images/offertoclose-logo.png')}}" alt="Logo" width="241" height="44">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright-wrap">
          <div class="container">
            <p>© 2018 Offer to close All rights reserved.</p>
          </div>
        </div>
      </footer>
      <div class="back-top" style="display: block;">
        <i class="fa fa-chevron-up"></i>
        <span>to top</span>
      </div>
      @yield('scripts')
    </body>
    <!-- <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> -->
    @yield('scripts')
</html>
