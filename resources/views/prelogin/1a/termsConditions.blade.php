@extends('prelogin.1a.layouts.master')

@section('custom_style')
    <style>
        .about-header .row{
            height: 500px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/sale_header_bg.png") }}');
            background-size: cover;
        }

        .about-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .about-header .row .container-fluid{
            margin-top: 170px;
        }
        .about-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .about-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }
        .about-title{
            font-size: 50px;
            line-height: 47px;
            font-weight: 200;
        }
        .description{
            margin-top: 100px;
        }
        .description p{
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }
        .press{
            padding-top: 50px;
            margin-top: 70px;
            margin-bottom: -100px;
        }
        .press-card{
            background-color: white;
            border-radius: 5px;
            padding: 30px;
            box-shadow: 0px 0px 25px 0px rgba(0, 0, 0, 0.15);
            height: 370px;
            margin-top:70px;
        }
        .press-card .logo{
            position: relative;
            height: 100px;
            margin: 20px;
            background-color: white;
            border-radius: 5px;
        }
        .press-card .logo img{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
        }
        .press-card-desc{
            height: 100px;
        }



        .team{
            padding-top: 170px;
            background-color: #F9F9F9;
        }
        .team-photo{

        }
        .team-photo img{
            border-radius: 50%;
            width: 100%;
            height: 100%;
            height: 232px;
            width: 232px;
            object-fit:cover;
        }
        .member-name{
            padding-right: 0px;
            padding-left: 0px;
        }
        .member-name h2{
            margin-top: 30px;
            font-size: 22px !important;
        }
        .member-name p{
            font-size: 14px;
        }

        .about-desc{
            margin-top: 70px;
        }

        .about-desc p{
            padding-top: 25px;
            font-size:25px;
            line-height: 37px;
            color: rgb(0,0,0);
            font-family: "Montserrat";
            font-weight: 300;
        }

        .mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .content-wrapper:before{
            display: none;
            padding-top: 0px !important;
        }
        .offer-assistant{
            padding-top: 0px;
        }
        .content-wrapper .wrap{
            background-image: none;
            padding-top: 100px;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }


    </style>

@endsection

@section('content')
    <section class="about-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>Terms & Conditions</strong></h1>
            </div>
        </div>
    </section>
    <div class="row" style="background-color:#cb4544; height: 5px;"></div>
    <section class="description">
        <div class="container">
                <div class="col-md-12">
                    <p>By accessing and using OfferToClose.com or any of the sites owned and operated by Home By Home, Inc. (the "Site"), you accept and agree to be bound by the Terms & Conditions set forth below. Offer To Close may revise these Terms of Use at any time by updating this posting. You should visit this page periodically to review the Terms of Use, because they are binding on You. The terms You and User as used herein refer to all individuals and/or entities accessing the Site for any reason</p>
                    <p style="font-weight: bold; text-transform: uppercase;">NOTICE OF ARBITRATION AGREEMENT AND CLASS ACTION WAIVER: THE TERMS OF USE INCLUDES A BINDING ARBITRATION CLAUSE AND A CLASS ACTION WAIVER, SET FORTH BELOW, WHICH AFFECT YOUR RIGHTS ABOUT RESOLVING ANY DISPUTE WITH OFFER TO CLOSE. PLEASE READ IT CAREFULLY IT AFFECTS YOUR RIGHTS, AND THOSE TO WHOM YOU PROVIDE ACCESS TO YOUR ACCOUNT. THE AGREEMENT TO ARBITRATE ALL CLAIMS IS A CONDITION OF YOUR USE OF THE OFFER TO CLOSE WEBSITE.</p>
                </div>

                <div class="col-md-12">
                    <h2 class="about-title"><span>1. Eligibility.</span></h2>
                    <p>You must be at least 18 years of age to visit or use the Site in any manner. By visiting the Site and / or accepting these Terms of Use, You represent and warrant to Offer To Close that You are 18 years of age or older, and that You have the right, authority and capacity to agree to and abide by these Terms of Use. You also represent and warrant to Offer To Close that You will use the Site in a manner consistent with any and all applicable laws and regulations.</p>
                </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>2. Use of Content on the Site.</span></h2>
                <p>Offer To Close authorizes You to view and access a single copy of the content available on or from the Site solely for your personal use. The contents of the Site, such as text, graphics, images, logos, button icons, software and other content (collectively, Offer To Close Content), are protected under both United States and foreign copyright, trademark and other laws. All Offer To Close Content is the property of Offer To Close or its content suppliers or clients. The compilation (meaning the collection, arrangement and assembly) of all content on the Offer To Close Sites are the exclusive property of Offer To Close and protected by U.S. and international copyright laws. Unauthorized use of the Offer To Close Content may violate copyright, trademark, and other laws. You must retain all copyright, trademark, servicemark and other proprietary notices contained in the original Offer To Close Content on any copy You make of the Offer To Close Content. You may not sell or modify the Offer To Close Content or reproduce, display, publicly perform, distribute, or otherwise use the Offer To Close Content in any way for any public or commercial purpose. The use of the Offer To Close Content on any other web site or in a networked computer environment for any purpose is prohibited. You shall not copy or adapt the HTML code that Offer To Close creates to generate any Offer To Close Content or the pages making up any Offer To Close Site which is also protected by such copyright.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>3. Proprietary Rights.</span></h2>
                <p>You represent and warrant to Offer To Close that the information posted in any profile you create on any Site, including any photographs, is posted by you and that you are the exclusive author of your profile and the exclusive owner of your photographs and videos. You assign to Offer To Close, with full title guarantee, all copyright in your profile, your photographs or videos posted, your comments on any Site, and any additional information posted on the Site or sent to Offer To Close at any time in connection with your use of the Site or any services offered through the Site. You waive absolutely any and all moral rights to be identified as author of any such content and any similar rights in any jurisdiction in the world. In addition, other users of the Site may post copyrighted information, which has copyright protection, whether or not it is identified as copyrighted. Except for that information which is in the public domain or for which you have been given express written permission, you will not copy, modify, publish, transmit, distribute, perform, display, or sell any such proprietary information. By posting information, photographs or content on any the Site, you automatically grant, and you represent and warrant that you have the right to grant, to Offer To Close and other members of the Site, without any additional compensation, an irrevocable, perpetual, non-exclusive, royalty-free, fully-paid up, worldwide license to use, copy, perform, display, promote, publish and distribute such information, content, videos and photographs and to prepare derivative works thereof, or incorporate such information and content into other works and to grant and authorize sub-licenses of the foregoing.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>4. Offer To Close Site Restrictions.</span></h2>
                <p>You may not use any Site in order to transmit, distribute, store or destroy material, including without limitation Offer To Close Content, (a) in violation of any applicable law or regulation, (b) in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy, publicity or other personal rights of others, or (c) that is defamatory, obscene, threatening, abusive or hateful.</p>
                <p>You are also prohibited from violating or attempting to violate the security of any Site, including, without limitation the following activities: (a) accessing data not intended for such user or logging into a server or account which the user is not authorized to access; (b) attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization; (c) attempting to interfere with service to any user, host or network, including, without limitation, via means of submitting a virus to any Site, overloading, flooding, spamming, mailbombing or crashing; or (d) forging any TCP/IP packet header or any part of the header information in any e-mail or newsgroup posting. Violations of system or network security may result in civil or criminal liability. Offer To Close will investigate occurrences which may involve such violations and may involve, and cooperate with, law enforcement authorities in prosecuting users who are involved in such violations.</p>

            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>5. Specific Prohibited Uses.</span></h2>
                <p>You specifically agree NOT to do any of the following: (a) post or submit to any Site any incomplete, false or inaccurate information or information which is not correct; (b) send unsolicited mail or e-mail, making unsolicited phone calls or send unsolicited faxes regarding promotions and/or advertising of products or services to a user of any Site; (c) delete or revise any material posted by any other person or entity; (d) take any action that imposes an unreasonable or disproportionately large load on any Site infrastructure; (e) notwithstanding anything to the contrary contained herein, use or attempt to use any engine, software, tool, agent or other device or mechanism (including without limitation browsers, spiders, robots, avatars or intelligent agents) to navigate or search any Site other than the search available from Offer To Close on such Site and other than generally available third party web browsers (e.g., Netscape Navigator, Microsoft Explorer); (f) attempt to decipher, decompile, disassemble or reverse engineer any of the software comprising or in any way making up a part of any Site; (g) aggregate, copy or duplicate in any manner any of the Offer To Close Content or information available from any Site; or (h) frame or link to any of Offer To Close Content or information available from any Site.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>6. Registration Information.</span></h2>
                <p>If you register with any Site, you may be asked to provide certain information including, without limitation, a valid email address (your Information). In addition to the Terms of Use and our <a href="{{route('prelogin.privacyPolicy')}}">Privacy Policy</a> or any privacy policy applicable to any such Site, You understand and agree that Offer To Close may disclose to third parties, on an anonymous basis, certain aggregate information contained in your registration application. Except to provide the services offered through the Site, for marketing purposes and to the extent necessary or appropriate to comply with applicable laws or in legal proceedings where such information is relevant or as permitted by any applicable <a href="{{route('prelogin.privacyPolicy')}}">privacy policy</a>, Offer To Close will not disclose to any third party your name, address, e-mail address or telephone number without your prior consent.</p>
                <p>Offer To Close reserves the right to offer third party services and products to you based on the preferences that you identify in your registration and at any time thereafter; such offers may be made by Offer To Close or by third parties. Please see Offer To Close's Privacy Statement for further details regarding your Information.</p>
                <p>Without limiting any of the other disclaimers of warranty set forth in these Terms of Use, Offer To Close does not provide or make any representation as to the quality or nature of any of the third party products or services purchased through the Offer To Close or any other Offer To Close Site, or any other representation, warranty or guaranty. Any such undertaking, representation, warranty or guaranty would be furnished solely by the provider or seller of such product or service, under the terms set forth by such provider or seller.</p>

            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>7. User Account.</span></h2>
                <p>If you create an account on any Site, you are responsible for maintaining the confidentiality of your Site account information, including, but not limited to the username and password. You shall be responsible for all uses of your Site account, whether or not authorized by you. You agree to immediately notify Offer To Close of any unauthorized use of your Site account.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>8. Offer To Close's Liability.</span></h2>
                <p>The Site and the Offer To Close Content may contain inaccuracies or typographical errors. Offer To Close makes no representations about the accuracy, reliability, completeness, or timeliness of any Site or the Offer To Close Content. Changes are periodically made to Site and the services offered thereon and such changes may be made at any time.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>9. Disclaimer of Warranty.</span></h2>
                <p style="text-transform: uppercase;">OFFER TO CLOSE DOES NOT WARRANT THAT ANY SITE WILL OPERATE ERROR-FREE OR THAT ANY SITE AND ITS SERVERS ARE FREE OF COMPUTER VIRUSES OR OTHER HARMFUL MECHANISMS. IF YOUR USE OF ANY SITE OR THE OFFER TO CLOSE CONTENT RESULTS IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA, OFFER TO CLOSE IS NOT RESPONSIBLE FOR THOSE COSTS. THE SITES AND OFFER TO CLOSE CONTENT ARE PROVIDED ON AN AS IS BASIS WITHOUT ANY WARRANTIES OF ANY KIND. OFFER TO CLOSE, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, INCLUDING THE WARRANTY OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND NON-INFRINGEMENT. OFFER TO CLOSE MAKES NO WARRANTIES ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS OF THE OFFER TO CLOSE CONTENT, SERVICES, SOFTWARE, TEXT, GRAPHICS, AND LINKS.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>10. Disclaimer of Consequential Damages.</span></h2>
                <p style="text-transform: uppercase;">IN NO EVENT SHALL OFFER TO CLOSE, ITS SUPPLIERS, OR ANY THIRD PARTIES MENTIONED ON ANY SITE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, INCIDENTAL AND CONSEQUENTIAL DAMAGES, LOST PROFITS, OR DAMAGES RESULTING FROM LOST DATA OR BUSINESS INTERRUPTION) RESULTING FROM THE USE OR INABILITY TO USE ANY OFFER TO CLOSE SITE, THE OFFER TO CLOSE CONTENT, OR ANY PURCHASE MADE ON ANY OFFER TO CLOSE SITE, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT OFFER TO CLOSE IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>11. Release.</span></h2>
                <p>If You have a dispute with one or more other Site user(s), you release Offer To Close (and our officers, directors, agents, subsidiaries, joint ventures and employees) from any claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with such dispute.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>12. Disclaimer of Consequential Damages.</span></h2>
                <p style="text-transform: uppercase;">IN NO EVENT SHALL OFFER TO CLOSE, ITS SUPPLIERS, OR ANY THIRD PARTIES MENTIONED ON ANY SITE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, INCIDENTAL AND CONSEQUENTIAL DAMAGES, LOST PROFITS, OR DAMAGES RESULTING FROM LOST DATA OR BUSINESS INTERRUPTION) RESULTING FROM THE USE OR INABILITY TO USE ANY OFFER TO CLOSE SITE, THE OFFER TO CLOSE CONTENT, OR ANY PURCHASE MADE ON ANY OFFER TO CLOSE SITE, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT OFFER TO CLOSE IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>13. Limitation of Liability.</span></h2>
                <p style="text-transform: uppercase;">TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT SHALL OFFER TO CLOSE, INCLUDING ITS OFFICERS, DIRECTORS, EMPLOYEES, REPRESENTATIVES, SUCCESSORS, ASSIGNS OR AFFILIATES (COLLECTIVELY, THE "COVERED PARTIES") BE LIABLE FOR ANY INJURY, DEATH, LOSS, CLAIM, DAMAGE, ACT OF GOD, ACCIDENT, DELAY, OR ANY SPECIAL, EXEMPLARY, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, WHETHER BASED IN CONTRACT, TORT OR OTHERWISE, WHICH ARISE OUT OF OR ARE IN ANY WAY CONNECTED WITH ANY USE OF THIS SITE OR WITH ANY DELAY OR INABILITY TO USE A SITE, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS OR SERVICES OBTAINED THROUGH THIS SITE, EVEN IF A PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO SOME OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO CERTAIN USERS. IN NO EVENT SHALL THE COVERED PARTIES' TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION (WHETHER IN CONTRACT OR TORT, INCLUDING BUT NOT LIMITED TO NEGLIGENCE) ARISING FROM THIS AGREEMENT OR YOUR USE OF THE SITE EXCEED THE AMOUNT PAID BY YOU TO OFFER TO CLOSE IN THE TRANSACTION IN QUESTION OR IF NO TRANSACTION IS AT ISSUE, IN THE PRECEDING TWELVE (12) MONTHS.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>14. Links to Other Sites.</span></h2>
                <p>The Site may contain links to third party web sites. These links are provided solely as a convenience to You and not as an endorsement by Offer To Close of the contents on such third-party Web sites. Offer To Close is not responsible for the content of linked third-party sites and does not make any representations regarding the content or accuracy of materials on such third party Web sites. If You decide to access linked third-party Web sites, You do so at your own risk.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>15. No Resale or Unauthorized Commercial Use.</span></h2>
                <p>You agree not to resell or assign your rights or obligations under these Terms of Use. You also agree not to make any unauthorized commercial use of any Site.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>16. Indemnity.</span></h2>
                <p>You agree to defend, indemnify, and hold harmless Offer To Close, its affiliates, and their respective officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from (i) any content or other material You provide to any Offer To Close Site, (ii) your use of any Offer To Close Content, (iv) your activities in connection with the Site, or (iv) your breach of the terms of these Terms of Use. Offer To Close shall provide notice to You promptly of any such claim, suit, or proceeding.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>17. Agreement to Mandatory Individual Arbitration.</span></h2>
                <p style="text-transform: uppercase;">PLEASE READ THIS CAREFULLY AS IT AFFECTS YOUR RIGHTS. INSTEAD OF SUING IN COURT, YOU AND OFFER TO CLOSE AGREE THAT THE EXCLUSIVE MEANS OR RESOLVING ANY DISPUTE, CONTROVERSY OR CLAIM ARISING OUT OF, OR RELATING TO, THESE TERMS OF USE OR ANY APPLICABLE <a href="{{route('prelogin.privacyPolicy')}}">Privacy Policy</a>, YOUR USE OR INABILITY TO USE THE SITE, ANY PURCHASE MADE THROUGH THE SITE OR ANY CONTACT WITH OFFER TO CLOSE OR ITS EMPLOYEES, CONTRACTORS OR AFFILIATES OR OTHERWISE ARISING OUT OF YOUR RELATIONSHIP OR INTERACTION WITH OFFER TO CLOSE , SHALL BE BINDING ARBITRATION ADMINISTERED BY THE AMERICAN ARBITRATION ASSOCIATION ON AN INDIVIDUAL BASIS. THERE IS NO JUDGE OR JURY IN ARBITRATION, AND COURT REVIEW OF AN ARBITRATION AWARD IS LIMITED. YOU AGREE THAT, BY ENTERING INTO THIS AGREEMENT, YOU AND OFFER TO CLOSE ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY AND THE RIGHT TO PARTICIPATE IN ANY CLASS OR REPRESENTATIVE ACTION OR OTHER CLASS PROCEEDING. YOU AND OFFER TO CLOSE FURTHER AGREE AS FOLLOWS:</p>
                <p>This agreement to arbitrate is intended to be broadly interpreted, and includes, but is not limited to: (1) disputes and claims arising out of or relating to any aspect of the relationship between You and Offer To Close, whether based in breach of contract, breach of any guarantees or warranties, tort, statute, fraud, misrepresentation or any other legal theory; (2) claims that arose before this Agreement or any prior agreement (including, but not limited to, claims relating to advertising); (3) claims that may arise after the termination of Your relationship with Offer To Close; and (4) claims that are currently the subject of purported class action litigation in which You are not a member of a certified class.</p>
                <p>You and Offer To Close hereby agree that the Federal Arbitration Act ("FAA") applies to any arbitration, and governs all questions of whether a dispute is subject to arbitration. Unless You and Offer To Close agree otherwise in writing, arbitration shall be: (i) administered by the American Arbitration Association ("AAA"), pursuant to the Consumer Arbitration Rules then in effect (the "AAA's Rules"); and (ii) conducted by a single arbitrator who is licensed to practice law. The AAA's Rules can be found at <a href="www.adr.org">www.adr.org</a>.</p>
                <p style="text-transform: uppercase;">THIS AGREEMENT DOES NOT ALLOW CLASS ARBITRATIONS EVEN IF THE PROCEDURES OR RULES OF THE AAA WOULD. RATHER, YOU AND OFFER TO CLOSE ARE ENTITLED TO PURSUE ARBITRATION ONLY ON AN INDIVIDUAL BASIS. FURTHER, AND UNLESS YOU AND PURCHASER AGREE OTHERWISE IN WRITING, THE ARBITRATOR MAY NOT CONSOLIDATE MORE THAN ONE INDIVIDUAL PARTY'S CLAIMS WITH ANY OTHER PARTY'S CLAIMS, AND MAY NOT OTHERWISE PRESIDE OVER ANY FORM OF A REPRESENTATIVE OR COLLECTIVE PROCEEDING.</p>
                <p>You and Offer To Close each are responsible for their respective costs relating to counsel, experts, and witnesses. This arbitration agreement does not preclude You or Offer To Close from bringing issues to the attention of federal, state, or local agencies. Such agencies can, if the law allows, seek relief against on a party's behalf. In addition, and notwithstanding the other provisions of this arbitration agreement, either party may bring an individual action in small claims court. If You or Offer To Close fail to comply with this arbitration provision, said breaching party shall be liable for the costs and attorneys' fees incurred by the other party in enforcing compliance with the arbitration agreement.</p>
                <p>Unless the AAA arbitrator rules otherwise, all claims or counterclaims shall be resolved by the submission of documents only / desk arbitration (see R-29 of the AAA's Rules). Any party, however, may ask for a hearing. The arbitrator also may decide that a face-to-face hearing is necessary. Any hearing, however, that is not held by telephone, shall take place in Los Angeles, California, unless the AAA arbitrator decides otherwise.</p>
                <p>Any proceeding to enforce this arbitration agreement, including any proceeding to confirm, modify, or vacate an arbitration award, may be commenced in any court of competent jurisdiction. In the event that this arbitration agreement is for any reason held to be unenforceable, any litigation against Offer To Close may be commenced only in the federal or state courts located in Los Angeles County, California. You hereby irrevocably consent to the jurisdiction of those courts for such purposes.</p>

            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>18. General.</span></h2>
                <p>Offer To Close makes no claims that the Offer To Close Content may be lawfully viewed or accessed outside of the United States. Access to the Offer To Close Content may not be legal by certain persons or in certain countries. If You access a Site or other U.S.-based Offer To Close Sites from outside of the United States, You do so at your own risk and are responsible for compliance with the laws of your jurisdiction. Offer To Close's mailing address is 5222 Cangas Drive, Calabasas, CA 91301. These Terms of Use are governed by the internal substantive laws of the State of California, without respect to its conflict of laws principles. If any provision of these Terms of Use are found to be invalid by any court having competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms of Use, which shall remain in full force and effect. No waiver of any term of these Terms of Use shall be deemed a further or continuing waiver of such term or any other term. Except as expressly provided in an additional agreement, additional terms of use for areas of Offer To Close, a particular Legal Notice, or Software License or material on particular Web pages, these Terms of Use constitute the entire agreement between You and Offer To Close with respect to the use of Offer To Close. No changes to these Terms of Use shall be made except by a revised posting on this page.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>19. Additional Terms of Use.</span></h2>
                <p>Certain areas of the Site may be subject to additional terms of use. By using such areas, or any part thereof, You agree to be bound by the additional terms of use applicable to such areas.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>20. Term and Termination</span></h2>
                <p>These Terms of Use will remain in full force and effect while You are a User of the Site at any level, including, but not limited to having a profile or account on the Site. Offer To Close reserves the right, at its sole discretion, to pursue all of its legal remedies, including but not limited to deletion of your Information and any content you have posted on the Site from Offer To Close and the other Offer To Close Sites and immediate termination of your registration with or ability to access the Sites and/or any other services provided to You by Offer To Close, upon any breach by You of these Terms of Use or if Offer To Close is unable to verify or authenticate any information You submit to Offer To Close or through any Site registration. Offer To Close reserves the right to terminate your account and access to the Site and its services at any time. Termination by Offer To Close may include removal of access to the Site, deletion of your account, deletion of all related information and files, may include the deletion of content associated with your account (or any part thereof), and other steps intended to bar your further use of the Site and its services. If you become dissatisfied with the Site, your sole and exclusive remedy is to immediately discontinue use of the Site.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>21. Amendments to Terms of Use.</span></h2>
                <p>Offer To Close reserves the right, at our sole discretion, to change, modify or otherwise alter the Terms of Use at any time. You agree that we may modify the Terms of Use and such modifications shall be effective immediately upon posting. You agree to review these terms and conditions periodically to be aware of modifications. Continued access or use of the Site following such posting shall be deemed conclusive evidence of your acceptance of the modified Terms of Use except and to the extent prohibited by applicable state or federal law.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>22. Changes to the Site.</span></h2>
                <p>We reserve the right, for any reason, in our sole discretion, to terminate, suspend or change any aspect of the Site including but not limited to content, prices, features or hours of availability. We may impose limits on certain features of the Site or restrict your access to any part or all of the Site without notice or penalty. You agree that Offer To Close will not be liable to you or to any third party for any such limitation, modification, change, suspension or discontinuance of the Site.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>23. Notices.</span></h2>
                <p>Except as explicitly stated otherwise, any notices shall be given by email to Offer To Close at support@OfferToClose.com or to you at the email address you provide to Offer To Close. Notice shall be deemed given 24 hours after the email is sent, unless the sending party is notified that the email address is invalid.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span>24. How do I contact Offer To Close?</span></h2>
                <p>Our postal address is:<br/>
                    5222 Cangas Drive<br/>
                    Calabasas, CA 91301</p>
                <p>We can be reached via email at support@OfferToClose.com or by telephone at (833) 633-3782.</p>
            </div>

            <div class="col-md-12">
                <h2 class="about-title"><span style="text-transform: uppercase;">25. NOTICE OF PROCEDURE FOR MAKING A CLAIM OF COPYRIGHT INFRINGEMENT.</span></h2>
                <p>If you believe that any content on any Offer To Close Site constitutes work that is owned by you or a third party, and is displayed on such site without proper authorization, please send the following information to the attention of the Copyright Agent noted below</p>
                <ol>
                    <li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;
                    <li>a description of the copyrighted work or other intellectual property that you claim has been infringed;
                    <li>a description of where the material that you claim is infringing is located on a Offer To Close Site;
                    <li>your address, telephone number, and email address;
                    <li>a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and
                    <li>a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's behalf.
                </ol>
                <p>Offer To Close Copyright Agent</p>

                <p>Offer To Close<br/>
                    Attn: Copyright Agent<br/>
                    5222 Cangas Drive<br/>
                    Calabasas, CA 91301<br/><br/>
                    By email: <a href="mailto:support@OfferToClose.com">support@OfferToClose.com</a></p>
            </div>


        </div>
    </section>



@endsection