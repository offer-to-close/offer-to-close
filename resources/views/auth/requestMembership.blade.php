<?php
    $action = $action ?? route('submitMembershipRequest');

?>
@extends('layouts.auth')

@section('content')
    <div class="container">
        @if(Session::has('inviteCodeErrors'))
            @include('2a.partials._failureMessage', [
                '_Message' => Session::get('inviteCodeErrors'),
            ])

        @endif
        <div class="row" id="requestMembershipForm">
            <div class="col-md-8 col-md-offset-2">
                <h1 class=""><i class="fas fa-hand-paper"></i> Membership Request Survey</h1>
                <div class="panel panel-default">

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ $action }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="NameFirst" value="{{$NameFirst}}">
                            <input type="hidden" name="NameLast"  value="{{$NameLast}}">
                            <input type="hidden" name="Email"     value="{{$Email}}">
                            <input type="hidden" name="requestID" value="{{$requestID}}">

                            <div class="form-group{{ $errors->has('NameFirst') ? ' has-error' : '' }}">
                                <label for="NameFirst" class="col-md-8 control-label"><h3 style="color:{{config('otc.color.red')}};">Proposed Member:
                                        {{ $NameFirst }} {{ $NameLast }} - ({{ $Email }})</h3></label>
                                </div>

                            <div class="form-group" style="padding-left: 20px;">
                                Transaction ID (if you have one) <br/>
                                <blockquote>
                                    <input type="text" value="{{$transactionID}}" style="width: 600px; " name="transactionID" placeholder="#####">
                                </blockquote>
                                <br/>

                                State: <br/>
                                <blockquote>
                                    <select name="stateRequested" class="form-control form-select" style="width: 200px;">
                                        {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default??'CA',
                                                                                                        config('otc.states'), 'index', 'value') !!}
                                    </select>
                                </blockquote>
                                <br/>

                                    Choose a role: <br/>
                                    <?php
                                        $roleOptions = [
                                            'Transaction Coordinator'   => 'tc',
                                            'Real Estate Agent'         => 'a',
                                            'Buyer'                     => 'b',
                                            'Seller'                    => 's',
                                        ];
                                    ?>
                                    <blockquote>
                                        @foreach($roleOptions as $display => $value)
                                            @if($roleRequested != '')
                                                @if($roleRequested == $value)
                                                    <input type="radio" name="roleRequested" value="{{$value}}" checked>{{$display}}
                                                @else
                                                    <input type="radio" name="roleRequested" value="{{$value}}">{{$display}}
                                                @endif
                                            @else
                                                <input type="radio" name="roleRequested" value="{{$value}}">{{$display}}
                                            @endif
                                        @endforeach
                                    </blockquote>
                                    @if($errors->has('roleRequested'))
                                        <span class="help-block">
                                            Selecting a role is required.
                                        </span>
                                    @endif
                                    <br/>

                                If you are an Agent or TC, who do you work for? If anyone? <br/>
                                <blockquote>
                                    <input type="text" value="{{$companyRequested}}" style="width: 600px; " name="companyRequested" placeholder="Company name">
                                </blockquote>
                                <br/>

                                Why do you want a membership? <br/>
                                <blockquote>
                                <textarea style="width: 600px; height: 150px;" name="whyRequested" placeholder="Describe why you wish to join Offer To Close">{!!$whyRequested!!}</textarea>
                                </blockquote>
                                @if($errors->has('whyRequested'))
                                    <span class="help-block">
                                            Please explain why you want membership with Offer To Close.
                                        </span>
                                @endif
                                <br/>

                                If you are here about a specific transaction what is the property's address? <br/>
                                <blockquote>
                                    <input type="text" style="width: 600px; " value="{{$addressRequested}}" name="addressRequested" placeholder="123 Main St., Yourtown, CA 91111">
                                </blockquote>
                                <br/>


                                How did you hear about Offer To Close? <br/>
                                <?php
                                    $heardOptions = [
                                        'Transaction Coordinator' => 'tc',
                                        'Real Estate Agent' => 'a',
                                        'Buyer and/or Seller' => 'h',
                                        'Online' => 'web',
                                        'Other'  => 'ad',
                                    ];

                                ?>
                                <blockquote>
                                @foreach ($heardOptions as $display => $value)
                                    @if($heardRequested != '')
                                        @if($value == $heardRequested)
                                            <input type="radio" name="heardRequested" value="{{$value}}" checked>{{$display}} <br>
                                        @else
                                            <input type="radio" name="heardRequested" value="{{$value}}">{{$display}} <br>
                                        @endif
                                    @else
                                        <input type="radio" name="heardRequested" value="{{$value}}">{{$display}} <br>
                                    @endif
                                @endforeach
                                </blockquote>
                                @if($errors->has('heardRequested'))
                                    <span class="help-block">
                                            Please let us know how you heard of Offer To Close.
                                        </span>
                                @endif
                                <br/>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit Request
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection