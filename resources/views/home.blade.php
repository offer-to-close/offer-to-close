@extends('layouts.homepage')

@section('content')
<!-- hbp -->
    <div class="banner">
        <div class="wraper">
            <div class="banr_txt">
                <h2>Transaction coordinators you can trust<br />
                    <span>from offer to close</span></h2>
                <a class="btnsb" href="https://www.offertoclose.com/get-started/">get started</a></div>

            <div class="clr"></div>
        </div>
    </div>

    <div class="prc_bnr1">
        <ul>
            <li>
                <p>$399</p>
                <span>Single Agency</span></li>
            <li>
                <p>$499</p>
                <span>Dual Agency</span></li>
        </ul>
    </div>

    <div class="we_do">
        <div class="wraper">
            <h4>What we can do for you</h4>

            <ul class="strt">
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn1.png" /></a>

                    <h6><a href="">Save your time</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn2.png" /></a>
                    <h6><a href="">Meet broker requirements</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn3.png" /></a>
                    <h6><a href="">Get paid on time</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn4.png" /></a>
                    <h6><a href="">Reduce liablity</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn5.png" /></a>
                    <h6><a href="">access file 24/7</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn6.png" /></a>
                    <h6><a href="">help get offers to close</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn7.png" /></a>
                    <h6><a href="">monitor dates and deadlines</a></h6>
                </li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/icn8.png" /></a>
                    <h6><a href="">keep you in the know</a></h6>
                </li>
            </ul>
            <a class="btnsb" href="">get started</a>

            <div class="clr"></div>
        </div>
    </div>

    <div class="call_us">
        <div class="wraper">
            <p>Please <a href="mailto:info@OfferToClose.com">email</a> or call us (<a href="tel:1-833-633-3782">833-OFFER-TC</a>) with any questions.</p>
        </div>
    </div>

@endsection