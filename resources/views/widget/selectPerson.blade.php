<?php
$size = min(15, $data->count());

// \Illuminate\Support\Facades\Log::debug(['route'=>($route),]);
// \Illuminate\Support\Facades\Log::debug(['url'=>route($route)]);
?>
<form role="form" class="modal_form" action="{{ route($route) }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="table" value="{{ $table }}">
    <input type="hidden" name="role"  value="{{ $role }}">

    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label for="">{{str_singular($table)}}:<br/></label>
                <select name="selection" size="{{$size}}">
                    {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromArray(null, $data->toArray(), 'NameFull', 'ID') !!}
                </select>
            </div>
            <button type="submit" id="submit_button" class="btn btn-primary">Go</button>
        </div>
    </div>
</form>
