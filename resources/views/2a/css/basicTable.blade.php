<style>
    .grid-on {
        border: 1px solid black;
    }

    .grid-off {
        border: 0;
    }

    .row-zebra-1 {
        background-color: #B0C4DE;
    }

    .row-zebra-2 {
        background-color: #eeeeee;
    }

    .transparent {
        background-color: transparent;
    }

    .cell-pad {
        padding: 2px 5px;
    }

    .cell-center {
        text-align: center;
    }

    table{
        table-layout: fixed;
        width: 100%;
    }

    table tr{
        word-break: break-all;
    }

</style>
