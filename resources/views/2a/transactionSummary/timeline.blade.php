@extends('2a.layouts.master')
@section('content')
    @include('2a.transactionSummary.subViews._transactionSummaryBanner')

    <section class="main-details dashboard clearfix transaction-summary">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="wrap">
            <div class="top">
                <div class="container-fluid">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                @include('2a.transactionSummary.subViews._subMenu')
                            </div>
                            <div class="tb-item right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid">
                    <div>
                        <div id="timeline" class="tab-content">
                            <div class="tb-wrap">
                                <div class="tb-layout">
                                    <div class="tb-item left" id="app">
                                        <v-app>
                                            <timeline
                                                    token="{{csrf_token()}}"
                                                    transaction-i-d="{{$transactionID}}"
                                                    user-role="{{session('userRole')}}"
                                            >

                                            </timeline>
                                        </v-app>
                                    </div>
                                    <div class="tb-item right">
                                        <div class="details-content">
                                            @include('2a.transactionSummary.property_details')
                                        </div>
                                        <div class="details-content">
                                            @include('2a.transactionSummary.key_people')
                                        </div>
                                        @include('2a.transactionSummary.subViews._requestReports')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection