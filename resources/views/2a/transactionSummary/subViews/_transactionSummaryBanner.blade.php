
    <section class="dashboard-banner">
        @if(isset($property['Address']))
            <!--
            <div class="banner-bg"
                 style="background-image: url('https://maps.googleapis.com/maps/api/staticmap?center={{str_replace(', ,', null, $property['Address'])}}&zoom=12&size=2500x500&scale=2&maptype=roadmap&key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8');">
            </div>
            -->
        @else
            <!--
            <div class="banner-bg"
                 style="background-image: url({{asset('images/transactionSummaryBanners/2.jpg')}});">
            </div>
            -->
        @endif
        <div class="wrap transaction-summary-banner-stats">
            <a href="{{ route('transaction.home', $transactionID) }}" class="edit-button"><i
                        class="far fa-edit"></i></a>
            <h1 class="title"><i class="fas fa-map-marker-alt"></i> {{ str_replace(', ,', null, $property['Address']) }}</h1>
            <div class="countdown">{{abs($daysUntilClose)}}
                Day{{$daysUntilClose > 1 ? 's' : null }} {{$daysUntilClose > 0 ? 'Until' : 'Past' }} Closing
            </div>
        </div>
    </section>