<?php /* Deprecated - Delete */ ?>
@extends('2a.layouts.transactionSummary.user-template')

@section('content')

    <?php
        date_default_timezone_set('America/Los_Angeles');
        $data = array();
        $data['Description'] = NULL;
        $data['DateDue'] = NULL;
        $FirstDay = date("Y-m-d", strtotime('monday this week'));
        $LastDay = date("Y-m-d", strtotime('sunday this week'));
        $Date =  date("Y-m-d");
        $dueToday = $dueThisWeek = $pastDue = $upcoming = array();
        $pastDueCounter = $thisWeekCounter = 0;
        $pastDueIDs = $dueThisWeekIDs = array();
        if(!$noTasks)
        {
            foreach($tasks as $t)
            {
                if ($t['DateDue'] > $Date && $t['DateDue'] <= $LastDay)
                {
                    $dueThisWeek[] = $t;
                    $dueThisWeekIDs[] = $t['lk_ID'];
                }else if($t['DateDue'] < $Date)
                {
                    $pastDue[] = $t;
                    $pastDueIDs[] = $t['lk_ID'];
                }else if($t['DateDue'] > $LastDay)
                {
                    $upcoming[] = $t;
                    $upcomingIDs[] = $t['lk_ID'];
                }
            }
        }
        ?>
        <script>
            var pastDue = [];
            var dueThisWeek =[];
            var tasks = [];
            var k = 0;
        </script>
    <?php
    for($i=0;$i<count($pastDue);$i++)
    {
    ?>
    <script>
        k = '<?php echo json_encode($i); ?>';
        pastDue[k] = '<?php echo json_encode($pastDue[$i]); ?>';
    </script>
    <?php
    }
    for($i=0;$i<count($dueThisWeek);$i++)
    {
    ?>
    <script>
        k = '<?php echo json_encode($i); ?>';
        dueThisWeek[k] = '<?php echo json_encode($dueThisWeek[$i]); ?>';
    </script>
    <?php
    }
    for($i=0;$i<count($tasks);$i++)
    {
    ?>
    <script>
        k = '<?php echo json_encode($i); ?>';
        tasks[k] = '<?php echo json_encode($tasks[$i]); ?>';
    </script>
    <?php
    }
    ?>
    <script>
        var id = '<?php echo json_encode($transactionID); ?>';
        var noTasks = '<?php echo json_encode($noTasks); ?>';
    </script>
    <section class="dashboard-banner">
        <div class="banner-bg"
             style="background-image: url({{asset('images/transactionSummaryBanners/2.jpg')}});"></div>
        <div class="wrap">
            {{-- <a href="{{ route('transaction.home', $transactionID) }}" class="edit-button"><i class="far fa-edit"></i></a>
            <h1 class="title"><i class="fas fa-map-marker-alt"></i> {{$property['Address']}}</h1>
            <div class="countdown">{{abs($daysUntilClose)}} Day{{$daysUntilClose > 1 ? 's' : null }} {{$daysUntilClose > 0 ? 'Until' : 'Past' }} Closing</div>
          </div> --}}

            <a href="/" class="edit-button"><i class="far fa-edit"></i></a>
            <h1 class="title"><i class="fas fa-map-marker-alt"></i>4615 Sunset Way, Calabasas,CA 91301</h1>
            <div class="countdown">45 Days Until Closing</div>
        </div>
    </section>
    <section class="main-details dashboard">
        <div class="wrap">
            <div class="top">
                <div class="container-fluid">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                <ul class="list-tabs">
                                    <li class="tab-button current-tab">
                                        <a href="#"><span class="mobile-view"><i
                                                        class="fas fa-clipboard-list"></i></span><span
                                                    class="desktop-view">TO DO ITEMS</span></a>
                                    </li>
                                    <li class="tab-button">
                                        <a href="#"><span class="mobile-view"><i
                                                        class="fas fa-file-alt"></i></span><span class="desktop-view">Documents</span></a>
                                    </li>
                                    <li class="tab-button">
                                        <a href="#"><span class="mobile-view"><i class="fas fa-clock"></i></span><span
                                                    class="desktop-view">Timeline</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tb-item right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid">
                    <div id="task-for-buyer" class="tab-content">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item left">
                                    <div class="content-wrap">
                                        <div class="section-item section-top text-right">
                                            <a href="#" id='btn-addtask' class="btn-danger">+ ADD A TASK</a>
                                        </div>
                                        <!-- modal window section start -->
                                        <div id="task-modal" class="modal-wdg">
                                            <form class="modal-content animate" method="post" action="{{url('/ts/todos/save')}}">
                                                {{ csrf_field() }}
                                                <div class="imgcontainer">
                                                    <span onclick="document.getElementById('task-modal').style.display='none'"
                                                          class="close" title="Close Modal">&times;</span>
                                                </div>
                                                <div class="container">
                                                    <input type="hidden" name="transactionID" value="{{$transactionID}}">
                                                    <input type="hidden" name="clientRole" value="{{$clientRole}}">
                                                    <label for="desc"><b>Description</b></label>
                                                    <input type="text" placeholder="Enter Description"
                                                           name="Description" value="{{$data['Description']}}" required>
                                                    <br/>
                                                    <label class="error_input"
                                                           for="Description">{{$errors->first('Description')}}</label>

                                                    <label for="due"><b>Due Date</b></label>
                                                    <input type="date" placeholder="Enter Due Date" name="DateDue"
                                                           value="{{$data['DateDue']}}" required> <br/>
                                                    <label class="error_input"
                                                           for="DateDue">{{$errors->first('DateDue')}}</label>
                                                </div>

                                                <div class="container btngroup">
                                                    <button name="saveTask" value="true" type="submit">Save</button>
                                                    <button type="button"
                                                            onclick="document.getElementById('task-modal').style.display='none'"
                                                            class="cancelbtn">Cancel
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- modal window section end -->
                                        <div class="display-change text-center">
                                            <a href="#" id="view-mode" class="dot-text">Switch to Full List View</a>
                                        </div>
                                        @if(count($pastDue) > 0)
                                        <div class="section-item"> <!-- PAST DUE SECTION -->
                                            <h3 class="title">Past Due</h3>
                                            <div class="list-dates w-actions">
                                                @foreach($pastDue as $past)
                                                    <div class="date-item" id="p{{$pastDueCounter}}">
                                                        <div class="date"><span class="month">{{date("M", strtotime($past['DateDue']))}}</span><span
                                                                    class="day">{{date("d", strtotime($past['DateDue']))}}</span></div>
                                                        <div class="description">
                                                            <div class="desc-wrap">
                                                                {{$past['Description']}}
                                                                <ul class="action-list">
                                                                    <li><a href="#"><i class="fas fa-check completed"></i></a></li>
                                                                    <li><a href="#"><i class="fas fa-edit edit"></i></a></li>
                                                                    <li><a href="#"><i class="fas fa-times destroy"></i></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="desc-note">
                                                                {{$past['Notes']}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @php
                                                        $pastDueCounter++;
                                                    @endphp
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        <!-- END PAST DUE SECTION -->
                                        <!-- BEGIN DUE THIS WEEK SECTION -->
                                        @if(count($dueThisWeek) > 0)
                                        <div class="section-item">
                                            <h3 class="title">Due This Week</h3>
                                            <div class="list-dates w-actions">
                                                @foreach($dueThisWeek as $thisWeek)
                                                <div class="date-item" id="t{{$thisWeekCounter}}">
                                                    @if(date('Y-m-d',strtotime($thisWeek['DateDue'])) == $Date)
                                                        <div class="date due"><span class="quote-due">Due Today</span></div>
                                                    @else
                                                    <div class="date"><span class="month">{{date("M", strtotime($thisWeek['DateDue']))}}</span><span
                                                                class="day">{{date("d", strtotime($thisWeek['DateDue']))}}</span></div>
                                                    @endif
                                                    <div class="description">
                                                        <div class="desc-wrap">
                                                            {{$thisWeek['Description']}}
                                                            <ul class="action-list">
                                                                <li><a href="#"><i class="fas fa-check completed"></i></a></li>
                                                                <li><a href="#"><i class="fas fa-edit edit"></i></a></li>
                                                                <li><a href="#"><i class="fas fa-times destroy"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="desc-note">
                                                            {{$thisWeek['Notes']}}
                                                        </div>
                                                    </div>
                                                </div>
                                                    @php
                                                        $thisWeekCounter++;
                                                    @endphp
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        <!-- END DUE THIS WEEK SECTION -->
                                        <div class="section-item">
                                            <h3 class="title">Coming Up</h3>
                                            <div class="list-dates w-actions">
                                                @foreach($upcoming as $u)
                                                    <div class="date-item">
                                                        <div class="date"><span class="month">{{date("M", strtotime($u['DateDue']))}}</span><span
                                                                    class="day">{{date("d", strtotime($u['DateDue']))}}</span></div>
                                                        <div class="description">
                                                            <div class="desc-wrap">
                                                                {{$u['Description']}}
                                                                <ul class="action-list">
                                                                    <li><a href="#"><i class="fas fa-check"></i></a></li>
                                                                    <li><a href="#"><i class="fas fa-edit"></i></a></li>
                                                                    <li><a href="#"><i class="fas fa-times"></i></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="desc-note">
                                                                {{$u['Notes']}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="section-item">
                                            <h3 class="title">Upcoming Milestones</h3>
                                            <div class="accordions">
                                                <!-- START ACCORDION ITEMS -->

                                                <div class="accordion-item">
                                                    <div class="accordion-trigger offers-trigger" data-toggle="collapse" data-target=".offers-accordion">
                                                        <h4 class="acc-title">OFFERS</h4>
                                                    </div>
                                                    <div class="offers-accordion">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                                <div class="accordion-item">
                                                    <div class="accordion-trigger">
                                                        <h4 class="acc-title">OPEN ESCROW</h4>
                                                    </div>
                                                    <div class="accordion-target">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                                <div class="accordion-item">
                                                    <div class="accordion-trigger">
                                                        <h4 class="acc-title">DISCLOSURES</h4>
                                                    </div>
                                                    <div class="accordion-target">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                                <div class="accordion-item">
                                                    <div class="accordion-trigger">
                                                        <h4 class="acc-title">Close of Escrow</h4>
                                                    </div>
                                                    <div class="accordion-target">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                                <div class="accordion-item">
                                                    <div class="accordion-trigger">
                                                        <h4 class="acc-title">Move Out</h4>
                                                    </div>
                                                    <div class="accordion-target">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                                <div class="accordion-item">
                                                    <div class="accordion-trigger">
                                                        <h4 class="acc-title">Other Tasks</h4>
                                                    </div>
                                                    <div class="accordion-target">
                                                        <div class="list-dates w-actions">
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">30</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Review Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="date-item">
                                                                <div class="date"><span class="month">AUG</span><span
                                                                            class="day">26</span></div>
                                                                <div class="description">
                                                                    <div class="desc-wrap">
                                                                        Type Up any Counter-Offers
                                                                        <ul class="action-list">
                                                                            <li><a href="#"><i class="fas fa-check"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-edit"></i></a>
                                                                            </li>
                                                                            <li><a href="#"><i class="fas fa-times"></i></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="desc-note">
                                                                        Text box for note added by user, Text box for
                                                                        note added by user Text box for note added by
                                                                        user Text box for note added by user, Text box
                                                                        for note added by user
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ACCOUDION ITEM -->
                                            </div>
                                            <div class="display-change alt text-center">
                                                <a href="#">Show Completed Tasks</a>
                                            </div>
                                        </div> <!-- END SECTION ACCORDION -->
                                    </div>
                                </div>
                                <div class="tb-item right">
                                    <div class="details-content">
                                        <h3 class="title">Property Details</h3>
                                        <div class="details-list">
                                            <div class="detail-item">
                                                <span class="item-title">Purchase Price:</span> $595,000
                                            </div>
                                            <div class="detail-item">
                                                <span class="item-title">Property Type:</span> Single Family House
                                            </div>
                                            <div class="detail-item">
                                                <span class="item-title">Loan Type:</span> Unknown
                                            </div>
                                            <div class="detail-item">
                                                <span class="item-title">Offer Prepared:</span> March 6, 2018
                                            </div>
                                            <div class="detail-item">
                                                <span class="item-title">Close of Escrow:</span> October 6, 2018
                                            </div>
                                        </div>
                                    </div>
                                    <div class="details-content">
                                        <h3 class="title">Key People</h3>
                                        <div class="details-list">
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Buyer:</span> David Rodriguez
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Buyer's Agent:</span> James Green
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Buyer's TC:</span> Jennifer Smith
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Seller:</span> David Rodriguez
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Seller's Agent:</span> James Green
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Seller's TC:</span> David Rodriguez
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Lender:</span> David Rodriguez
                                            </div>
                                            <div class="detail-item people-icon">
                                                <span class="item-title">Escrow:</span> David Rodriguez
                                            </div>
                                        </div>
                                    </div>
                                    <div class="links-list">
                                        <div class="link-item">
                                            <a href="#" class="link">Order Preliminary Title Report</a>
                                        </div>
                                        <div class="link-item">
                                            <a href="#" class="link">Order Natural Hazard Report</a>
                                        </div>
                                        <div class="link-item">
                                            <a href="#" class="link">Order Home Warranty</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- END ACCORDION ITEMS -->
                    </div>
                </div>

                <div class="tb-item right">
                  <div class="details-content">
                    <h3 class="title">Property Details</h3>
                    <div class="details-list">
                      <div class="detail-item">
                        <span class="item-title">Purchase Price:</span> $595,000
                      </div>
                      <div class="detail-item">
                        <span class="item-title">Property Type:</span> Single Family  House
                      </div>
                      <div class="detail-item">
                        <span class="item-title">Loan Type:</span> Unknown
                      </div>
                      <div class="detail-item">
                        <span class="item-title">Offer Prepared:</span> March 6, 2018
                      </div>
                      <div class="detail-item">
                        <span class="item-title">Close of Escrow:</span> October 6, 2018
                      </div>
                    </div>
                  </div>
                  <div class="details-content">
                    <h3 class="title">Key People</h3>
                    <div class="details-list">
                      <div class="detail-item people-icon">
                        <span class="item-title">Buyer:</span> David Rodriguez
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Buyer's Agent:</span> James Green
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Buyer's TC:</span> Jennifer Smith
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Seller:</span> David Rodriguez
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Seller's Agent:</span> James Green
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Seller's TC:</span> David Rodriguez
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Lender:</span> David Rodriguez
                      </div>
                      <div class="detail-item people-icon">
                        <span class="item-title">Escrow:</span> David Rodriguez
                      </div>
                    </div>
                  </div>

                  @include('2a.transactionSummary.subViews._requestReports')

                </div>
              </div>
            </div>
        </div>
    </section>
    <script>
        var token="{{Session::token()}}";
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log(pastDue);
            for(let i = 0; i < pastDue.length; i++)
            {
                let status = null;
                let id = pastDue[i][0];
                $("#p"+i+" ul li .destroy").click(function (){
                    console.log('testing past due');
                    status = 'delete';

                    //$("#p"+i).remove();

                    console.log(id);
                    console.log(status);
                    $.ajax({
                        url : "{{route('tasks.update')}}",
                        type: "post",
                        data: {'id':id, 'status':status, "_token":token},
                        success: function(data){
                            console.log('Success');
                        },
                        error: function(errorThrown){
                            console.log(errorThrown);
                        }
                    });
                });
            }
            for(let j = 0; j < dueThisWeek.length; j++)
            {
                $("#t"+j+" ul li .destroy").click(function (){
                    console.log('testing this week');
                    console.log(j);
                    $("#t"+j).remove();
                });
            }
        });
    </script>
@endsection