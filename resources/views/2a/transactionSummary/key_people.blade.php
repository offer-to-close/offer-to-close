<?php

$seller = $buyer = [];
foreach($people as $fld=>$value)
{
    if (stripos($fld, '_') === false) continue;

    list($f,$i) = explode ('_', $fld);

    if (substr($f, 0, 1) == 'B')
        {
            if (stripos($f, 'Buyer') !== false) $buyer[$i]['name'] = $value;
            if (stripos($f, 'Phone') !== false) $buyer[$i]['phone'] = $value;
            if (stripos($f, 'Email') !== false) $buyer[$i]['email'] = $value;
        }
    elseif (substr($f, 0, 1) == 'S')
        {
            if (stripos($f, 'Seller') !== false) $seller[$i]['name'] = $value;
            if (stripos($f, 'Phone') !== false) $seller[$i]['phone'] = $value;
            if (stripos($f, 'Email') !== false) $seller[$i]['email'] = $value;
        }
    $people->pull($fld);
}
$sellerCount = count($seller);
$buyerCount = count($buyer);

?>
<h3 class="title">Key People</h3>
<div class="details-list">
    @if($buyerCount == 0)
        <div class="detail-item people-icon" data-address="" data-email="" data-phone="" data-name="">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <span class="item-title">Buyer:</span>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <span class="teal add-buyer add-person">Add Buyer</span>
                </div>
            </div>
        </div>
    @endif
    @foreach ($buyer as $index => $row)
        <div class="detail-item people-icon" data-address="{{$row['address']??null }}" data-email="{{ $row['email']??null }}" data-phone="{{ $row['phone']??null }}" data-name="{{ $row['name']??null }}">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <span class="item-title">Buyer:</span>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    {{$row['name']??null}}
                </div>
            </div>
        </div>
    @endforeach
    @if($buyerCount < 2 && $buyerCount > 0)
            <div class="detail-item people-icon" data-address="" data-email="" data-phone="" data-name="">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <span class="item-title">Buyer:</span>
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <span class="teal add-buyer add-person">Add Buyer</span>
                    </div>
                </div>
            </div>
    @endif

    <div class="detail-item people-icon" data-address="{{$people['BA Address']??null }}" data-email="{{ $people['BA Email']??null }}" data-phone="{{ $people['BA Phone']??null }}" data-name="{{ $people['Buyer\'s Agent']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Buyer's Agent:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Buyer\'s Agent']??'<span class="teal add-buyers-agent add-person">Add Buyer\'s Agent</span>'!!}
            </div>
        </div>
    </div>

    <div class="detail-item people-icon" data-address="{{$people['BTC Address']??null }}" data-email="{{ $people['BTC Email']??null }}" data-phone="{{ $people['BTC Phone']??null }}" data-name="{{ $people['Buyer\'s Transaction Coordinator']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Buyer's TC:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Buyer\'s Transaction Coordinator']??'<span class="teal add-buyers-tc add-person">Add BTC</span>' !!}
            </div>
        </div>
    </div>
    @if($sellerCount == 0)
        <div class="detail-item people-icon" data-address="" data-email="" data-phone="" data-name="">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <span class="item-title">Seller:</span>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <span class="teal add-seller add-person">Add Seller</span>
                </div>
            </div>
        </div>
    @endif
    @foreach ($seller as $index => $row)
        <div class="detail-item people-icon" data-address="{{$row['address']??null }}" data-email="{{ $row['email']??null }}" data-phone="{{ $row['phone']??null }}" data-name="{{ $row['name']??null }}">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <span class="item-title">Seller:</span>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    {{$row['name']??null}}
                </div>
            </div>
        </div>
    @endforeach
    @if($sellerCount < 2 && $sellerCount > 0)
        <div class="detail-item people-icon" data-address="" data-email="" data-phone="" data-name="">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <span class="item-title">Seller:</span>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <span class="teal add-seller add-person">Add Seller</span>
                </div>
            </div>
        </div>
    @endif

    <div class="detail-item people-icon" data-address="{{$people['SA Address']??null }}" data-email="{{ $people['SA Email']??null }}" data-phone="{{ $people['SA Phone']??null }}" data-name="{{ $people['Seller\'s Agent']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Seller's Agent:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Seller\'s Agent']??'<span class="teal add-sellers-agent add-person">Add Seller\'s Agent</span>' !!}
            </div>
        </div>
    </div>

    <div class="detail-item people-icon" data-address="{{$people['STC Address']??null }}" data-email="{{ $people['STC Email']??null }}" data-phone="{{ $people['STC Phone']??null }}" data-name="{{ $people['Seller\'s Transaction Coordinator']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Seller's TC:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Seller\'s Transaction Coordinator']??'<span class="teal add-sellers-tc add-person">Add STC</span>' !!}
            </div>
        </div>
    </div>


    <div class="detail-item people-icon" data-address="{{$people['L Address']??null }}" data-email="{{ $people['L Email']??null }}" data-phone="{{ $people['L Phone']??null }}" data-name="{{ $people['Loan']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Lender:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Loan'] ?? '<span class="teal add-lender add-person">Add Lender</span>' !!}
            </div>
        </div>
    </div>

    <div class="detail-item people-icon" data-address="{{$people['E Address']??null }}" data-email="{{ $people['E Email']??null }}" data-phone="{{ $people['E Phone']??null }}" data-name="{{ $people['Escrow']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Escrow:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Escrow'] ?? '<span class="teal add-escrow add-person">Add Escrow</span>' !!}
            </div>
        </div>
    </div>

    <div class="detail-item people-icon" data-address="{{$people['T Address']??null }}" data-email="{{ $people['T Email']??null }}" data-phone="{{ $people['T Phone']??null }}" data-name="{{ $people['Title']??null }}">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <span class="item-title">Title:</span>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                {!! $people['Title'] ?? '<span class="teal add-title add-person">Add Title</span>' !!}
            </div>
        </div>
    </div>

</div>

@section('scripts')
<script>
    $(document).ready(function () {
        $('.people-icon').click(function ()
        {
            var self    = $(this);
            var name    = self.data('name');
            var phone   = self.data('phone');
            var email   = self.data('email');
            var address = $(this).data('address');
            if(address !== undefined && address !== null) address = address.replace(/\s+/g,' ').trim();

            //style modifications here can be found inside of app_2a.css
            var html = '<div class="keyperson-container">\n' +
                '<div class="row text-left">' +
                '<div class="keyperson-name">'+name+'</div>\n' +
                '<div class="keyperson-phone kp-detail">'+phone+'</div>\n' +
                '<div class="keyperson-email kp-detail">'+email+'</div>\n' +
                '<div class="keyperson-address kp-detail">'+address+'</div>\n' +
                '</div>\n' + //end of row
                '</div>'; //end of container div

            if (name !== undefined && name !== null && name !== "") {
                Swal2({
                    html: html,
                    showConfirmButton: false,
                    width:500,
                    showCloseButton: true,
                });
            }
        });

        function postAddRole()
        {
            Swal2({
               title: 'Success!',
               type: 'success',
               timer: 2000,
               showConfirmButton: false,
            });
            location.reload();
        }

        $('.add-escrow, .add-lender, .add-title').click(function ()
        {
            let roleCode;
            let table;
            let model;
            let self = $(this);
            if (self.hasClass('add-escrow'))
            {
                table = 'Escrows';
                model = 'App\\Models\\Escrow';
                roleCode = 'e';
            }
            else if (self.hasClass('add-lender'))
            {
                table = 'Loans';
                model = 'App\\Models\\Loan';
                roleCode = 'l';
            }
            else if (self.hasClass('add-title'))
            {
                table = 'Titles';
                model = 'App\\Models\\Title';
                roleCode = 't';
            }
            else genericError('Error code: aux-selector-001');
            let dataObject = {
                table: table,
                model: model,
                roleCode: roleCode,
                token: '{{csrf_token()}}',
                saveDataRoute: '{{route('saveAux')}}',
                searchRoute: '{{route('search.Person')}}',
                receiveDataRoute: '{{route('getAuxData')}}',
                transactionID: '{{$transactionID}}',
            };
            openAuxModal(dataObject,'addAuxModal', postAddRole);
        });

        $('.add-sellers-tc, .add-buyers-tc').click(function ()
        {
            let roleCode;
            if($(this).hasClass('add-buyers-tc'))roleCode = 'btc';
            else roleCode = 'stc';
            let dataObject = {
                table: 'TransactionCoordinators',
                model: 'App\\Models\\TransactionCoordinator',
                roleCode: roleCode,
                token: '{{csrf_token()}}',
                saveDataRoute: '{{route('saveAux')}}',
                searchRoute: '{{route('search.Person')}}',
                receiveDataRoute: '{{route('getAuxData')}}',
                transactionID: '{{$transactionID}}',
            };
            openTCModal(dataObject,'addTCModal', postAddRole);
        });

        $('.add-sellers-agent, .add-buyers-agent').click(function ()
        {
            let roleCode;
            if($(this).hasClass('add-buyers-agent')) roleCode = 'ba';
            else roleCode = 'sa';
            let dataObject = {
                table: 'Agent',
                model: 'App\\Models\\Agent',
                roleCode: roleCode,
                token: '{{csrf_token()}}',
                saveDataRoute: '{{route('saveAgent')}}',
                searchRoute: '{{route('search.Person')}}',
                receiveDataRoute: '{{route('getAgentData')}}',
                transactionID: '{{$transactionID}}',
            };
            openAgentModal(dataObject,'addAgentModal', postAddRole);
        });

        $('.add-seller, .add-buyer').click(function ()
        {
            let roleCode;
            let route;
            if($(this).hasClass('add-buyer'))
            {
                roleCode = 'b';
                route = '{{route('saveBuyer')}}';
            }
            else
            {
                roleCode = 's';
                route = '{{route('saveSeller')}}';
            }
            let dataObject = {
                roleCode: roleCode,
                token: '{{csrf_token()}}',
                saveDataRoute: route,
                searchRoute: false,
                transactionID: '{{$transactionID}}',
            };
            openBuyerSellerModal(dataObject,'addConsumerModal',postAddRole);
        });
    });
</script>
@endsection
