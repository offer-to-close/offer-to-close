@extends('2a.layouts.master')

@section('content')
@php


@endphp

@include('2a.transactionSummary.subViews._transactionSummaryBanner')
@if(Session::has('fail'))
    @include('2a.partials._failureMessage', [
        '_Message' => Session::get('fail'),
    ])
@endif
<section class="main-details dashboard clearfix transaction-summary">
    <div class="wrap">
        <div class="top">
            <div class="container-fluid">
                <div class="tb-wrap">
                    <div class="tb-layout">
                        <div class="tb-item left">
                            @include('2a.transactionSummary.subViews._subMenu')
                        </div>
                        <div class="tb-item right"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container-fluid">
                <!-- TIMELINE TAB -->
                <div id="timeline" class="tab-content">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                <div class="content-wrap">
                                    <div class="section-item" id="disclosure_title_section">
                                        <h3 class="title">Disclosures</h3>
                                    </div>


                                    <div class="section-item disclosure_past_due_section" style="border: none;">
                                        <div class="list-dates disclosure_past_due">
                                        </div>
                                    </div>
                                    <div class="section-item disclosure_this_week_section" style="border: none;">
                                        <div class="list-dates disclosure_this_week">
                                        </div>
                                    </div>
                                    <div class="section-item disclosure_upcoming_section" style="border: none;">
                                        <div class="list-dates disclosure_upcoming">
                                        </div>
                                    </div>
                                    <div class="section-item" style="border: none;">
                                        <h3 class="title disclosure-show-completed"></h3>
                                        <h3 class="title disclosure-hide-toggle disclosure-hide-completed"></h3>
                                        <div class="list-dates disclosure_completed">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tb-item right">
                                <div class="details-content">
                                    @include('2a.transactionSummary.property_details')
                                </div>

                                <div class="details-content">
                                    @include('2a.transactionSummary.key_people')
                                </div>

                                @include('2a.transactionSummary.subViews._requestReports')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

    var token="{{Session::token()}}";
    var transactionID = '{{$transactionID}}';
    $('.disclosure_completed').toggle(); //Initially hide completed disclosures
    $('.disclosure-hide-toggle').toggle(); //Initially hide the 'Hide Completed Disclosures' option.
    
    function getMonth(string) {
        var month_names =["0","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var res = string.split("-");
        return month_names[parseInt(res[1])];
    }

    function getDate(string) {
        var res = string.split("-");
        return res[2];
    }

    function getInterval(string) {
        var dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var res_date = string.split("-");

        var dtCompare = new Date();
        dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
        dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
        var ftCompare = dtCompare.toLocaleDateString("ko-KR", dateOptions);

        var dtToday = new Date();
        var ftToday = dtToday.toLocaleDateString("ko-KR", dateOptions);

        var diff = 7-dtToday.getDay();

        var dtWeek = new Date();
        dtWeek.setDate(dtWeek.getDate() + diff);
        var ftWeek = dtWeek.toLocaleDateString("ko-KR", dateOptions);

        if (ftCompare < ftToday) {
            return 'past';
        } else {
            if (ftCompare > ftWeek) return 'upcoming';
            else return 'week';
        }
    }


    function drawTimeline() {
        var id = $(location).attr('href').match(/\d+/);
        $(".timeline_past_due").html('');
        $(".timeline_upcoming").html('');
        $(".timeline_this_week").html('');
        $(".timeline_completed").html('');

        $.ajax({
            url : "{{route('get.disclosures')}}",
            type: "post",
            data: {'transactionID':transactionID, "_token":token},
            success: function(data){
                var items = data.disclosures;
                if(items.length === 0)
                {
                    $('#disclosure_title_section').append('<h4>There are no disclosures to show.</h4>');
                }
                $.each(items, function(idx, item) {
                    var isComplete = false;
                    if(item.DateCompleted != null) isComplete = true;

                    var month = getMonth(item.DateDue);
                    var date = getDate(item.DateDue);
                    var interval = getInterval(item.DateDue);

                    var item_html =
                        '<div class="date-item">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div id="disclosure-'+item.ShortName+'-'+item.lk_ID+'" class="desc-wrap" data-id="'+item.ID+'" data-status="0">\n' +
                        '                <span class="action"><i class="fas fa-times"></i></span>'+item.Description+' <a href="#" class="mark-complete">Fill Out</a>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    var item_html_complete =
                        '<div class="date-item">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div id="disclosure-'+item.ShortName+'-'+item.lk_ID+'" class="desc-wrap" data-id="'+item.ID+'" data-status="1">\n' +
                        '                <span class="action"><i class="fas fa-times"></i></span>'+item.Description+' <a href="#" class="mark-complete">Review</a>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';
                    if(isComplete === true) $(".disclosure_completed").append(item_html_complete);
                    else{
                        if(interval === 'past') $(".disclosure_past_due").append(item_html);
                        else if(interval === 'week') $(".disclosure_this_week").append(item_html);
                        else $(".disclosure_upcoming").append(item_html);
                    }



                });
                if($(".disclosure_past_due").children().length > 0) $(".disclosure_past_due_section").prepend('<h4 class="sub-title timeline-past-due">Past Due</h4>');
                if($(".disclosure_this_week").children().length > 0) $(".disclosure_this_week_section").prepend('<h4 class="sub-title">This Week</h4>');
                if($(".disclosure_upcoming").children().length > 0) $(".disclosure_upcoming_section").prepend('<h3 class="sub-title">Upcoming Disclosures</h3>');

                if($('.disclosure_completed').children().length > 0) {
                    $('.disclosure-show-completed').html('Show Completed Disclosures <i class="fa fa-caret-down"></i>');
                    $('.disclosure-hide-completed').html('Hide Completed Disclosures<i class="fa fa-caret-up"></i>');
                }
            },
            error : function(xhr, textStatus, errorThrown ) {
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        $.ajax(this);
                        return;
                    }
                    return;
                }
            },
            timeout: 0,
            statusCode: {
                500: function () {
                    location.reload();
                }
            }
        });
    }

    $(document).ready(function(){
        drawTimeline();
    });

    $(document).on('click', '.desc-wrap', function(){
        var self=$(this);
        var id = $(this).data('id');
        var lk_ID = self.prop('id').split("-");
        lk_ID = lk_ID[2];
        //url for questions route
        var url = '{{route("answer.questionnaire", ['questionnaireID' => ':did', 'transactionID' => ':tid', 'type' => 'disclosure'])}}';
        var reviewURL = '{{route("review.questionnaire", ['transactionID' => ':tid', 'lk_ID' => ':lkid'])}}';
        url = url.replace(':did' , id);
        url = url.replace(':tid', transactionID);

        reviewURL = reviewURL.replace(':tid',transactionID);
        reviewURL = reviewURL.replace(':lkid',lk_ID);
        if(self.data('status') === 0)
        {
            window.location.href = url;
        }
        else
        {
            window.location.href = reviewURL;
            //alert("Review Coming Soon");
        }
    });

    $(document).on('click', '.disclosure-show-completed', function(){
        $(".disclosure-show-completed").toggle();
        $(".disclosure-hide-toggle").toggle();
        $(".disclosure_completed").slideToggle();
    });

    $(document).on('click', '.disclosure-hide-toggle', function(){

        $(".disclosure-show-completed").toggle();
        $(".disclosure-hide-toggle").toggle();
        $(".disclosure_completed").slideToggle();
    });

</script>
</div>
<div style="clear: both"></div>
@endsection