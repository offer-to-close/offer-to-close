@extends('2a.layouts.master')
@section('content')
    @include('2a.transactionSummary.subViews._transactionSummaryBanner')

    <section class="main-details dashboard clearfix transaction-summary">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="wrap">
            <div class="top">
                <div class="container-fluid">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                @include('2a.transactionSummary.subViews._subMenu')
                            </div>
                            <div class="tb-item right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid docs-alt">
                    <div id="app">
                        <v-app>
                            <documents
                                    start-prop="Testtest"
                                    token="{{csrf_token()}}"
                                    transaction-i-d="{{$transactionID}}"
                                    get-documents-route="{{route('ajaxDocuments')}}"
                                    get-required-signers-route="{{route('getRequiredSigners')}}"
                                    get-filestack-signature-route="{{route('getFilestackSignature')}}"
                                    upload-document-route="{{route('save.TransactionDocument')}}"
                                    open-document-bag-route="{{route('openDocumentBag', ['transactionID' => $transactionID, 'documentCode' => ':dc'])}}"
                                    search-person-route="{{route('search.Person')}}"
                                    get-agent-info-route="{{route('getAgentData')}}"
                                    save-agent-data-route="{{route('saveAgent')}}"
                                    save-buyer-data-route="{{route('saveBuyer')}}"
                                    save-seller-data-route="{{route('saveSeller')}}"
                                    mark-document-complete-incomplete-route="{{route('markDocumentAsCompleteIncomplete')}}"
                            >

                            </documents>
                        </v-app>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection