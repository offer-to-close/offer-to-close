<div id="" class="agents-modal modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close">{!! config('otc.modalClose') !!}</button>
            </div>
            <div class="spinner"><i class="fas fa-spinner fa-spin"></i></div>
            <div class="modal-body modal-body-wrapper" style="width: 200%">

                <!-- STEP 1 OF THE AGENT MODAL -->
                <div class="step-1" style="width: 50%;">
                        <header class="header container-fluid">
                            <div class="col-sm-6">
                                <h2 class="modal-title"></h2>
                            </div>
                            <div class="col-sm-6 text-right">
                                <h1>STEP <span class="text-danger">1</span> OF <span class="text-danger">2</span></h1>
                            </div>
                        </header>
                        <div class="row field-row">
                                <input type="hidden" name="ID" value="">
                                <input type="hidden" name="STEP-1" value="1">
                                <input type="hidden" name="Transactions_ID" value="{{ @$data['transaction']['ID'] }}">
                                <div class="col-md-8 search-container col-xs-10 col-sm-10 text-left">
                                    <form action="{{ route('assignRole.Agent') }}">

                                         <div class="col-md-1 text-right" style="padding-right: 2px;padding-top: 7px;"><i class="fas fa-search" style="color: #c94a49"></i></div>
                                            <div class="col-md-12 col-xs-12 field-box">
                                                <input type="hidden" name="_table" value="Agent">
                                                <input type="hidden" name="_model" value="\App\Models\Agent">
                                                <input type="hidden" name="_pickedID" value="">
                                                <input type="hidden" name="_roleCode" value="">
                                                <input name="search_query" class="search_person" autocomplete="off" placeholder="Search existing agents" type="text" id="search_person" >
                                                <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> </ul>
                                                <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i>
                                            </div>

                                        <button type="submit" class="o-teal-btn deactivated select-button" id="select-button" disabled>SELECT</button>

                                    </form>
                                </div>
                        </div>

                        <p style="font-weight: 300">Enter information below to add a new agent or <a class="agentNone o-red-link" style="color: red">click here</a> if no agent is being used</p>

                        <div class="field-row agent-details overflow-hide">
                            <div class=" col-md-3 col-xs-12 form-group with-label ">
                                <label for="NameFirst" class="label-control">FIRST NAME <span class="important">*</span></label>
                                <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter your first Name">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-3 col-xs-12 form-group with-label">
                                <label for="NameLast" class="label-control">LAST NAME <span class="important">*</span></label>
                                <input name="NameLast" class="text-field padding-left-0" placeholder="Enter your last Name">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-3 col-xs-12 form-group with-label">
                                <label for="" class="label-control">NAME ON LICENSE</label>
                                <input name="NameFull" class="text-field padding-left-0" placeholder="">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-3 col-xs-12 form-group with-label">
                                <label for="NameFull" class="label-control">LICENSE #<span class="important"></span></label>
                                <input name="License" class="text-field padding-left-0" placeholder="#license">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                        </div>
                        <div class="field-row row text-left ">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-teal go_next">CONTINUE<span style="position: relative"><i style="position: absolute;font-size: 18px;margin-left: 15px; margin-top:-2px;"class="fas fa-chevron-right"></i></span></button>
                            </div>
                        </div>
                </div>
                <!-- END OF STEP 1 OF THE AGENT MODAL -->
                <!-- STEP 2 OF THE AGENT MODAL -->
                <div class="step-2" style="width: 50%">
                        <input type="hidden" name="STEP-2" value="1">
                        <header class="header container-fluid">
                                <div class="col-sm-6">
                                    <h2 class="modal-title"></h2>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <h1>STEP <span class="text-danger">2</span> OF <span class="text-danger">2</span></h1>
                                </div>
                        </header>
                        <div class="field-row row">
                            <div class=" col-md-4 form-group with-label ">
                                <label for="PrimaryPhone" class="label-control">CELL # <span class="important"><span class="cellphone-required">*</span></span></label>
                                <input name="PrimaryPhone" class="padding-left-0 text-field masked-phone" placeholder="### ### ####">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-4 form-group with-label">
                                <label for="SecondaryPhone" class="label-control">OFFICE # </label>
                                <input name="SecondaryPhone" class="text-field padding-left-0 masked-phone" placeholder="### ### ####">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-4 form-group with-label">
                                <label for="Email" class="label-control">EMAIL <span class="important">*</span></label>
                                <input name="Email" class="text-field padding-left-0" placeholder="">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                        </div>
                        <div class="field-row row">
                            <div class=" col-md-6 col-xs-12 form-group with-label ">
                                <label for="Address" class="label-control">ADDRESS <span class="important"><span class="address-required"></span></span></label>
                                <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address"/>
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                            </div>
                            <div class=" col-md-5 col-xs-12 form-group with-label" style="position: relative">
                                <label for="Unit" class="label-control">BROKERAGE OFFICE <span class="important"></span></label>
                                <input type="hidden" name="BrokerageOffices_ID" id="BrokerageOffices_ID" value="">
                                <input type="text" name="BrokerageOffice_Name" id="search_office" autocomplete="off" class="padding-left-0 text-field" placeholder="Search for Offices">
                                <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <ul id="office_search_list" class="office_search_list col-md-12 col-md-12 col-xs-12">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="field-row row">
                            <div class="col-md-2"><a href="#" style="color: #cd6563" id="add_notes" class="add-notes text-danger" ></a></div>
                            <div class="col-md-8"><textarea name="Notes" id="" class="modal-notes" cols="85" rows="1"></textarea></div>
                        </div>
                        <div class="field-row text-left" style="margin-top: 50px;">
                            <a href="#" id="" style="color:black" class="agents-previous o-link-previous">PREVIOUS</a>
                            <button type="button" id="save_exit" class="btn btn-teal save_exit">SAVE & EXIT&nbsp;&nbsp;&nbsp;<span style="position: relative"><i style="position: absolute;font-size: 18px;margin-left: 15px; margin-top:-2px;"class="fas fa-chevron-right"></i></span></button>
                        </div>
                </div>
                <!-- END OF STEP 2 OF THE AGENT MODAL -->

            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.agentNone').click(function(){
                var transactionID   = $(this).parents('.step-1').find('input[name="Transactions_ID"]').val();
                var role            = $(this).parents('.step-1').find('input[name="_roleCode"]').val();
                $.ajax({
                    type: "POST",
                    url: '{{route('assignRole.Agent')}}',
                    data: {"None": "None", "Transaction_ID" :transactionID, "_roleCode":role},
                    success: function (response) {
                        location.reload();
                    },
                    error: function (response) {
                        swal("Oops...", "Something went wrong!", "error", {timer: 4000});

                    },
                    timeout: 0,

                });
            });

            //-------Add new Brokerage office
            $(document).on('submit', '.add_new_office', function (e) {
                e.preventDefault();
                $('.error').hide();
                $('.spinner').show();
                $('#submit_button').attr('disabled', true);
                var el = $(this),
                    url = el.attr('action'),
                    method = el.attr('method'),
                    data = {};
                el.find(':input').each(function () {
                    var el = $(this),
                        name = el.attr('name'),
                        value = el.val();
                    data[name] = value;
                });
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    error: function (response) {

                        $('.spinner').fadeOut(700, function () {
                            for (x in response['responseJSON']['errors']) {
                                el = $('.modal_form').find('input[name = "' + x + '"]');
                                el.siblings('.error').text(response['responseJSON']['errors'][x]).show();
                            }
                            $('#submit_button').attr('disabled', false);
                        });
                    },
                    success: function (response) {
                        var id = response['ID'],
                            full_name = response['return']
                        $('.spinner').fadeOut(1000, function () {
                            $('#submit_button').attr('disabled', false);
                            $('.modal_form').trigger('reset');
                            $('.close').trigger('click');
                            $('.success_msg').show();
                            $('.success_msg').html('<div class="alert alert-success alert-dismissable text-center"><a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>' + data["modalModel"] + ' ' + full_name + ' added!</div>').fadeOut(10000);
                        });
                        $('#search_office').val(full_name);
                        $('#BrokerageOffices_ID').val(id);
                        $('#BrokerageOffices_ID').trigger('change');

                    }
                });


            });


            var request_data = {};
            $(document).on('input', '.agents-modal :input[name]', function () {
                CHANGES_MADE = true;
            });

            $('.agents-modal .add-notes').on('click', function () {
                var el = $(this);
                noteField = el.closest('.row').find('.modal-notes');
                if (el.text() == '+ ADD A NOTE') {
                    el.text('NOTES');
                    noteField.addClass('active');
                }
                else {
                    el.text('+ ADD A NOTE');
                    noteField.removeClass('active');
                }
            });


            $(document).on('click', '.agents-modal .close', function () {
                modal = $('.agents-modal');
                if (CHANGES_MADE === true) {
                    swal({
                        title: "Are you sure?",
                        text: "Changes will not be saved!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,

                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                CHANGES_MADE = false;

                                modal.find('.modal-body').removeClass('pan-left');
                                modal.find('.modal-body').removeClass('pan-right');
                                fields = modal.find(':input[name]')
                                    .not('input[name="STEP-1"]')
                                    .not('input[name="STEP-2"]')
                                    .not('input[name="Transactions_ID"]')
                                    .not('input[name="_table"]')
                                    .not('input[name="_model"]')
                                    .not('input[name="_pickedID"]')
                                    .not('input[name="_roleCode"]')
                                    .not('input[name="ID"]');
                                fields.each(function () {
                                    self = $(this);
                                    self.val("");
                                    (self.attr('name') == 'Notes') ? self.hide() : '';
                                });
                                modal.modal('toggle');
                            } else {

                            }
                        });
                }
                else {
                    modal.find('.modal-body').removeClass('pan-left');
                    modal.find('.modal-body').removeClass('pan-right');
                    fields = modal.find(':input[name]').not('input[name="Transactions_ID"]')
                        .not('input[name="STEP-1"]')
                        .not('input[name="STEP-2"]')
                        .not('input[name="Transactions_ID"]')
                        .not('input[name="_table"]')
                        .not('input[name="_model"]')
                        .not('input[name="_pickedID"]')
                        .not('input[name="_roleCode"]')
                        .not('input[name="ID"]');
                    fields.each(function () {
                        self = $(this);
                        self.val("");
                        (self.attr('name') == 'Notes') ? self.hide() : '';
                    });
                    modal.modal('toggle');
                }

                $('.errors').hide();
            });

            $(document).on('click', ' .agents-modal .save_exit', function () {
                var btn = $(this);
                var modal = $(this).parents('.agents-modal');
                var spinner = modal.find('.spinner');

                id = modal.find('input[name="ID"]').val();
                ta_ID = modal.find('input[name="Transactions_ID"]').val();
                $('.errors').hide();


                if (CHANGES_MADE === true)
                {
                    var stepData = modal.find('.step-1').find(':input[name]').not('input[name="ID"]');
                    stepData.each(function () {
                        var el = $(this),
                            name = el.attr('name');
                            request_data[name] = el.val();
                    });

                    stepData = modal.find('.step-2').find(':input[name]');
                    stepData.each(function () {
                        var el = $(this),
                            name = el.attr('name');
                            request_data[name] = el.val();
                    });
                }

                var url = "{{route('saveAgent')}}";
                request_data['Transactions_ID'] = ta_ID;
                if (!(id == '')) {
                    request_data['ID'] = id; // To insert agent ID field in both step
                }

                var nameFirst = modal.find('input[name="NameFirst"]').val();
                var nameLast = modal.find('input[name="NameLast"]').val();

                //savedNameFirst and savedNameLast are variables declared in home
                if(nameFirst !== savedNameFirst || nameLast !== savedNameLast) if(modal.find('input[name="ID"]')) delete request_data['ID'];

                if (request_data['ID'] && CHANGES_MADE === true) //Existing users
                {
                    modal.find('.modal-content').addClass('dim');
                    spinner.show();
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: request_data,
                        success: function (response) {
                            if (response['status'] == 'success') {
                                swal("Success", "Data Saved successfully", "success", {timer: 4000});
                                CHANGES_MADE = false;
                                setTimeout(function () {
                                    modal.find('.modal-body').addClass('pan-left');
                                }, 1000);
                            }
                            else {
                                swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                            }
                            modal.find('.modal-content').addClass('dim');
                            request_data = {};
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');
                            if (btn.hasClass('save_exit')) {
                                modal.find('.modal-body').removeClass('pan-left');
                                modal.find('.modal-body').removeClass('pan-right');
                                fields = modal.find(':input[name]')
                                    .not('input[name="STEP-1"]')
                                    .not('input[name="STEP-2"]')
                                    .not('input[name="Transactions_ID"]')
                                    .not('input[name="_table"]')
                                    .not('input[name="_model"]')
                                    .not('input[name="_pickedID"]')
                                    .not('input[name="_roleCode"]')
                                    .not('input[name="ID"]');
                                fields.each(function () {
                                    $(this).val("");
                                });
                                location.reload();
                                modal.modal('toggle');
                            }
                        },
                        error: function (response) {
                            swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');
                            //request_data = {};
                            if (response['responseText'].split(".")[1] == ' This means the address is not valid') {
                                el = $('input[name="Address"]');
                                el.siblings('.errors').text('The Address is not valid.').show();
                            }
                            else if (response['responseJSON']['status'] == 'AddressError') {
                                el = modal.find('input[name="Address"]');
                                el.siblings('.errors').text('The Address is not valid.').show();
                            }
                            for (x in response['responseJSON']['errors']) {
                                el = modal.find('input[name = "' + x + '"]');
                                el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                            }

                        },
                        timeout: 8000,

                    });
                }
                else if (!(request_data['ID']) && CHANGES_MADE == true) {
                    modal.find('.modal-content').addClass('dim');
                    spinner.show();
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: request_data,
                        success: function (response) {
                            if ((response['status'] == 'Pass') && (response['move_to_step'] == 1)) {
                                modal.find('.modal-body').addClass('pan-right');
                            }
                            else if ((response['status']) == 'Pass' && (response['move_to_step'] == 2)) {
                                modal.find('.modal-body').addClass('pan-left');
                            }
                            else if (response['status'] == 'success') {
                                swal("Success", "Data Saved successfully", "success", {timer: 4000});
                                CHANGES_MADE = false;
                                setTimeout(function () {
                                    modal.find('.modal-body').addClass('pan-left');
                                }, 1000);
                            }
                            else {
                                swal("Oops...", "Something went wrong!", "error", {timer: 4000});
                            }
                            modal.find('.modal-content').addClass('dim');
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');
                            if (btn.hasClass('save_exit')) {
                                modal.find('.modal-body').removeClass('pan-left');
                                modal.find('.modal-body').removeClass('pan-right');
                                fields = modal.find(':input[name]')
                                    .not('input[name="STEP-1"]')
                                    .not('input[name="STEP-2"]')
                                    .not('input[name="Transactions_ID"]')
                                    .not('input[name="_table"]')
                                    .not('input[name="_model"]')
                                    .not('input[name="_pickedID"]')
                                    .not('input[name="_roleCode"]')
                                    .not('input[name="ID"]');
                                fields.each(function () {
                                    $(this).val("");
                                });
                                location.reload(true);
                                modal.modal('toggle');
                            }
                        },
                        error: function (response) {
                            swal("Oops...", "Something went wrong!", "error", {timer: 4000});
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');
                            if (response['responseText'].split(".")[1] == ' This means the address is not valid') {
                                el = $('input[name="Address"]');
                                el.siblings('.errors').text('The Address is not valid.').show();
                                swal("Oops...", "Something went wrong!", "error", {timer: 4000});
                                spinner.hide();
                                modal.find('.modal-content').removeClass('dim');
                            }
                            else if (response['responseJSON']['status'] == 'AddressError') {
                                modal.find('.modal-body').addClass('pan-left');
                                el = modal.find('input[name="Address"]');
                                el.siblings('.errors').text('The Address is not valid.').show();
                            }
                            for (x in response['responseJSON']['errors']) {
                                el = modal.find('input[name = "' + x + '"]');
                                el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                            }

                            swal("Oops...", "Something went wrong!", "error", {timer: 4000});
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');
                        },
                        timeout: 8000,

                    });

                }
                else {
                    modal.find('.modal-body').addClass('pan-left');
                    spinner.hide();
                    modal.find('.modal-content').removeClass('dim');
                    if (btn.hasClass('save_exit')) {
                        modal.find('.modal-body').removeClass('pan-left');
                        modal.find('.modal-body').removeClass('pan-right');
                        fields = modal.find(':input[name]')
                            .not('input[name="STEP-1"]')
                            .not('input[name="STEP-2"]')
                            .not('input[name="Transactions_ID"]')
                            .not('input[name="_table"]')
                            .not('input[name="_model"]')
                            .not('input[name="_pickedID"]')
                            .not('input[name="_roleCode"]')
                            .not('input[name="ID"]');
                        fields.each(function () {
                            $(this).val(' ');
                        });
                        modal.modal('toggle');
                        location.reload();
                    }
                }
                //modal.find('.step-2').addClass('show');
            });

            $(document).on('click', '.agents-modal .go_next', function () {
                var el          = $(this);
                var modal       = el.closest('.agents-modal');
                var nameFirst   = modal.find('input[name="NameFirst"]');
                var nameLast    = modal.find('input[name="NameLast"]');
                var move_forward = true;
                if(nameFirst.val() === null || nameFirst.val() === '')
                {
                    nameFirst.siblings('.errors').text('The first name is not valid.').show();
                    move_forward = false;

                }
                if(nameLast.val() === null || nameLast.val() === '')
                {
                    nameLast.siblings('.errors').text('The last name is not valid.').show();
                    move_forward = false;
                }
                if(move_forward) modal.find('.modal-body').addClass('pan-left');
            });

            $('.agents-modal .agents-previous').on('click', function () {
                var el = $(this),
                    modal = el.closest('.agents-modal');
                modal.find('.modal-body').removeClass('pan-left');
            });


            //###############BROKERAGE OFFICE FUNCTIONS

            $(document).on('change', '#BrokerageOffices_ID', function () {
                el = $(this);
                data = {};
                data['id'] = el.val();
                data['_model'] = 'App\\Models\\BrokerageOffice';

                $.ajax({
                    url: '{!! route("details.Office") !!}',
                    type: "get",
                    data: data,
                    success: function (response) {
                        $('#search_office').val(response[0]['Name']);

                        if ($('input[name="Street1"]').val() == "") {
                            for (x in response[0]) {
                                if (!(x == 'PrimaryPhone')) {
                                    el = $('input[name="' + x + '"]');
                                    el.val(response[0][x]);
                                }
                            }
                            $('select[name="State"]').val(response[0]['State']);
                        }
                    }
                });
            });

            $('#BrokerageOffices_ID').trigger('change');
            //Brokerage Office search scripts
            $(document).on('keyup click', '#search_office', function () {
                $('#office_search_list').html(" ");
                $('#BrokerageOffices_ID').val("");
                var el = $(this),
                    LENGTH = 2,
                    data = {},
                    form = el.closest('form');
                APP_URL = '{!! route('search.Office') !!}';
                data['_model'] = 'App\\Models\\BrokerageOffice';
                data['search_query'] = el.val();
                var query = el.val();
                if (query.length > LENGTH) {
                    $('.search_spinner').fadeIn(100);
                    $.ajax({
                        url: APP_URL,
                        type: "POST",
                        data: data,
                        success: function (response) {
                            if (response.length == 0) {
                                $('#office_search_list').html("");
                                $('#office_search_list').append('<li style="pointer-events: none" class="text-muted text-center">No data found </li>');
                                $('.search_spinner').hide();
                            }
                            else {
                                for (i in response) {
                                    $('#office_search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["Name"] + '</li>');
                                }
                            }
                            $('#office_search_list').show();
                            $('#office_search_list').scrollTop(0);
                            $('.search_spinner').hide();
                        },
                        error: function (response) {
                            $('.search_spinner').hide();
                        }
                    });
                }
                else {
                    $('#office_search_list').hide();
                }

            });


            $(document).on('click', '#office_search_list li', function () {
                el = $(this);
                $('#office_search_list').html("");
                $('input[name="BrokerageOffices_ID"]').val(el.attr("data-id"));
                $('#BrokerageOffices_ID').trigger('change');
                $('#office_search_list').hide();
            });

            $(document).on('focusout', '#search_office', function () {
                $('#office_search_list').fadeOut(500);
                $('.search_spinner').fadeOut(500);
            });
            });

    </script>
@append