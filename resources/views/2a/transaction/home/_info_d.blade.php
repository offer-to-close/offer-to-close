<?php
$bigdata=$data;
$dateContract= \App\Library\Utilities\_Time::formatDate($data['transaction']['DateOfferPrepared'], 'fullMonth');
@$dateContract2=$data['transaction']['DateOfferPrepared'];

$dateAccept=\App\Library\Utilities\_Time::formatDate($data['transaction']['DateAcceptance'], 'fullMonth');
?>
<div class="col-xs-12 col-md-6 transaction_details_data-class">
    <div class="white col-xs-12">
        <div class="img"></div>
        <div class="transaction-details text-left">
            <h2>Transaction Details</h2>
            <div class="info">
                <div class="purchase-price col-lg-5 col-xs-12">
                    <span>Purchase Price</span>
                    <h1>{{ \App\Library\Utilities\_Convert::formatDollars($data['transaction']['PurchasePrice'])}}</h1>
                </div>
                <div class="right col-xs-7">
                    <div class="contract-date col-xs-12 ">
                        <span>Contract Date</span>
                        <h6>{{$dateContract}}</h6>
                    </div>
                    <div class="contract-date col-xs-12">
                        <span>Accepted Date</span>
                        <h6>{{$dateAccept}} </h6>
                    </div>
                </div>
                <a class="open-tds-modal" style="text-transform: uppercase;">Edit & View</a>
            </div>
        </div>
    </div>
</div>
