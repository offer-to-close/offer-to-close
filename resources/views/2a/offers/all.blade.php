@extends('2a.layouts.master')

@section('custom_css')
    <style>
        body{
            background-color: white
        }
        .btn-teal{
            background-color: #01bdb7;
            font-weight: 500;
            padding-left: 5px;
            padding-right: 5px;
            padding-top: 15px;
            padding-bottom: 15px;
        }
        .search-text{
            font-size: 20px;
            font-weight: 200;
            width: 100%;
            margin-top: 50px;
            margin-bottom: 50px;
            border-top: none;
            border-left: none;
            border-right: none;
            border-bottom: 1px solid #01bdb7;
            outline: none;
            padding-left:20px;
            padding-right:20px;
            position: relative;
        }
        .search-text:focus{
            border-top: none !important;
            border-left: none;
            border-right: none;
        }
        .search-bar{
            position: relative;
        }
        .main-content{
            background-color: white;
        }
        .add-offer-button{
            margin-top: -10px;
        }
        .o-table{
            margin-top: 10px;
        }
        .search_query{
            border: none;
            margin-bottom: 5px;
        }
        .search_query:focus{
            border: none;
            outline: none;
        }
        .address-col{
            text-align: left !important;
            padding-left: 10px;
        }
    </style>

@endsection

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 col-md-offset-3 search-bar">
                    <input type="text" placeholder="Search" class="search-text" id="search_table">
                    <span class="otc-red" style="position: absolute; top: 40%; left: -1%; font-size: 24px; "><i class="fas fa-map-marker-alt"></i></span>
                </div>
                @if( @$_screenMode == 'Search' )
                    <div class="col-md-3" style="padding-top: 50px">
                        <div class="col-md-12"><a  href="{{ route('offers.byTC', \App\Http\Controllers\CredentialController::current()->ID()) }}"><button class="btn btn-red" style="background-color: #D52A39; font-weight: 600; letter-spacing:1px">Show All</button></a></div>
                    </div>
                @endif
            </div>
            <div class="col-md-10 col-sm-12 col-md-offset-1">
                    <div class="o-box">
                    <div class="title">
                        <span><i class="otc-red fas fa-tags"></i></span> <strong>&nbsp;&nbsp;Offers</strong>
                        <ul class="list-inline pull-right" style="font-size: 15px;">
                            <li><a class="btn btn-teal add-offer-button" style="width: 200px;" href="{{ route('input.Offer') }}">+ ADD NEW OFFER</a></li>
                        </ul>
                    </div>
                    <table class="o-table table-responsive" id="offersTable">
                        @if( $data )
                            <thead>
                                <tr>
                                    <th>Offer #</th>
                                    <th>Listing Address</th>
                                    <th>Client Name</th>
                                    <th>Listing Price</th>
                                    <th>Offer Price</th>
                                    <th>Offer Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $data as $record )
                                    <tr>
                                        <td> {{ $record['ID'] }} </td>
                                        @php $address = \App\Library\otc\AddressVerification::formatAddress($record, 1) @endphp
                                        <td class="address-col"> {{ $address }} </td>
                                        <td> {{ $record["NameFirst"]. ' '. $record['NameLast'] }} </td>
                                        <td> {{ '$'. number_format( $record["PriceListing"] ) }} </td>
                                        <td> {{ '$'. number_format( $record["PriceAsking"] ) }} </td>
                                        <td>
                                            @if ( !empty($record["DateOffer"]) )
                                                {{ date("n/j/Y", strtotime( $record["DateOffer"]) ) }}
                                            @endif
                                        </td>
                                        <td class="">
                                        <span class=""><a class="otc-red edit-icon" title="Edit" href="{{ route('input.Offer', $record['ID']) }}"><i class="far fa-edit"></i></a></span>
                                        <span class=""><a class="otc-red edit-icon upload-icon" href="#" title="Upload" data-id="{{ $record['ID'] }}"><i class="fas fa-cloud-upload-alt"></i></a></span>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        @else
                             <div class="col-md-12 text-center text-danger" style="padding-bottom: 20px;"><h2>No Results found</h2></div>
                        @endif
                    </table>

                </div>
            </div>
        </div>
        @include('2a.offers.modals._modal_docUpload')
@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            $(document).on('click', '.upload-icon', function(){
                el = $(this);
                modal = $('#doc-upload');
                modal.modal({backdrop: 'static', keyboard: false});
                modal.find('input[name="ID"]').val( el.attr('data-id') );
            });

            offersTable = $('#offersTable').DataTable({
                "order" : [[0, "desc"]], //offer #
                "columnDefs": [
                    { "type": "date", "targets": 5 }, //Offer Date
                    { "orderable" : false, "targets" : 6} //Action
                ],
                "sDom":"ltipr"
            });

            $('#search_table').keyup(function () {
                offersTable.search(this.value).draw();
            });

        });
    </script>
@append