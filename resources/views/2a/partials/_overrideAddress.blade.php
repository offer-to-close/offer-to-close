@section('custom_style')
    <style>
        .error-icon{
            color: #E7002A;
        }
    </style>
@endsection
<div style="" id="o-failure-popup" data-address="{{ @$_Address }}" data-transactionid="{{ @$_TransactionId }}">
    <div class="row">
        <div class="col-md-12 text-center failure-icon"><span><i class="fas fa-exclamation-triangle"></i></span></div>
        <div class="col-md-12 failure-text text-center">
            <h4>The address '{{ @$_Address }}' could not be verified. Please check the spelling. If you believe the address is correct as is, you can override.</h4>
            <h4 style="line-height: 50px;">Do you want to <button class="btn btn-sm yes-button" id="save-override">Override</button> or <button class="btn btn-sm no-button" id="close-failure-box">Cancel</button></h4>
            <br>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).on('click', '#close-failure-box', function(){
            setTimeout(function(){
                $('#o-failure-popup').fadeOut(500);
            }, 200);
        });

        $(document).on('click', '#save-override', function(){
            var address=$("#o-failure-popup").data('address');
            var transaction_id=$("#o-failure-popup").data('transactionid');
            $.ajax({
                url : "{{route('override.address')}}",
                type: "post",
                tryCount : 0,
                retryLimit : 3,
                data: {"_token":token, "address":address, "transaction_id":transaction_id},
                success: function(data){
                    location.reload();
                },
                error : function(xhr, textStatus, errorThrown ) {
                    if (textStatus == 'timeout') {
                        this.tryCount++;
                        if (this.tryCount <= this.retryLimit) {
                            $.ajax(this);
                            return;
                        }
                        return;
                    }
                },
                timeout: 0
            });
            setTimeout(function(){
                $('#o-failure-popup').fadeOut(500);
            }, 200);
        });

    </script>
@append