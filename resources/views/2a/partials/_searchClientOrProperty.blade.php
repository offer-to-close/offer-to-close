<?php /*  This should be deleted - deprecated */ ?>
@section('custom_css')
    <style>
        #search_person{
            border-radius: 0px;
            border-color: grey;
            padding-left:10px;
        }
        .search_list{
            position: absolute;
            left: 0px;
            list-style: none;
            padding-left: 5px;
            background: white;
            max-height: 150px;
            overflow-y: scroll;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            z-index: 5;
        }

        .search_list li{
            letter-spacing: 1px;
            margin-top: 5px;
            margin-bottom: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .search_list li:hover{
            background: #f2f2f2;
            cursor: pointer;
        }
        .search_list a{
            list-style: none;
            color: 	#404040;
        }
        .search_list{
            font-weight: 1000;
            color: #090f0f;
            display: none;
        }

        #search_person_spinner{
            font-size: 20px;
            position: absolute;
            top: 20%;
            left: 87%;
            display: none;
        }
        .list-address{
            font-weight: 500;
        }
        .outter-div{
            padding: 0 !important;
        }
        .search_btn{
            border-radius: 3px;
            outline: none;
            border: none;
            background-color: #00C3B9;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .search_btn i{
            color: white !important;
        }
    </style>
@append

<!-- List Pick -->
<?php
// $modelDir = '\\App\\Models\\';
// $role = $_role['code'];
// $roleTable =array_search($role, \Illuminate\Support\Facades\Config::get('constants.USER_ROLE'));
// $roleTable = str_replace(['Buyers', 'Sellers', '-', '_',], null, title_case($roleTable));
// $roleModel = $modelDir . $roleTable;
?>

    <form role="form" action="{{ @$o_action }}" method="POST">
        <div class="outter-div {{ @$o_classes }}">
                {!! csrf_field() !!}
                <input type="hidden" name="_table" value="{{ @$Table }}">
                <input type="hidden" name="_model" value="{{ @$Model }}">
                <input type="hidden" name="_roleCode" value="{{ @$role }}">
                <input type="hidden" name="_pickedID" value="">
                <input name="search_query" style="width: 85%" class="search_query" autocomplete="off" placeholder="{{  @$o_placeholder }}" type="text" id="search_person" value="{{ @$search_query ? @$search_query : ''}}">
                <button type ="submit" name="" style="width: 14%" class="search_btn" type="button"><i class="fas fa-search"></i></button>
                <!-- <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i> -->

                <!-- <div class="col-md-12">
                    <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> </ul>
                </div> -->

        </div>
    </form>

@section('scripts')
@append