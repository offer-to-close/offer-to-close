@extends('2a.layouts.master')
@section('content')
    <div id="app">
        <div class="row">
            <document-bag-share
                    transaction-i-d-prop="{{$transactionID}}"
                    document-code-prop="{{$documentCode}}"
                    document-bag-share-route="{{route('documentBagShareData', ['transactionID' => $transactionID, 'documentCode' => $documentCode])}}"
                    bag-i-d-prop="{{$bagID}}"
                    token="{{csrf_token()}}"
                    user-role="{{session('userRole')}}"
                    document-share-edit-route="{{route('changeBagDocumentSharingSum')}}"
                    document-shared-email-route="{{route('documentSharedEmail')}}">
            </document-bag-share>
        </div>
    </div>
@endsection