<!DOCTYPE html>
<html>
<head>
    <title>AGENT VISUAL INSPECTION DISCLOSURE</title>

    <!-- Custom CSS -->
    <style type="text/css">

        *{
            padding: 0;
            margin: 0;
        }

        body{
            font-family: arial;
        }

        .wrapper{
            width: 1050px;
            margin: 5px auto;
            padding: 5px 10px;
        }

        #top{
            position: relative;

        }

        #p1{
            text-align: center;
            font-weight: bold;
            font-size: 18px;
        }

        #p2{
            word-spacing: 1px;
            line-height: 25px;
            font-size: 24px;
            line-height: 21px;
        }

        #content{
            padding: 15px;
        }

        #content .UL{
            text-decoration: underline;
            line-height: 28px;
        }

        #content p{
            word-spacing: 1px;
            line-height: 25px;
            font-size: 17px;
            line-height: 21px;
        }

        #content p2{
            word-spacing: 1px;
            line-height: 25px;
            font-size: 17.5px;
            line-height: 21px;
        }

        .wrapper-2 #content p{
            line-height: 26px;
        }

        .wrapper-3 #content p{
            line-height: 24px;
        }

        input:focus{
            outline: none;
        }

        input{
            border:none;
            border-bottom: 1px solid black;
            padding-left: 5px;
        }

        #content #input-3::placeholder{
            text-align: center;
            font-style: italic;
            font-weight: bold;
            font-size: 17px;
            color: purple;
        }

        #content #input-4::placeholder{

            font-style: italic;
            font-weight: bold;
            font-size: 17px;
            color: purple;
        }

        .table .col-1{
            padding-top: 7px;

            float: left;
            width: 150px;
            height: 70px;
        }

        .table .col-2{
            padding-top: 7px;

            float: left;
            width: 860px;
            height: 70px;

        }

        .table .col-2 input{
            width: 100%;
            height: 20px;
        }

        .square{
            position: relative;
            border: solid 1px black;
            height: 20px;
            width: 17px;
            display: inline-block;
        }

        .cover{
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            font-size: 25px;
        }

        input{
            font-style: italic;
            font-weight: bold;
            font-size: 14px;
            color: #000d40;
        }

        <?php
        if($data['PropertyType']->Answer == 'yes')
        {
            echo
            "
            .yes.PropertyType .cover:before{
                content: 'X';
                font-weight: bold;
            }
            ";
        }
        ?>

    </style>


<?php
        //dd($data);
        $attach = FALSE;
        $attachment = '<div style="margin-top:100px;"><h2>DESCRIPTIONS CONTINUED<h2></div>';
?>
</head>
<body>
<div class="wrapper">

    <div id="top">
        <img src="{{asset('images/templates/logo.png')}}" style="height: 90px; width: 230px; position: absolute; top:0; left: 0;" >
        <p id="p1">AGENT VISUAL INSPECTION DISCLOSURE<br>(CALIFORNIA CIVIL CODE § 2079 ET SEQ.) <br>For use by an agent when a transfer disclosure statement is<br>required or when a seller is exempt from completing a TDS <br>(C.A.R. Form AVID, Revised 11/13)</p>
    </div>
    <div id="content">
        <p style="line-height: 23px;">
            This inspection disclosure concerns the residential property situated in the City of <input type="text" name="" style="width: 298px; " value="{{$data['PropertyData']['City'] ?? NULL}}"> , County of
            <input type="text" name="" style="width: 270px; " value="{{$data['PropertyData']['County'] ?? NULL}}"> , State of California, described as <input type="text" name="" id="input-3" style="width: 473px;" value="{{$data['PropertyData']['Address'] ?? NULL}}">
            <input type="text" name="" style="width: 880px; "> ("Property").
        </p>
        <p>
            <div class="square yes PropertyType" style="font-size: 30px;"><div class="cover"></div></div>
            <span style="text-align: justify;">This Property is a duplex, triplex, or fourplex. This AVID form is for unit # <input type="text" name="" style="width: 180px;" value="{{$data['UnitNumber']->Answer ?? NULL}}"> . Additional AVID forms required for other units.</span>
            <br>Inspection Performed By (Real Estate Broker Firm Name) <input type="text" name="" style="width: 602px; margin-bottom: 10px;" value="{{$data['RealEstateBrokerFirmName']->Answer ?? NULL}}">
        </p>
        <p style="text-align: justify;">
            <b>California law requires,</b> with limited exceptions, that a real estate broker or salesperson (collectively, "Agent") conduct a reasonably competent and diligent <b>visual</b> inspection of reasonably and normally accessible areas of certain properties offered for sale and then disclose to the prospective purchaser material facts affecting the value or desirability of that property that the inspection reveals. The duty applies regardless of whom that Agent represents. The duty applies to residential real properties containing one-to-four dwelling units, and manufactured homes (mobilehomes). The duty applies to a stand-alone detached dwelling (whether or not located in a subdivision or a planned development) or to an attached dwelling such as a condominium. The duty also applies to a lease with an option to purchase, a ground lease or a real property sales contract of one of those properties.
        </p>
        <p style="margin-top: 7px;">
            <b>California law does not require</b> the Agent to inspect the following:
        <ul style="margin-left: 20px; font-size: 17px;">
            <li>Areas that are not reasonably and normally accessible</li>
            <li>Areas off site of the property</li>
            <li>Public records or permits</li>
            <li>Common areas of planned developments, condominiums, stock cooperatives and the like.</li>
        </ul>
        </p>
        <p id="p2" style="margin-top: 10px; font-size: 16.7px; word-spacing: 0.8px; text-align: justify;">
            <b style="font-size: 17px;">Agent Inspection Limitations:</b> Because the Agent's duty is limited to conducting a reasonably competent and diligent visual inspection of reasonably and normally accessible areas of only the Property being offered for sale, there are several things that the Agent will not do. What follows is a non-exclusive list of examples of limitations on the scope of the Agent's duty.
        </p>
        <p style="padding-left: 30px; text-align: justify; ">
            <b class="UL">Roof and Attic:</b> Agent will not climb onto a roof or into an attic.<br>
            <b class="UL">Interior:</b> Agent will not move or look under or behind furniture, pictures, wall hangings or floor coverings. Agent will not look up chimneys or into cabinets, or open locked doors.<br>
            <b class="UL">Exterior:</b> <span style="word-spacing: -2px;">Agent will not inspect beneath a house or other structure on the Property, climb up or down a hillside, move or look behind plants, bushes, shrubbery and other vegetation or fences, walls or other barriers.</span><br>
            <b class="UL">Appliances and Systems:</b> Agent will not operate appliances or systems (such as, but not limited to, electrical, plumbing, pool or spa, heating, cooling, septic, sprinkler, communication, entertainment, well or water) to determine their functionality.<br>
            <b class="UL">Size of Property or Improvements</b>: <span style="word-spacing: -2px;">Agent will not measure square footage of lot or improvements, or identify or locate boundary lines, easements or encroachments.</span><br>
            <b class="UL">Environmental Hazards:</b> <span style="word-spacing: -1.5px;">Agent will not determine if the Property has mold, asbestos, lead or lead-based paint, radon, formaldehyde or any other hazardous substance or analyze soil or geologic condition.</span><br>
            <b class="UL">Off-Property Conditions:</b> <span style="word-spacing: -1px;">By statute, Agent is not obligated to pull permits or inspect public records. Agent will not guarantee views or zoning, identify proposed construction or development or changes or proximity to transportation, schools, or law enforcement.</span><br>
            <b class="UL">Analysis of Agent Disclosures:</b> <span style="word-spacing: -1px;">For any items disclosed as a result of Agent's visual inspection, or by others, Agent will not provide an analysis of or determine the cause or source of the disclosed matter, nor determine the cost of any possible repair.</span><br>
        </p>
        <p style="text-align: justify; margin-top: 10px; font-size: 16.6px; word-spacing: 0.8px">
            <b>What this means to you:</b> An Agent's inspection is not intended to take the place of any other type of inspection, nor is it a substitute for a full and complete disclosure by a seller. Regardless of what the Agent's inspection reveals, or what disclosures are made by sellers, California Law specifies that a buyer has a duty to exercise reasonable care to protect himself or herself. This duty encompasses facts which are known to or within the diligent attention and observation of the buyer. Therefore, in order to determine for themselves whether or not the Property meets their needs and intended uses, as well as the cost to remedy any disclosed or discovered defect, <b style="font-size: 16.67px; font-weight: bolder;">BUYER SHOULD: (1) REVIEW ANY DISCLOSURES OBTAINED FROM SELLER; (2) OBTAIN ADVICE ABOUT, AND INSPECTIONS OF, THE PROPERTY FROM OTHER APPROPRIATE PROFESSIONALS; AND (3) REVIEW ANY FINDINGS OF THOSE PROFESSIONALS WITH THE PERSONS WHO PREPARED THEM. IF BUYER FAILS TO DO SO, BUYER IS ACTING AGAINST THE ADVICE OF BROKER.</b>
        </p>
        <div style="margin-top: 15px; width: 950px;">
            <div style="float: left;">
                <p>
                    Buyer's Initials
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                </p>
            </div>
            <div style="float: right;">
                <p>
                    Seller's Initials
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                </p>
            </div>
            <div style="clear: both;"></div>
            <p style="margin-top: 10px; margin-bottom: 10px; font-size: 12px;">©2007-2013 California Association of REALTORS®, Inc.</p>
        </div>
        <div id="footer">
            <div style="position: relative;">
                <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: -10px; height: 60px; width: 50px;">
                <p style="font-weight: bolder; font-size: 17px;">AVID REVISED 11/13 (PAGE 1 OF 3)</p>
                <p style="text-align: center; font-weight: bold;">AGENT VISUAL INSPECTION DISCLOSURE (AVID PAGE 1 OF 3)</p>
            </div>
            <div style="border:1px solid black; margin-top: 10px;">
                <p style="font-size: 11px; font-family: times new roman; line-height: 15px;">
                    <a href="#" style="color: black; text-decoration: none;">OfferToClose.com</a>, 5222 Cangas Dr Calabasas, CA 91301
                    <span style="margin-left: 300px;">Phone: 8189002029</span>
                    <span style="margin-left: 100px;">Fax: 8336333782</span>
                    <span style="margin-left: 120px;">Test</span>
                    David Rodriguez - Coordinator
                    <span style="margin-left: 130px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026
							<a href="#" style="color: black; text-decoration: none;">www.zipLogix.com</a>
						</span>
                </p>
            </div>
        </div>

    </div>
</div>



<div class="wrapper wrapper-2" style="padding-top: 20px; margin-top: 20px;">
    <br>
    <div id="content">
        <p style="margin-bottom: 5px;">
            <small>Property Address:</small>
            <input type="text" name="" id="input-4" placeholder="" style="width: 630px;" value="{{$data['PropertyData']['Address'] ?? NULL}}"><small> Date: </small><input type="text" name="" style="width: 216px;" value="">
        </p>
        <p>
            If this Property is a duplex, triplex, or fourplex, this AVID is for unit # <input type="text" name="" style="width: 150px;" value="{{$data['UnitNumber']->Answer ?? NULL}}"> .<br>
            Inspection Performed By (Real Estate Broker Firm Name) <input type="text" name="" style="width: 568px;" value="{{$data['RealEstateBrokerFirmName']->Answer ?? NULL}}">
            Inspection Date/Time: <input type="text" name="" style="width: 200px;" value="{{ ($data['InspectionDate']->Answer ?? NULL) . ' ' . ($data['InspectionTime']->Answer ?? NULL) }}"> Weather conditions: <input type="text" name="" style="width: 476px;" value="{{$data['WeatherConditions']->Answer ?? NULL}}">
            Other persons present:
            <?php
                $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['OtherPersonsPresent']->Answer, [110]);
                if($inputs['extra'])
                {
                    $attach = TRUE;
                    $attachment .= '<br><div><h5>Other Persons Present: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                }
            ?>
            <input maxlength="110" type="text" name="" style="width: 833px;" value="{{$inputs['inputs'][0]}}">
            <b>THE UNDERSIGNED, BASED ON A REASONABLY COMPETENT AND DILIGENT VISUAL INSPECTION OF THE REASONABLY AND NORMALLY ACCESSIBLE AREAS OF THE PROPERTY, STATES THE FOLLOWING:</b>
        </p>

        <div class="table" style="margin-top: 5px;">
            <div class="row-special">
                <div class="sub-row1">
                    <div style="float: left;">
                        <p><b>Entry </b>(excluding common areas):</p>
                    </div>
                    <div style="float: left; width: 755px;">
                        <?php
                            $inputs = [
                                109,
                                125,
                                125,
                            ];
                            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Entry']->Answer, $inputs);
                            if($inputs['extra'])
                            {
                                $attach = TRUE;
                                $attachment .= '<br><div><h5>Entry: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                            }
                        ?>
                        <input maxlength="111" type="text" name="" style="width: 100%; height: 15px;" value="{{$inputs['inputs'][0]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="sub-row2">
                    <div style="width: 150px; height: 50px; float: left;">

                    </div>
                    <div style="width: 860px; float: left;">
                        <input maxlength="128" type="text" name="" style="width: 100%; height: 15px;" value="{{$inputs['inputs'][1]}}">
                        <input maxlength="128" type="text" name="" style="width: 100%; height: 15px;" value="{{$inputs['inputs'][2]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <div class="row2">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['LivingRoom']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Living Room: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Living Room:</b></p>
                </div>
                <div class="col-2">
                    <input maxlength="128" type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input maxlength="128" type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input maxlength="128" type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row3">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['DiningRoom']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Dining Room: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Dining Room:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row4">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Kitchen']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Kitchen: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Kitchen:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row5">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['OtherRoom']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other Room: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other Room:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row-special">
                <div class="sub-row1">
                    <div style="float: left;">
                        <?php
                        $inputs = [
                            97,
                            125,
                            125,
                        ];
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['HallStairs']->Answer, $inputs);
                        if($inputs['extra'])
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>Hall Stairs: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                        }
                        ?>
                        <p><b>Hall/Stairs (excluding common areas):</b></p>
                    </div>
                    <div style="float: left; width: 700px;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][0]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="sub-row2">
                    <div style="width: 150px; height: 50px; float: left;">

                    </div>
                    <div style="width: 860px; float: left;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][1]}}">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][2]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <div class="row7">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bedroom1']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bedroom 1: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bedroom # <input type="text" name="" style="width: 25px;" value="{{$data['Bedroom1']->Answer ? '1': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row8">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bedroom2']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bedroom 2: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bedroom # <input type="text" name="" style="width: 25px;" value="{{$data['Bedroom2']->Answer ? '2': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row9">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bedroom3']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bedroom 3: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bedroom # <input type="text" name="" style="width: 25px;" value="{{$data['Bedroom3']->Answer ? '3': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row10">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bath1']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bath 1: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bath # <input type="text" name="" style="width: 50px;" value="{{$data['Bath1']->Answer ? '1': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row11">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bath2']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bath 2: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bath # <input type="text" name="" style="width: 50px;" value="{{$data['Bath2']->Answer ? '2': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row12">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Bath3']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Bath 3: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Bath # <input type="text" name="" style="width: 50px;" value="{{$data['Bath3']->Answer ? '3': NULL}}"> :</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row5">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['OtherRoom1']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other Room 2: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other Room:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

        <div style="margin-top: 5px; width: 950px;">
            <div style="float: left;">
                <p>
                    Buyer's Initials
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                </p>
            </div>
            <div style="float: right;">
                <p>
                    Seller's Initials
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                    <input type="text" name="" style="width: 70px; height: 20px; padding-left: 10px; border:1px solid black; border-top: none; border-radius: 8px;">
                </p>
            </div>
            <div style="clear: both;"></div>
        </div>

        <div id="footer" style="margin-top: 5px;">
            <div style="position: relative;">
                <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; height: 60px; width: 50px;">
                <p style="font-weight: bolder; font-size: 17px;">AVID REVISED 11/13 (PAGE 2 OF 3)</p>
                <p style="text-align: center; font-weight: bold;">AGENT VISUAL INSPECTION DISCLOSURE (AVID PAGE 2 OF 3)</p>
                <p style=" font-size: 12px; text-align: center;">
                    Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026
                    <a href="#" style="color: black; text-decoration: none;">www.zipLogix.com</a>
                </p>
            </div>
        </div>

    </div>
</div>

<br>

<div class="wrapper wrapper-3" style="margin-bottom: 0px; padding-bottom: 0px;">


    <div id="content">
        <p style="margin-bottom: 5px;">
            <small>Property Address:</small>
            <input type="text" name="" id="input-4" placeholder="" style="width: 630px; " value="{{$data['PropertyData']['Address'] ?? NULL}}"><small> Date: </small><input type="text" name="" style="width: 216px; " value="">
        </p>
        <p>
            If this Property is a duplex, triplex, or fourplex, this AVID is for unit # <input type="text" name="" style="width: 150px; " value="{{$data['UnitNumber']->Answer ?? NULL}}"> .<br>
        </p>

        <div class="table">
            <div class="row1">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['OtherRoom2']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other Room 3: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other Room:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{ $inputs['inputs'][0] }}">
                    <input type="text" name="" value="{{ $inputs['inputs'][1] }}">
                    <input type="text" name="" value="{{ $inputs['inputs'][2] }}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row1">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Other1']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other 1: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row1">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Other2']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other 2: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row1">
                <div class="col-1">
                    <?php
                    $inputs = [
                        125,
                        125,
                        125,
                    ];
                    $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Other3']->Answer, $inputs);
                    if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment .= '<br><div><h5>Other 3: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                    }
                    ?>
                    <p><b>Other:</b></p>
                </div>
                <div class="col-2">
                    <input type="text" name="" value="{{$inputs['inputs'][0]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][1]}}">
                    <input type="text" name="" value="{{$inputs['inputs'][2]}}">
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="row-special">
                <div class="sub-row1">
                    <div style="float: left;">
                        <?php
                        $inputs = [
                            91,
                            125,
                            125,
                        ];
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['GarageParking']->Answer, $inputs);
                        if($inputs['extra'])
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>Garage Parking: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                        }
                        ?>
                        <p><b>Garage/Parking (excluding common areas):</b></p>
                    </div>
                    <div style="float: left; width: 656px;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][0]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="sub-row2">
                    <div style="width: 150px; height: 50px; float: left;">

                    </div>
                    <div style="width: 860px; float: left;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][1]}}">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][2]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <div class="row-special">
                <div class="sub-row1">
                    <div style="float: left;">
                        <?php
                        $inputs = [
                            89,
                            125,
                            125,
                        ];
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['ExteriorBuildingYard']->Answer, $inputs);
                        if($inputs['extra'])
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>Exterior Building Yard: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                        }
                        ?>
                        <p><b>Exterior Building and Yard - Front/Sides/Back:</b></p>
                    </div>
                    <div style="float: left; width: 633px;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{ $inputs['inputs'][0] }}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="sub-row2">
                    <div style="width: 150px; height: 50px; float: left;">

                    </div>
                    <div style="width: 860px; float: left;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][1]}}">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][2]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <div class="row-special">
                <div class="sub-row1">
                    <div style="float: left;">
                        <?php
                        $inputs = [
                            72,
                            125,
                            125,
                        ];
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['OtherObservedConditions']->Answer, $inputs);
                        if($inputs['extra'])
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>Other Observed Conditions: continued: </h5><p>'.$inputs['extra'].'</p></div>';
                        }
                        ?>
                        <p><b>Other Observed or Known Conditions Not Specified Above:</b></p>
                    </div>
                    <div style="float: left; width: 525px;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{ $inputs['inputs'][0] }}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="sub-row2">
                    <div style="width: 150px; height: 50px; float: left;">

                    </div>
                    <div style="width: 860px; float: left;">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][1]}}">
                        <input type="text" name="" style="width: 100%; height: 20px;" value="{{$inputs['inputs'][2]}}">
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>

        <p style="line-height: 20px; text-align: justify;">
            <b>This disclosure is based on a reasonably competent and diligent visual inspection of reasonably and normally accessible areas of the Property on the date specified above.</b>
        </p>
        <p>
            Real Estate Broker (Firm who performed the Inspection) <input type="text" name="" style="width: 580px;" value="{{ $data['RealEstateBrokerFirmName']->Answer ?? NULL }}"><br>By<input type="text" name="" style="width: 680px;"> Date: <input type="text" name="" style="width: 258px;" value="">
        </p>
        <span style="margin-left: 160px; font-size: 15px;">(Signature of Associate Licensee or Broker)</span>

        <p style="margin-top: 15px; text-align: justify;">
            <b><span style="word-spacing: -2px;">Reminder: Not all defects are observable by a real estate licensee conducting an inspection. The inspection does not include testing of any system or component. Real Estate Licensees are not home inspectors or contractors. BUYER SHOULD OBTAIN ADVICE ABOUT AND INSPECTIONS OF THE PROPERTY FROM OTHER APPROPRIATE PROFESSIONALS. IF BUYER FAILS TO DO SO, BUYER IS ACTING AGAINST THE ADVICE OF BROKER. <br>I/we acknowledge that I/we have read, understand and received a copy of this disclosure.</span></b>
        </p>

        <p style="line-height: 24px;">
            SELLER <input type="text" name="" style="width: 720px;"> Date <input type="text" name="" style="width: 171px;">
        </p>
        <p style="line-height: 24px;">
            SELLER <input type="text" name="" style="width: 720px;"> Date <input type="text" name="" style="width: 171px;">
        </p>
        <p style="line-height: 24px;">
            BUYER <input type="text" name="" style="width: 727px;"> Date <input type="text" name="" style="width: 171px;">
        </p>
        <p style="line-height: 24px; margin-bottom: 10px;">
            BUYER <input type="text" name="" style="width: 727px;"> Date <input type="text" name="" style="width: 171px;">
        </p>

        <p>
            Real Estate Broker (Firm Representing Seller) <input type="text" name="" style="width: 656px;" value="{{$data['RealEstateBrokerSeller']->Answer ?? NULL}}"><br>By<input type="text" name="" style="width: 771px;"> Date <input type="text" name="" style="width: 168px;">
        </p>
        <span style="margin-left: 418px; font-size: 15px;">(Associate Licensee or Broker Signature)</span>
        <span style="margin-left: 423px; font-size: 17px; font-weight: 500; letter-spacing: -.9px; font-family: 'Courier'">Formatic Property Management</span>
        <p>
            Real Estate Broker (Firm Representing Buyer) <input type="text" name="" style="width: 656px;" value="{{$data['RealEstateBrokerBuyer']->Answer ?? NULL}}"><br>By<input type="text" name="" style="width: 771px;"> Date <input type="text" name="" style="width: 168px;">
        </p>
        <span style="margin-left: 418px; font-size: 15px;">(Associate Licensee or Broker Signature)</span>

        <p style="font-size: 12.5px; line-height: 15px; margin-top: 10px; text-align: justify;">
            <span style="">© 2007-2013, California Association of REALTORS®, Inc. United States copyright law (Title 17 U.S. Code) forbids the unauthorized distribution, display and reproduction of this form, or any portion thereof, by photocopy machine or any other means, including facsimile or computerized formats. </span><br>
            <span style="font-size: 12.7px;">THIS FORM HAS BEEN APPROVED BY THE CALIFORNIA ASSOCIATION OF REALTORS®. NO REPRESENTATION IS MADE AS TO THE LEGAL VALIDITY OR ACCURACY OF ANY PROVISION IN ANY SPECIFIC TRANSACTION. A REAL ESTATE BROKER IS THE PERSON QUALIFIED TO ADVISE ON REAL ESTATE TRANSACTIONS. IF YOU DESIRE LEGAL OR TAX ADVICE, CONSULT AN APPROPRIATE PROFESSIONAL.</span><br>
            This form is made available to real estate professionals through an agreement with or purchase from the California Association of REALTORS®. It is not intended to identify the user as a REALTOR®. REALTOR® is a registered collective membership mark which may be used only by members of the NATIONAL ASSOCIATION OF REALTORS® who subscribe to its Code of Ethics.
        </p>

        <div style="position: relative;">
            <img src="{{asset('images/templates/rebs.png')}}" style="width: 55px; height: 73px; position: absolute; top: 0; left: 5px;">
            <p style="margin-left: 60px; font-size: 12px; line-height: 17px;">
                Published and Distributed by:<br>
                REAL ESTATE BUSINESS SERVICES, INC.<br>
                a subsidiary of the CALIFORNIA ASSOCIATION OF REALTORS® <br>
                525 South Virgil Avenue, Los Angeles, California 90020
            </p>
        </div>
        <div id="footer">
            <div style="position: relative;">
                <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: -25px; height: 60px; width: 50px;">
                <p style="font-weight: bolder; line-height: 14px;  font-size: 13px;">AVID REVISED 11/13 (PAGE 3 OF 3)</p>
                <p style="text-align: center; line-height: 17px; font-size:15px; font-weight: bold;">AGENT VISUAL INSPECTION DISCLOSURE (AVID PAGE 3 OF 3)</p>
                <p style="line-height: 12px; font-size: 12px; text-align: center;">
                    Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026
                    <a href="#" style="color: black; text-decoration: none;">www.zipLogix.com</a>
                </p>
            </div>
        </div>
    </div>
</div>
@if($attach)
   {!! $attachment !!}
@endif
</body>
</html>