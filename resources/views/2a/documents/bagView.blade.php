@extends('2a.layouts.master')
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <document-bag-view
                        fetch-document-route-prop="{{ route('ajaxFetchDocument') }}"
                        document-bag-route-prop="{{ route('ajaxGetDocumentsInBag') }}"
                        upload-document-route-prop="{{route('save.TransactionDocument')}}"
                        get-property-route-prop="{{route('get.Property', ['taID' => $transactionID])}}"
                        passed-token="{{csrf_token()}}"
                        document-code-prop="{{$documentCode}}"
                        transaction-i-d-prop="{{$transactionID}}"
                        sig-route-prop="{{route('getFilestackSignature')}}"
                        all-documents-route-prop="{{route('transactionSummary.documents', ['transactionID' => $transactionID])}}"
                        transaction-home-route-prop="{{route('transaction.home', ['taid' => $transactionID])}}"
                        signature-integration-prop="{{route('signature.getDocuSignAuth')}}"
                        document-permission-route="{{route('documentPermissionTable', ['transactionID' => $transactionID, 'documentCode' => $documentCode])}}"
                        document-bag-share-route="{{route('documentBagShareView', ['transactionID' => $transactionID, 'documentCode' => $documentCode])}}"
                        transaction-summary-route="{{route('transactionSummary.timeline', ['transactionID' => $transactionID])}}"
                        permissions="{{$permissions}}"
                        signature-gateway-prop="{{route('signature.gateway', ['transactionID' => $transactionID, 'bagIDString' => ':bID', 'signatureAPI' => ':sigAPI'])}}"
                        get-required-signers-route="{{route('getRequiredSigners')}}"
                        search-person-route="{{route('search.Person')}}"
                        get-agent-info-route="{{route('getAgentData')}}"
                        save-agent-data-route="{{route('saveAgent')}}"
                        save-buyer-data-route="{{route('saveBuyer')}}"
                        save-seller-data-route="{{route('saveSeller')}}"
                        mark-bag-document-complete-incomplete-route="{{route('markBagDocumentAsCompleteIncomplete')}}"
                        signer-add-remove-route="{{route('signerAddRemove')}}"
                        user-i-d="{{auth()->id()}}"
                        save-ever-sign-route="{{route('saveEverSignSignedDocument')}}"
                        ever-sign-route="{{route('signature.everSign')}}"
                >
                    <!-- Note: since we do not know what document they will be selecting, we are passing in :bID to change that value inside of the component. -->
                </document-bag-view>
            </div>
        </div>
    </div>
@endsection