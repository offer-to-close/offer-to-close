<?php //  Master Tools Menu   // ?>
@extends('2a.layouts.master')
@section('css')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}">Offer To Close Admin Menu</h1>
                    </div>
                </div>

                @if(\App\Http\Controllers\AccessController::hasAccess('s'))
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}"><i class="fas fa-code-branch"></i> Staff Tools</h1>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @include('2a.dashboard.staff.justMenu')
                    </div>
                @endif

                @if(\App\Http\Controllers\AccessController::hasAccess('a'))
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}"><i class="fab fa-adn"></i> Administrator Tools</h1>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @include('2a.dashboard.admin.justMenu')
                    </div>
                @endif

                @if(\App\Http\Controllers\AccessController::hasAccess('d'))
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}"><i class="fas fa-cogs"></i> Developer Tools</h1>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @include('2a.dashboard.developer.justMenu')
                    </div>
                @endif

                @if(\App\Http\Controllers\AccessController::hasAccess('z'))
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}"><i class="fas fa-user-secret"></i> Super Tools</h1>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @include('2a.dashboard.extra.justMenu')
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection