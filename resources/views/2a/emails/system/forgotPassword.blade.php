@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['u'] ? 'Hi '.$data['u'].',':'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            To reset your password, click here: @include('2a.emails.emailTemplateComponents.link', [
                'href' => $data['resetPasswordLink'] ?? NULL,
                'display' => 'Reset Password'
            ])
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'If you did not forget your password, please ignore this message.',
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thanks,'
    ])
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'The Offer To Close Team'
    ])
@endsection