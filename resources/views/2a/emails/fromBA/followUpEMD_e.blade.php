@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I wanted to check and see if you have received the earnest money deposit from
            {{$data['b']??'_(Buyer)_'}} for {{$data['pStreet']??'_(Property Street Address)_'}}.
            If so, please send a receipt for our records. If not, please let us know and we’ll
            follow-up to see why you do not yet have it.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba'] ?? '_(Buyer\'s Agent)_'
    ])
@endsection