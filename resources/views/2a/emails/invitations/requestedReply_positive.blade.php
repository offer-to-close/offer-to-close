@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello '.($data['name'] ?? '').','
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Your request for an Offer To Close membership has been approved!
            To finish the registration process, you should click on the following link and follow the directions.
            @if(isset($data['transactionID']))
                When you do, you will be automatically attached to the transaction you requested.
            @endif
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Registration link: @include('2a.emails.emailTemplateComponents.link', [
                'href' => $data['url'] ?? '#',
                'display' => $data['url'] ?? '_(Registration Link)_'
            ])
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Welcome to the future of real estate.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Sincerely,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => 'The Offer To Close Team'
    ])
@endsection