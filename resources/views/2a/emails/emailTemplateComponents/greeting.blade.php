<table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
    <tbody>
    <tr>
        <td class="center-text content editor-col OneColumnMobile" width="100%" align="left" valign="top" style="text-align: center;">
            <div class="gl-contains-text">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                    <tr>
                        <td class="editor-text center-text content-text" align="left" valign="top" style="font-family: Verdana,Geneva,sans-serif; font-size: 14px; color: #2e2f2f; text-align: center; display: block; word-wrap: break-word; line-height: 1.2; padding: 10px 40px;">
                            <div></div>
                            <div class="text-container galileo-ap-content-editor">
                                <div>
                                    <div style="text-align: left;" align="left">
                                        <span style="font-size: 16px;">
                                            {!! $greeting !!}
                                        </span>
                                    </div>
                                </div>
                            </div> </td>
                    </tr>
                    </tbody>
                </table>
            </div> </td>
    </tr>
    </tbody>
</table>