@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">{{$data['ba'] ? $data['ba'].',':'Hello,'}}</h5>
            <p style="font-size: 15px;">
                Please see the attached request for repairs for your review on {{$data['p']??'_(Property Address)_'}}.
                Please let {{$data['ba']??'_(Buyer\'s Agent)_'}} know if you have any questions regarding the contents,
                thank you!
            </p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['ba'] ? $data['ba'].',':'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please see the attached request for repairs for your review on '.($data['p']??'_(Property Address)_').
                ' Please let '. ($data['ba']??'_(Buyer\'s Agent)_') .' know if you have any questions regarding the contents,
                thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc']??'_(Buyer\'s TC)_'
    ])
@endsection