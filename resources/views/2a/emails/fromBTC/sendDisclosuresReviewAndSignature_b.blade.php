
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p style="font-size: 15px;">
                Hello! Please see attached seller disclosures from the sellers for {{$data['p']??'_(Property Address)_'}}.
                You should review these to ensure you understand them prior to signing.
                If you have questions, please let your agent or us know and we’ll try our best to help out.
            </p><br>
            <p style="font-size: 15px;">
                Please confirm receipt, thank you!
            </p><br>
            <p>Best,</p><br>
            <p>{{$data['btc'] ?? '_(Buyer\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please see attached seller disclosures from the sellers for '.($data['p']??'_(Property Address)_').
                ' You should review these to ensure you understand them prior to signing.
                If you have questions, please let your agent or us know and we’ll try our best to help out.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please confirm receipt, thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection