<!-- Admin  Tools -->
<form action="{{route('admin.menuProcess')}}" method="post">
    {{csrf_field()}}
    <button type="submit" name="btnAction" value="admin.documentDefaults">Document Access Defaults</button>
    <button type="submit" name="btnAction" value="admin.taskDefaults">Task Definitions</button>
    <button type="submit" name="btnAction" value="admin.userSummary">User Summary</button>
    <button type="submit" name="btnAction" value="admin.summaries">Data Summaries</button>
    <hr/>
    <button class="teal" type="submit" name="btnAction" value="admin.switchCredentials"><strong>Switch Credentials</strong></button>
    <button type="submit" name="btnAction" value="admin.deactivateUser">Deactivate User</button>
</form>