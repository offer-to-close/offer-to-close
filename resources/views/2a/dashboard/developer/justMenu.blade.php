<!-- Developer  Tools -->
<form action="{{route('dev.menuProcess')}}" method="post">
    {{csrf_field()}}
    <button type="submit" name="btnAction" value="dev.transactionDocs">Documents by Transaction</button>
<!--    <button type="submit" name="btnAction" value="dev.shareRooms">Share Rooms</button>  -->
<!--    <button type="submit" name="btnAction" value="dev.shareRoomDocs">Documents by Share Room</button>  -->
    <button type="submit" name="btnAction" value="dev.bagDocs">Bag Documents Access</button>
    <button type="submit" name="btnAction" value="dev.transactionRoles">Roles by Transaction</button>
    <button type="submit" name="btnAction" value="dev.transactionFiles">Files by Transaction</button>
    <button type="submit" name="btnAction" value="dev.users">View Users</button>
    <button type="submit" name="btnAction" value="dev.userTransactions">Transactions By User</button>
    <button type="submit" name="btnAction" value="dev.routes">Route List</button>
    <br/><br/>
    <button type="submit" name="btnAction" value="dev.dataModel"><strong>Data Model</strong></button>
    <br/><br/>
    <button class="red" type="submit" name="btnAction" value="dev.makeInviteCode"><strong>Make Invite Code</strong></button>
    <br/><br/>
    <button class="red" type="submit" name="btnAction" value="dev.testApi"><strong>Test API</strong></button>
</form>