<?php
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// .. This code is for creating sub-navs based on certain user types
// .. This code should always be the last elements in the left nav
$typeCodes = \Illuminate\Support\Facades\Config::get('constants.UserType');
$isAdmin = session('isAdmin');
?>
@if(\App\Http\Controllers\AccessController::hasAccess('s'))
    <div class="item">
            <a class="main"> <span class="menu-icon teal"><i class="fas fa-cogs"></i></span>
            <span class="menu-title">System Tools</span>
            </a>
            <div class="sub">
            <a href="{{route('all.menuView')}}">Main Menu</a>
            </div>
        @if($isAdmin)
            <div class="sub">
                <a href="{{route('admin')}}">Admin Portal</a>
            </div>
            @endif

    </div>
    @endif

<?php // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ?>