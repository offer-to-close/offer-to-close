<?php
$jsonAlertIDs = json_encode(array_column($alerts, 'alertID'));
?>
<div class="item alerts">
    <div class="alerts-wrapper">
        <div class="alerts">
            <div class="notification item">
                {!! $icon ?? null!!}
                <div class="container" id="containerNotifications">
                    <div class="box">
                        <button class="alertButtons" id="mark_all_as_read_alerts">Mark All as Read</button>
                        <button class="alertButtons" id="clear_all_alerts">Clear All</button>
                    </div>
                    <?php
                    $dateNow = new DateTime('now');
                    $dateTomorrow = new DateTime("tomorrow");
                    foreach ($alerts as $alertItem)
                    {
                        $iconType = "";
                        $box = "box_teal";
                        $dateAlert = new DateTime($alertItem["dueDate"]);

                        switch (strtolower($alertItem["type"]))
                        {
                            case "document":
                                $iconType = "fas fa-file-alt fa";
                                $route = route('transactionSummary.documents', ['transactionID'=> $alertItem['transactionID']]);
                                break;
                            case "task":
                                $iconType = "fas fa-clipboard-list fa";
                                $route = route('transactionSummary.tasks', ['transactionID'=> $alertItem['transactionID']]);
                                break;
                            case "timeline":
                                $iconType = "fas fa-clock fa";
                                $route = route('transactionSummary.timeline', ['transactionID'=> $alertItem['transactionID']]);
                                break;
                            default:
                                $iconType = "far fa-question-circle";
                                break;
                        }
                        if (date_diff($dateAlert, $dateNow)->format('%R%a') == 0
                            or date_diff($dateAlert, $dateTomorrow)->format('%R%a') == 0)
                        {
                            $box = "box_red";
                        }

                        if(!is_null($alertItem['dateRead'])) $box .= ' read-notification';
                        ?>
                        <div class="{{$box}} box" data-id="{{$alertItem['alertID']}}">
                            <a class="overlayLink" href="{{$route}}"></a>
                            <div class="type {{$alertItem['type']}}">
                                <i class="{{$iconType}}"></i>
                            </div>
                            <div class="address">
                                <p>{{$alertItem["address"]}}</p>
                            </div>
                            <div class="description">
                                <p>{{$alertItem["description"]}}</p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var windowHeight = $(window).height();
        var docHeight = $(document).height();
        let alertIDs = JSON.parse('{!! $jsonAlertIDs !!}');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click','.alerts-wrapper .alerts .item',function(){
           showAlerts();
        });

        function showAlerts()
        {
            showNotifications(windowHeight);
        }

        $(document).on('click','#mark_all_as_read_alerts', function(){
            $.ajax({
                url: '{{route('markAlertsAsRead')}}',
                method: 'POST',
                data: {
                   alerts: alertIDs,
                },
                success: function(r){
                   if(r.status === 'success')
                   {
                       Swal2({
                           toast: true,
                           position: 'top-end',
                           timer: 1500,
                           showConfirmButton: false,
                           text: 'Notifications marked as read!',
                           type: 'success',
                       }).then(()=>{
                           location.reload();
                       });
                   }
                },
                error: function(){

                },
            });
        });

        $(document).on('click', '#clear_all_alerts', function(){
            $.ajax({
                url: '{{route('clearAllAlerts')}}',
                method: 'POST',
                data: {
                    alerts: alertIDs,
                },
                success: function(r){
                    if(r.status === 'success')
                    {
                        Swal2({
                            toast: true,
                            position: 'top-end',
                            timer: 1500,
                            showConfirmButton: false,
                            text: 'Notifications Cleared!',
                            type: 'success',
                        }).then(()=>{
                            location.reload();
                        });
                    }
                },
                error: function(){

                },
            });
        });
    });
</script>
