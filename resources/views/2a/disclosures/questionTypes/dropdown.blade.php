<div class="" style="margin-top: 20px;">    
    <h3> {{ $question }} </h3>    
    <a href="#"><p style="color: #21d7d1;">What does this mean?</p></a>
    <div class="col-md-4 answer-section" style="margin-top: 50px;">
      <select name="{{ $fieldName }}[]" class="form-control">
        @foreach ($answers as $a)
                <option type="radio" name="" value="{{ $a['value'] }}" >{{ @$a['display']}}</option>
        @endforeach
      </select>
    </div>
</div>