<div class="" style="margin-top: 20px;">    
    <h3> {{ $question }} </h3>    
    <a href="#"><p style="color: #21d7d1;">What does this mean?</p></a>
    <div class="answer-section col-md-2" style="margin-top: 50px;">
        @foreach ($answers as $a)
                <input type="date" class="form-control" name="{{ $fieldName }}[]" value="{{ $a['value'] }}" >
        @endforeach
    </div>
</div>