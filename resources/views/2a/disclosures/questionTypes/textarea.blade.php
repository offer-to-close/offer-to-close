<div class="" style="margin-top: 20px;">    
    <h3> {{ $question }} </h3>    
    <a href="#"><p style="color: #21d7d1;">What does this mean?</p></a>
    <div class="answer-section" style="margin-top: 50px;">
        @foreach ($answers as $a)
                <textarea type="radio" name="{{ $fieldName }}[]" value="" placeholder="{{ $a['display'] }}">{{ $a['value'] }}</textarea>
        @endforeach
    </div>
</div>