@php
    $date_created = ($invoice['DateCreated']==null) ? now() : $invoice['DateCreated'];
    $date_created = date('Y-m-d', strtotime($date_created));

    $DateDue = ($invoice['DateDue']==null) ? $invoice['DateInvoiced'] : $invoice['DateDue'];
    $DateDue=date('Y-m-d', strtotime($DateDue));

    //dd( $invoice,$invoiceItems);
    //dd( route('PrintInvoice',['invoiceID'=>$invoice["ID"]]) );
@endphp
@extends('2a.layouts.master')

@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
        .bal-due-gt{
            background-color: #EAEAEA;
            overflow: hidden;
            height: 30px;
            line-height: 31px;
        }
        textarea{
            resize: none !important;
            position: static !static;
        }
    </style>
@endsection

@section('content')

    <div class="col-xs-12 col-sm-10 col-md-10 col-ls-10 container" style="left:0">
        <div class="container">
            <div class="add-new-transaction-title col-xs-10">
                <div class="mc-heading">
                    <h2 style="text-transform: uppercase;">Invoice {{$invoice['ID'] ?? null}}</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="">
                <div class="col-xs-12">
                    <h4>Bill To</h4><br>
                </div>
                <div class="">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <textarea class="col-xs-12 col-sm-9 BillTo" rows="1" cols="50" placeholder="Who is this invoice for? (required)">{{ $invoice['BillTo'] }}</textarea>
                        <textarea class="col-xs-12 col-sm-9 BillToAddress" style="margin-top: 15px;" rows="3" cols="50" placeholder="Address(required)">{{ $invoice['BillToAddress'] }}</textarea>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6" style="text-align:right">
                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Date</label>
                            <div class="col-sm-6">
                                <input type="date" class="form-control DateInvoiced" value="{{$date_created}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Payment Terms</label>
                            <div class="col-sm-6">
                                <input type="" class="form-control PaymentTerms" value="{{$invoice['PaymentTerms']}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Due Date</label>
                            <div class="col-sm-6">
                                <input type="date" class="form-control DateDue" value="{{$DateDue}}">
                            </div>
                        </div>
                        <div class="form-group bal-due-gt">
                            <label class="col-sm-6 col-xs-7 col-form-label">Balance Due</label>
                            <div class="col-sm-6 col-xs-5" style="text-align:left">
                                $<label class="Balance-due">{{($invoice['Total']==null)?'0.00':$invoice['Total']}}</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <input type="hidden" id="h_line_count">
            <div class="">
                <div id="a_inserted" style="overflow-y: hidden;  white-space: nowrap;">
                    <div class="col-sm-12 col-xs-12">
                        <div class="col-sm-6 col-xs-4" style="background-color: #000; color:#fff; padding:5px 15px 5px 15px">Item</div>
                        <div class="col-sm-2 col-xs-2" style="background-color: #000; color:#fff; padding:5px 15px 5px 15px">Quantity</div>
                        <div class="col-sm-2 col-xs-3" style="background-color: #000; color:#fff; padding:5px 15px 5px 15px">Rate</div>
                        <div class="col-sm-2 col-xs-3" style="background-color: #000; color:#fff; padding:5px 15px 5px 15px">Amount</div>
                    </div>
                    <div class="line_i col-sm-12 col-xs-12 c_line_" data-id="null">
                        <div class="col-sm-6 col-xs-4" style="padding:5px 0px"><input type="text" class="form-control item"  placeholder=""></div>
                        <div class="col-sm-2 col-xs-2" style="padding:5px"><input type="number"  class="form-control Quantity" min="1" step="1"  value="1" placeholder=""></div>
                        <div class="col-sm-2 col-xs-3" style="padding:5px 0px"><input type="text" class="form-control Rate" value="1" id="Rate" placeholder=""></div>
                        <div class="col-sm-2 col-xs-3" style="padding:10px"><label>$</label><label class="sum" >1.00</label></div>
                    </div>
                </div>
                <div class="error-meassage col-sm-12 text-danger" style="display: none"></div>

                <div class="col-xs-12">
                    <button class="col-xs-12 col-sm-2 btn light-green" id="btn_add_line">+ Line Item</button>
                </div>
                <div class="col-xs-12" style="padding-top:60px">
                    <hr>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <div class="col-sm-10 col-xs-8" style="padding:10px; text-align:right"><label>Total</label></div>
                    <div class="col-sm-2 col-xs-4" style="padding:10px; text-align:left ">$<label id="sh_total" class="Total">{{($invoice['Total']==null)?'0.00':$invoice['Total']}}</label></div>
                </div>
                <div class="col-xs-12" style="padding-top:30px">

                    @php $status = $invoice['Status']; @endphp
                    @if( !empty($status) && DateTime::createFromFormat('Y-m-d', $status) !== TRUE )
                             <h2 class="otc-red text-center">Paid on {{ date('n/j/Y', strtotime($status)) }} </h2>
                    @endif
                    <hr>
                    <label>Notes</label>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <textarea class="col-xs-12 col-sm-12 Notes" rows="4" cols="50" placeholder="Any relevant information not already covered.">{{$invoice['Notes']}}</textarea>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <div style=" padding-top:30px">
                        <a class="btn mid-red col-xs-12 col-sm-3 save-print-button"  style="float:right;"><i class="fas fa-cloud-download-alt"></i> Save and Print</a>
                        <button class="btn light-green send-invoice-btn col-xs-12 col-sm-3" style="float:right;"><i class="fas fa-envelope"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    $('#h_line_count').val(1);
// ######################################   CREATE THE ITEMS
    var invoiceItems= "{{ json_encode($invoiceItems->toArray())}}";
    var invoiceItems=JSON.parse(invoiceItems.replace(/&quot;/g,'"'));
    var Total_items_price=parseInt("{{$invoice['Total']}}");
    console.log(invoiceItems.length);
    if(invoiceItems.length >0 && invoiceItems != null){
            $('#a_inserted').children('.c_line_').remove();
            var Total_Amount=0;
            for (invitem in invoiceItems) {
                //console.log(invoiceItems[invitem]);
                var tempd = parseInt($('#h_line_count').val())+1;
                $('#h_line_count').val(tempd);
                var line='<div class="line_i col-sm-12 col-xs-12 c_line_' + tempd + '" data-id="'+invoiceItems[invitem]["ID"]+'"> \
                                            <div class="col-sm-6 col-xs-4" style="padding:5px 0px"><input type="text" class="item form-control" value="'+invoiceItems[invitem]["Description"]+'"></div>\
                                            <div class="col-sm-2 col-xs-2" style="padding:5px"><input type="number"  class="form-control Quantity" step="100" min="0"  value="'+invoiceItems[invitem]['Quantity']+'"></div>\
                                            <div class="col-sm-2 col-xs-3" style="padding:5px 0px"><input max="5" min="0" type="number"  class="form-control Rate" value="'+invoiceItems[invitem]['Rate']+'" ></div>\
                                            <div class="col-sm-2 col-xs-3" style="padding:10px"><label>$</label><label class="sum" >'+invoiceItems[invitem]['Rate']*invoiceItems[invitem]['Quantity']+'</label></div>\
                                        </div>';
                line=$(line).clone(true,true);
                $('#a_inserted').append(line);
                Total_Amount+=(invoiceItems[invitem]['Rate']*invoiceItems[invitem]['Quantity']);
            }
            $('.Total').text(Total_Amount);

        }

    // ########################################
    function input_change_function(){
        $('#a_inserted').children('.line_i').children('div').children('input.Quantity , input.Rate').on('input',function(){
            var el1=$(this).val();
            var el2=($(this).hasClass('Quantity'))?
                    $(this).parent('div').siblings('div').children('.Rate').val():
                    $(this).parent('div').siblings('div').children('.Quantity').val();
            $(this).parent('div').siblings('div').children('.sum').text(el1*el2);

            var all_lines=$('#a_inserted').children('.line_i');
            var Total_Amount = 0;
            all_lines.each(function(){
                el=$(this);
                var single_line={};
                var qua=el.children('div').children('input.Quantity').val();
                var rate=el.children('div').children('input.Rate').val();
                Total_Amount+=(qua*rate);
            });
            $('.Total').text(Total_Amount);
            $('.Balance-due').text(Total_Amount);
            Total_items_price=Total_Amount;
        });
    }
    input_change_function();
    $('.Amount-paid').on('input',function(){
        $('.Balance-due').text(Total_items_price);
    });
    $('#btn_add_line').on('click', function(){
        $('.error-meassage').hide();
        $('*').removeClass('border-red');
        var there_is_an_error=false;
        var last_line_childrens=$('#a_inserted').children('.line_i').last().children('div').children('input');
        last_line_childrens.each(function(element){
            var el=$(this);
            if(el.val().length==0){
                el.addClass('border-red');
                there_is_an_error=true;
                return;
            }
        });
        if(there_is_an_error){
            $('.error-meassage').text('the last field is empty').show();
            return;
        }
        var tempd = parseInt($('#h_line_count').val())+1;
        $('#h_line_count').val(tempd);
        var line='<div class="line_i col-sm-12 col-xs-12 c_line_' + tempd + '" data-id="null"> \
                                        <div class="col-sm-6 col-xs-4" style="padding:5px 0px"><input type="text" class="item form-control" ></div>\
                                        <div class="col-sm-2 col-xs-2" style="padding:5px"><input type="number"  class="form-control Quantity" min="1" step="1" value="1"></div>\
                                        <div class="col-sm-2 col-xs-3" style="padding:5px 0px"><input type="text"  class="form-control Rate" id="Rate" value="1"></div>\
                                        <div class="col-sm-2 col-xs-3" style="padding:10px"><label>$</label><label class="sum" >1.00</label></div>\
                                    </div>';
        line=$(line).clone(true,true);
        $('#a_inserted').append(line);
        input_change_function();
    });
    // ################################################# send

    $('.send-invoice-btn').click(function(){
        save_invoice_to_DB(false);
    });
    $('.save-print-button').click(function(){
        save_invoice_to_DB(true);
    });
    function save_invoice_to_DB(redirect)
    {
        // var this_button=$(this);
        var token="{{Session::token()}}";
        $('.error-meassage').hide();
        $('*').removeClass('border-red');
        var there_is_an_error=false;
        var all_lines_input=$('#a_inserted').children('.line_i').children('div').children('input');
        all_lines_input.each(function(element){
            var el=$(this);
            if(el.val().length==0){
                el.addClass('border-red');
                there_is_an_error=true;
                // console.log(el);
            }
        });
        if(there_is_an_error){
            $('.error-meassage').text('all fields are required').show();
            swal('Error',"All Item Field Are Required",'error');
            return;
        }

        var opj={
            "ID":null,
            "_token":token,
            "BillTo":$('.BillTo').val(),
            "BillToAddress":$('.BillToAddress').val(),
            "DateInvoiced":$('.DateInvoiced').val(),
            "DateDue":$('.DateDue').val(),
            "Total":0,
            "Notes":$('.Notes').val(),
            "PaymentTerms":$('.PaymentTerms').val(),
            "items":{},
        };
        var all_lines=$('#a_inserted').children('.line_i');
        var i=0;
        var Total_Amount = 0;
        all_lines.each(function(){
            el=$(this);
            var single_line={};
            single_line['Description']=el.children('div').children('input.item').val();
            single_line['Quantity']=el.children('div').children('input.Quantity').val();
            single_line['Rate']=el.children('div').children('input.Rate').val();
            single_line['ID']=(el.attr('data-id')=='null')?null:  el.attr('data-id');
            single_line['Invoices_ID']="{{$invoice['ID']}}";
            Total_Amount+=(single_line['Rate']*single_line['Quantity']);
            opj['items'][i]={};
            opj['items'][i]=single_line;
            i++;
        });
        opj['Total']=Total_Amount;
        opj['SubTotal']=Total_Amount+parseInt($('.Amount-paid').val());
        $('.Total').text(Total_Amount);
        var ID="{{$invoice['ID']}}";
        if(ID != null && ID != '') opj['ID']=ID;
        var the_url="{{route('saveInvoice')}}";
        $.ajax({
                url : the_url,
                data:opj,
                method: "POST",
                error:function(response){
                    console.log(response['responseJSON']['errors']);
                    for(x in response['responseJSON']['errors']){
                        swal("Error", response['responseJSON']['errors'][x][0] , "error");
                        return;
                    }
                },
                success:function(str){
                         //console.log(str); return;
                    swal({
                        title: "Success",
                        text: "Invoice Saved Successfully",
                        icon: "success",
                        })
                        .then((willDelete) => {
                        if (willDelete) {
                            if(redirect){
                                window.location.href=str[0];
                                //window.location.reload(false);
                            }
                            else window.location.href=str[1];
                            // window.location.reload(false);
                        } else {
                            if(redirect){
                                window.location.href=str[0];
                            }
                            else window.location.href=str[2];

                        }
                    });
                    console.log(str);
                },
                async:false,
            });
    }

    $(window).on('pushstate', function(event) {
        alert("push");
        });


    </script>

@endsection
