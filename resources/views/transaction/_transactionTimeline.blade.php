
    <div class="kp-content-area">
           <table class="table">
                <thead>
                <tr>
                    <th class="table-head">Date</th>
                    <th class="table-head">&nbsp;</th>
                    <th class="table-head text-left">Milestone</th>
                    <th class="table-head">&nbsp;</th>
                </tr>
                </thead>

                <?php
                $milestones = \App\Library\Utilities\_Convert::toArray($milestones);

                foreach($milestones as $ms){ ?>
                <tr>
                    <td class="text-right">{!!  date('l, M j, Y', strtotime($ms['MilestoneDate']))  !!}</td>
                    <td class="text-center"> {!!  $ms['diff'] < 1 ? '<i class="fas fa-clock"></i>' : '<i class="far fa-clock"></i>'  !!} </td>
                    <td class="text-left">{!!  $ms['MilestoneName']  !!}</td>
                    <td> </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
