@section('custom_css')
    <style>
        .tooltiptext {
            visibility: hidden;
            margin: 10px;
            padding-left: 10px;
            padding-right: 10px;
            min-width: 180px;
            background-color: white;
            color: black;
            text-align: center;
            padding: 5px 0;
            border-radius: 6px;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        
            /* Position the tooltip text - see examples below! */
            position: absolute;
            z-index: 1;
        }

        .tip_show{
            visibility: visible;
        }

    </style>
@endsection

<div class="kp-content-area">
    <div class="kp-ctrls">
        <!-- <a href="" class="btn red">Buyer Docs Only</a><a href="" class="btn red-transparent">Seller Docs Only</a> --> </div>

        
    <!-- Show errors if they exist -->
    @if ($propertyError)
        <div class="alert alert-danger alert-dismissable text-center">
            <a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
            Property information must be complete to generate document list
            ({{ $propertyError ? 'TRUE' : 'FALSE' }})
        </div>
    @endif
    <div class="row success_msg"></div>

    @if (!empty($detailErrors))
        <div class="alert alert-danger alert-dismissable text-center">
            <a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
            Detail information must be complete to generate document list
            ({{ implode(', ', $detailErrors) }})

        </div>
    @endif
    <div class="row success_msg"></div>


    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <?php foreach($docsHeaders as $hdr){ ?>
                    <th class="table-head">{{ $hdr }}</th>
                <?php } ?>
                <th class="table-head">&nbsp;</th>
            </tr>
            </thead>

            <?php
            $documents = \App\Library\Utilities\_Convert::toArray($documents); 

            foreach($documents as $doc){ ?>
            <tr>
                <?php
                foreach($docsFields as $fld){
                    $val = $doc[$fld];
                    if ($fld == 'isIncluded')
                    {
                        $val = ($doc[$fld]) ? '<i class="fas fa-check-circle"></i>' : '&nbsp;';
                    }

                    if ($fld == 'UploadedDocuments' && is_array($val))
                    {
                        $new = [];
                        foreach ($val as $file)
                        {
                            $new[] = '<a href="' . $file["url"] . '" target="_blank"  aria-label="Signers go here">' . '<i style="color:#CB4544;" aria-hidden class="fas fa-file tooltip_toggle"></i>' .'<span class="tooltiptext"> <b>Signed By : </b><hr style="margin-top:5px; margin-bottom: 5px"/>'.$file["signedBy"].'</span></a>';
                        }
                        $val = implode('<br/>', $new);
                    }
                ?>
                <td class="text-center">{!!  $val  !!}</td>
                <?php } ?>
                <td><i class="fas fa-pen-square"></i></td>
            </tr>
            <?php } ?>
        </table>
    </div>
</div>


@section('scripts')
    
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip(); 

            $(document).on('mouseover mouseout', '.tooltip_toggle', function(){
                var el = $(this);
                el.siblings('.tooltiptext').toggleClass("tip_show");
            });
        });
    </script>

@endsection