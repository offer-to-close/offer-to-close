@extends('layouts.app')


@section('content')
<div id="keypeople">
    <div class="kp-heading">


        <h1 class="kp-head-address">{{ $data['address'] }}</h1>
        <div class="kp-head-content">
            <div class="left">
               <div class="kp-head-name">Jo Client</div>
               <div class="kp-head-escrow">Close of Escrow: {{ $data['dateClose'] }}</div>
            </div>
            <div class="right">
                <div class="ctrls"><a href="" class="btn white">EDIT</a></div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
    <div class="kp-tabs">
        <div class="nav-select-wrapper">
            <select class="nav-select">
                <option value="#kp-dashboard">Key People</option>
                <option value="{{ route('transaction.toDo') }}">To Do</option>
                <option value="#kp-files">Files</option>
                <option value="#kp-activity">Activity</option> 
            </select>
        </div>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#kp-dashboard">Key People</a></li>
          <li><a data-toggle="tab" href="{{ route('transaction.toDo', ['taID' => $transactionID]) }}">To Do</a></li>
          <li><a data-toggle="tab" href="#kp-files">Files</a></li>
          <li><a data-toggle="tab" href="#kp-activity">Activity</a></li> 
        </ul>
        <div class="tab-content">
            <div id="kp-dashboard" class="tab-pane fade in active">
               <div class="kp-content-area">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="table-head">&nbsp;</th>
                                    <?php foreach($dataIndices as $di){ ?>
                                       <th class="table-head">{{ $di }}</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>

                            <?php foreach($roles as $role){ ?>
                                <tr>
                                    <td class="text-right">{{ $role }}</td>
                            <?php foreach($dataIndices as $di){ ?>
                                    <td class="text-center">{{ $data[$role][$di] }}</td>
                                <?php } ?>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kp-todo" class="tab-pane fade">
                <div class="kp-content-area">
                    <div class="kp-ctrls"><a href="" class="btn red">Buyer Tasks Only</a><a href="" class="btn red-transparent">Buyer Tasks Only</a></div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="table-head">Description</th>
                                    <th class="table-head">Deadline</th>
                                    <th class="table-head">Who is responsible for it</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php for($ctr=1; $ctr<=15; $ctr++){ ?>
                                <tr>
                                    <td class="text-left">Apply for financing</td>
                                    <td class="text-center">(November 6, 2017</td>
                                    <td class="text-center">BUYER</td>
                                </tr>
                                <tr>
                                    <td class="text-center">Appraisal must be done or you remove the contingency</td>
                                    <td class="text-center">(November 20, 2017</td>
                                    <td class="text-center">SELLER</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kp-files" class="tab-pane fade">
                <div class="kp-content-area">

                    <div class="table-responsive">
                        <table class="table bordered">
                            <thead>
                                <tr>
                                    <th >&nbsp;</th>
                                    <th class="table-head" colspan="4">Completed by</th>
                                    <th >&nbsp;</th>
                                    <th >&nbsp;</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th class="table-head">1. Agency disclosures</th>
                                    <th class="table-head">la</th>
                                    <th class="table-head">s</th>
                                    <th class="table-head">b</th>
                                    <th class="table-head">ba</th>
                                    <th class="table-head">due date</th>
                                    <th class="table-head">notes</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">AD1 - Disclosures Regarding Agency Relationship (Listing to Seller)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">AD2 - Disclosures Regarding Agency Relationship (Selling to Buyer)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">AD3 - Disclosures Regarding Agency Relationship (Seliing to Seller)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">BNA - Buyer Non-Agency</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">BRA - Buyer Broker Agreement</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">Other</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">PRBS - Disclosure and Consent for Representaion of more than one Buyer or Seller</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">SNA - Seller Non-Agency</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th class="table-head">2. Contracts & deposit check / trust log</th>
                                    <th class="table-head">la</th>
                                    <th class="table-head">s</th>
                                    <th class="table-head">b</th>
                                    <th class="table-head">ba</th>
                                    <th class="table-head">due date</th>
                                    <th class="table-head">notes</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">AD1 - Disclosures Regarding Agency Relationship (Listing to Seller)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">AD2 - Disclosures Regarding Agency Relationship (Selling to Buyer)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">AD3 - Disclosures Regarding Agency Relationship (Seliing to Seller)</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">BNA - Buyer Non-Agency</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">BRA - Buyer Broker Agreement</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">Other</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">PRBS - Disclosure and Consent for Representaion of more than one Buyer or Seller</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-left">SNA - Seller Non-Agency</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div id="kp-activity" class="tab-pane fade">
                <div class="kp-content-area">
                    <div class="kp-list-wrapper">
                        <h2 class="kp-list-heading">List of Activities</h2>
                        <ul class="kp-list">
                            <li><strong>David Rodriguez</strong> uploaded the Residential Purchase Agreement on<strong> Friday, March 23,2018 at 11:42 AM PST</strong> from <strong>183.23.234.34</strong></li>
                            <li><strong>Karl Ryser</strong> downloaded Transfer Disclosure Statement on <strong>Saturday, March 24, 2018 at 1:23 PM PST</strong> from <strong>198.23.43.45</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
      //  $('.kp-tabs .nav-select').prop('selectedIndex');

        // $('.nav-select-wrapper').on('click','span',function(){
        //    //lert('test');
        //    $(this).closest('.nav-select-wrapper').find('select').click();
        //     return false;
        // });

        $('.kp-tabs').on('change','.nav-select',function(){
            var selTab = $(this).val();

            $("a[href$='"+selTab+"']").click();

            return false;
        });
        $('.kp-tabs .nav').on('click','a',function(){
           var thisValue = $(this).attr('href');

           $('.kp-tabs .nav-select').val(thisValue);
        })

    });
</script>
@endsection