@component('mail::message')
    # OTC A-001-TC
    from {{ $sender }}

    Welcome {{ $ }}
    @component('mail::panel')
        {{ $property_address }}
    @endcomponent


    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
