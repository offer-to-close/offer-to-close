@extends('2a.layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h1>Hack Database Schema to send to Documentor</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div>
                {!! $outputSchema !!}
            </div>
            <div class="pull-right">
 <!--               <a class="btn btn-primary" href="{{ route('transaction.index') }}"> Back</a>  -->
            </div>
        </div>
    </div>


@endsection
