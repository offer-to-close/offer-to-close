<?php
/*
 * Usage:
 *      Config::get('constants.CONSTANT_NAME')
 *      To share pieces of data across your views:
 *
 *      View::share('my_constant', Config::get('constants.CONSTANT_NAME'));
 *      Put that at the top of your routes.php and the constant will then be accessible
 *      in all your blade views as:
 *        {{ $my_constant }}
 */

return [

    'LOG_ON' => false,
    'LOG_FILE' => './otc_debug.log',
    'DEBUG_ON' => false,

    'StandardColumns' => ['isTest', 'DateCreated', 'DateUpdated', 'deleted_at'],

    'DefaultRoute' => ['dashboard'=>'dashboard',],


    /*
    |--------------------------------------------------------------------------
    | HTML to PDF Conversion
    |--------------------------------------------------------------------------
    |
    | These values are for the various methods of converting HTML to PDF
    |
    */

    'Restpack_Token'       => env('Restpack_Token', 'FoKeAJEsphujwNW61OX7Ko3N9gp9Qtcb0tqaZBEHfcsVQAjE'), // PDF API
    'Restpack_URL'         => 'https://restpack.io/api/html2pdf/v5/convert', // API Endpoint
    'Html2PdfRocket_Token' => env('Html2PdfRocket_Token', '62a4a162-5d92-42f3-a261-e00eeb0aef01'), // PDF API
    'Html2PdfRocket_URL'   => 'http://api.html2pdfrocket.com/pdf', // API Endpoint

    /*
    |--------------------------------------------------------------------------
    | PDF Conversion
    |--------------------------------------------------------------------------
    |
    | These values are for the various methods of converting HTML to PDF
    |
    */

    'ConvertApi_Secret'    => env('ConvertApi_Secret', 'cif32rKbQoBn1rhD'), // Code
    'ConvertApi_ApiKey'    => env('ConvertApi_ApiKey', '946954414'), // PDF API
    'ConvertApi_URL'         => '', // API Endpoint

    /*
    |--------------------------------------------------------------------------
    | List of United States
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to determine on which field to sort
    | the return values
    |
    */

    'states' => [
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    ],

    'color' => [    'red' => '#ca4544',
                    'teal' => '#21d7d1',
                    'orange' => '#FF8C00',
                    'disabled' => '#dddddd',
    ],

    'red' => '#ca4544',
    'teal' => '#21d7d1',
    'orange' => '#FF8C00',
    'modalClose' => '<span  class="text-danger" style="font-size: 50px">&times;</span>',


    'UI' => [
        'version'  => env('UI_VERSION', '2a'),
        'fallback' => env('UI_VERSION_FALLBACK', '2a'),
    ],


    'Prelogin' => [
        'version'  => env('PRELOGIN_VERSION', '1b'),
        'fallback' => env('PRELOGIN_VERSION_FALLBACK', '1a'),
    ],


    'API' => [
        'version'  => env('API_VERSION', '1a'),
    ],

    'format' => [
      'date' => 'Y-m-d',
      'datetime' => 'Y-m-d H:i:s',
    ],

    'testing' =>[
        'addressVerification' => [
            'SmartyStreets' => env('Test_SmartyStreets', false),
            'USPS' => env('Test_USPS', false),
            'Google' => env('Test_Google', false),
        ],
        'email' => [
            'SparkPost' => env('Test_SparkPost', false),
        ],
    ],

    /*
 |--------------------------------------------------------------------------
 | Data Model
 |--------------------------------------------------------------------------
 |
 */
    'ColumnNameToTableMap' => [
        'OverrideUsers'=>'users',
        'SellersTransactions'=>'Transactions',
        'BuyersTransactions'=>'Transactions',
        'SellersTransactionCoordinators'=>'TransactionCoordinators',
        'BuyersTransactionCoordinators'=>'TransactionCoordinators',
        'OwnedByUsers'=>'users',
        'BuyersOwner'=>'users',
        'SellersOwner'=>'users',
        'ApprovedByTC'=>'TransactionCoordinators',
        'UploadedBy'=>'users',
        'SourceDocument_bag'=>'bag_TransactionDocuments',
        'ReportCategories'=>'lu_ReportCategories',
        'Users'=>'users',
        'TaskFilters'=>'lu_TaskSets',
        'InvitedBy'=>null,
        'InviteeRole'=>null,
        'Envelopes'=>null,
        'TaskSets'=>'lu_TaskSets',
        'DeciderUsers'=>'users',
        'Decision_LogMail'=>'log_Mail',
        'DocumentSets'=>'lu_DocumentSets',
        'ResponderUsers'=>'users',

    ],
];
