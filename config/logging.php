<?php

return [

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['syslog', 'slack', 'mail'],
        ],

        'syslog' => [
            'path' => storage_path('logs/sys.log'),
            'driver' => 'syslog',
            'level' => 'debug',
        ],


        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'mail' => [
            'driver' => 'daily',
//            'tap' => [App\Logging\CustomizeFormatter::class],
            'path' => storage_path('logs/mail.log'),
            'level' => 'critical',
        ],
    ],
];