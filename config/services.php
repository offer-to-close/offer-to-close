<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    // - OTC

    'snap' => [
        'apiKey' => env('SNAP_NHD_API_KEY', 'staging_eKaDqPzpTaHn8tUmtsqrZYQoeCCkaWTe'),
        'url'    => env('SNAP_NHD_URL', 'https://staging.snapnhd.com/api/v1/orders'),
    ],

    'smartyStreets' => [
        'authID'     => env('SMARTYSTREETS_AUTH_ID', '0d289546-40f6-4adf-bdfd-ae9b291a85c4'),
        'authToken'  => env('SMARTYSTREETS_AUTH_TOKEN', 'L7Ram0JWtXhI4u4wDmyt'),
        'websiteKey' => env('SMARTYSTREETS_WEBSITE_KEY', '19443821959804804'),
        'url'        => env('SMARTYSTREETS_URL', 'https://us-street.api.smartystreets.com/street-address'),
    ],

    'mailTrap' => [
        'username' => env('MAILTRAP_USERNAME', 'e8ce884be2e12d'),
        'password' => env('MAILTRAP_PASSWORD', '347c8e47e0e15f'),
        'apiToken' => env('MAILTRAP_API_TOKEN', '30622a644aee4537e2a5e51482303330'),
        'jwtToken' => env('MAILTRAP_JWT_TOKEN', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ0b2tlbiI6IjMwNjIyYTY0NGFlZTQ1MzdlMmE1ZTUxNDgyMzAzMzMwIn0.eBCbPaKesdIM-JeW3umX7iTxHLMx9W2s1rrcaKVNWkxwo42jKS1RNBz_iI7KGXYwDWGpwsIMNxGqlFM3aNjR_g'),
    ],

];
